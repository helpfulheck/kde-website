---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (nenhum registro de alterações indicado)

### KConfig

- Geração das classes de prova QML usando o <i>kconfigcompiler</i>

### KCoreAddons

- Nova macro do CMake kcoreaddons_add_plugin para facilitar a criação de plugins baseados no KPluginLoader.

### KDeclarative

- Correção da falha no <i>cache</i> de texturas.
- e outras correções

### KGlobalAccel

- Adição do novo método <i>globalShortcut</i>, que devolve o atalho como estiver definido na configuração global.

### KIdleTime

- Evitar que o <i>kidletime</i> falhe na plataforma Wayland

### KIO

- Adição dos métodos KPropertiesDialog::KPropertiesDialog(urls) e KPropertiesDialog::showDialog(urls).
- Obtenção assíncrona de dados, baseada no QIODevice, para o KIO::storedPut e o KIO::AccessManager::put.
- Correção de condições com o valor devolvido pelo QFile::rename (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343329'>343329</a>)
- Correção do KIO::suggestName para sugerir nomes melhores (erro <a href='https://bugs.kde.org/show_bug.cgi?id=341773'>341773</a>)
- kioexec: Correção do caminho de gravação do kurl (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343329'>343329</a>)
- Armazenamento dos favoritos apenas no user-places.xbel (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345174'>345174</a>)
- Item RecentDocuments duplicado, caso dois arquivos diferentes tenham o mesmo nome
- Mensagem de erro mais esclarecedora, caso um único arquivo seja muito grande para a Lixeira (erro <a href='https://bugs.kde.org/show_bug.cgi?id=332692'>332692</a>)
- Correção da falha do KDirLister durante o redirecionamento, quando o <i>slot</i> chama o <i>openURL</i>

### KNewStuff

- Novo conjunto de classes, chamadas de KMoreTools ou outros nomes relacionados. As KMoreTools ajudam a adicionar dicas sobre as ferramentas externas que potencialmente não estejam instaladas. Além disso, torna os menus longos mais curtos, oferecendo uma seção principal e outra adicional, que também possa ser configurada pelo usuário.

### KNotifications

- Correção do KNotifications quando é usado com o NotifyOSD do Ubuntu (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345973'>345973</a>)
- Não acionar as atualizações das notificações quando definir as mesmas propriedades (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345973'>345973</a>)
- Introdução da opção <i>LoopSound</i>, que permite que as notificações reproduzam um som repetido, caso precisem disso (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346148'>346148</a>)
- Não falha se uma notificação não tiver um widget

### KPackage

- Adição de uma função KPackage::findPackages, semelhante ao KPluginLoader::findPlugins

### KPeople

- Uso do KPluginFactory para instanciar os plugins, em vez do KService (mantido por questões de compatibilidade).

### KService

- Correção da divisão errada do caminho do item (erro <a href='https://bugs.kde.org/show_bug.cgi?id=344614'>344614</a>)

### KWallet

- O agente de migração agora também verifica, antes de começar, se a carteira antiga está vazia (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346498'>346498</a>)

### KWidgetsAddons

- KDateTimeEdit: Correção para que os dados introduzidos pelo usuário sejam efetivamente registrados. Correção das margens duplas.
- KFontRequester: Correção da seleção apenas de fontes monoespaçadas

### KWindowSystem

- Não depender do QX11Info no KXUtils::createPixmapFromHandle (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346496'>346496</a>)
- Novo método NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Correção dos atalhos quando definir um atalho secundário (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345411'>345411</a>)
- Atualização da lista de componentes/produtos no Bugzilla para os relatórios de erros (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346559'>346559</a>)
- Atalhos globais: Permitir a configuração do atalho alternativo

### NetworkManagerQt

- Os cabeçalhos instalados são agora organizados como em todos os outros <i>frameworks</i>.

### Framework do Plasma

- PlasmaComponents.Menu agora tem suporte para seções
- Uso do KPluginLoader em vez do ksycoca para carregar os mecanismos de dados em C++
- Considerar a rotação do <i>visualParent</i> no <i>popupPosition</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345787'>345787</a>)

### Sonnet

- Não tentar realçar, caso não seja encontrado um verificador ortográfico. Isto iria provocar um <i>loop</i> infinito com o temporizador do <i>rehighlighRequest</i> a disparar de forma constante.

### Frameworkintegration

- Correcção das janelas de ficheiros nativas no QFileDialog: ** As janelas de ficheiros abertas com o exec() e sem um item-pai eram abertas, mas qualquer interacção com o utilizador era bloqueada de tal forma que não era possível seleccionar nenhum ficheiro nem fechar a janela. ** As janelas de ficheiros abertas com o open() ou o show() com um item-pai não eram abertas de todo.

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
