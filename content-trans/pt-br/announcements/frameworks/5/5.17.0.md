---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correção do filtro de dados usado pelo timeline://
- Balooctl: Voltar após os comandos
- Limpeza e proteção do Baloo::Database::open(), para lidar com mais condições de falhas
- Adição de verificação no Database::open(OpenDatabase) para falhar se o BD não existir

### Ícones Breeze

- Foram adicionados e melhorados diversos ícones
- Uso de folhas de estilo nos ícones do tema Breeze (erro <a href='https://bugs.kde.org/show_bug.cgi?id=126166'>126166</a>)
- Correção e modificação da tela de bloqueio do sistema (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355902'>355902</a>)
- Adição da informação de caixas de diálogo de 24px para aplicativos em GTK (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355204'>355204</a>)

### Módulos extra do CMake

- Não avisar quando os ícones SVG(Z) forem fornecidos em vários tamanhos/níveis de detalhe
- Confirmação de que as traduções são carregadas na tarefa principal (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346188'>346188</a>)
- Revisão geral do sistema de compilação ECM.
- Possibilidade de ativar o Clazy em qualquer projeto do KDE
- Não localizar a biblioteca XINPUT do XCB por padrão
- Limpeza da pasta de exportação antes de gerar novamente um APK
- Usar o <i>quickgit</i> para a URL do repositório Git

### Integração do Framework

- Adição de falha na instalação do plasmoide no plasma_workspace.notifyrc

### KActivities

- Correção de um bloqueio ao iniciar o serviço pela primeira vez
- Transferência da criação do QAction para a tarefa principal (erro <a href='https://bugs.kde.org/show_bug.cgi?id=351485'>351485</a>)
- Em alguns casos, o <i>clang-format</i> toma uma decisão errada (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355495'>355495</a>)
- Correção de problemas potenciais de sincronização
- Uso do org.qtproject em vez do com.trolltech
- Remoção do uso da <i>libkactivities</i> a partir dos plugins
- A configuração do KAStats foi removida da API
- Adição da associação/dissociação do ResultModel

### Ferramentas Doxygen do KDE

- Tornar o <i>kgenframeworksapidox</i> mais robusto

### KArchive

- Correção do KCompressionDevice::seek(), chamado ao criar um KTar no topo de um KCompressionDevice.

### KCoreAddons

- KAboutData: Permitir <b>https://</b> e outros esquemas de URLs na página Web. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355508'>355508</a>)
- Reparação da propriedade MimeType ao usar o kcoreaddons_desktop_to_json()

### KDeclarative

- Migração do KDeclarative para usar o KI18n diretamente
- O <i>delegateImage</i> do DragArea pode agora ser uma <i>string</i>, do qual será criado automaticamente um ícone
- Adição da nova biblioteca CalendarEvents

### KDED

- Remoção da variável de ambiente SESSION_MANAGER em vez de defini-la como "vazia"

### KDELibs 4 Support

- Correção de algumas chamadas 'i18n'

### KFileMetaData

- Marcação do 'm4a' como legível pela biblioteca <i>taglib</i>

### KIO

- Janela de cookies: fazê-la funcionar como desejado
- Correção da sugestão de nomes de arquivos para algo aleatório, quando mudar o tipo MIME na gravação
- Registro do nome DBus do <i>kioexec</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353037'>353037</a>)
- Atualização do KProtocolManager após a mudança da configuração

### KItemModels

- Correção do uso do KSelectionProxyModel no QTableView (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352369'>352369</a>)
- Correção da restauração ou alteração do modelo de origem de um KRecursiveFilterProxyModel.

### KNewStuff

- O <i>registerServicesByGroupingNames</i> pode agora definir mais itens por padrão
- Mudança do KMoreToolsMenuFactory::createMenuFromGroupingNames para carregamento posterior

### KTextEditor

- Adição do realce de sintaxe para o TaskJuggler e o PL/I
- Possibilidade de desativação da completação de palavras-chave pela interface de configuração
- Redimensionamento da árvore quando o modelo de completação foi redefinido

### KWallet Framework

- Tratamento correto do caso em que o usuário nos desativou

### KWidgetsAddons

- Correção de um pequeno problema do KRatingWidget em altas resoluções
- Remodelação e correção da funcionalidade introduzida no erro <a href='https://bugs.kde.org/show_bug.cgi?id=171343'>171343</a>

### KXMLGUI

- Não chamar o QCoreApplication::setQuitLockEnabled(true) na inicialização

### Plasma Framework

- Adição de um plasmoide básico como exemplo para o guia de desenvolvimento
- Adição de um conjunto de modelos de plasmoides para o kapptemplate/kdevelop
- [calendário] Atrasar a redefinição do modelo até que a visualização esteja pronta (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355943'>355943</a>)
- Não reposicionar ao ocultar. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354352'>354352</a>)
- [IconItem] Não falhar com um tema KIconLoader nulo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355577'>355577</a>)
- Soltar arquivos de imagens sobre um painel não oferece mais a opção de os definir como papel de parede do painel
- Soltar um arquivo <i>.plasmoid</i> sobre um painel ou área de trabalho fará com que seja instalado e adicionado
- Remoção do módulo do <i>kded platformstatus</i>, que já não é mais usado (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348840'>348840</a>)
- Permitir colar caracteres em campos de senhas
- Correção do posicionamento do menu de edição e adição de um botão para seleção
- [calendário] Uso do idioma da interface gráfica para obter o nome do mês (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353715'>353715</a>)
- [calendário] Ordenação dos eventos também pelo seu tipo
- [calendário] Transferência da biblioteca de plugins para o KDeclarative
- [calendário] O <i>qmlRegisterUncreatableType</i> precisa de mais alguns argumentos
- Possibilidade de adicionar mais categorias de configuração de forma dinâmica
- [calendário] Transferência do tratamento dos plugins para uma classe separada
- Permitir que os plugins forneçam dados de eventos ao miniaplicativo Calendário (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349676'>349676</a>)
- Verificação da existência do <i>slot</i> antes de conectar ou desconectar (erro <a href='https://bugs.kde.org/show_bug.cgi?id=354751'>354751</a>)
- [plasmaquick] Não se associar explicitamente ao OpenGL
- [plasmaquick] Remoção da dependência do XCB::COMPOSITE e DAMAGE

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
