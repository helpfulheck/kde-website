---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Respeitar a tarefa a ser interrompida de imediato (erro 429939)

### Baloo

* [ExtractorProcess] Mover o sinal do DBus do utilitário para o processo principal
* [timeline] Consolidação de código para as estatísticas e listagem da pasta de topo
* Tornar os itens de topo do 'ioslave' UDS apenas para leitura
* Evitar erros no arranque da aplicação se nunca foi criado o índice do Baloo
* [BasicIndexingJob] Retirar a barra final das pastas (erro 430273)

### Ícones Breeze

* Novo ícone de acção de bússola
* Adição do ícone 'image-missing' ao tema
* Adição do ícone para as imagens WIM

### Módulos extra do CMake

* Indicação ao MSVC que os nossos ficheiros de código estão codificados em UTF-8
* Adição do Findepoxy.cmake
* Ter em consideração os itens de imagens do 'fastlane' local
* Pacotes reproduzidos apenas com o 'tar' da GNU
* Preservar o sub-conjunto de texto formatado suportado pelo F-Droid
* Avanço da versão necessária do CMake para o Android.cmake (erro 424392)
* Detecção automática das dependências d ebibliotecas de 'plugins' no Android
* Verificação se o ficheiro existe antes de remover o pacote 'fastlane'
* Limpar a pasta da imagem e o ficheiro do pacote antes de transferir/gerar os mesmos
* Manter a ordem das imagens no ficheiro Appstream
* Windows: correcção do QT_PLUGIN_PATH para os testes
* Não estoirar se não tiver encontrado nenhumas categorias
* Fazer com que o KDEPackageAppTemplates crie um pacote fácil de reproduzir

### KActivitiesStats

* Remoção da funcionalidade do 'lastQuery' com erros, corrigindo os estoiros no KRunner

### KCalendarCore

* CMakeLists.txt - aumentado da versão mínima da libical para a 3.0

### KCMUtils

* O KPluginSelector implementa um indicador do realce predefinido
* kcmoduleqml: não associar a largura da coluna à largura da área (erro 428727)

### KCompletion

* [KComboBox] correcção do estoiro ao invocar o setEditable(false) com um menu de contexto aberto

### KConfig

* Correcção das janelas a serem maximizadas de forma errada no lançamento (erro 426813)
* Correcção do formato do texto de janela maximizada
* Correcção do tamanho e posição das janelas no Windows (erro 429943)

### KConfigWidgets

* KCodecAction: adição de sinais não-substituíveis 'codecTriggered' e 'encodingProberTriggered'

### KCoreAddons

* Migração do KJobTrackerInterface para a sintaxe do 'connect' do Qt5
* KTextToHtml: correcção de validação devido a uma chamada a 'at()' fora dos limites
* Usar uma hierarquia plana dos locais dos 'plugins' no Android
* conversão de 'desktop' para JSON: ignorar os itens "Actions="
* Descontinuação do KProcess::pid()
* ktexttohtml: correcção da utilização do KTextToHTMLHelper

### KCrash

* Uso do std::unique_ptr<char[]> para evitar fugas de memória

### KDeclarative

* Mudança para o Findepoxy fornecido pelo ECM
* KCMShell: Adição do suporte para a passagem de argumentos
* Contornar o estoiro na detecção do GL e o kwin_wayland
* [KQuickAddons] QtQuickSettings::checkBackend() para usar em alternativa a infra-estrutura por 'software' (erro 346519)
* [abstractkcm] Correcção da versão importada no exemplo de código
* Evitar configurar o QSG_RENDER_LOOP se já estiver configurado
* ConfigPropertyMap : carregamento do valor predefinido da propriedade no mapa

### KDocTools

* Adição de uma entidade para o acrónimo MathML
* Mudança de 'Batalha Naval' para 'KNavalBattle' para cumprir questões legais

### KGlobalAccel

* Evitar iniciar automaticamente o 'kglobalaccel' ao encerrar (erro 429415)

### KHolidays

* Atualização dos feriados japoneses

### KIconThemes

* Ignorar o aviso de alguns ícones do Adwaita por questões de compatibilidade
* O QSvgRenderer::setAspectRatioMode() foi introduzido no Qt 5.15

### KImageFormats

* Adição do AVIF à lista de formatos suportados
* Adição de 'plugin' para o formato de ficheiros de imagens AV1 (AVIF)

### KIO

* [KFileItemDelegate] não desperdiçar espaço para ícones inexistentes nas colunas que não a primeira
* KFilePlacesView, KDirOperator: migração para o 'askUserDelete()' assíncrono
* Modificação da forma como o CopyJob descobre as extensões do JobUiDelegate
* Introdução do AskUserActionInterface, uma API assíncrona para as janelas Mudar o Nome/Ignorar
* RenameDialog: só invocar o compareFiles() para ficheiros
* kcm/webshortcuts: Correcção do botão Reiniciar
* KUrlNavigatorMenu: correcção do tratamento do 'click' com o botão do meio
* Remoção do item do 'knetattach' da janela do 'ioslave' remote:// (erro 430211)
* CopyJob: migração para o AskUserActionInterface
* Jobs: adição de sinal não substituível "mimeTypeFound" para descontinuar o "mimetype"
* RenameDialog: Adição de inicialização de 'nullptr' em falta (erro 430374)
* KShortUriFilter: não filtrar os textos "../" e co. 
* Não dar um erro se o KIO::rawErrorDetail() receber um URL sem esquema (erro 393496)
* KFileItemActions: correcção da condição, já que só queremos excluir as pastas remotas (erro 430293)
* KUrlNavigator: remoção da utilização do 'kurisearchfilter'
* KUrlNavigator: fazer com que as completações de locais relativos funcionem (erro 319700)
* KUrlNavigator: resolução de locais relativos de pastas (erro 319700)
* Silenciar os avisos devidos a questões de configuração do Samba quando não está a usar o Samba de forma explícita
* KFileWidget: permitir a selecção de ficheiros que comecem por um ':' (erro 322837)
* [KFileWidget] correcção da posição do botão de favorito na barra de ferramentas
* KDirOperator: descontinuação do mkdir(const QString &, bool)
* KFilePlacesView: permitir definir um tamanho de ícones estático (erro 182089)
* KFileItemActions: adição de novo método para inserir as acções 'abrir com' (erro 423765)

### Kirigami

* [controls/SwipeListItem]: Mostrar sempre as acções no ecrã por omissão
* [overlaysheet] Uso de um posicionamento mais condicional para o botão de fecho (erro 430581)
* [controls/avatar]: Abertura do AvatarPrivate interno como a API pública NameUtils
* [controls/avatar]: exposição da cor gerada
* Adição do componente Hero
* [controls/Card]: Remoção da animação à passagem
* [controls/ListItem]: Remoção da animação à passagem
* Mudança dos ListItems para usarem o 'veryShortDuration' à passagem em vez do 'longDuration'
* [controls/Units]: Adição do veryShortDuration
* Coloração de ícones de ActionButton's
* uso de imagens dos ícones tão grandes quanto for necessário
* [controls/avatar]: melhor aparência por omissão
* [controls/avatar]: Correcção de erros visuais
* Criação do componente CheckableListItem
* [controls/avatar]: escala do contorno de acordo com o tamanho do avatar
* Reversão do "[Avatar] Mudança do gradiente do fundo"
* Reversão do "[Avatar] Mudança da espessura do contorno para 1px para corresponder às outras espessuras"
* [controls/avatar]: Tornar o Avatar válido para acessibilidade
* [controls/avatar]: Aumento do preenchimento de salvaguarda dos ícones
* [controls/avatar]: Fazer com que o modo da imagem defina o 'sourceSize'
* [controls/avatar]: Ajuste do tamanho do texto
* [controls/avatar]: Ajuste das técnicas usadas para a forma circular
* [controls/avatar]: Adição da acção primária/secundária do Avatar
* Fixação do valor de preenchimento do item de cabeçalho do OverlaySheet
* compilação do qmake: adição dos ficheiros de código/inclusão do 'sizegroup' em falta
* Ícones de cores, não botões (erro 429972)
* Correcção dos botões do cabeçalho para avançar/recuar sem largura
* [BannerImage]: correcção do título do cabeçalho não centrado na vertical com temas não-Plasma

### KItemModels

* Adição da propriedade 'count', o que permite a associação ao 'rowCount' no QML

### KItemViews

* O KWidgetItemDelegate permite agora desencadear um 'resetModel' a partir do KPluginSelector

### KNewStuff

* Descontinuação dos métodos 'standardAction' e 'standardActionUpload'
* Correcção do modelo do QtQuick se só existir um conteúdo, mas sem ligações de transferência
* Adição de um 'dptr' à Cache, passando o temporizador de congestionamento para lá para evitar um estoiro (erro 429442)
* Remodelação do KNS3::Button para usar internamente a nova janela
* Criação de classe de interface para a janela do QML
* Verificar se a versão está vazia antes de concatenar a versão

### KNotification

* Melhoria da documentação da API do KNotification

### KParts

* Descontinuação do BrowserHostExtension

### KQuickCharts

* Uso de uma macro personalizada para as mensagens de descontinuação no QML
* Uso do ECMGenerateExportHeader para as macros de descontinuação e uso das mesmas
* A mudança do intervalo não precisa de limpar o histórico
* Mudança do gráfico de linhas contínuas para o exemplo de código do 'proxy' do histórico
* Descontinuação do Model/ValueHistorySource
* Introdução do HistoryProxySource como substituto do Model/ValueHistorySource
* Adição de categorias de registo para os gráficos e usá-las para os avisos existentes

### KRunner

* [Execução de DBus] Adição do suporte para tamanhos de imagens personalizados dos resultados
* Adição de chave para verificar se a configuração foi migrada
* Separação dos ficheiros de configuração e de dados
* Nova API para ocorrências de execuções e para histórico
* Não compilar o RunnerContextTest no Windows

### KService

* KSycoca: evitar uma reconstrução da base de dados se o XDG_CONFIG_DIRS tiver duplicados
* KSycoca: garantia que os ficheiros extra são ordenados para a comparação (erro 429593)

### KTextEditor

* Mudança de "Variable:" para "Document:Variable:"
* Expansão de Variáveis: Correcção das correspondências do prefixo a pesquisar com vários dois-pontos (:)
* Passagem da pintura do KateTextPreview para o KateRenderer
* Garantia que só são usadas as linhas na área para pintar a imagem
* Uso do KateTextPreview para desenhar a imagem
* Expansão de variáveis: Adição do suporte para o %{Document:Variable:<nome>}
* Mostrar o texto arrastado no arrastamento (erro 398719)
* Correcção do destacamento no TextRange::fixLookup()
* Não pintar o fundo da 'currentLine' se existir uma selecção sobreposta
* KateRegExpSearch: correcção da lógica ao adicionar um '\n' entre as linhas do intervalo
* Mudança de nome da acção para 'Trocar com o conteúdo da área de transferência'
* Adição de uma acção para desencadear o 'copiar & colar' como uma única acção
* funcionalidade: adição de ícone de acção 'text-wrap' para a Mudança de Linha Dinâmica
* Anulação da indentação num único passo (erro 373009)

### KWidgetsAddons

* KSelectAction: adição dos sinais não-substituíveis 'indexTriggered' e 'textTriggered'
* KFontChooserDialog: tratamento da remoção da janela pelo pai durante o exec()
* KMessageDialog: invocação do setFocus() no botão predefinido
* Migração do QLocale::Norwegian para o QLocale::NorwegianBokmal
* Migração do KToolBarPopupActionTest para o QToolButton::ToolButtonPopupMode

### KXMLGUI

* KXmlGui: ao actualizar um ficheiro .rc local, manter as barras de ferramentas novas da aplicação
* Correcção da gravação de teclas pelo 'setWindow' antes de iniciar a captura (erro 430388)
* Remoção da dependência não usada do KWindowSystem
* Limpeza do KXMLGUIClient no documento XML em memória após gravar os atalhos no disco

### Ícones do Oxygen

* Adição do 'upindicator'

### Plasma Framework

* Exposição da informação de erros no plasmóide respectivo de uma forma mais estruturada
* [componentes] Mnemónicas de Fixação
* [svg] Iniciar sempre o temporizador da SvgRectsCache da tarefa actual
* [ProgressBar do PC3] Definição da associação à largura (erro 430544)
* correcção da compilação em Windows + inversão das variáveis
* [PlasmaComponents/TabGroup] Correcção da verificação se o item herda de Page
* Migração de vários componentes para o 'veryShortDuration' à passagem
* Mudança dos ListItems para usarem o 'veryShortDuration' à passagem em vez do 'longDuration'
* Adição do veryShortDuration
* Não permitir anos negativos no calendário (erro 430320)
* Correcção do fundo problemático (erro 430390)
* Substituição dos ID's da 'cache' do QString por uma versão baseada em estruturas
* [TabGroup] Reversão das animações no modo RTL
* Só remover os atalhos na remoção do elemento, não na sua destruição
* Esconder as acções de contexto desactivadas do ExpandableListItem

### Purpose

* KFileItemActions: adição do 'windowflag' do menu
* 'Plugin' de item de ficheiros para Partilhar: usar o Widget-pai como pai do menu (erro 425997)

### QQC2StyleBridge

* Actualização do org.kde.desktop/Dialog.qml
* Desenho do ScrollView com uma Frame em vez de um Edit (erro 429601)

### Sonnet

* Melhoria da performance do createOrderedModel com o QVector
* Evitar um aviso durante a execução se não existir nenhum resultado adivinhado

### Realce de sintaxe

* Realce do C++: QOverload e companhia
* Correcção das legendas que começam por pontos que não são realçadas no GAS
* realce do C++: adição da macro qGuiApp
* Melhoria do tema Drácula
* correcção #5: Bash, Zsh: ! com 'if', 'while', 'until' ; Bash: estilo de padrões para ${var,padrão} e ${var^padrão}
* correcção #5: Bash, Zsh: comentários dentro de uma lista
* Sintaxe de funcionalidades do Cucumber
* Zsh: incremento da versão da sintaxe
* Zsh: correcção da expansão dos parêntesis num comando
* adição do 'weakDeliminator' e 'additionalDeliminator' com as palavras-chave, WordDetect, Int, Float, HlCOct e HlCHex
* Indexação: reposição do 'currentKeywords' e 'currentContext' ao abrir uma nova definição
* Zsh: Muitas correcções e melhorias
* Bash: correcção dos comentários no 'case', expressões de sequências e o ')' após o ']'
* Tratamento da cor da importação na base correcta e especialização para o C/C++
* Actualização do tema Monokai
* Verificação da correcção/presença dos estilos personalizados nos temas
* Adição dos temas GitHub Claro/Escuro
* incremento da versão, não mudando a versão do Kate, até que se compreenda porque é necessário
* Adição das licenças
* Adição dos temas Atom One claro/escuro
* não foi incrementada a versão na alteração
* Correcção da cor do operador '&' do atributo no Monokai
* Adição do tema Monokai
* CMake: Adição de variáveis do 3.19 em falta e algumas novas do 3.19.2
* Kotlin: correcção de alguns problemas e outras melhorias
* Groovy: correcção de alguns problemas e outras melhorias
* Scala: correcção de alguns problemas e outras melhorias
* Java: correcção de alguns problemas e outras melhorias
* correcção do && e do || num sub-contexto e correcção do padrão de nomes das funções
* adição do QRegularExpression::DontCaptureOption quando não existe nenhuma regra dinâmica
* Bash: adição do (...), ||, && no [[ ... ]] ; adição de plicas invertidas no [ ... ] e [[ ... ]]

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
