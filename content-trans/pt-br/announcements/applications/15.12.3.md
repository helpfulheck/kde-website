---
aliases:
- ../announce-applications-15.12.3
changelog: true
date: 2016-03-16
description: O KDE disponibiliza o KDE Applications 15.12.3
layout: application
title: O KDE disponibiliza o KDE Applications 15.12.3
version: 15.12.3
---
15 de Março de 2016. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../15.12.0'>Aplicações do KDE 15.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 15 correções de erros registradas incluem melhorias no kdepim, akonadi, Ark, KBlocks, Kcalc, Ktouch e Umbrello, dentre outras.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.18.
