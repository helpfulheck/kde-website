---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Datumfilter gebruikt door timeline:// repareren
- BalooCtl: terugkeer na commando's
- Opschoene en versterken van Baloo::Database::open(), behandel meer crashcondities
- Controle toevoegen aan Database::open(OpenDatabase) om te mislukken als db niet bestaat

### Breeze pictogrammen

- Veel pictogrammen toegevoegd en verbeterd
- stijlsheets in breeze pictogrammen gebruiken (bug 126166)
- BUG: 355902 repareer en wijzig systeemschermvergrendeling (bug 355902 repareer en wijzig systeemschermvergrendeling)
- 24px dialooginformatie toevoegen voor GTK apps (bug 355204)

### Extra CMake-modules

- Niet waarschuwen wanneer SVG(Z) pictogrammen worden geleverd met meerdere groottes/niveau van details
- Ga na dat we vertalingen laden in de hoofdthread. (bug 346188)
- Het ECM bouwsysteem overhoop halen.
- Maak het mogelijk om Clazy in te schakelen in elk KDE-project
- Zoek niet standaard naar XCB's XINPUT bibliotheek.
- Exportmap opschonen alvorens een APK opnieuw te genereren.
- quickgit gebruiken voor URL van Git-repository.

### Frameworkintegratie

- Installatie van plasmoid is mislukt toevoegen aan plasma_workspace.notifyrc

### KActivities

- Een vergrendeling bij de eerste start van de daemon repareren
- Creatie van QAction verplaatsen naar de hoofdthread. (bug 351485)
- clang-format maakt soms een slechte besluit (bug 355495)
- Potentiële problemen met synchronisatie doden
- org.qtproject gebruiken in plaats van com.trolltech
- Het gebruik van libkactivities uit de plug-ins verwijderen
- KAStats configuratie verwijderd uit de API
- Koppelen en koppeling verwijderen toegevoegd aan ResultModel

### KDE Doxygen hulpmiddelen

- kgenframeworksapidox meer robuust maken.

### KArchive

- KCompressionDevice::seek(), aangeroepen bij maken van een KTar bovenop een KCompressionDevice repareren.

### KCoreAddons

- KAboutData: sta https:// toe en andere URL schema's in homepagina. (bug 355508)
- Eigenschappen van MIME-typen repareren bij gebruik van kcoreaddons_desktop_to_json()

### KDeclarative

- KDeclarative overzetten om KI18n direct te gebruiken
- DragArea delegateImage kan nu een tekenreeks zijn waaruit een pictogram automatisch wordt gemaakt
- Nieuwe CalendarEvents bibliotheek toevoegen

### KDED

- Omgevingsvariabele SESSION_MANAGER verwijderen in plaats van het leeg maken

### Ondersteuning van KDELibs 4

- Enkele i18n aanroepen repareren.

### KFileMetaData

- m4a markeren als leesbaar door taglib

### KIO

- Cookie-dialoog: het laten werken zoals bedoeld
- Suggestie voor wijziging van de bestandsnaam gerepareerd tot iets willekeurigs bij wijziging van het mimetype bij opslaan als.
- DBus-naam registreren voor kioexec (bug 353037)
- KProtocolManager bijwerken na wijziging in configuratie.

### KItemModels

- Gebruik van KSelectionProxyModel repareren in QTableView (bug 352369)
- Resetten of wijzigingen van het broncodemodel van een KRecursiveFilterProxyModel repareren.

### KNewStuff

- registerServicesByGroupingNames kan standaard meer items definiëren
- KMoreToolsMenuFactory::createMenuFromGroupingNames trager maken

### KTextEditor

- Syntaxisaccentuering voor TaskJuggler en PL/I toevoegen
- Het mogelijk maken om sleutelwoord aanvullen uit te schakelen via het configuratieinterface.
- Stel de grootte van de boomstructuur opnieuw in wanneer het aanvullingsmodel gereset wordt

### KWallet Framework

- Behandel het geval waar de gebruiker ons deactiveert op de juiste manier

### KWidgetsAddons

- Een klein artifact van KRatingWidget bij hi-dpi repareren.
- Maak opnieuw en repareer de functie geïntroduceerd in bug 171343 (bug 171343)

### KXMLGUI

- Roep QCoreApplication::setQuitLockEnabled(true) niet aan bij initialisatie.

### Plasma Framework

- Voeg een basis plasmoid als voorbeeld toe voor developerguide
- Voeg een aantal plasmoidsjablonen toe voor kapptemplate/kdevelop
- [calendar] Resetten van het model vertragen totdat het beeld gereed is (bug 355943)
- Niet opnieuw positioneren bij verbergen. (bug 354352)
- [IconItem] Geen crash bij null KIconLoader thema (bug 355577)
- Laten vallen van afbeeldingsbestanden op een paneel zal niet langer aanbieden om ze in te stellen als achtergrondafbeeldingen voor het paneel
- Laten vallen van een .plasmoid bestand in een paneel of op het bureaublad zal het installeren en toevoegen
- de nu ongebruikte platformstatus kded module verwijderen (bug 348840)
- sta plakken toe in wachtwoordvelden
- positionering van menu voor bewerken repareren, voeg een knop toe om te selecteren
- [calendar] taal van ui gebruiken voor ophalen van de naam van de maand (bug 353715)
- [calendar] sorteer de gebeurtenissen ook op hun type
- [calendar] de plug-inbibliotheek verplaatsen naar KDeclarative
- [calendar] qmlRegisterUncreatableType heeft iets meer argumenten nodig
- Dynamisch toevoegen van configuratiecategorieën toestaan
- [calendar] verplaats het handelen van plug-ins naar een aparte klasse
- Sta plug-ins toe om gegevens van gebeurtenissen te verstrekken aan het applet Agenda (bug 349676)
- controleren op bestaan van slot alvorens te verbinden of verbreken (bug 354751)
- [plasmaquick] OpenGL niet expliciet koppelen
- [plasmaquick] Laat XCB::COMPOSITE en DAMAGE afhankelijkheid vallen

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
