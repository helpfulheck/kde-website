---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Verklein en herschrijf de baloo-tags van KIO-slave (bug 340099)

### BluezQt

- Geen file-descriptors van rfkill lekken (bug 386886)

### Breeze pictogrammen

- Ontbrekende pictogramgroottes toevoegen (bug 384473)
- pictogrammen voor installeren en deïnstalleren voor ontdekken toevoegen

### Extra CMake-modules

- De beschrijvingstag toevoegen aan de gegenereerde pkgconfig-bestanden
- ecm_add_test: juiste padscheider gebruiken onder Windows
- FindSasl2.cmake aan ECM toevoegen
- Geef de ARG-dingen alleen door bij het doen van Makefiles
- FindGLIB2.cmake en FindPulseAudio.cmake toevoegen
- ECMAddTests: QT_PLUGIN_PATH instellen zodat lokaal gebouwde plug-ins zijn te vinden
- KDECMakeSettings: meer documentatie over de indeling van de build-map

### Frameworkintegratie

- Ondersteuning voor downloaden van de 2de of 3de download-link uit een KNS-product (bug 385429)

### KActivitiesStats

- Repareren starten van libKActivitiesStats.pc: (bug 386933)

### KActivities

- Race repareren die kactivitymanagerd meerdere keren start

### KAuth

- Sta toe om alleen de kauth-policy-gen codegenerator te bouwen
- Een notitie toevoegen over het aanroepen van de helper uit toepassingen met multithread

### KBookmarks

- Laat geen acties zien om bladwijzers te bewerken als keditbookmarks niet is geïnstalleerd
- Overbrengen van verouderde KAuthorized::authorizeKAction naar authorizeAction

### KCMUtils

- navigatie met het toetsenbord in en uit QML kcms

### KCompletion

- Niet crashen bij instellen van nieuwe regelbewerking in een bewerkbare keuzelijst
- KComboBox: vroeg terugkeren bij instellen van een bewerkbare naar vorige waarde
- KComboBox: het bestaannde object voor voltooien opnieuw gebruiken bij een nieuwe regelbewerking

### KConfig

- Kijk niet elke keer naar /etc/kderc

### KConfigWidgets

- Werk standaard kleuren bij om overeen te komen met nieuwe kleuren in D7424

### KCoreAddons

- Valideren van invoer van SubJobs
- Waarschuwen over fouten bij ontleden van json-bestanden
- MIME-type definities voor kcfg/kcfgc/ui.rc/knotify &amp; qrc bestanden
- Een nieuwe functie toevoegen om de lengte van tekst te meten
- Bug in KAutoSave repareren bij bestand met witruimte erin

### KDeclarative

- Maak dat het compileert onder Windows
- Maak dat het compileert met QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Accepteren van een muisgebeurtenis toestaan
- een enkele QML-engine gebruiken

### KDED

- kded: dbus aanroepen naar ksplash verwijderen

### KDocTools

- Braziliaans/Portugese vertalingen bijwerken
- Russische vertalingen bijwerken
- Russische vertalingen bijwerken
- customization/xsl/ru.xml bijwerken (nav-home ontbrak)

### KEmoticons

- KEmoticons: plug-ins naar JSON overbrengen en ondersteuning voor laden met KPluginMetaData toevoegen
- Geen symbolen van pimpl-klassen lekken, beschermen met Q_DECL_HIDDEN

### KFileMetaData

- De usermetadatawritertest vereist Taglib
- Als de eigenschapwaarde null is, verwijder het attribuut user.xdg.tag (bug 376117)
- Bestanden in TagLib-extractor alleen-lezen openen

### KGlobalAccel

- Enige blokkerende dbus aanroepen groeperen
- kglobalacceld: een pictogramlader niet laden zonder reden
- juiste sneltoetstekenreeksen genereren

### KIO

- KUriFilter: gedupliceerde plug-ins uitfilteren
- KUriFilter: gegevensstructuren vereenvoudigen, geheugenlek repareren
- [CopyJob] Niet alles opnieuw starten nadat een bestand is verwijderd
- Aanmaken van een map via KNewFileMenu+KIO::mkpath onder Qt 5.9.3+ repareren (bug 387073)
- Een extra functie 'KFilePlacesModel::movePlace' aangemaakt
- Rol van KFilePlacesModel 'iconName' zichtbaar maken
- KFilePlacesModel: onnodig signaal 'dataChanged' vermijden
- Een geldig bladwijzerobject teruggeven voor elk item in KFilePlacesModel
- Een functie 'KFilePlacesModel::refresh' aanmaken
- Een statische functie 'KFilePlacesModel::convertedUrl' aanmaken
- KFilePlaces: 'remote' sectie aanmaken
- KFilePlaces: een sectie voor verwijderbare apparaten toevoegen
- Baloo-url's toegevoegd in plaatsenmodel
- KIO::mkpath met qtbase 5.10 beta 4 repareren
- [KDirModel] stuur een wijziging voor HasJobRole wanneer jobs wijzigen
- Label "Advanced options" &gt; "Terminal options" wijzigen

### Kirigami

- De schuifbalk verschuiven met de headergrootte (bug 387098)
- ondermarge gebaseerd op aanwezigheid van actieknoppen
- niet aannemen dat applicationWidnow() aanwezig is
- Niet melden over wijziging van waarden als we nog steeds in de constructor zijn
- De bibliotheeknaam in de broncode vervangen
- kleuren op meer plaatsen ondersteunen
- pictogrammen in werkbalken kleuren indien nodig
- pictogramkleuren ovderwegen in de hoofdactieknoppen
- begin van een "pictogram" gegroepeerde eigenschap

### KNewStuff

- Draai terug "Detach before setting the d pointer" (bug 386156)
- geen ontwikkelhulpmiddel installeren om bureaubladbestanden samen te nemen
- [knewstuff] ImageLoader niet lekken bij fout

### KPackage-framework

- Doe tekenreeks op de juiste manier in het kpackage-framework
- Genereren van metadata.json niet proberen als er geen metadata.desktop is
- cache in kpluginindex repareren
- Uitvoer van fouten verbeteren

### KTextEditor

- VI-Mode buffercommando's repareren
- per ongeluk zoomen voorkomen

### KUnitConversion

- Overbrengen van QDom naar QXmlStreamReader
- Https gebruiken voor downloaden van wisselkoersen 

### KWayland

- wl_display_set_global_filter als een virtuele methode bekend maken
- kwayland-testXdgShellV6 repareren
- Ondersteuning voor zwp_idle_inhibit_manager_v1 toevoegen (bug 385956)
- [server] onderbreken van het IdleInterface ondersteunen

### KWidgetsAddons

- Inconsistente wachtwoorddialoog vermijden
- enable_blur_behind-hint op aanvraag instellen
- KPageListView: breedte bijwerken bij wijziging van lettertype

### KWindowSystem

- [KWindowEffectsPrivateX11] aanroep van reserve() toevoegen

### KXMLGUI

- Vertaling van werkbalknaam wanneer het een i18n-context heeft

### Plasma Framework

- De directive #warning is niet universeel en in het bijzonder NIET ondersteunt door MSVC
- [IconItem] ItemSceneHasChanged gebruiken in plaats van verbinden op windowChanged
- [Pictogramitem] Stuur expliciet overlaysChanged uit in de zetten ijn plaats van ermee verbinden
- [Dialog] KWindowSystem::isPlatformX11() gebruiken
- Verminder de hoeveelheid van kleine wijzigingen in eigenschap op ColorScope
- [Pictogramitem] validChanged alleen uitsturen als het echt is gewijzigd
- Onderdruk schuifindicatoren als het schuifbare een ListView is met bekende oriëntatie
- [AppletInterface] signalen alleen uitsturen voor configurationRequired en -Reason
- setSize() gebruiken in plaats van setProperty breedte en hoogte
- Een probleem gerepareerd waar PlasmaComponents Menu zou verschijnen met gebroken hoeken (bug 381799)
- Een probleem gerepareerd waar contextmenu's zouden verschijnen met gebroken hoeken (bug 381799)
- API docs: melding over veroudering toevoegen gevonden in de gitlog
- Synchroniseer de component met die in Kirigami
- Doorzoek alle KF5-componenten als geheel in plaats van als separate frameworks
- Zeldzame uitzending van signalen verminderen (bug 382233)
- Signalen toevoegen die aangegeven of een scherm is toegevoegd of verwijderd
- Switch-spullen installeren
- Niet vertrouwen op includes van includes
- SortFilterModel-rolnamen optimaliseren
- DataModel::roleNameToId verwijderen

### Prison

- Aztec codegenerator toevoegen

### QQC2StyleBridge

- QQC2 versie op moment van bouwen bepalen (bug 386289)
- houd de achtergrond standaard onzichtbaar
- een achtergrond in ScrollView toevoegen

### Solid

- UDevManager::devicesFromQuery sneller

### Sonnet

- Het mogelijk maken om sonnet te crosscompileren

### Accentuering van syntaxis

- PKGUILD toevoegen aan bash-syntaxis
- JavaScript: standaard MIME-typen invoegen
- debchangelog: Bionic Beaver toevoegen
- SQL (Oracle) syntaxisbestand bijwerken (bug 386221)
- SQL: detecteren van commentaar verplaatsen voor operators
- crk.xml: &lt;?xml&gt;-header-regel toegevoegd

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
