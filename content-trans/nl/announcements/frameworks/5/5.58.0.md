---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Wacht op extractieproces om te beginnen
- [balooctl] commando toevoegen om bestanden te tonen die mislukten bij indexeren (bug 406116)
- QML toevoegen aan typen broncode
- [balooctl] de constante totaalgrootte vangen in de lambda
- [balooctl] multiregel uitvoer naar nieuwe helper omschakelen
- [balooctl] nieuwe helper gebruiken in json-uitvoer
- [balooctl] nieuwe helper voor eenvoudig formaat uitvoer gebruiken
- [balooctl] Factor out file index status collection from output
- Lege Json metagegevensdocumenten buiten DocumentData DB houden
- [balooshow] verwijzen naar bestanden met URL uit harde koppeling toestaan
- [balooshow] waarschuwing onderdrukken wanneer URL verwijst naar niet geïndexeerd bestand
- [MTimeDB] tijdstempel toestaan nieuwer dan het nieuwste document in de reeks van de overeenkomst
- [MTimeDB] exacte overeenkomst gebruiken wanneer exacte overeenkomst wordt vereist
- [balooctl] behandeling van opschonen van verschillende positionele argumenten
- [balooctl] helptekst van opties uitbreiden, foutcontrole verbeteren
- [balooctl] beter te begrijpen namen gebruiken voor grootte in statusuitvoer
- [balooctl] commando opschonen: onnodige controle naar documentData verwijderen, opschonen
- [kio_search] waarschuwing repareren, UDSEntry toevoegen voor "." in listDir
- Hex-notatie voor DocumentOperation vlag enum gebruiken
- Totale DB-grootte juist berekenen
- Ontleden van term uitstellen totdat het nodig is, niet zowel term als zoektekenreeks instellen
- Geen standaard datumwaarden voor filters aan json toevoegen
- Compact json formaat gebruiken bij converteren van query URL's
- [balooshow] geen onzinnige waarschuwing afdrukken voor een niet geïndexeerd bestand

### Breeze pictogrammen

- Niet-symbolische 16px versies van zoek-locatie en markering-locatie toevoegen
- Symbolische koppeling preferences-system-windows-effect-flipswitch naar preferences-system-tabbox
- Symbolische koppeling pictogram "edit-delete-remove" toevoegen en 22px versie van "paint-none" en "edit-none"
- Consistent standaard gebruikerspictogram van Kickoff gebruiken
- Een pictogrammen toevoegen voor Thunderbolt KCM
- Sharpen Z's in system-suspend* pictogrammen
- "widget-alternatives" pictogram verbeteren
- go-up/down/next/previous-skip toevoegen
- KDE logo bijwerken om dichter bij origineel te zijn
- Alternatieven pictogram toevoegen

### Extra CMake-modules

- Bug reparatie: zoek c++ stl met regex
- Schakel -DQT_STRICT_ITERATORS onvoorwaardelijk in, niet alleen in modus debug

### KArchive

- KTar: tegen negatieve longlinkgroottes beschermen
- Ongeldig schrijven in geheugen bij misvormde tar-bestanden repareren
- Geheugenlek repareren bij lezen van sommige tar-bestanden
- Niet geïnitialiseerd geheugen repareren bij lezen van verkeerd gevormde tar-bestanden
- Stack-buffer-overflow repareren bij lezen van verkeerd gevormde bestanden
- Geen nul-verwijzing gebruiken bij misvormde tar-bestanden repareren
- krcc.h-header installeren
- Dubbel verwijderen repareren bij gebroken bestanden
- Kopie van KArchiveDirectoryPrivate en KArchivePrivate niet toestaan
- KArchive::findOrCreate repareren die uit gebruik van stapel loopt op ERG LANGE paden
- KArchiveDirectory::addEntryV2 introduceren en gebruiken
- removeEntry kan mislukken dus is het goed dat te weten indien het zo deed
- KZip: Heap-use-after-free repareren in gebroken bestanden

### KAuth

- KAuth helpers afdwingen om UTF-8 ondersteuning te hebben (bug 384294)

### KBookmarks

- Ondersteuning voor KBookmarkOwner toevoegen om te communiceren als het tabbladen open heeft

### KCMUtils

- Hints voor grootte uit het ApplicationItem zelf gebruiken
- Oxygen achtergrondgradient voor QML-modules

### KConfig

- Mogelijkheid van melding toevoegen aan KConfigXT

### KCoreAddons

- Foute waarschuwing "Niet in staat om servicetype te vinden" repareren
- Nieuwe klasse KOSRelease - een parser voor os-release bestanden

### KDeclarative

- [KeySequenceItem] De knop wissen dezelfde hoogte maken als sneltoetsknop
- Plotter: Scoop GL Program naar lifespan van scenegraph node (bug 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: Geen Engelse tekst aan de gebruiker tonen
- [ConfigModule] Initiële eigenschappen in push() doorgeven
- glGetGraphicsResetStatus ondersteuning standaard op Qt &gt;= 5.13 inschakelen (bug 364766)

### KDED

- .desktop bestand installeren voor kded5 (bug 387556)

### KFileMetaData

- [TagLibExtractor] crash repareren op ongeldige Speex bestanden (bug 403902)
- exivextractor crash repareren met misvormde bestanden (bug 405210)
- Eigenschappen declareren als metatype
- Eigenschapattributen wijzigen voor consistentie
- Behandel variantenlijst in formatteringsfuncties
- Reparatie voor LARGE_INTEGER type van Windows
- (Compilatie) fouten voor Windows UserMetaData implementatie
- Ontbrekend MIME-type toevoegen aan taglibwriter
- [UserMetaData] wijzigingen behandelen in attribuutgegevensgrootte juist behandelen
- [UserMetaData] Windows, Linux/BSD/Mac en stubcode uiteenrafelen

### KGlobalAccel

- Container in Component::cleanUp kopiëren vóór iteratie
- Geen qAsConst gebruiken bovenop een tijdelijke variabele (bug 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb - Zambia vakantiedagen toevoegen
- holidays/plan2/holiday_lv_lv - Midzomerdag repareren
- holiday_mu_en - vakanties 2019 in Mauritius
- holiday_th_en-gb - bijgewerkt voor 2019 (bug 402277)
- Japanse vakantiedagen bijgewerkt
- Publieke vakantiedagen toevoegen voor Laag Saksisch (Duitsland)

### KImageFormats

- tga: niet proberen om meer dan max_palette_size in palet te lezen
- tga: memset dst als lezen mislukt
- tga: memset het gehele paletarray, niet alleen de palette_size
- De ongelezen bits van _starttab initialiseren
- xcf: niet geïnitialiseerd gebruik van geheugen op gebroken documenten repareren
- ras: Niet teveel invoegen lezen uit misvormde bestanden
- xcf: laag is const in kopiëren en samenvoegen, markeer het als zodanig

### KIO

- [FileWidget] "Filter:" vervangen door "Bestandstype:" bij opslaan met een bepekte lijst van MIME-types (bug 79903)
- Nieuw aangemaakte bestanden 'Koppeling naar toepassing' hebben een algemeen pictogram
- [Properties dialog] De tekenreeks "Vrije ruimte" gebruiken in plaats van "Schijfgebruik" (bug 406630)
- UDSEntry::UDS_CREATION_TIME invullen onder linux bij glibc &gt;= 2.28
- [KUrlNavigator] URL-navigatie repareren bij verlaten van archive met krarc en Dolphin (bug 386448)
- [KDynamicJobTracker] Wanneer kuiserver niet beschikbaar is, ook terugvallennaar widgetdialoog (bug 379887)

### Kirigami

- [aboutpage] kop met auteurs verbergen als er geen auteurs zijn
- qrc.in bijwerken om overeen te komen met .qrc (ontbrekende ActionMenuItem)
- Ga na dat we de ActionButton  niet uitknijpen bug 406678)
- Pages: juiste contentHeight/implicit grootte exporteren
- [ColumnView] Ook controleren op index in childfilter..
- [ColumnView] De muis terugknop verder terug laten gaan dan de eerste pagina
- header heeft onmiddellijk de juiste grootte

### KJobWidgets

- [KUiServerJobTracker] kuiserver serviceleeftijd volgen en jobs opnieuw registreren indien nodig

### KNewStuff

- Pixelated rand verwijderen (bug 391108)

### KNotification

- [Notify by Portal] Standaard actie en tips voor prioriteit ondersteunen
- [KNotification] HighUrgency toevoegen
- [KNotifications] Bijwerken wanneer vlaggen, url's of urgentie wijzigt
- Urgentie toestaan in te stellen voor meldingen

### KPackage-framework

- Ontbrekende eigenschappen toevoegen in kpackage-generic.desktop
- kpackagetool: kpackage-generic.desktop lezen uit qrc
- AppStream generation: ga na dat we naar de pakketstructuur zoeken op pakketten die ook metadata.desktop/json hebben

### KTextEditor

- Pagina's van kate configuratie herzien om vriendelijkheid van onderhoud te verbeteren
- Wijzigen van de modus toestaan na wijziging van de accentuering
- ViewConfig: nieuw generiek configuratie-interface gebruiken
- Bladwijzer pixmap tekenen repareren op pictogrambalk
- Ga na dat de linker rand geen wijzigen van het aantal regelnummercijfers mist
- Tonen van invouwen vooruitblik repareren wanneer de muis van onder naar boven verplaatst
- Review van IconBorder
- Invoermethoden toevoegen aan statusbalkknop invoermethode
- Markering voor invouwen tonen in de juiste kleur en maak het beter zichtbaar
- standaard sneltoets F6 verwijderen voor pictogramrand tonen
- Actie om invouwen omschakelen toevoegen van childreeksen (bug 344414)
- Knop "Sluiten" andere titel geven naar "Bestand sluiten" wanneer een bestand is verwijderd op de schijf (bug 406305)
- up copy-right, dat zou misschien ook een definiëren moeten zijn
- tegenstrijdige sneltoetsen vermijden voor omschakelende tabbladen
- KateIconBorder: Invouwen popupbreedte en -hoogte repareren
- weergave sprong naar onder bij invouwen van wijzigingen
- DocumentPrivate: inspringmodus respecteren bij selectie van blok (bug 395430)
- ViewInternal: makeVisible(..) repareren (bug 306745)
- DocumentPrivate: behandeling van haakjes slim maken (bug 368580)
- ViewInternal: gebeurtenis laten vallen herzien
- Een document sluiten toestaan waarvan bestand op schijf was verwijderd
- KateIconBorder: UTF-8 teken gebruiken in plaats van speciale pixmap als dynwrapindicator
- KateIconBorder: Nagaan Dyn Wrap Marker worden getoond
- KateIconBorder: Cosmetisch coderen
- DocumentPrivate: automatisch haakje ondersteunen in modus blokselectie (bug 382213)

### KUnitConversion

- l/100 km naar MPG conversie repareren (bug 378967)

### KWallet Framework

- Juiste kwalletd_bin_path instellen
- Pad van binair programma kwalletd voor kwallet_pam

### KWayland

- CriticalNotification venstertype aan PlasmaShellSurface protocol toevoegen
- wl_eglstream_controller serverinterface implementeren

### KWidgetsAddons

- kcharselect-data bijwerken naar Unicode 12.1
- Interne model van KCharSelect: zorg dat rowCount() 0 is voor geldige indexen

### KWindowSystem

- CriticalNotificationType introduceren
- NET_WM_STATE_FOCUSED ondersteunen
- Documenteer dat modToStringUser en stringUserToMod alleen werken met Engelse tekenreeksen

### KXMLGUI

- KKeySequenceWidget: geen Engelse tekenreeksen aan de gebruiker tonen

### NetworkManagerQt

- WireGuard: Niet vereisen dat 'private-key' niet-leeg mag zijn voor 'private-key-flags'
- WireGuard: werk om verkeerd type geheime vlag heen
- WireGuard: private-key en preshared-keys kunnen samen gevraagd worden

### Plasma Framework

- PlatformComponentsPlugin: plug-in iid naar QQmlExtensionInterface repareren
- IconItem: overblijvende &amp; ongebruikte soepele eigenschapsbits verwijderen
- [Dialog] type CriticalNotification toevoegen
- Verkeerde groepsnamen voor 22, 32 px in audio.svg repareren
- de tekstwerkbalk van mobiel alleen laten verschijnen bij indrukken
- de nieuwe Kirigami.WheelHandler gebruiken
- Meer pictogramgroottes voor geluid, configureren, distribueren toevoegen
- [FrameSvgItem] filteren op soepele wijzigingen bijwerken
- Air/Oxygen bureaubladthema: hoogte voortgangsbalk bij gebruik "hint-bar-size" repareren
- Ondersteuning van stijlsheet voor audio-volume-medium repareren
- Pictogrammen voor geluid, station, bewerken, ga naar, lijst, medium, plasmakluis bijwerken om overeen te komen met breeze-icons
- Z's uitlijnen op pixelraster in system.svg
- de mobiletextcursor gebruiken uit juiste naamruimte
- [FrameSvgItem] eigenschap soepel respecteren
- Oxygen bureaubladthema: opvulling naar handen toevoegen, tegen geribbelde omlijning bij draaien
- SvgItem, IconItem: eigenschap "soepel" overschrijven laten vallen, node bij wijziging bijwerken
- gzipping van svgz ook ondersteunen op windows, met 7z
- Bureaubladthema Air/Oxygen: offsets van wijzers repareren met hint-*-rotation-center-offset
- Aan te roepen publieke API voor uitzenden van contextualActionsAboutToShow toevoegen
- Breeze bureaubladthema klok: wijzerschaduwoffset tip van Plasma 5.16 ondersteunen
- Bureaubladthema SVG bestanden niet gecomprimeerd houden in repo, installeer svgz
- tekstselectie uit mobiel scheiden om recursieve import te vermijden
- Meer toepasselijk pictogram en tekst "Alternatieven" gebruiken
- FrameSvgItem: eigenschap "masker" toevoegen

### Prison

- Aztec: opvulling repareren als het laatste gedeeltelijke codewoord bestaat uit één-bits

### QQC2StyleBridge

- Nesten van besturing vermijden in TextField (bug 406851)
- de tekstwerkbalk van mobiel alleen laten verschijnen bij indrukken
- [TabBar] hoogte bijwerken wanneer TabButtons dynamisch worden toegevoegd
- de nieuwe Kirigami.WheelHandler gebruiken
- Aangepaste pictogramgrootte ondersteunen voor ToolButton
- Het compileert prima zonder foreach

### Solid

- [Fstab] ondersteuning voor niet-netwerk bestandssysteem toevoegen
- [FsTab] cache toevoegen voor type apparaatbestandssysteem
- [Fstab] voorbereidend werk voor inschakelen van bestandssystemen verder dan NFS/SMB
- Geen member genaamd 'setTime_t' repareren in 'QDateTime' fout bij bouwen (bug 405554)

### Accentuering van syntaxis

- Syntaxisaccentuering toevoegen voor fish-shell
- AppArmor: toekennen aan variabele en aliasregels binnen profielen niet accentueren

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
