---
aliases:
- ../../kde-frameworks-5.78.0
date: 2021-01-09
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* De taak die wordt afgebroken direct doen (bug 429939)

### Baloo

* [ExtractorProcess] DBus signaal verplaatsen van helper naar hoofdproces
* [timeline] code consolideren voor stat en list van hoofdmap
* Topniveau ioslave UDS items alleen-lezen maken
* Fouten vermijden voor opstarten van toepassingen als geen baloo-index ooit is aangemaakt
* [BasicIndexingJob] slash achteraan mappen strippen (bug 430273)

### Breeze pictogrammen

* Nieuw actiepictogram voor kompas
* Pictogram voor ontbrekende afbeelding aan thema toevoegen
* Pictogram voor WIM-afbeeldingen toevoegen

### Extra CMake-modules

* MSVC vertellen dat onze broncodebestanden UTF-8 gecodeerd zijn
* Findepoxy.cmake toevoegen
* Lokale fastlane afbeeldingenbezit overwegen
* Te reproduceren tarballs alleen met GNU tar only
* De subset van rich-text ondersteund door F-Droid behouden
* Versie van vereiste cmake voor Android.cmake ophogen (bug 424392)
* Automatisch plug-in lib deps op Android detecteren
* Controleren of bestand bestaat alvorens het fastlane archief te verwijderen
* Afbeeldingsmap en archiefbestand schonen alvorens deze te downloaden/genereren
* Volgorde van schermafdruk uit het appstream-bestand behouden
* Windows: tests van QT_PLUGIN_PATH repareren
* Niet mislukken als we geen categorieën hebben gevonden
* Zorg dat KDEPackageAppTemplates een reproduceerbare tarball aanmaakt

### KActivitiesStats

* Gebroken lastQuery functie verwijderen, repareert voor mij crashes van krunner

### KCalendarCore

* CMakeLists.txt - minimale libical versie naar 3.0 verhogen

### KCMUtils

* KPluginSelector standaard indicator van accentuering implementeren
* kcmoduleqml: kolombreedte niet binden aan weergavebreedte (bug 428727)

### KCompletion

* [KComboBox] crash bij aanroepen van setEditable(false) met open contextmenu repareren

### KConfig

* Vensters die niet juist gemaximeerd zijn bij starten repareren (bug 426813)
* Formaat van tekenreeks venster gemaximeerd corrigeren
* Grootte en positionering van venster op Windows repareren (bug 429943)

### KConfigWidgets

* KCodecAction: niet-overladen signalen codecTriggered & encodingProberTriggered  toevoegen

### KCoreAddons

* KJobTrackerInterface overzetten naar Qt5 verbindingssyntaxis
* KTextToHtml: toekenning vanwege buiten grenzen bij aanroep van at() repareren
* Vlakke hiërarchie voor plug-in-paden op Android gebruiken
* conversie van desktop naar JSON: item "Actions=" negeren
* KProcess::pid() afkeuren
* ktexttohtml: gebruik van KTextToHTMLHelper repareren

### KCrash

* std::unique_ptr<char[]> gebruiken om geheugenlekken te voorkomen

### KDeclarative

* Naar Findepoxy geleverd door ECM omschakelen
* KCMShell: ondersteuning voor doorgeven van arguments toevoegen
* Om crash heen werken met GL detectie en kwin_wayland
* [KQuickAddons] QtQuickSettings::checkBackend() voor terugvallen naar software backend (bug 346519)
* [abstractkcm] versie in voorbeelden van code importeren repareren
* Instelling van QSG_RENDER_LOOP vermijden indien al ingesteld
* ConfigPropertyMap : standaard waarde van eigenschap in de map laden

### KDocTools

* Een entity voor MathML-acroniem toevoegen
* 'Naval Battle' naar 'KNavalBattle' wijzigen om juridische problemen te vermijden

### KGlobalAccel

* Automatisch starten van kglobalaccel vermijden bij afsluiten (bug 429415)

### KHolidays

* Japanse vakantiedagen bijgewerkt

### KIconThemes

* Waarschuwing voor enige Adwaita pictogrammen voor achterwaartse compatibiliteit
* QSvgRenderer::setAspectRatioMode() is geïntroduceerd in Qt 5.15

### KImageFormats

* AVIF toevoegen aan de lijst met ondersteunde formaten
* Plug-in toevoegen voor AV1 afbeeldingsbestandsformaat (AVIF)

### KIO

* [KFileItemDelegate] geen ruimte verspillen voor niet-bestaande pictogrammen in kolommen anders dan de eerste
* KFilePlacesView, KDirOperator: naar asynchroon askUserDelete() overzetten
* De manier waarop CopyJob de JobUiDelegate extensies zoekt opnieuw bewerken
* AskUserActionInterface introduceren, een asynchrone API voor hernoemen/overslaan-dialogen
* RenameDialog: compareFiles() alleen aanroepen op bestanden
* kcm/webshortcuts: Resetknop repareren
* KUrlNavigatorMenu: behandeling van middenklik repareren
* knetattach-item verwijderen uit de remote:// weergave van ioslave (bug 430211)
* CopyJob: naar AskUserActionInterface overzetten
* Jobs: signaal niet-overladen van "mimeTypeFound" toevoegen aan "mimetype" afkeuren
* RenameDialog: ontbrekende nullptr initialisatie toevoegen (bug 430374)
* KShortUriFilter: geen tekenreeksen "../" and co. filteren
* Niet toekennen als aan KIO::rawErrorDetail() een URL is gegeven zonder schema (bug 393496)
* KFileItemActions: voorwaarde, we willen alleen mappen op afstand uitsluiten, repareren (bug 430293)
* KUrlNavigator: gebruik van kurisearchfilter verwijderen
* KUrlNavigator: volledig maken van relatieve paden laten werken (bug 319700)
* KUrlNavigator: relatieve paden van mappen oplossen (bug 319700)
* Waarschuwingen het zwijgen opleggen vanwege problemen met samba-configuratie wanneer samba niet expliciet wordt gebruikt
* KFileWidget: toestaan dat bestanden die met een ':' beginnen worden geselecteerd (bug 322837)
* [KFileWidget] positie van bladwijzerknop in werkbalk gerepareerd
* KDirOperator: mkdir(const QString &, bool) afkeuren
* KFilePlacesView: instelling van een statische pictogramgrootte toestaan (bug 182089)
* KFileItemActions: nieuwe methode toevoegen om acties open-met in te voegen (bug 423765)

### Kirigami

* [controls/SwipeListItem]: standaard altijd acties op bureaublad tonen
* [overlaysheet] meer voorwaardelijke positionering voor sluitknop gebruiken (bug 430581)
* [controls/avatar]: interne AvatarPrivate als publiek NameUtils API publiceren
* [controls/avatar]: gegenereerde kleur tonen
* Hero-component toevoegen
* [controls/Card]: animatie van er boven zweven verwijderen
* [controls/ListItem]: animatie er boven zweven verwijderen
* ListItems verplaatsen om veryShortDuration gebruiken voor er boven zweven in plaats van longDuration
* [controls/Units]: veryShortDuration toevoegen
* Kleuring van ActionButton-pictogram
* pictogrampixmaps alleen zo groot als nodig gebruiken
* [controls/avatar]: beter standaard uiterlijk
* [controls/avatar]: visuele bugs repareren
* Component CheckableListItem aanmaken
* [controls/avatar]: rand schalen volgens grootte van avatar
* "[Avatar] achtergrondverloop wijzigen" terugdraaien
* "[Avatar] randbreedte wijzigen naar 1px om overeen te laten komen met andere randbreedten" terugdraaien
* [controls/avatar]: Avatar toegangsvriendelijk maken
* [controls/avatar]: terugvallen bij opvullen van pictogram verhogen
* [controls/avatar]: modus van afbeelding sourceSize laten zetten
* [controls/avatar]: Afmetingen van tekst aanpassen
* [controls/avatar]: technieken gebruikt voor circulaire vorm aanpassen
* [controls/avatar]: primaire/secondaire actie toevoegen aan Avatar
* Opvullen van item in kop van hard gecodeerde OverlaySheet
* qmake build: ontbrekende sizegroup source/header toevoegen
* Pictogrammen kleur geven, geen knoppen (bug 429972)
* Knoppen in kop voor terug en vooruit die geen breedte hebben repareren
* [BannerImage]: niet verticaal gecentreerde koptitel met niet-plasma thema's repareren

### KItemModels

* Eigenschap aantal toevoegen, die binding van rowCount in QML toestaat

### KItemViews

* KWidgetItemDelegate toestaan om een resetModel uit KPluginSelector te starten

### KNewStuff

* Methoden standardAction en standardActionUpload afkeuren
* QtQuick model repareren als er alleen iets te laden is, maar geen downloadkoppeling
* Een dptr toevoegen aan Cache en de afknijp-timer daar verplaatsen om crash te repareren (bug 429442)
* KNS3::Button opschonen om nieuwe dialoog intern te gebruiken
* Wrapperklasse aanmaken voor QML dialoog
* Controleren of versie leeg is alvorens versie achter te voegen

### KNotification

* KNotification API documenten verbeteren

### KParts

* BrowserHostExtension afkeuren

### KQuickCharts

* Een aangepaste macro gebruiken voor bericht over afkeuren in QML
* ECMGenerateExportHeader gebruiken voor afkeurmacro's en hun gebruik
* Interval wijzigen heeft het wissen van de geschiedenis niet nodig
* Doorgaande lijngrafiek wijzigen naar geschiedenis van proxy-bronvoorbeeld
* Model/ValueHistorySource afkeuren
* HistoryProxySource introduceren als een vervanging voor Model/ValueHistorySource
* Log-categorieën toevoegen voor grafieken en gebruik ze voor bestaande waarschuwingen

### KRunner

* [DBus Runner] ondersteuning voor aangepaste pixmap-pictogrammen voor resultaten toevoegen
* Sleutel toevoegen om te controleren of de configuratie gemigreerd is
* Configuratie- en gegevensbestanden scheiden
* Nieuwe API om overeenkomsten uit te voeren en voor geschiedenis
* RunnerContextTest niet bouwen op Windows

### KService

* KSycoca: database opnieuw bouwen vermijden als XDG_CONFIG_DIRS duplicaten bevat
* KSycoca: verzekeren dat extrabestanden geordend zijn voor vergelijken (bug 429593)

### KTextEditor

* "Variable:" hernoemen tot "Document:Variable:"
* Expansie van variabele: zoeken van prefix overeenkomsten met meerdere dubbele punten repareren
* Tekenen verplaatsen van KateTextPreview in KateRenderer
* Ga na dat alleen lijnen in het zicht gebruikt worden om de pixmap te tekenen
* KateTextPreview gebruiken om de pixmap te renderen
* Uitvouwen van variabele: Ondersteuning voor %{Document:Variable:<name>} toevoegen
* De versleepte tekst tonen tijdens slepen (bug 398719)
* Losmaken in TextRange::fixLookup() repareren
* currentLine bg niet tekenen als er een overlappende selectie is
* KateRegExpSearch: logica repareren bij toevoegen van '\n' tussen reeksregels
* actie naar 'Omwisselen met inhoud van klembord' hernoemen
* een actie toevoegen om kopiëren & plakken als één actie te starten
* feat: actiepictogram regelafbreking van tekst voor dynamische regelafbreking toevoegen
* Inspringen in een stap ongedaan maken (bug 373009)

### KWidgetsAddons

* KSelectAction: non-overload signalen indexTriggered & textTriggered toevoegen
* KFontChooserDialog: dialoog die door ouder is verwijderd tijdens exec() behandelen
* KMessageDialog: setFocus() aanroepen op de standaard knop
* Overzetten van QLocale::Norwegian naar QLocale::NorwegianBokmal
* KToolBarPopupActionTest naar QToolButton::ToolButtonPopupMode overzetten

### KXMLGUI

* KXmlGui: bij opwaardering een lokaal .rc bestand, nieuwe toepassing werkbalken behouden
* Opnemen van sleutel door setWindow voordat opnemen begint repareren (bug 430388)
* Ongebruikte KWindowSystem afhankelijkheid verwijderen
* KXMLGUIClient in memory xml doc wissen na opslaan van sneltoetsen naar schijf

### Oxygen-pictogrammen

* Omhoog-indicator toevoegen

### Plasma Framework

* Foutinformatie aan fouten-plasmoid voorleggen op een meer gestructureerde manier
* [components] Mnemonics aanhaken
* [svg] Altijd SvgRectsCache-timer starten uit de juiste thread
* [PC3 ProgressBar] binding zetten voor breedte (bug 430544)
* Windows bouwen + inversie van variabelen repareren
* [PlasmaComponents/TabGroup] controle repareren als item uit pagina erft
* Verschillende componenten overzetten naar veryShortDuration bij er boven zweven
* ListItems verplaatsen om veryShortDuration gebruiken voor er boven zweven in plaats van longDuration
* veryShortDuration toevoegen
* Geen negatieve kalenderjaren toestaan (bug 430320)
* Gebroken achtergrond repareren (bug 430390)
* QString cache-ID's vervangen door een op struct gebaseerde versie
* [TabGroup] animaties omdraaien in modus RnL
* Alleen sneltoetsen verwijderen bij verwijderen van applet niet bij destructie
* Uitgeschakelde contextuele acties uit ExpandableListItem verbergen

### Omschrijving

* KFileItemActions: menu windowflag toevoegen
* fileitemplugin delen: ouder-widget als menu-ouder gebruiken (bug 425997)

### QQC2StyleBridge

* org.kde.desktop/Dialog.qml bijwerken
* ScrollView tekenen met Frame in plaats van Edit (bug 429601)

### Sonnet

* Prestatie verbeteren van createOrderedModel bij gebruik van QVector
* Waarschuwing vermijden bij actief als geen geraden resultaten bestaan

### Accentuering van syntaxis

* C++ accentuering: QOverload en co
* Labels die beginnen met punt die niet in GAS geaccentueerd worden repareren
* C++ accentuering: qGuiApp-macro toevoegen
* Dracula-thema verbeteren
* #5 repareren: bash, zsh: ! met if, while, until ; bash: patroonstijl voor ${var,patt} en ${var^patt}
* #5: bash, zsh repareren: commentaar binnen array
* Syntaxis van komkommerfunctie
* Zsh: syntaxisversie verhogen
* Zsh: expanderen van accolade in een commando
* weakDeliminator en additionalDeliminator met sleutelwoord, WordDetect, Int, Float, HlCOct en HlCHex toevoegen
* Indexer: currentKeywords en currentContext resetten bij openen van een nieuwe definitie
* Zsh: veel reparaties en verbeteringen
* Bash: commentaar repareren in geval, opvolgende expressie en ) na ]
* Importeren van kleur in basis juist behandelen and specialiseren voor c/c++
* Monokai-thema bijwerken
* De juistheid/aanwezigheid van aangepaste stijlen in thema's verifiëren
* GitHub donker/licht thema's toevoegen
* versie verhogen, versie van kate niet wijzigen, totdat ik begrijp waarom het nodig is
* Licenties toevoegen
* Atom One donker/licht thema's toevoegen
* ontbrekend verhogen van versie bij wijziging
* monokai-attribuut & kleur van operator repareren
* Monokai-thema toevoegen
* CMake: ontbrekende 3.19 variabelen en enige nieuwe toegevoegd in 3.19.2
* Kotlin: enige problemen en andere verbeteringen repareren
* Groovy: enige problemen en andere verbeteringen repareren
* Scala: enige problemen en andere verbeteringen repareren
* Java: enige problemen en andere verbeteringen repareren
* && en || in een subcontext repareren en functienaampatroon repareren
* QRegularExpression::DontCaptureOption toevoegen wanneer er geen dynamische regel is
* Bash: toevoegen (...), ||, && in [[ ... ]] ; toevoegen backquote in [ ... ] en [[ ... ]]

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
