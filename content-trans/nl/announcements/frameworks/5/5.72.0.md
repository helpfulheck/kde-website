---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV : nieuwe module

Protocolimplementatie van DAV met KJobs.

Agenda's en te-doens worden ondersteund met gebruik van ofwel GroupDAV of CalDAVe en contactpersonen worden ondersteund met gebruik van GroupDAV of CardDAV.

### Baloo

+ [Indexers] Op naam gebaseerde MIME-typen negeren voor initiële beslissingen over indexering (bug 422085)

### BluezQt

+ De gegevens voor adverteren van services van een apparaat blootstellen

### Breeze pictogrammen

+ Drie 16px pictogrammen gerepareerd voor application-x-... om meer pixel-perfect te zijn (bug 423727)
+ Ondersteuning toevoegen voor &gt;200% schaal voor pictogrammen die monochrome zouden moeten blijven
+ Centrum van window-close-symbolic uitknippen
+ cervisia toepassings- en actiepictogram bijgewerkt
+ README-sectie over het webfont toevoegen
+ grunt-webfonts gebruiken in plaats van grunt-webfont en schakel generatie voor symbolisch gekoppelde pictogrammen uit
+ kup-pictogram toevoegen uit kup-opslagruimte

### Extra CMake-modules

+ Ondersteuning voor png2ico verwijderen
+ Met CMake-code van Qt omgaan die CMAKE_SHARED_LIBRARY_SUFFIX wijzigt
+ FindTaglib zoekmodule toevoegen

### KDE Doxygen hulpmiddelen

+ "repo_id" in metainfo.yaml ondersteunen om geraden repo-naam te overschrijven

### KCalendarCore

+ Op schrijffout controleren in save() als de schijf vol is (bug 370708)
+ Pictogramnamen voor herhaalde te-doens
+ Serieel maken van startdatums van terugkerende te-doens repareren (bug 345565)

### KCMUtils

+ Gewijzigd signaal voor selector van plug-in repareren

### KCodecs

+ Enige n invoegen anders kan tekst erg groot worden in berichtenvak

### KConfig

+ Ook locationType aan KConfigSkeleton doorgeven bij constructie uit groep
+ Tekst "Taal van toepassing omschakelen..." consistenter maken

### KConfigWidgets

+ Tekst "Taal van toepassing omschakelen..." consistenter maken

### KCoreAddons

+ KRandom::random -&gt; QRandomGenerator::global()
+ KRandom::random afkeuren

### KCrash

+ Ontbrekende declaratie van omgeving toevoegen, anders alleen beschikbaar op GNU

### KDocTools

+ Consistente stijl voor FDL notitie gebruiken (bug 423211)

### KFileMetaData

+ kfilemetadata aanpassen aan "audio/x-speex+ogg" als recent gewijzigd in shared-mime-info

### KI18n

+ Ook accenten toevoegen rond opgemaakte tekst met van tag &lt;filename&gt; voorziene tekst

### KIconThemes

+ Toekenning in resetPalette laten vallen
+ QPalette() teruggeven als we geen aangepast palet hebben
+ QIcon::fallbackSearchpaths() respecteren (bug 405522)

### KIO

+ [kcookiejar] cookie-instelling "Accepteren voor sessie" lezen repareren (bug 386325)
+ KDirModel: regressie in hasChildren() repareren voor boomstructuur met verborgen bestanden, symbolische koppelingen, pipes en karakterapparaten (bug 419434)
+ OpenUrlJob: ondersteuning voor shellscripts met een spatie in de bestandsnaam repareren (bug 423645)
+ Websneltoets voor DeepL en ArchWiki toevoegen
+ [KFilePlacesModel] AFC (Apple File Conduit) apparaten tonen
+ Ook spatietekens coderen voor URL's van websneltoetsen (bug 423255)
+ [KDirOperator] standaard dirHighlighting echt inschakelen
+ [KDirOperator] de vorige map accentueren bij terug/omhoog gaan (bug 422748)
+ [Trash] ENOENT fout behandelen bij hernoemen van bestanden (bug 419069)
+ Ioslave van bestand: nanoseconde tijdstempel instellen bij kopiëren (bug 411537)
+ URL's aanpassen voor Qwant zoekleveranciers (bug 423156)
+ File ioslave: ondersteuning voor reflink kopiëren toevoegen (bug 326880)
+ Instelling van standaard sneltoets in websneltoetsen van KCM repareren (bug 423154)
+ Leesbaarheid van websneltoetsen van KCM verzekeren door een minimum breedte in te stellen (bug 423153)
+ FileSystemFreeSpaceJob: fout uitzenden als de kioslave de metagegevens niet leverde
+ [Trash] trashinfo-bestanden verwijzend naar bestanden/mappen die niet bestaan verwijderen (bug 422189)
+ kio_http: het mime-type zelf raden als de server application/octet-stream terug geeft
+ Signalen totalFiles en totalDirs afkeuren, niet uitgezonden
+ Opnieuw laden van nieuwe websneltoetsen repareren (bug 391243)
+ kio_http: status van hernoemen met overschrijven ingeschakeld repareren
+ Naam niet als verborgen bestand of bestandspad interpreteren (bug 407944)
+ Verwijderde/verborgen websneltoetsen niet tonen (bug 412774)
+ Crash repareren bij verwijderen van item (bug 412774)
+ Niet toestaan om bestaande sneltoetsen toe te kennen
+ [kdiroperator] betere tekstballonnen voor achter- en voorwaartse acties gebruiken (bug 422753)
+ [BatchRenameJob] KJob::Items gebruiken bij rapporteren van de voortgangsinformatie (bug 422098)

### Kirigami

+ [aboutpage] OverlaySheet voor licentietekst gebruiken
+ pictogrammen voor ingevouwen modus repareren
+ Lichte scheidingstekens gebruiken voor DefaultListItemBackground
+ Eigenschap gewicht toevoegen aan scheidingsteken
+ [overlaysheet] fractionele hoogte voor contentLayout vermijden (bug 422965)
+ Documenten van pageStack.layers repareren
+ Laagondersteuning opnieuw introduceren aan Page.isCurrentPage
+ pictogramkleur ondersteunen
+ de interne component MnemonicLabel gebruiken
+ Globale werkbalk links opvullen verminderen wanneer titel niet wordt gebruikt
+ implicit{Width,Height} alleen instellen voor Separator
+ KF5Kirigami2Macros.cmake bijwerken om https te gebruiken met git-repo om fout te vermijden bij poging tot toegang
+ Reparatie: laden van avatar
+ PageRouterAttached#router SCHRIJFbaar maken
+ OverlaySheet sluiting repareren bij klikken binnen indeling (bug 421848)
+ Ontbrekende id toevoegen aan GlobalDrawerActionItem in GlobalDrawer
+ OverlaySheet die te slank is, repareren
+ Voorbeeld van PlaceholderMessage code repareren
+ rand renderen onderaan groepen
+ Een LSH-component gebruiken
+ Achtergrond aan headers toevoegen
+ Betere behandeling van inklappen
+ Betere presentatie list-header-items
+ Eigenschap toevoegen aan BasicListItem
+ Kirigami.Units.devicePixelRatio=1.3 repareren wanneer het 1.0 zou moeten zijn bij 96dpi
+ OverlayDrawer-hendel verbergen bij niet interactief
+ ActionToolbarLayoutDetails berekeningen aanpassen om beter gebruik van beschikbare schermruimte te maken
+ ContextDrawerActionItem: teksteigenschap voorkeur geven boven tekstballon

### KJobWidgets

+ De KJob::Unit::Items integreren (bug 422098)

### KJS

+ Crash bij KJSContext::interpreter gebruik repareren

### KNewStuff

+ Tekst met uitleg uit sheetkop verplaatsen naar lijstkop
+ Paden voor installatiescript en uitpakken repareren
+ ShadowedRectangle verbergen voor niet geladen voorbeelden (bug 422048)
+ Overflow van inhoud niet toestaan in de rastergedelegeerden (bug 422476)

### KNotification

+ notifybysnore.h niet gebruiken op MSYS2

### KParts

+ PartSelectEvent en co afkeuren

### KQuickCharts

+ Waarde van Label van LegendDelegate verkorten wanneer er niet genoeg breedte is
+ Reparatie voor fout "C1059: niet constante expressie ..."
+ Rekening houden met lijnbreedte bij controle van begrenzing
+ Geen fwidth gebruiken bij renderen van lijnen in lijngrafieken
+ removeValueSource herschrijven zodat het geen vernietigde QObjects gebruikt
+ insertValueSource in Chart::appendSource gebruiken
+ ModelHistorySource::{m_row,m_maximumHistory} op de juiste manier initialiseren

### KRunner

+ RunnerContextTest repareren om niet aanwezigheid van .bashrc aan te nemen
+ Ingebedde JSON metagegevens voor binaire plug-ins &amp; aangepast voor D-Bus plug-ins
+ queryFinished uitzenden wanneer alle jobs voor de huidige afvraging zijn beëindigd (bug 422579)

### KTextEditor

+ "goto line" achterwaarts laten werken (bug 411165)
+ crash bij verwijdering van weergave als reeksen nog steeds levend zijn (bug 422546)

### KWallet Framework

+ Drie nieuwe methoden introduceren die alle "entries" in een map teruggeven introduceren

### KWidgetsAddons

+ KTimeComboBox voor locale tekst met ongebruikelijke tekens in de formaten repareren (bug 420468)
+ KPageView: onzichtbare pixmap aan de rechterkant van de kop verwijderen
+ KTitleWidget: uit eigenschap QPixmap naar eigenschap QIcon verplaatsen
+ Enige KMultiTabBarButton/KMultiTabBarTab API die QPixmap gebruikt afkeuren
+ KMultiTabBarTab: nog meer styleoption statuslogica QToolButton laten volgen
+ KMultiTabBar: geactiveerde knoppen in QStyle::State_Sunken niet tonen
+ [KCharSelect] focus als eerste aan het regelbewerking zoeken geven

### KWindowSystem

+ [xcb] juist geschaalde pictogramgeometrie verzenden (bug 407458)

### KXMLGUI

+ kcm_users gebruiken in plaats van user_manager
+ Nieuwe KTitleWidget::icon/iconSize API gebruiken
+ "Omschakelen naar taal van toepassing" verplaatsen naar menu Instellingen (bug 177856)

### Plasma Framework

+ Meer heldere waarschuwing tonen als de vereiste KCM niet is te vinden
+ [spinbox] Geen QQC2 items gebruiken wanneer we PlasmaComponents zouden moeten gebruiken (bug 423445)
+ Plasma componenten: verloren kleurcontrole herstellen van TabButton-label
+ PlaceholderMessage introduceren
+ [calendar] grootte van maandlabel
+ [ExpandableListItem] zelfde logica voor actie en zichtbaarheid van pijlknop gebruiken
+ [Dialog Shadows] Naar KWindowSystem shadows API overzetten (bug 416640)
+ Symbolische koppelingen van widgets/plasmoidheading.svgz in breeze light/dark
+ Kirigami.Units.devicePixelRatio=1.3 repareren wanneer het 1.0 zou moeten zijn bij 96dpi
+ Eigenschap toevoegen voor toegang tot het item van de ExpandableListItem lader

### Prison

+ AbstractBarcode::minimumSize() afkeuren ook voor de compiler

### Omschrijving

+ Expliciet Kirigami eenheden gebruiken in plaats van impliciet Plasma eenheden gebruiken
+ Jobdialogen overzetten naar Kirigami.PlaceholderMessage

### QQC2StyleBridge

+ selectByMouse op true zetten voor Draaischakelaar
+ Menupictogrammen die vervaagd worden bij sommige lettertypegrootte repareren (bug 423236)
+ Afgeleiden en schuifbalk overlapping in keuzelijst popups voorkomen
+ Schuifregelaar verticaal implicitWidth en een binding loop repareren
+ Tekstballonnen consistenter maken met Breeze-widgetstijl tekstballonnen
+ Te-bewerken op standaard op true zetten voor Draaischakelaar
+ Waarschuwingen over verbindingen repareren in Menu.qml

### Solid

+ UDisks2-backend inschakelen zonder voorwaarde op FreeBSD inschakelen

### Sonnet

+ "QCharRef gebruiken met een index wijzend buiten de geldige reeks van een QString" repareren
+ Standaard automatische detectiegedrag herstellen
+ Standaard taal repareren (bug 398245)

### Syndication

+ Een paar BSD-2-Clause license headers repareren

### Accentuering van syntaxis

+ CMake: Elementen voor bijwerken voor CMake 3.18
+ Snort/Suricata taalaccentuering toevoegen
+ YARA-taal toevoegen
+ afgekeurde binaire json verwijderen in plaats van cbor
+ JavaScript/TypeScript: tags in sjablonen accentueren

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
