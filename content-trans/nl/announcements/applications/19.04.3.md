---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE stelt Applicaties 19.04.3 beschikbaar.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE stelt KDE Applicaties 19.04.3 beschikbaar
version: 19.04.3
---
{{% i18n_date %}}

Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../19.04.0'>KDE Applicaties 19.04</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 60 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello, naast andere.

Verbeteringen bevatten:

- Konqueror en Kontact veroorzaken geen crash bij afsluiten met QtWebEngine 5.13
- Groepen met composities knippen veroorzaken niet langer een crash bij de Kdenlive videobewerker
- Het Python importprogramma in Umbrello UML designer behandelt nu parameters met standaard argumenten
