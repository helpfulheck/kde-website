---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE stelt KDE Applicaties 18.08.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 18.08.3 beschikbaar
version: 18.08.3
---
9 november 2017. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../18.08.0'>KDE Applicaties 17.08</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Ark, Dolphin, KDE Games, Okular, Umbrello, naast andere.

Verbeteringen bevatten:

- Weergavemodus HTML in KMail wordt herinnerd en laad externe afbeeldingen opnieuw indien toegestaan
- Kate herinnert nu meta-informatie (inclusief bladwijzers) tussen bewerkingssessies
- Automatisch schuiven in de tekst-UI van Telepathy is gerepareerd met nieuwere versies van QtWebEngine
