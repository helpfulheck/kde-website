---
aliases:
- ../announce-applications-17.04.0
changelog: true
date: 2017-04-20
description: KDE stelt KDE Applicaties 17.04.0 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.04.0 beschikbaar
version: 17.04.0
---
20 april 2017. KDE Applications 17.04 is hier. Over het geheel hebben we er voor gezorgd dat zowel de toepassingen als de onderliggende bibliotheken stabieler zijn en gemakkelijker te gebruiken. Door plooien glad te strijken en te luisteren naar uw terugkoppeling, hebben we de suite KDE Applications minder gevoelig voor uitglijders gemaakt en veel vriendelijker.

Heb plezier met uw nieuwe toepassingen!

#### <a href="https://edu.kde.org/kalgebra/">KAlgebra</a>

{{<figure src="/announcements/applications/17.04.0/kalgebra1704.jpg" width="600px" >}}

De ontwikkelaars van KAlgebra zijn op hun eigen specifieke weg naar convergentie, na het overbrengen van de mobiele versie van het beknopte educatieve programma naar Kirigami 2.0 -- het framework met voorkeur voor integreren van KDE toepassingen op bureaublad en het mobiele platforms.

Verder heeft de versie voor het bureaublad ook de 3D back-end gemigreerd naar GLES, de software die het programma toestaat om 3D-functies te renderen zowel op het bureaublad als op mobiele apparaten. Dit maakt de code eenvoudiger en gemakkelijker te onderhouden.

#### <a href="http://kdenlive.org/">Kdenlive</a>

{{<figure src="/announcements/applications/17.04.0/kdenlive1704.png" width="600px" >}}

De videobewerker van KDE wordt stabieler en krijgt meer functies met elke nieuwe versie. Dit keer hebben de ontwikkelaars de dialoog voor de selectie van het profiel opnieuw ontworpen om het gemakkelijker te maken de schermgrootte, framesnelheid en andere parameters van uw film in te stellen.

U kunt nu ook uw video direct afspelen vanaf de melding dat rendering gereed is. Enige crashes die gebeurden bij verplaatsen van clips op de tijdlijn zijn gecorrigeerd en de DVD assistent is verbeterd.

#### <a href="https://userbase.kde.org/Dolphin">Dolphin</a>

{{<figure src="/announcements/applications/17.04.0/dolphin1704.png" width="600px" >}}

Onze bestandsbeheerder met voorkeur en portaal naar alles (behalve misschien de onderwereld) heeft verschillende wijzigingen ondergaan en verbeteringen voor gebruik om het nog krachtiger te maken.

De contextmenu's in het paneel <i>Plaatsen</i> (standaard aan de linkerkant van het hoofdweergavegebeid) zijn opgeschoond en het is nu mogelijk om samen te werken met de metagegevenswidgets in de tekstballonnen. De tekstballonnen, tussen haakjes, werken nu ook op Wayland.

#### <a href="https://www.kde.org/applications/utilities/ark/">Ark</a>

{{<figure src="/announcements/applications/17.04.0/ark1704.png" width="600px" >}}

De populaire grafische toepassing voor maken, uitpakken en beheren van gecomprimeerde archieven voor bestanden en mappen komt nu met een functie <i>Zoeken</i> om u te helpen bestanden te vinden in grote archieven.

Het stelt u ook in staat om plug-ins direct in en uit te schakelen vanuit de dialoog <i>Configureren</i>. Praten over plug-ins, de nieuwe Libzip plug-in verbetert ondersteuning voor Zip archiveren.

#### <a href="https://minuet.kde.org/">Minuet</a>

{{<figure src="/announcements/applications/17.04.0/minuet1704.png" width="600px" >}}

Als u spelen van muziek onderwijst of leert, dan zou u Minuet eens moeten bekijken. De nieuwe versie biedt meer oefeningen voor toonladders en oortraining voor bebop, harmonisch mineur/majeur, pentatonisch en symmetrische toonladders.

U kunt ook testen maken en ondergaan met de new <i>Testmodus</i> voor het doen van oefeningen. U kunt uw voortgang minitoren door een reeks van 10 oefeningen en u krijgt statistieken van uw resultaten bij beëindigen.

### En meer!

<a href='https://okular.kde.org/'>Okular</a>, de viewer van documenten van KDE, heeft minstens een half dozijn wijzigingen ondergaan die functies toevoegen en zijn bruikbaarheid opschroeven op aanraakschermen. <a href='https://userbase.kde.org/Akonadi'>Akonadi</a> en verschillende andere toepassingen die onderdeel zijn van <a href='https://www.kde.org/applications/office/kontact/'>Kontact</a> (de suite voor e-mail/agenda/groupware van KDE) is herzien, gerepareerd en geoptimaliseerd om u meer productief te maken.

<a href='https://www.kde.org/applications/games/kajongg'>Kajongg</a>, <a href='https://www.kde.org/applications/development/kcachegrind/'>KCachegrind</a> en meer (<a href='https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based'>Uitgavenotities</a>) zijn nu overgezet naar KDE Frameworks 5. We zien uit naar uw terugkoppeling en inzicht in de nieuwste mogelijkheden die met deze uitgave zijn geïntroduceerd.

<a href='https://userbase.kde.org/K3b'>K3b</a> doet mee met de uitgave van KDE Applications.

### Op bugs jagen

Meer dan 95 bugs zijn opgelost in toepassingen inclusief de Kopete, KWalletManager, Marble, Spectacle en meer!

### Volledige log met wijzigingen
