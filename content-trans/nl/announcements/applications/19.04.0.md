---
aliases:
- ../announce-applications-19.04.0
changelog: true
date: 2019-04-18
description: KDE stelt KDE Applicaties 19.04 beschikbaar.
layout: application
release: applications-19.04.0
title: KDE stelt KDE Applicaties 19.04.0 beschikbaar
version: 19.04.0
version_number: 19.04.0
version_text: '19.04'
---
{{%youtube id="1keUASEvIvE"%}}

{{% i18n_date %}}

De KDE-gemeenschap is verheugd de uitgave van KDE Applications 19.04 aan te kondigen.

Onze gemeenschap werkt continue aan het verbeteren van de software in onze KDE Application serie. Samen met nieuwe functies verbeteren we het ontwerp, bruikbaarheid en stabiliteit van al onze hulpmiddelen, spellen en hulpmiddelen voor creativiteit. Ons doel is om uw leven gemakkelijker te maken door KDE software met meer plezier te gebruiken. We hopen dat u van alle nieuwe verbeteringen en reparaties van bugs, die u in 19.04 vindt, veel plezier heeft!

## Wat is er nieuw in KDE Applicaties 19.04

Meer dan 150 bugs zijn opgelost. Deze reparaties implementeren opnieuw uitgeschakelde functies, normaliseren sneltoetsen en lossen crashes op, waarmee KDE Applications vriendelijker wordt en u productiever wordt.

### Bestandsbeheer

{{<figure src="/announcements/applications/19.04.0/app1904_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a> is de bestandsbeheerder van KDE. Het maakt ook verbinding met netwerkservices, zoals SSH, FTP en Samba servers, en komt met geavanceerde hulpmiddelen om uw gegevens te vinden en te organiseren.

Nieuwe mogelijkheden:

+ We hebben ondersteuning voor miniaturen uitgebreid, Dolphin kan dus nu miniaturen voor verschillende nieuwe bestandstypen tonen: <a href='https://phabricator.kde.org/D18768'>Microsoft Office</a> bestanden, <a href='https://phabricator.kde.org/D18738'>.epub en .fb2 eBook</a> bestanden, <a href='https://bugs.kde.org/show_bug.cgi?id=246050'>Blender</a> bestanden en <a href='https://phabricator.kde.org/D19679'>PCX</a> bestanden. bovendien wordt voor tekstbestanden nu <a href='https://phabricator.kde.org/D19432'>syntaxisaccentuering</a> voor de tekst in de miniatuur getoond. </li>
+ U kunt nu kiezen <a href='https://bugs.kde.org/show_bug.cgi?id=312834'>welk gesplitst weergavepaneel te sluiten</a> bij klikken op de knop 'Splitsing sluiten'. </li>
+ Deze versie van Dolphin introduceert <a href='https://bugs.kde.org/show_bug.cgi?id=403690'>slimmer plaatsen van tabbladen</a>. Wanneer u een map in een nieuw tabblad opent zal het nieuwe tabblad nu geplaatst worden onmiddellijk rechts van de huidige, in plaats van altijd aan het eind van de tabbladbalk. </li>
+ <a href='https://phabricator.kde.org/D16872'>Items taggen</a> is nu veel praktischer, omdat tags toegevoegd of verwijderd kunnen worden met het contextmenu. </li>
+ We hebben parameters van de <a href='https://phabricator.kde.org/D18697'>de standaard sortering verbeterd</a> voor enige algemeen gebruikte mappen. Standaard wordt de map Downloads nu gesorteerd op datum met groepering ingeschakeld en weergave van de Recente documenten (toegankelijk door te navigeren naar recentdocuments:/) wordt op datum gesorteerd met een lijstweergave geselecteerd. </li>

Reparaties omvatten:

+ Wanneer een moderne versie van het SMB-protocol wordt gebruikt, kunt u nu <a href='https://phabricator.kde.org/D16299'>Samba-shares ontdekken</a> voor Mac- en Linux-machines. </li>
+ <a href='https://bugs.kde.org/show_bug.cgi?id=399430'>Opnieuw arrangeren van items in het paneel Plaatsen</a> werkt opnieuw op de juiste manier wanneer sommige items verborgen zijn. </li>
+ Na het openen van een nieuw tabblad in Dolphin krijgt dat nieuwe tabblad nu automatisch <a href='https://bugs.kde.org/show_bug.cgi?id=401899'>toetsenbordfocus</a>. </li>
+ Dolphin waarschuwt u nu als u probeert af te sluiten terwijl het <a href='https://bugs.kde.org/show_bug.cgi?id=304816'>terminalpaneel open is</a> met een actief programma erin. </li>
+ We hebben vele geheugenlekken gerapporteerd, met verbetering van algehele prestaties.</li>

De <a href='https://cgit.kde.org/audiocd-kio.git/'>AudioCD-KIO</a> biedt andere KDE-toepassingen om audio van cd's te lezen en automatisch te converteren naar andere formaten.

+ De AudioCD-KIO ondersteunt nu rippen naar <a href='https://bugs.kde.org/show_bug.cgi?id=313768'>Opus</a>. </li>
+ We hebben <a href='https://bugs.kde.org/show_bug.cgi?id=400849'>Cd infotekst</a> echt transparent gemaakt voor weergave. </li>

### Video bewerken

{{<figure src="/announcements/applications/19.04.0/app1904_kdenlive.png" width="600px" >}}

Dit is een mijlpaalversie voor de videobewerker van KDE. De kerncode van <a href='https://kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> is intensief herschreven omdat meer dan 60%% van de interne code is gewijzigd, waarmee de gehele architectuur is verbeterd.

Verbeteringen bevatten:

+ De tijdlijn is herschreven om gebruik te maken van QML.
+ Wanneer u een clip op de tijdlijn zet, gaan geluid en video altijd naar aparte tracks.
+ De tijdlijn ondersteunt nu navigatie met het toetsenbord: clips, composities en sleutelframes kunnen verplaatst worden met het toetsenbord. De hoogte van de tracks zelf zijn ook aan te passen.
+ In deze versie van Kdenlive komt de in-track geluidsopname met een nieuwe functie <a href='https://bugs.kde.org/show_bug.cgi?id=367676'>voice-over</a>.
+ Kopiëren/plakken is verbeterd: het werkt tussen verschillende projectvensters. Het beheer van proxyclip is ook verbeterd, omdat clips nu individueel verwijderd kunnen worden.
+ Version 19.04 ziet de terugkeer van ondersteuning voor externe BlackMagic monitorschermen en er zijn ook nieuwe on-monitor gidsen voor voorinstellingen.
+ We hebben de behandeling van sleutelframes verbeterd, die een consistenter uiterlijk en werkmethode biedt. De titelmaker is ook verbeterd door de uitlijnknoppen te laten klikken aan veilige zones, met toevoegen van te configureren hulplijnen en achtergrondkleuren en ontbrekende items te tonen.
+ We hebben de corruptiebug van de tijdlijn gerepareerd die clips verkeerd plaatste of liet verdwijnen wat gebeurde wanneer u een groep clips verplaatste.
+ We hebben de JPG afbeeldingsbug gerepareerd die afbeeldingen weergaf als witte schermen op Windows. We hebben ook bugs gerepareerd die schermopname op Windows veroorzaakte.
+ Naast al het bovenstaande hebben we vele kleine verbeteringen toegevoegd die het gebruik van Kdenlive gemakkelijker en soepeler maken.

### Kantoor

{{<figure src="/announcements/applications/19.04.0/app1904_okular.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/okular/'>Okular</a> is de documentviewer van KDE met vele mogelijkheden. Ideaal voor het lezen en annoteren van PDF's, het kan ook open ODF-bestanden openen (zoals gebruikt door LibreOffice en OpenOffice), ebooks gepubliceerd als ePub-bestanden, de meest algemene stripboekbestanden, PostScript-bestanden en nog veel meer

Verbeteringen bevatten:

+ Om te verzekeren dat uw documenten altijd passen hebben we <a href='https://bugs.kde.org/show_bug.cgi?id=348172'>schalingopties</a> toegevoegd aan de afdrukdialoog van Okular.
+ Okular ondersteunt nu bekijken en verifiëren van <a href='https://cgit.kde.org/okular.git/commit/?id=a234a902dcfff249d8f1d41dfbc257496c81d84e'>digitale ondertekening</a> in PDF-bestanden.
+ Met dank aan verbeterde integratie van kruistoepassingen ondersteunt Okular nu bewerken van LaTeX documenten in <a href='https://bugs.kde.org/show_bug.cgi?id=404120'>TexStudio</a>.
+ Verbeterde ondersteuning voor <a href='https://phabricator.kde.org/D18118'>navigatie in aanraakschermen</a> betekent dat u in staat bent achterwaarts en voorwaarts te gaan met een aanraakscherm in modus Presentatie.
+ Gebruikers die graag documenten manipuleren vanaf de opdrachtregel zullen in staat zijn om slim <a href='https://bugs.kde.org/show_bug.cgi?id=362038'>tekst te zoeken</a> met de nieuwe vlag op de opdrachtregel die u een document laat openen en alle locaties van een gegeven stuk tekst te accentueren.
+ Okular toont nu op de juiste manier koppelingen in <a href='https://bugs.kde.org/show_bug.cgi?id=403247'>Markdown documenten</a> die over meer dan een regel lopen.
+ De <a href='https://bugs.kde.org/show_bug.cgi?id=397768'>hulpmiddelen voor trimmen</a> hebben elegante nieuwe pictogrammen.

{{<figure src="/announcements/applications/19.04.0/app1904_kmail.png" width="600px" >}}

<a href='https://kde.org/applications/internet/kmail/'>KMail</a> is e-mailclient van KDE die KDE's privacy beschermt. Onderdeel van de <a href='https://kde.org/applications/office/kontact/'>Kontact groupwaresuite</a>, KMail ondersteunt alle e-mailsystemen en biedt u om uw berichten in een gedeelde virtueel postvak in te organiseren of in gescheiden accounts (uw keuze). Het ondersteunt allerlei soorten van versleuteling van berichten en ondertekening en laat u gegevens delen zoals contactpersonen, vergaderdata en reisinformatie met andere Kontact-toepassingen.

Verbeteringen bevatten:

+ Bovendien, Grammarly! Deze versie van KMail komt met ondersteuning voor taalhulpmiddelen (grammatica controle) en grammalecte (alleen voor Frans grammatica controle).
+ Telegramnummers in e-mails worden nu gedetecteerd en kunnen direct via <a href='https://community.kde.org/KDEConnect'>KDE Connect</a> worden gebeld.
+ KMail heeft nu een optie om <a href='https://phabricator.kde.org/D19189'>direct in het systeemvak te starten</a> zonder het hoofdvenster te openen.
+ De ondersteuning voor de plug-in voor Markdown hebben we verbeterd.
+ Ophalen van -emails via IMAP stokt niet langer wanneer aanmelden mislukt.
+ We hebben ook talloze reparaties gedaan in de backend Akonadi van KMail om betrouwbaarheid en prestaties te verbeteren.

<a href='https://kde.org/applications/office/korganizer/'>KOrganizer</a> is de agendacomponent van Kontact, waarmee al uw afspraken beheerd worden.

+ Terugkeergebeurtenissen uit <a href='https://bugs.kde.org/show_bug.cgi?id=334569'>Google Calendar</a> worden opnieuw op de juiste manier gesynchroniseerd.
+ Het venster voor herinneringen voor gebeurtenissen onthoudt om <a href='https://phabricator.kde.org/D16247'>zich op alle bureaubladen tonen</a>.
+ We hebben het uiterlijk van de <a href='https://phabricator.kde.org/T9420'>gebeurtenissenweergave</a> gemoderniseerd.

<a href='https://cgit.kde.org/kitinerary.git/'>Kitinerary</a> is brandnieuwe reisassistent die zal helpen om u naar uw locatie te brengen en u op uw weg van raad te voorzien.

- Er is een nieuwe algemene uitpakker voor RCT2 tickets (bijv. gebruikt door spoorwegbedrijven zoals DSB, ÖBB, SBB, NS).
- Detectie van namen van luchthavens en ondubbelzinnig maken is belangrijk verbeterd.
- We hebben nieuwe aangepaste uitpakkers voor eerder niet ondersteunde leveranciers (bijv. BCD Travel, NH Group) toegevoegd en formaat/taalvariaties van al ondersteunde leveranciers (bijv. SNCF, Easyjet, Booking.com, Hertz) verbeterd.

### Ontwikkeling

{{<figure src="/announcements/applications/19.04.0/app1904_kate.png" width="600px" >}}

<a href='https://kde.org/applications/utilities/kate/'>Kate</a> is tekstbewerker van KDE met volledige functies, ideaal voor programmeren dankzij functies zoals tabs, modus gesplitste weergave, syntaxisaccentuering, een ingebouwd paneel, woordaanvulling, zoeken met reguliere expressies en substitutie, en nog veel meer via de flexibele plug-in infrastructuur.

Verbeteringen bevatten:

- Kate kan nu alle onzichtbare witruimte tekens tonen, niet slechts sommige.
- U kunt gemakkelijk statische zinafbreking in- en uitschakelen door zijn eigen menu-item te gebruiken op een per-documentbasis, zonder de globale standaard instelling te hoeven wijzigen.
- De contextmenu's van bestand en tabblad bevatten nu een aantal nuttige nieuwe acties, zoals Hernoemen, Verwijderen, Bevattende map openen, Bestandspad openen, Vergelijken [met een ander geopend bestand] en Eigenschappen.
- Deze versie van Kate wordt geleverd met meer plug-ins standaard ingeschakeld, inclusief de populaire en nuttige inline Terminal functie.
- Bij afsluiten vraagt Kate niet langer om te bevestigen dat bestanden die zijn gewijzigd op de schijf door een ander proces, zoals een wijziging door broncodebesturing.
- De boomstructuurweergave van de plug-in toont nu alle menu-items voor git-items die umlauts in hun namen hebben.
- Bij openen van meerdere bestanden met de opdrachtregel worden de bestanden geopend in nieuwe tabbladen in dezelfde volgorde zoals gespecificeerd in de opdrachtregel.

{{<figure src="/announcements/applications/19.04.0/app1904_konsole.png" width="600px" >}}

<a href='https://kde.org/applications/system/konsole/'>Konsole</a> is de terminalemulator van KDE. Het ondersteunt tabbladen, doorzichtige achtergronden, modus gesplitste weergave, aan te passen kleurschema's en sneltoetsen, bladwijzers voor mappen en SSH en vele andere functies.

Verbeteringen bevatten:

+ Beheer van tabbladen heeft een aantal verbeteringen gezien die u zullen helpen productiever te zijn. Nieuwe tabbladen kunnen aangemaakt worden door met een middenklik op lege delen van de tabbladbalk en er is ook een optie waarmee u tabbladen sluit met een middenklik erop. Sluitknoppen worden standaard op tabbladen getoond en pictogrammen zullen alleen getoond worden wanneer een profiel wordt gebruikt met een aangepast pictogram. Als laatste maar niet het minste, biedt de sneltoets Ctrl+Tab u om snel te schakelen tussen het huidige en vorige tabblad.
+ De dialoog voor het bewerkingsprofiel ontving een enorme <a href='https://phabricator.kde.org/D17244'>gebruikersinterface herziening</a>.
+ Het kleurschema Breeze wordt nu gebruikt als het standaard kleurschema van Konsole en we hebben zijn contrast en consistentie verbeterd met het systeembrede Breeze-thema.
+ We hebben de problemen opgelost bij tonen van vette tekst.
+ Konsole toont nu correct de cursor met de onderstrepingstijl.
+ We hebben het weergeven van <a href='https://bugs.kde.org/show_bug.cgi?id=402415'>vakjes- en lijntekens</a> verbeterd, evenals die van <a href='https://bugs.kde.org/show_bug.cgi?id=401298'>Emoji-tekens</a>.
+ Sneltoetsen voor omschakelen van profiel schakelen het profiel van het huidige tabblad in plaats van een nieuw tabblad te openen met het andere profiel.
+ Inactieve tabbladen die een melding ontvangen en waarvan de titeltekst van kleur wijzigt resetten de kleur naar de normale tekstkleur van de tabbladtitel wanneer het tabblad wordt geactiveerd.
+ De functie 'De achtergrond voor elk tabblad variëren' werkt nu wanneer de basisachtergrondkleur erg donker is of zwart.

<a href='https://kde.org/applications/development/lokalize/'>Lokalize</a> is een computer-ondersteunt vertaalsysteem dat zich focust op productiviteit en verzekering van kwaliteit. De doelstelling is gericht op vertaling van software, maar integreert ook externe hulpmiddelen voor conversie voor vertalingen van kantoordocumenten.

Verbeteringen bevatten:

- Lokalize ondersteunt nu het bekijken van de te vertalen broncode met een aangepaste bewerker.
- We hebben de lokatie van DockWidgets verbeterd en de manier waarop instellingen worden opgeslagen en hersteld.
- De positie in .po bestanden wordt nu bewaard bij filteren van berichten.
- We hebben een aantal bugs in UI gerepareerd (commentaar van ontwikkelaars, RegExp omschakelen in massaal vervangen, vage lege aantal berichten, …).

### Hulpmiddelen

{{<figure src="/announcements/applications/19.04.0/app1904_gwenview.png" width="600px" >}}

<a href='https://kde.org/applications/graphics/gwenview/'>Gwenview</a> is een geavanceerde afbeeldingsviewer en organisator met intuïtieve en gemakkelijk te gebruiken hulpmiddelen voor bewerking.

Verbeteringen bevatten:

+ De versie van Gwenview die geleverd wordt met Applications 19.04 bevat volledige <a href='https://phabricator.kde.org/D13901'>ondersteuning voor aanraakscherm</a>, met gebaren voor vegen, zoomen, pannen en meer.
+ Een andere verbetering toegevoegd aan Gwenview is volledige <a href='https://bugs.kde.org/show_bug.cgi?id=373178'>ondersteuning voor High DPI</a>, die afbeeldingen er geweldig uit laten zien op hoge-resolutie schermen.
+ Verbeterde ondersteuning voor <a href='https://phabricator.kde.org/D14583'>terug en vooruit muisknoppen</a> biedt u het navigeren tussen afbeeldingen door deze knoppen in te drukken.
+ U kunt Gwenview nu gebruiken om afbeeldingsbestanden gemaakt met <a href='https://krita.org/'>Krita</a> te openen – het favoriete digitale hulpmiddel voor schilderen.
+ Gwenview ondersteunt nu grote <a href='https://phabricator.kde.org/D6083'>512 px miniaturen</a>, waarmee u uw afbeeldingen gemakkelijker kunt bekijken.
+ Gwenview gebruikt nu de <a href='https://bugs.kde.org/show_bug.cgi?id=395184'>standaard Ctrl+L sneltoets</a> om focus te verplaatsen naar het URL-veld.
+ U kunt nu de functie <a href='https://bugs.kde.org/show_bug.cgi?id=386531'>Filter-op-naam</a> met de sneltoets Ctrl+I gebruiken, net als in Dolphin.

{{<figure src="/announcements/applications/19.04.0/app1904_spectacle.jpg" width="600px" >}}

<a href='https://kde.org/applications/graphics/spectacle/'>Spectacle</a> is de toepassing schermafdruk van Plasma. U kunt volledige bureaubladen die meerdere schermen omvatten, individuele schermen, vensters, secties van vensters of aangepaste regio's met de rechthoekige selectiefunctie pakken.

Verbeteringen bevatten:

+ We hebben de modus Rechthoekige regio uitgebreid met een paar nieuwe opties. Deze kan geconfigureerd worden naar <a href='https://bugs.kde.org/show_bug.cgi?id=404829'>automatisch accepteren</a> van het gesleepte vak in plaats van u te vragen het eerst aan te passen. Er is ook een nieuwe standaard optie om het huidige selectievak <a href='https://phabricator.kde.org/D19117'>rechthoekige regio</a> te herinneren, maar alleen tot het programma wordt gesloten.
+ U kunt wat er gebeurt configureren wanneer de sneltoets voor schermafdruk wordt ingedrukt <a href='https://phabricator.kde.org/T9855'>tijdens het actief zijn van Spectacle</a>.
+ Spectacle biedt u het wijzigen van het <a href='https://bugs.kde.org/show_bug.cgi?id=63151'>compressieniveau</a> voor afbeeldingsformaten met verlies.
+ Instellingen opslaan toont u hoe de <a href='https://bugs.kde.org/show_bug.cgi?id=381175'>bestandsnaam van een schermafdruk</a> er uit gaat zien. U kunt ook gemakkelijk het <a href='https://bugs.kde.org/show_bug.cgi?id=390856'>bestandsnaamsjabloon</a> aanpassen aan uw voorkeur door eenvoudig te klikken op plaatshouders.
+ Spectacle toont niet langer zowel de optie “Volledig scherm (alle monitors)” en “Huidige scherm” wanneer uw computer slechts één scherm heeft.
+ De hulptekst in modus Rechthoekige regio verschijnt nu in het midden van de hoofddisplay, in plaats van gesplitst tussen de schermen.
+ Bij actief op Wayland bevat Spectacle alleen de functies die werken.

### Spellen en onderwijs

{{<figure src="/announcements/applications/19.04.0/app1904_kmplot.png" width="600px" >}}

Onze serie toepassingen omvat een groot aantal <a href='https://games.kde.org/'>spellen</a> en <a href='https://edu.kde.org/'>onderwijskundige toepassingen</a>.

<a href='https://kde.org/applications/education/kmplot'>KmPlot</a> is een plotter van wiskundige functies. Het heeft een krachtige ingebouwde ontleder. De grafieken kunnen gekleurd worden en de weergave is te schalen, waarmee u op het benodigde niveau kan zoomen. Gebruikers kunnen tegelijk verschillende functies plotten en ze combineren om nieuwe functies te bouwen.

+ U kunt nu inzoomen door Ctrl ingedrukt te houden en het <a href='https://bugs.kde.org/show_bug.cgi?id=159772'>muiswiel</a> te gebruiken.
+ Deze versie van Kmplot introduceert de optie <a href='https://phabricator.kde.org/D17626'>afdrukvoorbeeld</a>.
+ Waarde van oorsprong of (x,y) paar kan nu gekopieerd worden naar het <a href='https://bugs.kde.org/show_bug.cgi?id=308168%'>klembord</a>.

<a href='https://kde.org/applications/games/kolf/'>Kolf</a> is een miniatuur golfspel.

+ We hebben <a href='https://phabricator.kde.org/D16978'>geluidsondersteuning</a> hersteld.
+ Kolf is met succes overgezet weg van kdelibs4.
