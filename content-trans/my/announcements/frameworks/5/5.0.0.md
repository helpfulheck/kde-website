---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: ကေဒီအီးသည် မူဘောင်များ ၅ ၏ပထမဆုံးထုတ်ဝေမှုကို တင်ပို့ခဲ့သည်။
layout: framework
qtversion: 5.2
title: ကေဒီအီးမူဘောင်များ ၅ ၏ပထမဆုံးထုတ်ဝေမှု
---
ဇူလိုင် ၇ရက်၊ ၂၀၁၄။ ကေဒီအီးအသိုင်းအဝိုင်းသည် ကေဒီအီးမူဘောင်များ ၅.၀ ကို ဂုဏ်ယူစွာ ကြေညာခဲ့သည်။ မူဘောင်များ ၅ သည် ကျူတီအပ္ပလီကေးရှင်းများနှင့် လွယ်ကူစွာပေါင်းစည်းလုပ်ဆောင်နိုင်ရန်နှင့် အစိတ်အပိုင်းများစွာခွဲထားသော ကေဒီအီးကုတ်ကြည့်တိုက်များ၏ နောက်မျိုးဆက်သစ်ဖြစ်ပေသည်။ မူဘောင်မှ များစွာလှသော အသုံးများသည့် လုပ်ဆောင်ချက်များကို ပျော့ပြောင်းသော လိုင်စင်သဘောတူညီချက်များနှင့် သေချာစမ်းသပ်အကဲဖြတ်စိစစ်ထားသော ကုတ်ကြည့်တိုက်များပေးသည်။ ဤထုတ်ဝေမှုတွင် ဟာ့ဒ်ဝဲလ်နှင့်ပေါင်းစည်းမှု၊ ဖိုင်လ်အချိုးအစားထောက်ပံ့မှု၊ နောက်ထပ်ဝစ်ဂျတ်များ၊ ပုံဆွဲဖန်ရှင်များ၊ စာလုံးပေါင်းစစ်ဆေးမှုနှင့် အခြားအသုံးဝင်သော လုပ်ဆောင်ချက်များပါသည့် မူဘောင် ၅၀ကျော်ပါဝင်ပါသည်။ မူဘောင်အများစုသည် cross-platform ဖြစ်၍ မှီခိုပရိုဂရမ်အနည်းငယ်သာ လိုအပ်သောကြောင့် တည်ဆောက်ရာတွင် လွယ်ကူပြီး မည်သည့်ကျူတီအပ္ပလီကေးရှင်းသို့ ထည့်သွင်းနိုင်သည်။

စွမ်းဆောင်ရည်ပြည့်ဝသော ကေဒီအီး ပလက်ဖောင်း ၄ ကုတ်ကြည့်တိုက်များကို မှီခိုမှုကင်းမဲ့သော ပလက်ဖောင်းမျိုးစုံသုံး၍ရသော မော်ဂျူးများအစုအဖြစ် ကြိုးပမ်းမှုကြောင့် ကေဒီအီးမူဘောင်များ ထွက်ပေါ်လာသည်။ ၎င်းသည် ကျူတီအပ်ဖန်တီးမှုအား ရိုးရှင်းစေရန်၊ အရှိန်မြင့်တင်စေရန်၊ ကုန်ကျမှုလျော့နည်းစေရန်ကျူတီပရိုဂရမ်မာများထံ အလွယ်တကူရရှိစေနိုင်ခဲ့သည်။ ကေဒီအီးမှ ခေါင်းဆောင်၍ မူဘောင်များကို ပြင်လွယ်ပြောင်းလွယ်လိုင်စင်(အယ်ဂျီပီအယ်)နှင့် တိုးတက်မှုပြုလုပ်ခဲ့သည်။

မူဘောင်များတွင် မြင်သာသော မှီခိုပရိုဂရမ်ဖွဲ့စည်းမှုရှိသည်။ ၎င်းကို အမျိုးအစားနှင့် အတန်းအစားများအဖြစ် ခွဲထားသည်။ အမျိုးအစားများသည် လုပ်ဆောင်ချိန်မှီခိုပရိုဂရမ်းများကို ဆိုလိုသည် -

- <strong>ဖန်ရှင်နယ်</strong> ကုတ်စင်များ ၌ လုပ်ဆောင်ချိတ်မှီခိုပရိုဂရမ်များ မရှိပါ။
- <strong>ပေါင်းစည်းလုပ်ဆောင်မှု</strong> သည် မောင်းနှင်စနစ် သို့မဟုတ် ပလက်ဖောင်းပေါ်မူတည်၍ ပေါင်းစည်ရန် အတွက် လုပ်ဆောင်ချိန်မှီခိုပရိုဂရမ်များလိုအပ်သော ကုတ်ကို သတ်မှတ်သည်။
- <strong>ဖြေရှင်းချက်များ</strong> တွင် လုံးဝလိုအပ်သော လုပ်ဆောင်ချိန်မှီခိုပရိုဂရမ်များ ရှိသည်။

<strong>အတန်းအစားများ</strong>သည် အခြားမူဘောင်များအပေါ် ကွန်ပိုင်းချိန်မှီခိုမှုများကို ဆိုလိုသည်။ အတန်းအစား ၁ မူဘောင်များ၌ မူဘောင်များအတွင်း မှီခိုမှုလုံးဝမရှိပါ။ ၎င်းတို့သည် ကျူတီနှင့် အခြားဆက်စပ်ကုတ်ကြည့်တိုက်များသာ လိုအပ်သည်။ အတန်းအစား ၂ မူဘောင်များသည် အတန်းအစား ၁ မူဘောင်များပေါ်သာ မှီခိုသည်။ အတန်းအစား ၃ မူဘောင်များသည် အခြားအတန်းအစား ၃ မူဘောင်များနှင့် အတန်းအစား ၂၊၁ မူဘောင်များပေါ် မှီခိုသည်။

ကေဒီအီးနည်းပညာဆိုင်ရာအကူအညီပေးသူများမှ နည်းပြ၍ ပလက်ဖောင်းမှ မူဘောင်များသို့ ကူးပြောင်းမှုသည် သုံးနှစ်ကျော်ကြာခဲ့ပြီးဖြစ်သည်။ မူဘောင်များ ၅ <a href='http://dot.kde.org/2013/09/25/frameworks-5'>ကို ယခင်နှစ်က ဤအာတီကယ်တွင် ဖတ်ရှုလေ့လာပါ</a>။

## အထူးဖော်ပြချက်များ

မူဘောင် ၅၀ကျော် ရရှိနိုင်ပါသည်။ အွန်လိုင်းအေပီအိုင်ဒိုကူမန်တေးရှင်းတွင် အားလုံး<a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>ကို ရှောက်ကြည့်ပါ။ အောက်တွင် မူဘောင်များမှ ကျူတီအပ္ပလီကေးရှင်းပရိုးရမ်မာများထံ ပေးသော လုပ်ဆောင်ချက်များအချို့၏ အမြင်များ ဖော်ပြထားသည်။

<strong>KArchive</strong> offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there's no need to reinvent an archiving function in your Qt-based application!

<strong>ThreadWeaver</strong> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them satisfying these dependencies, greatly simplifying the use of multiple threads.

<strong>KConfig</strong> is a Framework to deal with storing and retrieving configuration settings. It features a group-oriented API. It works with INI files and XDG-compliant cascading directories. It generates code based on XML files.

<strong>Solid</strong> offers hardware detection and can inform an application about storage devices and volumes, CPU, battery status, power management, network status and interfaces, and Bluetooth. For encrypted partitions, power and networking, running daemons are required.

<strong>KI18n</strong> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.
