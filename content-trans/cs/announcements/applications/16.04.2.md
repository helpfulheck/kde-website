---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE vydává Aplikace KDE 16.04.2
layout: application
title: KDE vydává Aplikace KDE 16.04.2
version: 16.04.2
---
June 14, 2016. Today KDE released the second stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 25 recorded bugfixes include improvements to akonadi, ark, artikulate, dolphin. kdenlive, kdepim, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.21.
