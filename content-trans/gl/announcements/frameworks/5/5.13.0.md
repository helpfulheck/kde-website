---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Novas infraestruturas

- KFileMetadata: biblioteca de metados de ficheiros e extracción
- Baloo: infraestrutura de indexación e busca

### Cambios que afectan a todas as infraestruturas

- O requisito de versión de Qt aumentouse de 5.2 a 5.3
- A saída de depuración migrouse a saída con categoría, para reducir o ruído de maneira predeterminada
- Revisouse e actualizouse a documentación de Docbook

### Integración de infraestruturas

- Corrixir unha quebra no diálogo de ficheiros de só directorios
- Non usar options()-&gt;initialDirectory() para Qt &lt; 5.4

### Ferramentas de Doxygen de KDE

- Engadir páxinas de manual para scripts de kapidox e actualizar a información do mantedor en setup.py

### KBookmarks

- KBookmarkManager: usar KDirWatch en vez de QFileSystemWatcher para detectar a creación de user-places.xbel.

### KCompletion

- Correccións de HiDPI para KLineEdit e KComboBox
- KLineEdit: Non permitir ao usuario eliminar texto cando a liña de edición é de só lectura

### KConfig

- Non recomendar usar unha API obsoleta
- Non xerar código obsoleto

### KCoreAddons

- Engadir Kdelibs4Migration::kdeHome() para casos que os recursos non satisfagan
- Corrixir un aviso de tr()
- Corrixir a construción de KCoreAddons con Clang en ARM

### KDBusAddons

- KDBusService: documentar como elevar a xanela activa en Activate()

### KDeclarative

- Corrixir unha chamada obsoleta a KRun::run
- O mesmo comportamento que MouseArea para asociar coordenadas con eventos subordinados filtrados
- Detectar a icona de cara inicial que se crea
- Non actualizar a xanela enteira cando se renderiza o gráfico (fallo 348385)
- engadir a propiedade de contexto userPaths
- Xestionar correctamente un QIconItem baleiro

### Compatibilidade coa versión 4 de KDELibs

- kconfig_compiler_kf5 moveuse a libexec, usar kreadconfig5 no seu lugar para a proba de findExe
- Documentar as substitucións (subóptimas) de KApplication::disableSessionManagement

### KDocTools

- cambiar unha oración sobre informar de fallos, recoñecido por dfaure
- adaptar o user.entities alemán a en/user.entities
- Actualizar general.entities: cambiar as etiquetas das infraestruturas e Plasma das aplicacións ao nome do produto
- Actualizar en/user.entities
- Actualizar os modelos de libro e páxina de manual
- Usar CMAKE_MODULE_PATH en cmake_install.cmake
- ERRO: 350799 (fallo 350799)
- Actualizar general.entities
- Buscar os módulos de Perl necesarios
- Engadir un espazo de nomes a un macro de asistencia no ficheiro de macros instalado.
- Adaptáronse traducións de nomes clave a traducións estándar fornecidas por Termcat

### KEmoticons

- Instalar o tema Breeze
- Kemoticons: facer as emoticonas de Breeze estándar en vez de Glass
- Paquete de emoticonas de Breeze feito por Uri Herrera

### KHTML

- Permitir usar KHtml sen buscar dependencias privadas

### KIconThemes

- Retirar asignacións temporais de cadeas
- Retirar a entrada de depuración da árbore de temas

### KIdleTime

- As cabeceiras privadas de complementos de plataformas instálanse.

### KIO

- Retirar envoltorios QUrl innecesarios

### KItemModels

- Novo proxy: KExtraColumnsProxyModel, permite engadir columnas a un modelo existente.

### KNotification

- Corrixir a posición inicial de Y para xanelas emerxentes de último recurso
- Reducir dependencias e mover a Tier 2
- capturar entradas de notificacións descoñecidas (nullptr deref) (fallo 348414)
- Retirar unha mensaxe de aviso practicamente inútil

### Infraestrutura de paquetes

- facer os subtítulos subtítulos ;)
- kpackagetool: Corrixir a saída de texto non latino á saída estándar

### KPeople

- Engadir AllPhoneNumbersProperty
- Agora PersonsSortFilterProxyModel pode usarse en QML

### Kross

- krosscore: Instalar a cabeceira de IniciaisEnMaiúsculas «KrossConfig»
- Corrixir as probas de Python 2 para executar con PyQt 5

### KService

- Corrixir kbuildsycoca --global
- KToolInvocation::invokeMailer: corrixir o anexo cando hai varios anexos

### KTextEditor

- gardar o nivel de rexistro predeterminado de Qt &lt; 5.4.0, corrixir o nome de lectura de rexistro
- engadir hl para Xonotic (fallo 342265)
- engadir Groovy HL (fallo 329320)
- actualizar o salientado de J (fallo 346386)
- Facer compilar con MSVC2015
- reducir o uso de iconloader, corrixir as iconas máis pixeladas
- activar ou desactivar o botón de «Atopalo todo» cando cambia o padrón
- Mellorouse a barra de buscar e substituír
- retirar unha regra innecesaria do modo de potencia
- barra de busca máis fina
- vi: Corrixir a lectura incorrecta da marca markType01
- Usar a cualificación correcta para chamar a métodos base.
- Retirar comprobacións, QMetaObject::invokeMethod xa se garda a si mesmo contra iso.
- corrixir problemas de HiDPI con selectores de cor
- Limpar coe: QMetaObject::invokeMethod permite nullptr.
- máis comentarios
- cambiar como as interfaces permiten nulo
- só emitir por saída avisos e mensaxes máis importantes de maneira predeterminada
- retirar tarefas pendentes do pasado
- Usar QVarLengthArray para gardar a iteración temporal de QVector.
- Mover o truco para sangrar os grupos de etiquetas a tempo de construción.
- Corrixir algúns problemas serios con KateCompletionModel no modo de árbore.
- Corrixir un deseño de modelo roto que esperaba o comportamento de Qt 4.
- obedecer as regras de umask ao gardar un ficheiro novo (fallo 343158)
- engadir meson HL
- As Varnish 4.x introduces various syntax changes compared to Varnish 3.x, I wrote additional, separate syntax highlighting files for Varnish 4 (varnish4.xml, varnishtest4.xml).
- corrixir problemas de HiDPI
- vimode: non quebrar se a orde &lt;c-e&gt; se executa ao final dun documento. (fallo 350299)
- Permitir cadeas de varias liñas de QML.
- corrixir a sintaxe de oors.xml
- Engadir CartoCSS hl de Lukas Sommer (fallo 340756)
- corrixir o HL de coma flotante, usar a coma flotante nativa como en C (fallo 348843)
- as direccións divididas invertéronse (fallo 348845)
- Bug 348317 - [PATCH] Katepart syntax highlighting should recognize u0123 style escapes for JavaScript (bug 348317)
- engadir *.cljs (fallo 349844)
- Actualizar o ficheiro de salientado de GLSL.
- corrixíronse as cores predeterminadas para que se distingan mellor

### KTextWidgets

- Eliminar o salientador anterior

### Infraestrutura de KWallet

- Corrixir a construción en Windows
- Imprimir un aviso con código de erro cando falla a apertura da carteira mediante PAM
- Devolver o código de erro da infraestrutura en vez de -1 cando falle a apertura dunha carteira
- Facer que o código devolto pola infraestrutura ante un cifrador descoñecido sexa negativo
- Vixiar as aparicións de PAM_KWALLET5_LOGIN para KWallet 5
- Corrixir unha quebra cando a comprobación MigrationAgent::isEmptyOldWallet() falla
- Agora KWallet pode desbloquearse mediante PAM usando o módulo kwallet-pam

### KWidgetsAddons

- Nova API que acepta parámetros QIcon para estabelecer as iconas na barra de separadores
- KCharSelect: Corrixir unha categoría de Unicode e usar boundingRect para o cálculo de anchura
- KCharSelect: corrixir a anchura da cela para axustala ao contido
- Agora as marxes de KMultiTabBar funcionan ben en pantallas HiDPI
- KRuler: marcar como obsoleto KRuler::setFrameStyle(), que nunca funcionou, e limpar os comentarios
- KEditListWidget: retirar a marxe para que se aliñe mellor con outros trebellos

### KWindowSystem

- Reforzar a lectura de datos de NETWM (fallo 350173)
- gardar para versións anteriores de Qt como en kio-http
- As cabeceiras privadas de complementos de plataformas instálanse.
- Partes específicas de plataforma cargadas como complementos.

### KXMLGUI

- Corrixir o comportamento do método KShortcutsEditorPrivate::importConfiguration

### Infraestrutura de Plasma

- Usar un xesto de pinchar agora pódese cambiar entre distintos niveis de ampliación no calendario
- comentario sobre duplicación de código en icondialog
- A cor de marca do control desprazábel estaba fixa, modificouse para usar o esquema de cores
- Usar QBENCHMARK en vez de un requisito ríxido sobre o rendemento da máquina
- A navegación do calendario mellorouse de maneira significativa, fornecendo un resumo de ano e década
- Agora PlasmaCore.Dialog ten a propiedade «opacity»
- Facer oco para o botón de alternativa
- Non mostrar o fondo circular se hai un menú
- Engadir a definición de X-Plasma-NotificationAreaCategory
- Configurar as notificacións e o OSD para mostrarse en todos os escritorios
- Imprimir un aviso útil cando non se pode obter un KPluginInfo válido
- Corrixir unha potencial recursividade infinita en PlatformStatus::findLookAndFeelPackage()
- Cambiar o nome de software-updates.svgz a software.svgz

### Sonnet

- Engadir cousas de CMake para activar a construción do complemento Voikko.
- Crear a fábrica de Sonnet::Client para os correctores ortográficos de Voikko.
- Crear un corrector ortográfico baseado en Voikko (Sonnet::SpellerPlugin)

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
