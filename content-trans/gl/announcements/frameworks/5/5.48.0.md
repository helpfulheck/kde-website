---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Migrar os usos restantes de qDebug() a qcDebug(ATTICA)
- Corrixir a comprobación de URL incorrecto de fornecedor
- Corrixir un URL roto á especificación da API

### Baloo

- Retirar a entrada sen usar X-KDE-DBus-ModuleName dos metadatos de complemento de kded
- [tags_kio] A consulta do URL deberían ser parellas de valores
- O sinal do estado da enerxía só debería emitirse cando o estado da enerxía cambia
- baloodb: Facer cambios na descrición de argumento da liña de ordes tras o cambio de nome de «prune» a «clean»
- Mostrar claramente nomes de ficheiro duplicados nos cartafoles de etiquetas

### BluezQt

- Actualizar os ficheiros XML de D-Bus «Out*» para anotacións de Qt de tipo sinal
- Engadir un sinal para o cambio de enderezo dos dispositivos

### Iconas de Breeze

- Usar a icona con forma de vasoira tamén para edit-clear-all
- Usar unha icona con forma de vasoira para edit-clear-history
- cambiar a icona view-media-artist de 24 px

### Módulos adicionais de CMake

- Android: Permitir sobrepoñer o directorio de APK do obxectivo
- Retirar o QT_USE_FAST_OPERATOR_PLUS por desactualizado
- Engadir -Wlogical-op -Wzero-as-null-pointer-constant aos avisos de KF5
- [ECMGenerateHeaders] Engadir unha opción para extensións de ficheiro de cabeceira distintas de «.h»
- Non incluír un 64 ao construír arquitecturas de 64 bits en Flatpak

### KActivitiesStats

- Corrixir o erro de desprazamento de un en Cache::clear (fallo 396175)
- Corrixir o movemento de elementos ResultModel (fallo 396102)

### KCompletion

- Retirar unha inclusión innecesaria de moc
- Asegurarse de que se emite KLineEdit::clearButtonClicked

### KCoreAddons

- Retirar definicións duplicadas de QT de KDEFrameworkCompilerSettings
- Asegurarse de que compila con marcas de compilación estritas
- Retirar a clave non usada X-KDE-DBus-ModuleName dos metadatos de tipo de servizo de proba

### KCrash

- Reducir QT_DISABLE_DEPRECATED_BEFORE á dependencia mínima de Qt
- Retirar definicións duplicadas de QT de KDEFrameworkCompilerSettings
- Asegurarse de construír con marcas de compilación estritas

### KDeclarative

- comprobar que o modo ten realmente unha textura válida (fallo 395554)

### KDED

- Definición de tipo de servizo de KDEDModule: retirar a clave X-KDE-DBus-NomeDeModulo por desuso

### Compatibilidade coa versión 4 de KDELibs

- Definir QT_USE_FAST_OPERATOR_PLUS nós mesmos
- Non exportar kf5-config ao ficheiro de configuración de CMake
- Retirar a entrada sen usar X-KDE-DBus-ModuleName dos metadatos de complemento de kded

### WebKit de KDE

- Migrar KRun::runUrl() e KRun::run() a unha API non obsoleta
- Migrar KIO::Job::ui() a KIO::Job::uiDelegate()

### KFileMetaData

- Evitar avisos do compilador sobre cabeceiras de taglib
- PopplerExtractor: usar directamente os argumentos de QByteArray() en vez de punteiros a 0
- taglibextractor: restaurar as propiedades de extraer son sen existir etiquetas
- OdfExtractor: xestionar nomes de prefixo inhabituais
- Non engadir -ltag á interface de ligazóns pública
- ler a etiqueta lyrics de taglibextractor
- probas automáticas: non incrustar EmbeddedImageData se xa está na biblioteca
- Engadir a posibilidade de ler ficheiros coa portada incrustada
- ler a etiqueta de puntuación
- comprobar se están as versións necesarias de libavcode, libavformat e libavutil

### KGlobalAccel

- Actualizar o ficheiro XML de D-Bus «Out*» para anotacións de Qt de tipo sinal

### KHolidays

- holidays/plan2/holiday_jp_* - engadir metadatos que faltaban
- actualizáronse as vacacións de Xapón (en xaponés e en inglés) (fallo 365241)
- holidays/plan2/holiday_th_en-gb - actualizouse Tailandia (inglés británico) para 2018 (fallo 393770)
- engadir vacacións de Venezula (en castelán) (fallo 334305)

### KI18n

- Usar CMAKE_CURRENT_LIST_DIR consecuentemente no ficheiro de macro de CMake en vez de dun uso mesturado con KF5I18n_DIR
- KF5I18NMacros: non instalar un directorio baleiro cando non existen ficheiros PO

### KIconThemes

- Permitir escoller ficheiros .ico no selector de ficheiro de icona personalizada (fallo 233201)
- Permitir a escala de icona da versión 0.13 da especificación de nomeamento de iconas (fallo 365941)

### KIO

- Usar o novo método fastInsert alí onde corresponda
- Restaurar a compatibilidade de UDSEntry::insert engadindo o método fastInsert
- Migrar KLineEdit::setClearButtonShown a QLineEdit::setClearButtonEnabled
- Actualizar o ficheiro XML de D-Bus «Out*» para anotacións de Qt de tipo sinal
- Retirar a definición de QT duplicada de KDEFrameworkCompilerSettings
- Usar unha icona de emblema correcta para ficheiros e cartafoles de só lectura (fallo 360980)
- Permitir volver subir á raíz no trebello de ficheiro
- [Diálogo de propiedades] Mellorar algunhas cadeas relacionadas con permisos (fallo 96714)
- [KIO] Engadir compatibilidade con XDG_TEMPLATES_DIR a KNewFileMenu (fallo 191632)
- actualizar o docbook do lixo a 5.48
- [Diálogo de propiedades] Permitir seleccionar todos os valores do separador xeral (fallo 105692)
- Retirar a entrada non usada X-KDE-DBus-ModuleName dos metadatos dos complementos de kded
- Activar a comparación de KFileItems por URL
- [KFileItem] Comprobar se o URL máis local está compartido
- Corrixir unha regresión ao pegar datos binarios do portapapeis

### Kirigami

- cor máis consistente de rato por riba
- non abrir un menú subordinado para accións sen subordinados
- Facer cambios internos no concepto de barra de ferramentas global (fallo 395455)
- Xestionar a propiedade enabled de modelos simples
- Introducir ActionToolbar
- corrixir baixar para actualizar
- Retirar a documentación de Doxygen dunha clase interna
- Non ligar con Qt5::DBus cando DISABLE_DBUS está definida
- sen marxe adicional para follas de capa en capas
- corrixir o menú para Qt 5.9
- Comprobar tamén a propiedade visible da acción
- mellor aparencia e aliñamento no modo compacto
- non buscar complementos por cada creación de platformTheme
- librarse do conxunto «custom»
- engadir redefinidores a todas as cores personalizadas
- Migrar a coloración de toolbutton a un colorSet personalizado
- introducir o conxunto de cores Personalizado
- buddyFor que permite escritura para etiquetas de posicións relacionadas con elementos subordinados
- Ao usar unha cor de fondo distinta usar highlightedText como cor de texto

### KNewStuff

- [KMoreTools] Permitir instalar ferramentas mediante URL de appstream
- Retirar a chapuza de d-pointer de KNS::Engine

### KService

- Retirar a clave non usada X-KDE-DBus-ModuleName dos metadatos do servizo de proba

### KTextEditor

- gardar updateConfig contra barras de estado desactivadas
- engadir un menú contextual á barra de estado para conmutar mostrar o número total de liñas e palabras
- Engadiuse a funcionalidade de mostrar as liñas totais en Kate (fallo 387362)
- Facer que os botóns de barra de ferramentas con menús mostren os seus menús cun clic normal en vez de requirir manter o clic (fallo 353747)
- CVE-2018-10361: aumento de privilexios
- Corrixir a anchura do cursor (fallo 391518)

### KWayland

- [servidor] Enviar o evento de marco en vez de baleirar ao mover o punteiro relativo (fallo 395815)
- Corrixir a proba de xanela emerxente de XDGV6
- Corrixir un fallo estúpido de copia e pega no cliente XDGShellV6
- Non cancelar a selección anterior do portapapeis se é igual que a nova (fallo 395366)
- Respectar BUILD_TESTING
- Corrixir algúns problemas ortográficos suxeridos pola nova ferramenta de estilización
- Engadir o ficheiro arclint a KWayland
- Corrixir @since na API de saltar o selector

### KWidgetsAddons

- [KMessageWidget] Actualizar a folla de estilos nos cambios de paleta
- Actualizar kcharselect-data a Unicode 11.0
- [KCharSelect] Migrar generate-datafile.py a Python 3
- [KCharSelect] Preparar as traducións para a actualización a Unicode 11.0

### ModemManagerQt

- Permitir as interfaces de Voice e Call
- Non definir regras de filtro de dominio personalizadas

### Iconas de Oxygen

- Mostrar unha icona para os ficheiros ocultos en Dolphin (fallo 395963)

### Infraestrutura de Plasma

- FrameSvg: Actualizar o marco da máscara se se cambiou a ruta da imaxe
- FrameSvg: Non destruír os marcos de máscaras compartidas
- FrameSvg: simplificar updateSizes
- Iconas para o indicador de teclado T9050
- corrixir a cor para iconas de son e vídeo
- FrameSvg: Gardar maskFrame de novo na caché se enabledBorders se cambiou (fallo 391659)
- FrameSvg: Debuxar esquinas só se se activan os bordos nas dúas direccións
- Ensinar a ContainmentInterface::processMimeData como xestionar caídas de xestores de tarefas
- FrameSVG: Retirar comprobacións redundantes
- FrameSVG: Corrixir a inclusión de QObject
- Usar QDateTime para interactuar con QML (fallo 394423)

### Purpose

- Engadir a acción de compartir ao menú contextual de Dolphin
- Restabelecer os complementos correctamente
- Ignorar complementos duplicados

### QQC2StyleBridge

- retirar valores en píxeles de checkindicator
- usar RadioIndicator para todos
- Non saírse das xanelas emerxentes
- en Qt&lt;5.11 a paleta do control ignórase completamente
- fondo do botón de áncora

### Solid

- Corrixir unha etiqueta de dispositivo de tamaño descoñecido

### Realce da sintaxe

- Correccións de comentarios de Java
- Salientar os ficheiros de Gradle tamén coa sintaxe de Groovy
- CMake: corrixir o realce tras cadeas cun único símbolo <code>@</code>
- CoffeeScript: engadir a extensión .cson
- Rust: Engadir palabras claves e bytes, corrixir os identificadores, e outras melloras e correccións
- Awk: corrixir unha expresión regular nunha función e actualizar a sintaxe de gawk
- Pony: corrixir un escape de comiñas simples e un posíbel bucle infinito con #
- Actualizar a sintaxe de CMake para a próxima versión 3.12

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
