---
date: 2013-08-14
hidden: true
title: A versión 4.11 das aplicacións de KDE supón un enorme paso adiante na xestión
  de información persoal e melloras xerais
---
The Dolphin file manager brings many small fixes and optimizations in this release. Loading large folders has been sped up and requires up to 30&#37; less memory. Heavy disk and CPU activity is prevented by only loading previews around the visible items. There have been many more improvements: for example, many bugs that affected expanded folders in Details View were fixed, no &quot;unknown&quot; placeholder icons will be shown any more when entering a folder, and middle clicking an archive now opens a new tab with the archive contents, creating a more consistent experience overall.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`O novo fluxo de traballo de enviar máis tarde en Kontact` width="600px">}}

## Melloras da colección Kontact

O desenvolvemento da colección Kontact centrouse de maneira significativa en estabilidade, rendemento e uso de memoria. Durante os últimos 6 meses melloráronse a importación de cartafoles, o cambio entre mapas, a obtención de correo, marcar e mover grandes números de mensaxes, e o tempo de inicio. Consulte <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>este blog</a> (en inglés) para máis información. Tamén <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>se solucionaron moitos fallos da ferramenta de arquivo</a> e fixéronse melloras no asistente de importación, permitindo importar opcións do cliente de correo Trojitá e mellorando a importación doutras aplicacións. Descubra máis <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>aquí</a> (en inglés).

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`O axente de arquivo xestiona o almacenamento de correo en forma comprimida` width="600px">}}

This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks. You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.

## Extensións das linguaxes compatíbeis con Kate

O editor de texto avanzado Kate introduce novos complementos: Python (2 e 3), JavaScript e jQuery, Django e XML. Introducen funcionalidades como completado automático estático e dinámico, os comprobadores de sintaxe, inserción de fragmentos de código e a posibilidade de sangrar automaticamente XML cun atallo. Pero para os amigos de Python hai máis: unha consola de Python que fornece información detallada nun ficheiro fonte aberto. Tamén se realizaron pequenas melloras da interface de usuario, incluídas <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>novas notificacións pasivas para a funcionalidade de busca</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizacións para o modo de VIM</a> e <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>unha nova funcionalidade d pregado de texto</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars mostra vindeiros eventos interesantes visíbeis desde onde esta o usuario` width="600px">}}

## Melloras doutras aplicacións

Na categoría de xogos e educación chegaron varias novas funcionalidades e optimizacións, grandes e pequenas. Os futuros mecanógrafos agradecerán a compatibilidade con escritura de dereita a esquerda en KTouch, mentres que o mellor amigo do observador de estrelas, KStars, fornece agora unha ferramenta que mostra vindeiros eventos interesantes na súa zona. As ferramentas de matemáticas Rocs, Kig, Cantor e KAlgebra recibiron atención tamén, e permiten agora infraestruturas e cálculos adicionais. E o xogo KJumpingCube ten agora tamaños máis grandes de taboleiro, novos niveis de habilidade, respostas máis rápidas e unha interface de usuario mellorada.

A aplicación de debuxo simple, Kolourpaint, pode traballar co formato de imaxe WebP e o visor de documentos universal Okular ten ferramentas de revisión que se poden configurar e introduce a funcionalidade de desfacer e refacer en formularios e anotacións. O reprodutor e etiquetador de son JuK permite reproducir e editar metadatos do novo formato de son Ogg Opus (porén, isto require que o controlador de son e TagLib tamén sexan compatíbeis con Ogg Opus).

#### Instalar aplicacións de KDE

Os programas de KDE, incluídas todas as súas bibliotecas e as súas aplicacións, están dispoñíbeis de maneira gratuíta baixo licenzas de código aberto. Os programas de KDE funcionan en varias configuracións de soporte físico e arquitecturas de CPU como ARM e x86, sistemas operativos e funcionan con calquera xestor de xanelas ou contorno de escritorio. Ademais de Linux e outros sistemas operativos baseados en UNIX pode atopar versións para Microsoft Windows da meirande parte das aplicacións de KDE no sitio web de <a href='http://windows.kde.org'>programas de KDE para Windows</a> e versións para o Mac OS X de Apple no <a href='http://mac.kde.org/'>sitio web de programas de KDE para Mac</a>. En Internet poden atoparse construcións experimentais de aplicacións de KDE para varias plataformas móbiles como MeeGo, MS Windows Mobile e Symbian pero non están mantidas. <a href='http://plasma-active.org'>Plasma Active</a> é unha experiencia de usuario para un maior abanico de dispositivos, como computadores de tableta e outros soportes físicos móbiles.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paquetes

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Lugares dos paquetes

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Requisitos do sistema

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Hoxe tamén se anunciaron:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

En preparación para mantemento a longo prazo, os espazos de traballo de Plasma inclúen melloras adicionais en funcionalidades básicas cunha barra de tarefas máis suave, un trebello de batería máis intelixente e un mesturador de son mellorado. A introdución de KScreen trae consigo xestión intelixente de varios monitores nos espazos de traballo, e melloras de rendemento a gran escala en combinación con pequenos axustes de facilidade de uso que conseguen unha mellor experiencia xeral.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Esta versión 4.11 da plataforma de KDE continúa centrándose en estabilidade. Estanse realizando novas funcionalidades para a futura versión 5.0 das infraestruturas de KDE, pero para a versión estábel conseguimos introducir optimizacións para a nosa infraestrutura Nepomuk.
