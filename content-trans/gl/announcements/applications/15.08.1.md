---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE publica a versión 15.08.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.08.1 das aplicacións de KDE
version: 15.08.1
---
September 15, 2015. Today KDE released the first stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 40 correccións de erros inclúen melloras en, entre outros, KDE Libs, KDE PIM, Kdenlive, Dolphin, Marble, Kompare, Konsole, Ark e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.12.
