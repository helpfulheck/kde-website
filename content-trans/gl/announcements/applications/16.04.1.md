---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE publica a versión 16.04.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.04.1 das aplicacións de KDE
version: 16.04.1
---
May 10, 2016. Today KDE released the first stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 25 correccións de erros inclúen melloras en, entre outros, KDE PIM, Ark, Kate, Dolphin, Kdenlive, Lokalize e Spectacle.

This release also includes Long Term Support version of KDE Development Platform 4.14.20.
