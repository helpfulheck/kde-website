---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE publica a versión 16.08.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.08.0 das aplicacións de KDE
version: 16.08.0
---
18 de agosto de 2016. KDE introduce a versión 16.08 das aplicacións de KDE con innumerábeis anovacións no que respecta a unha maior facilidade de acceso, a introdución de funcionalidades moi útiles e solucionar algúns pequenos problemas que achegan ás aplicacións de KDE un pouco máis a ofrecer a configuración perfecta para o seu dispositivo.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> and KDiskFree have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release.

No esforzo continuado por dividir as bibliotecas da colección de Kontact para que terceiros poidan usalas con máis facilidade, dividimos o arquivo TAR de kdepimlibs en akonadi-contacts, akonadi-mime e akonadi-notes.

Detivemos o desenvolvemento dos seguintes paquetes: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu e mplayerthumbs. Isto nos permitirá concentrarnos no resto do código.

### Mantemento de Kontact

<a href='https://userbase.kde.org/Kontact'>The Kontact Suite</a> has got the usual round of cleanups, bug fixes and optimizations in this release. Notable is the use of QtWebEngine in various compontents, which allows for a more modern HTML rendering engine to be used. We have also improved VCard4 support as well as added new plugins that can warn if some conditions are met when sending an email, e.g. verifying that you want to allow sending emails with a given identity, or checking if you are sending email as plaintext, etc.

### Nova versión de Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 is part of KDE Applications 16.08 and includes more than 450 code changes including improvements in navigation, rendering and an experimental vector rendering of OpenStreetMap data.

### Arquivar máis

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> can now extract AppImage and .xar files as well as testing the integrity of zip, 7z and rar archives. It can also add/edit comments in rar archives

### Melloras terminais

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> has had improvements regarding font rendering options and accessibility support.

### E máis!

<a href='https://kate-editor.org'>Kate</a> got movable tabs. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>More information...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a>, has added provinces and regions maps of Burkina Faso.

### Exterminación de erros

Solucionáronse máis de 120 fallos en aplicacións como, entre outras, Kontact Suite, Ark, Cantor, Dolphin, KCalc e Kdenlive!

### Historial completo de cambios
