---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV: Mòdul nou

Implementació del protocol DAV amb KJobs.

S'admeten calendaris i tasques pendents, usant GroupDAV o CalDAV, i els contactes s'admeten usant GroupDAV o CardDAV.

### Baloo

+ [Indexers] Ignora el tipus MIME basat en el nom de les decisions inicials d'indexació (error 422085)

### BluezQt

+ Exposa el servei d'anunci de dades d'un dispositiu

### Icones Brisa

+ S'han corregit tres icones «application-x-...» de 16px per ser més ajustades als píxels (error 423727)
+ Afegeix el funcionament per a escales &gt;200% s de les icones que han de romandre monocromes
+ Retalla el centre de «window-close-symbolic»
+ Actualitza la icona de l'aplicació Cervisia i l'acció
+ Afegeix la secció README quant al «webfont»
+ Usa «grunt-webfonts» en lloc de «grunt-webfont» i desactiva la generació de les icones enllaçades simbòlicament
+ Afegeix la icona del Kup del repositori del Kup

### Mòduls extres del CMake

+ Elimina el suport per a «png2ico»
+ Tracta el codi CMake de les Qt que modifica CMAKE_SHARED_LIBRARY_SUFFIX
+ Afegeix el mòdul de cerca de FindTaglib

### Eines de Doxygen del KDE

+ Implementa «repo_id» al «metainfo.yaml» per substituir el nom del repositori estimat

### KCalendarCore

+ Comprova l'error d'escriptura al «save()» si el disc és ple (error 370708)
+ Noms correctes d'icones per a les tasques pendents recurrents
+ Corregeix la serialització de les dates d'inici de les tasques pendents recurrents (error 345565)

### KCMUtils

+ Corregeix el senyal canviat del selector de connectors

### KCodecs

+ Afegeix diverses «n», altrament el text pot ser molt gros dins el quadre de missatge

### KConfig

+ Passa també «locationType» a KConfigSkeleton en la construcció des d'un grup
+ Fa més coherent el text «Canvia l'idioma de l'aplicació...»

### KConfigWidgets

+ Fa més coherent el text «Canvia l'idioma de l'aplicació...»

### KCoreAddons

+ KRandom::random -&gt; QRandomGenerator::global()
+ Fa obsolet KRandom::random

### KCrash

+ Afegeix una declaració d'entorn que manca, altrament només és disponible a GNU

### KDocTools

+ Usa un estil coherent per a l'avís de la FDL (error 423211)

### KFileMetaData

+ Adapta «kfilemetadata» a «audio/x-speex+ogg» tal com s'ha canviat recentment a «shared-mime-info»

### KI18n

+ Afegeix també cometes al voltant del text enriquit del text etiquetat &lt;filename&gt;

### KIconThemes

+ Elimina una assignació a «resetPalette»
+ Retorna QPalette() si no hi ha cap paleta personalitzada
+ Respecta QIcon::fallbackSearchpaths() (error 405522)

### KIO

+ [kcookiejar] Corregeix la lectura de la definició de la galeta «Accept For Session» (error 386325)
+ KDirModel: corregeix una regressió de «hasChildren()» per als arbres que mostren fitxers ocults, enllaços simbòlics, canonades i dispositius de caràcter (error 419434)
+ OpenUrlJob: corregeix el funcionament dels scripts de l'intèrpret d'ordres amb un espai al nom de fitxer (error 423645)
+ Afegeix una drecera web per a DeepL i ArchWiki
+ [KFilePlacesModel] Mostra els dispositius AFC (Apple File Conduit)
+ Codifica també els caràcters d'espai per als URL de drecera web (error 423255)
+ [KDirOperator] Activa realment «dirHighlighting» de manera predeterminada
+ [KDirOperator] Ressalta el directori anterior en tornar enrere/pujar (error 422748)
+ [Trash] Gestiona l'error ENOENT en reanomenar fitxers (error 419069)
+ File ioslave: estableix els nanosegons del segell de temps en copiar (error 411537)
+ Ajusta els URL dels proveïdors de cerca Qwant (error 423156)
+ File ioslave: afegeix la implementació per copiar «reflink» (error 326880)
+ Corregeix la definició de la drecera web predeterminada al KCM de Dreceres web (error 423154)
+ Assegura la intel·ligibilitat del KCM de Dreceres web establint una amplada mínima (error 423153)
+ FileSystemFreeSpaceJob: emet un error si el «kioslave» no proporciona les metadades
+ [Trash] Elimina els fitxers «trashinfo» que fan referència a fitxers/directoris que no existeixen (error 422189)
+ kio_http: estima per si mateix el tipus MIME si el servidor retorna «application/octet-stream»
+ Fa obsolets els senyals «totalFiles» i «totalDirs», que no s'emeten
+ Corregeix la recàrrega de les dreceres web noves (error 391243)
+ kio_http: corregeix l'estat del reanomenament amb la sobreescriptura activada
+ No interpreta el nom com a fitxer ocult o camí de fitxer (error 407944)
+ No mostra les dreceres web suprimides/ocultes (error 412774)
+ Corregeix una fallada en eliminar una entrada (error 412774)
+ No permet assignar les dreceres web existents
+ [kdiroperator] Usa consells d'eina millors per a les accions enrere i endavant (error 422753)
+ [BatchRenameJob] Usa KJob::Items per reportar la informació de progrés (error 422098)

### Kirigami

+ [aboutpage] Usa OverlaySheet per al text de la llicència
+ Corregeix les icones en mode reduït
+ Usa separadors fins a DefaultListItemBackground
+ Afegeix la propietat «weight» a Separator
+ [overlaysheet] Evita l'alçada fraccionària per a «contentLayout» (error 422965)
+ Corregeix la documentació de «pageStack.layers»
+ Torna a posar la implementació de capes a Page.isCurrentPage
+ Implementa el color de les icones
+ Usa el component intern MnemonicLabel
+ Redueix el farciment esquerre de la barra d'eines global quan no s'usa el títol
+ Només estableix «implicit{Width,Height}» per a Separator
+ Actualitza KF5Kirigami2Macros.cmake per usar «https» amb el repositori «git» per tal d'evitar un error en intentar accedir-hi
+ Correcció: càrrega de l'avatar
+ Fa que es pugui escriure PageRouterAttached#router
+ Corregeix el tancament d'OverlaySheet en fer clic dins la disposició (error 421848)
+ Afegeix un ID que manca al GlobalDrawerActionItem de GlobalDrawer
+ Corregeix l'OverlaySheet que és massa alt
+ Corregeix l'exemple de codi de PlaceholderMessage
+ Renderitza una vora a la part inferior dels grups
+ Usa un component LSH
+ Afegeix un fons a les capçaleres
+ Gestió millorada de la reducció
+ Presentació millorada per als elements de capçaleres de llista
+ Afegeix la propietat «bold» a BasicListItem
+ Corregeix Kirigami.Units.devicePixelRatio=1.3 quan hauria de ser 1.0 a 96ppp
+ Oculta la maneta de l'OverlayDrawer quan no és interactiu
+ Ajusta els càlculs d'ActionToolbarLayoutDetails per a fer un ús millor de l'estat real de la pantalla
+ ContextDrawerActionItem: Prefereix la propietat «text» per sobre del consell d'eina

### KJobWidgets

+ Integra els KJob::Unit::Items (error 422098)

### KJS

+ Corregeix una fallada en usar KJSContext::interpreter

### KNewStuff

+ Mou el text explicatiu des de la capçalera de full a la capçalera de llista
+ Corregeix els camins de l'script d'instal·lació i descompressió
+ Oculta ShadowRectangle per a les vistes prèvies no carregades (error 422048)
+ No permet que el contingut desbordi als delegats de la quadrícula (error 422476)

### KNotification

+ No usa «notifybysnore.h» al MSYS2

### KParts

+ Fa obsolet PartSelectEvent i companyia

### KQuickCharts

+ Elideix el valor de Label del LegendDelegate quan no hi ha prou amplada
+ Corregeix l'error «C1059: non constant expression...»
+ Té en compte l'amplada de la línia en comprovar els límits
+ No usa «fwidth» en renderitzar les línies dels gràfics
+ Reescriu «removeValueSource» de manera que no usa QObjects destruïts
+ Usa «insertValueSource» a Chart::appendSource
+ Inicialitza adequadament ModelHistorySource::{m_row,m_maximumHistory}

### KRunner

+ Corregeix RunnerContextTest per a no assumir la presència del «.bashrc»
+ Usa metadades JSON incrustades per als connectors binaris i personalitzades per als connectors D-Bus
+ Emet «queryFinished» quan han finalitzat tots els treballs de la consulta actual (error 422579)

### KTextEditor

+ Fa que «Ves a la línia» funcioni cap enrere (error 411165)
+ Corregeix una fallada en suprimir la vista si els intervals encara són actius (error 422546)

### Framework del KWallet

+ Presenta tres mètodes nous que retornen totes les «entrades» d'una carpeta

### KWidgetsAddons

+ Corregeix KTimeComboBox per a configuracions regionals amb caràcters inusuals als formats (error 420468)
+ KPageView: elimina el mapa de píxels invisible de la banda dreta de la capçalera
+ KTitleWidget: mou des de la propietat QPixmap a la propietat QIcon
+ Fa obsoletes algunes API de KMultiTabBarButton/KMultiTabBarTab que usen QPixmap
+ KMultiTabBarTab: fa que la lògica d'estat de «styleoption» segueixi encara més QToolButton
+ KMultiTabBar: no mostra els botons activats a QStyle::State_Sunken
+ [KCharSelect] Dona el focus inicial a la línia d'edició de la cerca

### KWindowSystem

+ [xcb] Envia correctament la geometria de la icona escalada (error 407458)

### KXMLGUI

+ Usa «kcm_users» en lloc de «user_manager»
+ Usa l'API KTitleWidget::icon/iconSize nova
+ Mou «Canvia l'idioma de l'aplicació» al menú Arranjament (error 177856)

### Frameworks del Plasma

+ Mostrar un avís més clar si no s'ha pogut trobar el KCM sol·licitat
+ [spinbox] No usa elements QQC2 quan s'haurien d'usar PlasmaComponents (error 423445)
+ Plasma components: restaura el control perdut del color de l'etiqueta TabButton
+ Presenta PlaceholderMessage
+ [calendar] Redueix la mida de l'etiqueta de mes
+ [ExpandableListItem] Usa la mateixa lògica per a l'acció i la visibilitat de botó de fletxa
+ [Dialog Shadows] Adapta a l'API d'ombres del KWindowSystem (error 416640)
+ Enllaç simbòlic de «widgets/plasmoidheading.svgz» als Brisa clara/fosca
+ Corregeix Kirigami.Units.devicePixelRatio=1.3 quan hauria de ser 1.0 a 96ppp
+ Afegeix la propietat per accedir a l'element del carregador d'ExpandableListItem

### Prison

+ Fa obsolet AbstractBarcode::minimumSize() també per al compilador

### Purpose

+ Usa explícitament les unitats del Kirigami en lloc de les unitats del Plasma implícitament
+ Adapta els diàlegs de treball a Kirigami.PlaceholderMessage

### QQC2StyleBridge

+ Estableix «selectByMouse» a «true» per a SpinBox
+ Corregeix les icones de menú que esdevenen borroses amb algunes mides de tipus de lletra (error 423236)
+ Evita que els delegats i les barres de desplaçament se superposin als quadres combinats emergents
+ Corregeix la «implicitWidth» d'un Slider vertical i bucle de vinculació
+ Fa que ToolTips sigui més coherent amb els consells d'eina d'estil de giny del Breeze
+ Estableix «editable» a «true» de manera predeterminada per a SpinBox
+ Corregeix els avisos de les connexions a Menu.qml

### Solid

+ Activa el dorsal per UDisks2 al FreeBSD incondicionalment

### Sonnet

+ Corregeix «Ús de QCharRef amb un índex apuntant fora de l'interval vàlid d'una QString»
+ Restaura el comportament predeterminat de detecció automàtica
+ Corregeix l'idioma predeterminat (error 398245)

### Sindicació

+ Corregeix un parell de capçaleres de llicència BSD de 2 clàusules

### Ressaltat de la sintaxi

+ CMake: actualitzacions per al CMake 3.18
+ Afegeix el ressaltat del llenguatge Snort/Suricata
+ Afegeix el llenguatge YARA
+ Elimina un binari JSON obsolet a favor del CBOR
+ JavaScript/TypeScript: ressalta les etiquetes a les plantilles

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
