---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Treu i reescriu l'esclau KIO d'etiquetes del Baloo (error 340099)

### BluezQt

- Evita fuites dels descriptors de fitxers del «rfkill» (error 386886)

### Icones Brisa

- Afegeix mides d'icones que manquen (error 384473)
- Afegeix les icones d'instal·lar i desinstal·lar per al Discover

### Mòduls extres del CMake

- Afegeix l'etiqueta de descripció als fitxers «pkgconfig» generats
- ecm_add_test: usa el separador de camí adequat al Windows
- Afegeix FindSasl2.cmake a l'ECM
- Només passa els ARGS en crear Makefiles
- Afegeix FindGLIB2.cmake i FindPulseAudio.cmake
- ECMAddTests: defineix QT_PLUGIN_PATH de manera que els connectors construïts localment es puguin trobar
- KDECMakeSettings: més documentació quant a la disposició del directori de construcció

### Integració del marc de treball

- Permet baixar el 2n i 3r enllaç de baixada des d'un producte KNS (error 385429)

### KActivitiesStats

- Inicia l'esmena de «libKActivitiesStats.pc»: (error 386933)

### KActivities

- Esmena un error de concurrència que inicia el «kactivitymanagerd» múltiples vegades

### KAuth

- Permet construir només el generador de codi del «kauth-policy-gen»
- Afegeix una nota quant a cridar el «helper» des d'aplicacions multifil

### KBookmarks

- No mostra l'acció d'edició de les adreces d'interès si no està instal·lat el «keditbookmarks»
- Adapta des del «KAuthorized::authorizeKAction» obsolet a l'«authorizeAction»

### KCMUtils

- Navegació amb el teclat dins i fora dels KCM escrits en QML

### KCompletion

- No falla en establir l'edició d'una línia nova en un quadre combinat editable
- KComboBox: retorna ràpidament en establir un editable al seu valor previ
- KComboBox: reutilitza l'objecte de compleció existent en editar una línia nova

### KConfig

- No cerca a /etc/kderc cada vegada

### KConfigWidgets

- Actualitza els colors predeterminats per coincidir els colors nous a D7424

### KCoreAddons

- Validació d'entrada de les subfeines
- Avisa quant als errors en analitzar els fitxers «json»
- Instal·la les definicions dels tipus MIME dels fitxers kcfg/kcfgc/ui.rc/knotify i qrc
- Afegeix una funció nova per mesurar la longitud a partir del text
- Esmena un error a KAutoSave amb un fitxer que tingui un espai en blanc

### KDeclarative

- Fa que compili al Windows
- Fa que compili amb QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Permet l'acceptació dels esdeveniments de ratolí
- Usa un únic motor QML

### KDED

- kded: elimina les crides de D-Bus al ksplash

### KDocTools

- Actualitza la traducció al portuguès del Brasil
- Actualitza la traducció russa
- Actualitza la traducció russa
- Actualitza customization/xsl/ru.xml (mancava «nav-home»)

### KEmoticons

- KEmoticons: adapta els connectors al JSON i afegeix la implementació per carregar-los amb KPluginMetaData
- Evita les fuites de símbols de les classes «pimpl», les protegeix amb Q_DECL_HIDDEN

### KFileMetaData

- El «usermetadatawritertest» requereix Taglib
- Si el valor de la propietat és nul, elimina l'atribut «user.xdg.tag» (error 376117)
- Obre els fitxers a l'extractor TagLib en mode només lectura

### KGlobalAccel

- Agrupa diverses crides blocants de D-Bus
- kglobalacceld: evita carregar un carregador d'icones sense cap motiu
- Genera les cadenes correctes de dreceres

### KIO

- KUriFilter: filtra els connectors duplicats
- KUriFilter: simplifica les estructures de dades, esmena d'una fuita de memòria
- [CopyJob] No torna a començar-ho tot després d'haver eliminat un fitxer
- Esmena la creació d'un directori via KNewFileMenu+KIO::mkpath a les Qt 5.9.3+ (error 387073)
- S'ha creat una funció auxiliar «KFilePlacesModel::movePlace»
- Exposa el rol «iconName» de KFilePlacesModel
- KFilePlacesModel: evita els senyals «dataChanged» innecessaris
- Retorna un objecte vàlid d'adreça d'interès per a qualsevol entrada a KFilePlacesModel
- Crea la funció «KFilePlacesModel::refresh»
- Crea la funció estàtica «KFilePlacesModel::convertedUrl»
- KFilePlaces: s'ha creat la secció «remote»
- KFilePlaces: afegeix una secció per a dispositius extraïbles
- S'han afegit els URL del Baloo en el model de llocs
- Esmena KIO::mkpath amb Qtbase 5.10 beta 4
- [KDirModel] Emet canvi per HasJobRole quan els treballs canvien
- Canvia l'etiqueta «Opcions avançades» &gt; «Opcions del terminal»

### Kirigami

- Desplaçament de les barres de desplaçament per la mida de la capçalera (error 387098)
- Marge inferior basat en la presència d'«actionbutton»
- No assumeix que «applicationWidnow()» estigui disponible
- No notifica els canvis de valor si encara està en el constructor
- Substitueix el nom de la biblioteca al codi font
- Permet colors a més llocs
- Icones de color a les barres d'eines si són necessàries
- Considera els colors de les icones als botons de l'acció principal
- Inici per una «icona» agrupada adequadament

### KNewStuff

- Reverteix «Desacobla abans d'establir l'apuntador p» (error 386156)
- No instal·la l'eina de desenvolupament per agregar els fitxers «desktop»
- [knewstuff] No provoca una fuita a ImageLoader en cas d'error

### Framework del KPackage

- Fa les cadenes adequades al framework «kpackage»
- Intenta no generar cap «metadata.json» si no hi ha cap «metadata.desktop»
- Esmena l'ús de la memòria cau del «kpluginindex»
- Millora la sortida d'error

### KTextEditor

- Esmena les ordres de la memòria intermèdia en el mode VI
- Evita fer zoom accidentalment

### KUnitConversion

- Adapta des de QDom a QXmlStreamReader
- Usa «https» per baixar els tipus de canvi de les divises

### KWayland

- Exposa «wl_display_set_global_filter» com un mètode virtual
- Esmena «kwayland-testXdgShellV6»
- Afegeix la implementació per «zwp_idle_inhibit_manager_v1» (error 385956)
- [server] Permet inhibir la «IdleInterface»

### KWidgetsAddons

- Evita un «passworddialog» incoherent
- Estableix el consell «enable_blur_behind» a demanda
- KPageListView: actualitza l'amplada en canviar el tipus de lletra

### KWindowSystem

- [KWindowEffectsPrivateX11] Afegeix una crida «reserve()»

### KXMLGUI

- Esmena la traducció del nom de la barra d'eines quan té context «i18n»

### Frameworks del Plasma

- La directiva #warning no és universal, i particularment NO està permesa pel MSVC
- [IconItem] Usa «ItemSceneHasChanged» abans que connectar amb «windowChanged»
- [Icon Item] Emet explícitament «overlaysChanged» en el «setter» abans en lloc de connectar-hi
- [Dialog] Usa KWindowSystem::isPlatformX11()
- Redueix la quantitat de canvis de propietat erronis a ColorScope
- [Icon Item] Només emet «validChanged» si realment ha canviat
- Suprimeix els indicadors de desplaçament innecessaris si l'element «flickable» és una ListView amb una orientació coneguda
- [AppletInterface] Emet canvi de senyals per «configurationRequired» i «-Reason»
- Usa «setSize()» en lloc de «setProperty» per l'amplada i l'alçada
- S'ha esmenat el problema del menú de PlasmaComponents que apareixia amb les cantonades trencades (error 381799)
- S'ha esmenat el problema dels menús contextuals que apareixien amb les cantonades trencades (error 381799)
- API docs: afegeix un avís d'obsolescència trobat en el registre del «git»
- Sincronitza el component amb el del Kirigami
- Cerca tots els components dels KF5 com a tals en lloc de Frameworks separats
- Redueix les emissions errònies de senyals (error 382233)
- Afegeix senyals que indiquen si s'ha afegit o eliminat una pantalla
- Instal·la el material del Switch
- No confia en inclusions d'inclusions
- Optimitza els noms de rol del SortFilterModel
- Elimina DataModel::roleNameToId

### Prison

- Afegeix el generador de codi Aztec

### QQC2StyleBridge

- Determina la versió del QQC2 en temps de construcció (error 386289)
- Manté el fons invisible de manera predeterminada
- Afegeix un fons a ScrollView

### Solid

- UDevManager::devicesFromQuery més ràpid

### Sonnet

- Fa possible la compilació creuada del Sonnet

### Ressaltat de la sintaxi

- Afegeix PKGUILD a la sintaxi del «bash»
- JavaScript: inclou els tipus MIME estàndard
- debchangelog: afegeix Bionic Beaver
- Actualitza el fitxer de sintaxi SQL (Oracle) (error 386221)
- SQL: mou la detecció de comentaris abans dels operadors
- crk.xml: s'ha afegit la línia de capçalera &lt;?xml&gt;

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
