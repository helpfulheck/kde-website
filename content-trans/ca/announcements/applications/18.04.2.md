---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE distribueix les aplicacions 18.04.2 del KDE
layout: application
title: KDE distribueix les aplicacions 18.04.2 del KDE
version: 18.04.2
---
7 de juny de 2018. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../18.04.0'>aplicacions 18.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 25 esmenes registrades d'errors que inclouen millores al Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize i Okular entre d'altres.

Les millores inclouen:

- Les operacions d'imatge al Gwenview ara es poden refer després de desfer-les
- El KGpg ja no falla en desencriptar missatges sense una capçalera de versió
- L'exportació de fulls de treball del Cantor al LaTeX s'ha esmenat per a les matrius del Maxima
