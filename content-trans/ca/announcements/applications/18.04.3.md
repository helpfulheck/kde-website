---
aliases:
- ../announce-applications-18.04.3
changelog: true
date: 2018-07-12
description: KDE distribueix les aplicacions 18.04.3 del KDE
layout: application
title: KDE distribueix les aplicacions 18.04.3 del KDE
version: 18.04.3
---
12 de juliol de 2018. Avui KDE distribueix la tercera actualització d'estabilització per a les <a href='../18.04.0'>aplicacions 18.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 20 esmenes registrades d'errors que inclouen millores al Kontact, Ark, Cantor, Dolphin, Gwenview i KMag, entre d'altres.

Les millores inclouen:

- S'ha restaurat la compatibilitat amb els servidors IMAP que no anuncien les seves capacitats
- Ara l'Ark pot extreure arxius ZIP amb manca de les entrades apropiades per a carpetes
- Les notes en pantalla del KNotes tornen a seguir el ratolí en moure-les
