---
aliases:
- ../../kde-frameworks-5.60.0
date: 2019-07-13
layout: framework
libCount: 70
---
### Allmänt

- Qt ≥ 5.11 krävs nu, dvs. Qt 5.13 stöds inte längre.

### Baloo

- [QueryTest] Testa om oberoende fraser verkligen är oberoende
- [TermGenerator] Infoga en tom position mellan oberoende termer
- [QueryTest] Strukturera om tester för att tillåta enklare utökning
- [TermGenerator] Lämna enstaka termfraser utanför PositionDB
- [TermGenerator] Gör avkortning av Term innan konvertering till UTF-8
- [PostingIterator] Flytta metoden positions() till VectorPositionInfoIterator
- [TermGenerator] Använd UTF-8 ByteArray för termList
- [WriteTransactionTest] Rensa hopblandning mellan QString och QByteArray
- [experimental/BalooDB] Rätta trivial varning om 0 / nullptr
- [PositionDbTest] Rätta trivial minnesläcka i test
- [PendingFileQueueTest] Verifiera att skapa + borttagning inte skickar extra händelser
- [PendingFileQueueTest] Verifiera att borttagning + skapa verkligen fungerar
- [PendingFileQueue] Undvik kapplöpning mellan borttagning + skapa / skapa + borttagning
- [PendingFileQueueTest] Använd syntetisk tidtagningshändelse för att snabba upp test
- [XAttrIndexer] Uppdatera DocumentTime när XAttrs uppdateras
- [PendingFileQueueTest] Förkorta tidsgränser, verifiera följningstid
- [PendingFileQueue] Använd noggrannare beräkning av återstående tid
- [ModifiedFileIndexer] Använd korrekt Mime-typ för kataloger, fördröj till den behövs
- [NewFileIndexer] Utelämna symboliska länkar från indexet
- [ModifiedFileIndexer] Undvik skuggning av XAttr ändringar av innehållsändringar
- [NewFileIndexer] Använd korrekt Mime-typ för kataloger, kontrollera excludeFolders
- [UnindexedFileIndexer] Ta upp kommentarer, etiketter och betygsändringar
- [UnindexedFileIndexer] Hoppa över kontroll av filtid för nya filer
- [DocumentUrlDB] Undvik manipulering av hela trädet vi trivialt namnbyte
- [DocumentUrlDB] Upptäck ogiltiga webbadresser tidigt
- [DocumentUrlDB] Ta bort den oanvända metoden 'rename'
- [balooctl] Strömlinjeforma indexerarens kontrollkommandon
- [Transaction] Ersätt mall för functor med std::function
- [FirstRunIndexer] Använd riktig Mime-typ för kataloger
- Flytta invariant IndexingLevel utanför snurran
- [BasicIndexingJob] Hoppa över uppslagning av baloo dokumenttyp för kataloger
- [FileIndexScheduler] Säkerställ att indexering inte körs i viloläge
- [PowerStateMonitor] Var konservativ när strömläge bestäms
- [FileIndexScheduler] Stoppa indexering när quit() anropas via D-Bus
- Undvik bortkoppling av behållare på några ställen
- Försök inte lägga till i slutet av QLatin1String
- Inaktivera detektering av valgrind vid kompilering med MSVC
- [FilteredDirIterator] Kombinera alla suffix i ett stort reguljärt uttryck
- [FilteredDirIterator] Undvik kostnad av reguljära uttryck för exakta matchningar
- [UnindexedFileIterator] Fördröj bestämning av Mime-typ till det behövs
- [UnindexedFileIndexer] Försök inte att lägga till icke-existerade fil i index
- Detektera valgrind, undvik borttagning av databas när valgrind används
- [UnindexedFileIndexer] Optimeringar av snurror (undvik detach, invarianter)
- Fördröj att köra UnindexedFileIndexer och IndexCleaner
- [FileIndexScheduler] Lägg till nytt tillstånd för overksam med batteri
- [FileIndexScheduler] Fördröj underhållsuppgifter medan batteri används
- [FileIndexScheduler] Undvik att skicka tillståndsändringar flera gånger
- [balooctl] Klargör och utöka statusutmatning

### BluezQt

- Lägg till MediaTransport programmeringsgränssnitt
- Lägg till programmeringsgränssnitt för LE Advertising och GATT

### Breeze-ikoner

- Lägg till id="current-color-scheme" för collapse-all ikoner (fel 409546)
- Lägg till disk-quota ikoner (fel 389311)
- Skapa symbolisk länk för install till edit-download
- Ändra styrspak inställningsikon till spelkontroll (fel 406679)
- Lägg till edit-select-text, gör 16 bildpunkters draw-text lik 22 bildpunkters
- Uppdatera KBruch ikon
- Lägg till help-donate-[currency] ikoner
- Gör så att Breeze mörk använder samma Kolourpaint ikon som Breeze
- Lägg till 22 bildpunkters underrättelseikoner

### KActivitiesStats

- Rätta en krasch i KactivityTestApp när Result har strängar som inte är ASCII

### KArchive

- Krascha inte om den inre filen vill vara större än maxstorleken av QByteArray

### KCoreAddons

- KPluginMetaData: använd Q_DECLARE_METATYPE

### KDeclarative

- [GridDelegate] Rätta gap i hörnen av thumbnailArea färgläggning
- bli av med blockSignals
- [KCM GridDelegate] Tysta varning
- [KCM GridDelegate] Ta hänsyn till implicitCellHeight för inre delegathöjd
- Rätta GridDelegate ikon
- Rätta ömtålig jämförelse med i18n("None") och beskriv beteendet i dokumentationen (fel 407999)

### KDE WebKit

- Nedgradera KDEWebKit från Lager 3 till konverteringshjälp

### KDocTools

- Uppdatera pt-BR user.entities

### KFileMetaData

- Rätta extrahering av vissa egenskaper för att matcha vad som skrevs (fel 408532)
- Använd avlusningskategori i taglib extrahering/skrivning
- Formatera fotons exponeringsavvikelsevärde (fel 343273)
- rätta egenskapsnamn
- Ta bort prefixet photo från alla exif-egenskapsnamn (fel 343273)
- Byt namn på egenskaper i ImageMake och ImageModel (fel 343273)
- [UserMetaData] Lägg till metod för att fråga efter vilka egenskaper som är angivna
- Formatera fokallängd som millimeter
- Formatera fotons exponeringstid som rationellt när tillämpligt (fel 343273)
- Aktivera usermetadatawritertest för alla UNIX-varianter, inte bara Linux
- Formatera bländarens värden som bländartal (fel 343273)

### KDE GUI Addons

- KModifierKeyInfo: vi delar den interna implementeringen
- Ta bort dubbla uppslagningar
- Flytta beslutet att använda X11 eller inte till körtid

### KHolidays

- Uppdatera allmän helgdag tidigt under maj 2020 i Storbritannien (fel 409189)
- Rätta ISO-kod för Hesse / Tyskland

### KImageFormats

- QImage::byteCount -&gt; QImage::sizeInByes

### KIO

- Rätta test av KFileItemTest::testIconNameForUrl för att reflektera annorlunda ikonnamn
- Rätta i18n fel för antal argument i knewfilemenu varningsmeddelande
- [ftp] Rätta felaktig åtkomsttid i Ftp::ftpCopyGet() (fel 374420)
- [CopyJob] Rapportera behandlat antal tillsammans
- [CopyJob] Rapportera resultat efter avsluta kopiering (fel 407656)
- Flytta redundant logik i KIO::iconNameForUrl() till KFileItem::iconName() (fel 356045)
- Installera KFileCustomDialog
- [Platspanel] Visa inte normalt Root
- Nedgradera dialogrutan "Kunde inte ändra rättigheter" till en qWarning
- O_PATH är bara tillgänglig på linux. För att förhindra kompilatorn från att avge ett fel
- Visa återkoppling på plats när nya filer eller kataloger skapas
- Auth-stöd: Släpp rättigheter om målet inte ägs av root
- [copyjob] Ställ bara in ändringstid om kio-slave tillhandahåller den (fel 374420)
- Avbryt rättighetsåtgärd för skrivskyddat mål med aktuell användare som ägare
- Lägg till KProtocolInfo::defaultMimetype
- Spara alltid visningsinställningar vid byte mellan ett visningsläge till ett annat
- Återställ exklusiv grupp för sortering av menyalternativ
- Visningslägen med Dolphin-stil i fildialogrutan (fel 86838)
- kio_ftp: förbättra felhantering när kopiering till FTP misslyckas
- kioexec: ändra skrämmande avlusningsmeddelande för fördröjd borttagning

### Kirigami

- [ActionTextField] Gör så att åtgärden glöder vid nedtryckning
- stöd textläge och position
- effekt för länkstig på skrivbordet när musen hålls över
- tvinga en minimal höjd av 2 rutnätsenheter
- Ställ in SwipeListItem implicitHeight att vara det maximala av innehåll och åtgärder
- Dölj verktygstips när PrivateActionToolButton trycks ner
- Ta bort oavsiktligt släppta bakåtspårningar av cmake-kod för Plasma-stil
- ColumnView::itemAt
- tvinga breeze-internal om inget tema är specificerat
- korrigera navigering för vänster fästa sida
- håll reda på utrymmet som täcks av fästa sidor
- visa en skiljelinje i vänster sidoradsläge
- med enkelt kolumnläge, har fäst ingen effekt
- första halvt fungerande prototyp av fästning

### KJobWidgets

- [KUiServerJobTracker] Hantera ändring av ägarskap

### KNewStuff

- [kmoretools] Lägg till ikoner för att åtgärderna download och install

### KNotification

- Sök inte efter phonon på Android

### KParts

- Lägg till gränssnitt för profilstöd i TerminalInterface

### Kör program

- Fördröj inte att skicka matchesChanged obegränsat

### KService

- Lägg till X-Flatpak-RenamedFrom som en igenkänd nyckel

### KTextEditor

- rätta gå till radcentrering (fel 408418)
- Rätta visning av bokmärkesikon på ikonkant vid låg upplösning
- Rätta åtgärden "Show Icon Border" att ändra kanten igen
- Rätta tomma sidor i utskriftsförhandsgranskning och rader som skrivs ut två gånger (fel 348598)
- ta bort sidhuvud som inte längre används
- Rätta hastighet för automatisk rullning neråt (fel 408874)
- Lägg till förvalda variabler för variabelgränssnitt
- Gör så att automatisk stavningskontroll fungerar efter att ha läst in ett dokument igen (fel 408291)
- öka förvald gräns för radlängd till 10000
- WIP:Inaktivera färgläggning efter 512 tecken på en rad
- KateModeMenuList: byt till QListView

### Kwayland

- Inkludera en beskrivning
- Bevis på konceptet för ett wayland-protokoll som tillåter att datagränssnittet tangenttillstånd fungerar

### KWidgetsAddons

- KPasswordLineEdit ärver nu focusPolicy från QLineEdit korrekt (fel 398275)
- Ersätt knappen "Details" med KCollapsibleGroupBox

### Plasma ramverk

- [Svg] Rätta konverteringsfel från QRegExp::exactMatch
- ContainmentAction: Rätta laddning från KPlugin
- [TabBar] Ta bort yttre marginaler
- Listningsmetoder för miniprogram, datagränssnitt och omgivning i Plasma::PluginLoader filtrerar inte längre insticksprogrammen med X-KDE-ParentAppprovided när en tom sträng skickas
- Gör så att knip fungerar igen i kalendern
- Lägg till disk-quota ikoner (fel 403506)
- Gör Plasma::Svg::elementRect lite smalare
- Ställ automatiskt in version för desktopthemes-paket till KF5_VERSION
- Underrätta inte om ändring till samma tillstånd som det hade
- Rätta justering av verktygsknappens beteckning
- [PlasmaComponents3] Centrera också knapptext vertikalt

### Syfte

- Ändra initial storlek på inställningsdialogrutan
- Förbättra ikoner och text för jobbdialogrutans knappar
- Rätta översättning av actiondisplay
- Visa inte felmeddelanden om delning avbryts av användaren
- Rätta varning när metadata för insticksprogram läses
- Konstruera om inställningssidor
- ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers
- phabricator: Rätta fall igenom i switch

### QQC2StyleBridge

- Visa genväg i menyalternativ när angivet (fel 405541)
- Lägg till MenuSeparator
- Rätta att ToolButton förblir i ett nedtryckt tillstånd efter nedtryckning
- [ToolButton] Skicka egen ikonstorlek till StyleItem
- ta hänsyn till synlighetspolicy (fel 407014)

### Solid

- [Fstab] Välj lämplig ikon för hem- eller rotkatalog
- [Fstab] Visa monterad "överlagrade" filsystem
- [UDev-gränssnitt] Smal enhet efterfrågad

### Syntaxfärgläggning

- Fortran: ändra licens till MIT
- Förbättra syntaxfärgläggning för Fortran fast formatsyntax
- Fortran: implementera fria och fasta format
- Rätta färgläggning av CMake COMMAND nästlade parenteser
- Lägg till fler nyckelord och stöd också rr i gdb-färgläggning
- Detektera kommentarrader tidigt i GDB-färgläggning
- AppArmor: uppdatera syntax
- Julia: uppdatera syntax och lägg till konstantnyckelord (fel 403901)
- CMake: Färglägg de vanliga CMake miljövariablerna
- Lägg till syntaxdefinition för ninja-byggen
- CMake: Stöd för funktioner i 3.15
- Jam: diverse förbättringar och rättningar
- Lua: uppdatera för Lua54 och slut av funktion som Keyword istället för Control
- C++: uppdatering för C++20
- debchangelog: lägg till Eoan Ermine

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
