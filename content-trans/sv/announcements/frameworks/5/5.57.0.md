---
aliases:
- ../../kde-frameworks-5.57.0
date: 2019-04-13
layout: framework
libCount: 70
---
### Attica

- Acceptera vilken HTTP-status som helst mellan 100 och 199 som godartad

### Baloo

- [DocumentIdDB] Tysta alla avlusningsmeddelanden som inte är fel, varna vid fel
- [baloosearch] Tillåt ange en tid när t.ex. mtime stöds
- [indexcleaner] Undvik borttagning av inkluderade kataloger under exkluderade
- [MTimeDB] Rätta uppslagning för intervallet LessEqual
- [MTimeDB] Rätta uppslagning när tidsintervall ska returnera tom mängd
- Korrigera assert/felhantering i MTimeDB
- Skydda mot ogiltiga överliggande objekt i IdTreeDB
- Ta också bort dokument från MTimeDB/DocumentTimeDB när tidsstämpeln är 0
- Var precisare med detektering av Mime-typ (fel 403902)
- [tidslinje] Gör webbadress kanonisk
- [tidslinje] Rätta saknade/felplacerade anrop till SlaveBase::finished()
- [balooshow] Utmatning av grundläggande filinformation för flera filändelser
- [tidslinje] Rätta varning, lägg till saknad UDS-post för "."
- [balooctl] Reducera nästlingsnivå för addOption arguments, städning
- Reagera på inställningsuppdateringar inne i indexering (fel 373430)
- Rätta regression när databas öppnas i läs-skriv läge (fel 405317)
- [balooctl] Städa efterföljande blanktecken
- [gränssnitt] Laga kod, ångra namnbyte av Transaction::abort()
- Harmonisera  hantering av understreck i frågetolken
- Baloo gränssnitt: behandla alla icke-lyckade koder som ett misslyckande (fel 403720)

### BluezQt

- Flytta Media-gränssnitt in i Adapter
- Manager: Kräv inte Media1-gränssnitt för initiering (fel 405478)
- Enhet: Kontrollera objektsökväg i gränssnitt slot removed (fel 403289)

### Breeze-ikoner

- Lägg till ikonerna "notifications" och "notifications-disabled" (fel 406121)
- gör start-here-kde också tillgänglig som start-here-kde-plasma
- Sublim Merge ikon
- Ge applications-games och input-gaming mer kontrast med Breeze mörk
- Gör 24 bildpunkters go-up verkligen till 24 bildpunkter
- Lägg till ikonerna preferences-desktop-theme-applications och preferences-desktop-theme-windowdecorations
- Lägg till symboliska länkar från "preferences-desktop-theme" till "preferences-desktop-theme-applications"
- Ta bort preferences-desktop-theme som förberedelse för att göra den till en symbolisk länk
- Lägg till collapse/expand-all, window-shade/unshade (fel 404344)
- Förbättra konsekvens för window-* och lägg till flera
- Gör så att go-bottom/first/last/top ser mer ut som media-skip*
- Ändra mål för symboliska länkarna go-up/down-search till go-up/down
- Förbättra justering till bildpunktsrutnät för go-up/down/next/previous/jump
- Ändra stil för media-skip* och media-seek*
- Tvinga ny dämpad ikonstil för alla åtgärdsikoner

### Extra CMake-moduler

- Aktivera tilldelning av QT_PLUGIN_PATH igen
- ecm_add_wayland_client_protocol: Förbättra felmeddelande
- ECMGeneratePkgConfigFile: gör alla variabler beroende på ${prefix}
- Lägg till UDev sökmodul
- ECMGeneratePkgConfigFile: lägg till variabler använda av pkg_check_modules
- Återställ FindFontconfig bakåtkompatibilitet för plasma-desktop
- Lägg till Fontconfig sökmodul

### Integrering med ramverk

- använd lämpligare plasma-specifik ikon för plasmakategori
- använd plasma ikon som ikon för plasma underrättelsekategori

### KDE Doxygen-verktyg

- Uppdatera webbadresser att använda https

### KArchive

- Rätta krasch i KArchive::findOrCreate med felaktiga filer
- Rätta läsning av oinitierat minne i KZip
- Lägg till Q_OBJECT i KFilterDev

### KCMUtils

- [KCModuleLoader] Skicka argument till skapad KQuickAddons::ConfigModule
- Skicka fokus till underliggande sökrad när KPluginSelector är fokuserad (fel 399516)
- Förbättra felmeddelande för inställningsmodul
- Lägg till skydd vid körtid att sidor är inställningsmoduler i KCMultiDialog (fel 405440)

### KCompletion

- Tilldela inte en null komplettering för en icke-redigerbar kombinationsruta

### KConfig

- Lägg till Notify möjlighet i revertToDefault
- peka readme på wiki-sidan
- kconfig_compiler: ny kcfgc args HeaderExtension och SourceExtension
- [kconf_update] ändra från egen loggningsteknologi till qCDebug
- Ta bort referens från const KConfigIniBackend::BufferFragment &amp;
- KCONFIG_ADD_KCFG_FILES makro: säkerställ att en ändring av File= i kcfg plockas upp

### KCoreAddons

- Rätta "* något *" vi vill inte använda fetstil för strängen
- Rätta fel 401996 - klicka kontaktwebbadress =&gt; ofullständig webbadress markeras (fel 401996)
- Skriv ut strerror när inotify misslyckas (typisk orsak: "too many open files")

### KDBusAddons

- Konvertera två anslutningar med gammal stil till ny stil

### KDeclarative

- [GridViewKCM] Rätta implicit breddberäkning
- flytta gridview till en separat fil
- Undvik bråkdelar i GridDelegate storlekar och justeringar

### Stöd för KDELibs 4

- Ta bort hitta moduler som tillhandahålls av ECM

### KDocTools

- Uppdatera ukrainsk översättning
- Katalanska uppdateringar
- it-entiteter: uppdatera webbadresser att använda https
- Uppdatera webbadresser att använda https
- Använd indonesisk översättning
- Uppdatera design så att den är mer liknade kde.org
- Lägg till nödvändiga filer för att använda inhemskt indonesiskt språk för alla indonesiska dokument

### KFileMetaData

- Implementera stöd för att skriva betygsinformation för taglib writer
- Implementera fler taggar för taglib writer
- Skriv om taglib writer att använda egenskapsgränssnitt
- Testa ffmpeg extrahering genom att använda Mime-typ hjälpfunktion (fel 399650)
- Föreslå Stefan Bruns som underhållsansvarig av KFileMetaData
- Deklarera PropertyInfo som QMetaType
- Skydda mot ogiltig filer
- [TagLibExtractor] Använd korrekt Mime-typ i fallet med arv
- Lägg till hjälpfunktion för att bestämma verklig överliggande Mime-typ som stöds
- [taglibextractor] Testa extrahering av egenskaper med flera värden
- Generera deklarationsfiler för ny MimeUtils
- Använd Qt-funktioner för formatering av stränglistor
- Rätta landsinställningar för nummeregenskaper
- Verifiera Mime-typer för alla befintliga exempelfiler, lägg till några fler
- Lägg till hjälpfunktion för att bestämma Mime-typ baserat på innehåll och filändelse (fel 403902)
- Lägg till stöd för att extrahera data från ogg och dess filer (fel 399650)
- [ffmpegextractor] Lägg till testfall för Matroska Video (fel 403902)
- Skriv om taglib extrahering för att använda det generella PropertyMap gränssnittet (fel 403902)
- [ExtractorCollection] Ladda insticksprogram för extrahering vid behov
- Rätta extrahering av proportionsegenskap
- Öka precision för bildfrekvensegenskapen

### KHolidays

- Sortera de polska helgkategorierna

### KI18n

- Rapportera mänskligt läsbart fel om Qt5Widgets krävs men inte hittas

### KIconThemes

- Rätta vadderingsikon som inte exakt motsvarar den begärda storleken (fel 396990)

### KImageFormats

- ora:kra: qstrcmp -&gt; memcmp
- Rätta RGBHandler::canRead
- xcf: Krascha inte med filer som har lagerlägen som inte stöds

### KIO

- Ersätt currentDateTimeUtc().toTime_t() med currentSecsSinceEpoch()
- Ersätt QDateTime::to_Time_t/from_Time_t med to/fromSecsSinceEpoch
- Förbättra ikoner för dialogrutans knappar (fel 406090)
- [KDirOperator] Visa normalt detaljerad trädvy
- KFileItem: anropa stat() vid behov, lägg till alternativet SkipMimeTypeDetermination
- KIOExec: rätta fel när fjärrwebbadresser inte har något filnamn
- KFileWidget i läget för att spara enstaka fil ger ett tryck på enter/returtangenten på KDirOperator utlöser slotOk (fel 385189)
- [KDynamicJobTracker] Använd genererat D-Bus gränssnitt
- [KFileWidget] Vid spara, markera filnamn efter klick på befintlig fil också när dubbelklick används
- Skapa inte miniatyrbilder för krypterade valv (fel 404750)
- Rätta namnbyte av WebDAV-katalog om KeepAlive är av
- Visa lista över etiketter i PlacesView (fel 182367)
- Bekräftelsedialogruta för Ta bort/Flytta till papperskorg: Rätta vilseledande rubrik
- Visa korrekt fil/sökväg i felmeddelandet "too bit for fat32" (fel 405360)
- Formulera felmeddelande med GiB, inte GB (fel 405445)
- openwithdialog: använd rekursiv flagga som proxyfilter
- Ta bort hämtning av webbadresser när listningsjobb är färdigt (fel 383534)
- [CopyJob] Behandla webbadress som smutsig vid namnbyte av fil som konfliktupplösning
- Skicka lokal filsökväg till KFileSystemType::fileSystemType()
- Rätta namnbyte mellan små och stora bokstäver på skiftlägesokänsliga filsystem
- Rätta varningar som "Invalid URL: QUrl("fil.txt")" i dialogrutor för att spara (fel 373119)
- Rätta krasch när filer flyttas
- Rätta NTFS dold check för symbolisk länk till NTFS monteringspunkter (fel 402738)
- Gör överskrivning av fil något säkrare (fel 125102)

### Kirigami

- rätta implicitWidth för listobjekt
- shannon-entropi för att gissa svartvit ikon
- Förhindra att sammanhangslåda försvinner
- ta bort actionmenuitembase
- försök inte hämta versionen för statiska byggen
- [Mnemonikhantering] Ersätt bara första förekomsten
- synkronisera när någon modellegenskap uppdateras
- använd icon.name i bakåt/framåt
- rätta verktygsrader för lager
- Rätta fel i kirigami exempelfiler
- Lägg till SearchField och PasswordField komponent
- rätta greppikoner (fel 404714)
- [InlineMessage] Rita inte skuggor omkring meddelandena
- layout omedelbart när ordning ändras
- rätta layout för länkstig
- visa aldrig verktygsrad när det aktuella objektet ber att det inte ska göras
- hantera dessutom bakåt/framåt i filtret
- stöd bakåt/framåt musknappar
- Lägg till lat instansiering för undermenyer
- rätta verktygsrader för lager
- kirigami_package_breeze_icons: Sök dessutom bland storlek 16 ikoner
- Rätta Qmake-baserat bygge
- hämta tillhörande egenskap för det riktiga objektet
- rätta logik när verktygsrad ska visas
- möjligt att inaktivera verktygsrad för lagrens sidor
- visa alltid globala verktygsrad för globala lägen
- Page.contextualActionsAboutToShow signal
- en del utrymme till höger om titeln
- vidarebefordra ut när synlighet ändras
- ActionTextField: Placera åtgärder riktigt
- topPadding och BottomPadding
- text på bilder måste alltid vara vit (fel 394960)
- beskär överlagringsblad (fel 402280)
- undvik att använda ColumnView som överliggande objekt till OverlaySheet
- använd en qpointer för temainstansen (fel 404505)
- dölj länkstig på sidor som inte vill ha en verktygsrad (fel 404481)
- försök inte överskrida den aktiverade egenskapen (fel 404114)
- Möjlighet för eget sidhuvud och egen sidfot i ContextDrawer (fel 404978)

### KJobWidgets

- [KUiServerJobTracker] Uppdatera destUrl innan jobbet avslutas

### KNewStuff

- Ändra webbadresser till https
- Uppdatera länk till fsearch-projekt
- Hantera OCS-kommandon som inte stöds, och överrösta inte (fel 391111)
- Ny plats för KNSRC-filer
- [knewstuff] Ta bort qt5.13 metod som avråds från

### KNotification

- [KStatusNotifierItem] Skicka desktop-entry tips
- Tillåt att ange egna tips för underrättelser

### KNotifyConfig

- Tillåt att bara välja ljudfiler som stöds (fel 405470)

### Ramverket KPackage

- Rätta hitta värddatorns verktygsmålfil i Android dockningsmiljö
- Lägg till korskompileringsstöd för kpackagetool5

### KService

- Lägg till X-GNOME-UsesNotifications som en igenkänd nyckel
- Lägg till minimal version för bison 2.4.1 på grund av %code

### KTextEditor

- Rättning: verkställ textfärgerna för det valda schemat korrekt (fel 398758)
- DocumentPrivate: Lägg till alternativet "Auto Reload Document" i menyn Visa (fel 384384)
- DocumentPrivate: Stöd för att ange ordlista för blockmarkering
- Rätta ord- och teckensträng på katestatusbar
- Rätta Minimap med QtCurve stil
- KateStatusBar: Visa låsikon för ändrad beteckning i skrivskyddat läge
- DocumentPrivate: Hoppa över automatiska citationstecken när de redan ser balanserade ut (fel 382960)
- Lägg till variabelgränssnitt i KTextEditor::Editor
- lätta på kod för att bara göra assert i avlusningsbygge, fungera i utgivningsbygge
- säkerställ kompatibilitet med gamla inställningar
- mer användning av generellt inställningsgränssnitt
- förenkla QString KateDocumentConfig::eolString()
- överför sonnet-inställningar till KTextEditor-inställning
- säkerställ nu gap i inställningsnycklar
- konvertera mer saker till generellt inställningsgränssnitt
- mer användning av det generella inställningsgränssnittet
- generellt inställningsgränssnitt
- Krascha inte för felaktiga syntaxfärgläggningsfiler
- IconBorder: Acceptera dra-och-släpp händelser (fel 405280)
- ViewPrivate: Gör avmarkering med piltangenter bekvämare (fel 296500)
- Rättning för att visa tipsträd för argument på icke-primär skärm
- Konvertera några metoder som avråds från
- Återställ sökomgivna meddelandet till dess föregående typ och position (fel 398731)
- ViewPrivate: Gör så att 'Apply Word Wrap' är bekvämare (fel 381985)
- ModeBase::goToPos: Säkerställ att hoppmålet är giltigt (fel 377200)
- ViInputMode: Ta bort textegenskaper som inte stöds från statusraden
- KateStatusBar: Lägg till ordlisteknapp
- lägg till exempel på radhöjdsproblem

### KWidgetsAddons

- Gör KFontRequester konsekvent
- Uppdatera kcharselect-data till Unicode 12.0

### KWindowSystem

- Skicka suddighet/bakgrundskontrast i enhetens bildpunkter (fel 404923)

### NetworkManagerQt

- WireGuard: gör så att kodning/avkodning av hemligheter från avbildning fungerar
- Lägg till saknat stöd för WireGuard i basinställningsklass
- Wireguard: hantera privat nyckel som hemligheter
- Wireguard: egenskapen peer ska vara NMVariantMapList
- Lägg till stöd för Wireguard anslutningstyp
- ActiveConnecton: lägg till signalen stateChangedReason där vi kan se orsaken till en tillståndsändring

### Plasma ramverk

- [AppletInterface] Titta efter corona innan försök att komma åt den
- [Dialogruta] Skicka inte vidare hållhändelse när det inte finns någonstans att skicka den till
- [Menu] Rätta utlösningssignal
- Reducera vikten hos viss felsökningsinformation så verkliga varningar går att se
- [PlasmaComponents3 ComboBox] Rätta textColor
- FrameSvgItem: fånga också marginaländringar för FrameSvg utanför egna metoder
- Lägg till Theme::blurBehindEnabled()
- FrameSvgItem: rätta textureRect för delobjekt sida vid sida så den inte krymper till 0
- Rätta bakgrund för breeze dialogruta med Qt 5.12.2 (fel 405548)
- Ta bort krasch i plasmashell
- [Icon Item] Rensa också bildikon när Plasma Svg används (fel 405298)
- testfältshöjd bara baserad på klar text (fel 399155)
- bind alternateBackgroundColor

### Syfte

- Lägg till KDE-anslut SMS-insticksprogram

### QQC2StyleBridge

- plasma skrivbordsstil stöder ikonfärgläggning
- [SpinBox] Förbättra mushjulets beteende
- lägg till en del vaddering i verktygsrader
- rätta RoundButton ikoner
- rullningslistbaserad vaddering för alla delegater
- sök efter en scrollview för att använda dess rullningslist för marginaler

### Solid

- Tillåt att bygga utan UDev på Linux
- Hämta bara clearTextPath när använd

### Syntaxfärgläggning

- Lägg till syntaxdefinition för språket Elm i syntaxfärgläggning
- AppArmor och SELinux: ta bort en indentering i XML-filer
- Doxygen: använd inte svart färg i etiketter
- Tillåt sammanhangsbyten för radslut på tomma rader (fel 405903)
- Rätta vikning av endRegion i regler med beginRegion+endRegion (använd length=0) (fel 405585)
- Lägg till utökningar i färgläggning av groovy (fel 403072)
- Lägg till Smali syntaxfärgläggningsfil
- Lägg till "." som weakDeliminator i Octave syntaxfil
- Logcat: rätta dsError färg med underline="0"
- rätta krasch av färgläggning för felaktig färgläggningsfil
- skydda målets länkbibliotek för äldre CMake version (fel 404835)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
