---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nytt ramverk: KActivitiesStats, ett bibliotek för att komma åt statistisk användningsdata insamlad av KDE:s aktivitetshanterare.

### Alla ramverk

Qt ≥ 5.4 krävs nu, dvs. Qt 5.3 stöds inte längre.

### Attica

- Lägg till const variant av hämtningsmetod

### Baloo

- Centralisera omgångsstorlek i inställning
- Ta bort kod som blockerar indexering av text/plain-filer utan filändelse .txt (fel 358098)
- Kontrollera både filnamn och filinnehåll för att bestämma Mime-type (fel 353512)

### BluezQt

- ObexManager: Dela felmeddelanden för saknade objekt

### Breeze-ikoner

- Lägg till Breeze-ikoner för lokalize
- Synkronisera programikoner mellan Breeze och Breeze mörk
- Uppdatera temaikoner och ta bort program-system rättning av ikoner för Kicker-grupper
- Lägg till XPI-stöd för Firefox tillägg (fel 359913)
- Uppdatera Okular-ikon till den rätta
- Lägg till stöd för ktnef programikon
- Lägg till kmenueditor, kmouse och knotes ikoner
- Ändra ikon att använda för ljudvolym tyst, för tyst istället för bara röd färg (fel 360953)
- Lägg till stöd för djvu Mime-typ (fel 360136)
- Lägg till länk istället för dubbel post
- Lägg till ms-shortcut ikon för gnucash (fel 360776)
- Ändra skrivbordsunderläggets bakgrund till en generell
- Uppdatera ikoner att använda ett generellt skrivbordsunderlägg
- Lägg till ikon för Konqueror (fel 360304)
- Lägg till process-arbetar ikon för förloppsanimering i KDE (fel 360304)
- Lägg till ikon för programvaruinstallation och uppdatera uppdateringsikon med rätt färg
- Lägg till och ta bort emblem-ikoner för markering i Dolphin, lägg till monteringsikon
- Ta bort stilmall från ikoner för miniprogrammen analogclock och kickerdash
- Synkronisera Breeze och Breeze mörk (fel 360294)

### Extra CMake-moduler

- Rätta _ecm_update_iconcache för att bara uppdatera installationsplatsen
- Återställ "ECMQtDeclareLoggingCategory: Inkludera &lt;QDebug&gt; med den genererade filen"

### Integrering med ramverk

- Använd implementeringen i QCommonStyle för standardikon som reserv
- Ange en standardtidsgräns för menystängning

### KActivities

- Ta bort kompilatorkontroller att alla ramverk nu kräver c++11
- Ta bort QML ResourceModel eftersom den överskrids av KAStats::ResultModel
- Att infoga i tom QFlatSet returnerade en ogiltig iterator

### KCodecs

- Förenkla kod (qCount -&gt; std::count, hemmagjord isprint -&gt; QChar::isPrint)
- Detektering av kodning: Rätta krasch för fel användning av isprint (fel 357341)
- Rätta krasch på grund av oinitierad variabel (fel 357341)

### KCompletion

- KCompletionBox: Tvinga användning av fönster utan ram och ge inte fokus
- KCompletionBox ska *inte* vara ett verktygstips

### KConfig

- Lägg till stöd för att hämta platser från QStandardPaths i skrivbordsfiler

### KCoreAddons

- Rätta kcoreaddons_desktop_to_json() på Windows
- src/lib/CMakeLists.txt - Rätta länkning till ett Threads-bibliotek
- Lägg till anslutningar för att tillåta kompilering på Android

### KDBusAddons

- Undvik introspektion av DBus-gränssnitt när det inte används

### KDeclarative

- Likformig användning av std::numeric_limits
- [DeclarativeDragArea] Överskrid inte "text" i Mime-data

### Stöd för KDELibs 4

- Rätta föråldrad länk i kdebugdialog5 docbook
- Läck inte Qt5::Network som ett nödvändigt bibliotek för resten av ConfigureChecks

### KDESU

- Ställ in funktionsmakron för att möjliggöra att bygga med musl libc

### KEmoticons

- KEmoticons: Rätta krasch när loadProvider misslyckas av någon orsak

### KGlobalAccel

- Gör kglobalaccel5 möjlig att döda på ett riktigt sätt, för att rätta extremt långsam avstängning

### KI18n

- Använd språk från Qt:s systemlandsinställningar som reserv på annat än Unix

### KInit

- Städa upp och omstrukturera konverteringen av klauncher till XCB

### KIO

- FavIconsCache: Synkronisera efter skrivning, så att andra program ser det, och för att undvika krasch vid destruering
- Rätta många trådhanteringsproblem i KUrlCompletion
- Rätta krasch i dialogruta för namnbyte (fel 360488)
- KOpenWithDialog: Förbättra fönsterrubrik och beskrivningstext (fel 359233)
- Tillåt bättre plattformsoberoende driftsättning av I/O-slavar genom att lägga in protokollinformation i insticksprogrammens metadata

### KItemModels

- KSelectionProxyModel: Förenkla hantering av radborttagning, förenkla avmarkeringslogik
- KSelectionProxyModel: Återskapa bara avbildning vid borttagning vid behov (fel 352369)
- KSelectionProxyModel: Rensa bara avbildningar för firstChild på toppnivå
- KSelectionProxyModel: Säkerställ riktig signalering när senast markerade tas bort
- Gör DynamicTreeModel sökbar enligt visningsroll

### KNewStuff

- Krascha inte om .desktop-filer saknas eller är felaktiga

### KNotification

- Hantera klick med vänsterknapp på föråldrade ikoner i systembrickan (fel 358589)
- Använd bara X11BypassWindowManagerHint på X11-plattform

### Paketet Framework

- Efter att ett paket har installerats, läs in det
- Misslyckas inte om paketet finns och är aktuellt
- Lägg till Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Använd kontaktwebbadressen som personwebbadress i PersonData när det inte finns någon person
- Ange ett namn på databasanslutningen

### Kör program

- Importera mall för körprogram från KAppTemplate

### KService

- Rätta ny varning från kbuildsycoca, när en Mime-typ ärver från ett alias
- Rätta hantering av x-scheme-handler/* i mimeapps.list
- Rätta hantering av x-scheme-handler/* vid tolkning av mimeapps.list (fel 358159)

### KTextEditor

- Återställ "Öppna/spara inställningssida: Använd termen "Folder" istället för "Directory""
- Tvinga användning av UTF-8
- Öppna/spara inställningssida: Använd termen "Folder" istället för "Directory"
- kateschemaconfig.cpp: Använd riktiga filter med dialogrutorna öppna/spara (fel 343327)
- c.xml: använd standardstil för kontrollflödesnyckelord
- isocpp.xml: använd standardstil "dsControlFlow" för kontrollflödesnyckelord
- c/isocpp: Lägg till fler C-standardtyper
- KateRenderer::lineHeight() returnerar ett heltal
- Utskrift: Använd teckenstorlek från valt utskriftsschema (fel 356110)
- Uppsnabbning av cmake.xml: Använd WordDetect istället för RegExpr
- Ändra tabulatorbredd till 4 istället för 8
- Rätta ändring av färg på aktuellt radnummer
- Rätta val av kompletteringspost med musen (fel 307052)
- Lägg till syntaxfärgläggning för gcode
- Rätta uppritning av bakgrund för MiniMap-markering
- Rätta kodning av gap.xml (använd UTF-8)
- Rätta nästlade kommentarblock (fel 358692)

### KWidgetsAddons

- Ta hänsyn till innehållets marginaler när storlekstips beräknas

### KXMLGUI

- Rätta att redigeringsverktygsrader förlorar instoppade åtgärder

### NetworkManagerQt

- ConnectionSettings: Initiera tidsgräns för ping av förmedlingsnod
- Ny TunSetting och Tun anslutningstyp
- Skapa enheter för alla kända typer

### Oxygen-ikoner

- Installera index.theme i samma katalog som den alltid har funnits i
- Installera i oxygen/base/ så att ikoner som flyttade från program inte kolliderar med versioner installerade av programmen
- Duplicera symboliska länkar från breeze-icons
- Lägg till nya ikoner emblem-added och emblem-remove för synkronisering med Breeze

### Plasma ramverk

- [kalender] Rätta att kalenderminiprogrammet inte rensar markeringen när det döljes (fel 360683)
- Uppdatera ljudikon att använda stilmall
- Uppdatera tysta ljud ikon (fel 360953)
- Rätta att miniprogram tvingas skapas när Plasma är oföränderligt
- [Borttonande nod] Blanda inte ogenomskinlighet separat (fel 355894)
- [SVG] Tolka inte om konfigurationen som svar på Theme::applicationPaletteChanged
- Dialogruta: Ställ in tillstånd SkipTaskbar/Pager innan fönster visas (fel 332024)
- Återintroducera egenskapen busy i miniprogram
- Säkerställ att PlasmaQuick exportfil hittas på ett riktigt sätt
- Importera inte en icke existerande layout
- Gör det möjligt för ett miniprogram att erbjuda ett testobjekt
- Ersätt QMenu::exec med QMenu::popup
- FrameSvg: Rätta felaktiga pekare i sharedFrames när temat ändras
- IconItem: Schemalägg uppdatering av bildpunktsavbildning när fönster ändras
- IconItem: Animera aktiv och aktiverad ändring även när animeringar är inaktiverade
- DaysModel: Gör update till en slot
- [Icon Item] Animera inte från föregående punktavbildning när den har blivit osynlig
- [Icon Item] Anropa inte loadPixmap i setColorGroup
- [Miniprogram] Skriv inte över flaggan "Persistent" för underrättelser om ångra
- Tillåt att förändringsinställning i Plasma överskrids när omgivning skapas
- Lägg till icon/titleChanged
- Ta bort beroende av QtScript
- Deklarationsfil för plasmaquick_export.h finns i katalogen plasmaquick
- Installera några deklarationsfiler för plasmaquick

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
