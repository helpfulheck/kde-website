---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE levererar KDE-program 18.04.0
layout: application
title: KDE levererar KDE-program 18.04.0
version: 18.04.0
---
19:e april, 2018. KDE-program 18.04.0 är nu utgivna.

Vi arbetar kontinuerligt på att förbättra programvaran som ingår i vår KDE-programserie, och vi hoppas att du finner alla nya förbättringar och felrättningar användbara.

## Vad är nytt i KDE-program 18.04

### System

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

Den första huvudutgåvan under 2018 av <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE:s kraftfulla filhanterare, har många förbättringar av panelerna:

- Sektionerna i panelen 'Platser' kan nu döljas om du fördrar att inte visa dem, och den nya sektionen 'Nätverk' är nu tillgänglig för att samla poster för platser på andra datorer.
- Panelen 'Terminal' kan dockas på vilken sida som helst av fönstret, och om du försöker öppna den när Terminal inte är installerad, visar Dolphin en varning och hjälper till att installera den.
- Stödet för hög upplösning i panelen 'Information' har förbättrats.

Katalogvyn och menyerna har också uppdaterats:

- Papperskorgskatalogen visar nu knappen 'Töm papperskorgen'.
- Menyalternativet 'Visa mål' har lagts till för att hjälpa till att hitta mål för symboliska länkar.
- Integrering med Git har förbättrats, eftersom den sammanhangsberoende menyn för Git-kataloger nu visar två nya alternativ för 'git log' och 'git merge'.

Ytterligare förbättringar omfattar:

- En ny genväg har lagts till som ger möjlighet att öppna filterraden genom att helt enkelt trycka på snedstreckstangenten (/).
- Det går nu att sortera och organisera foton enligt datum då de togs.
- Att dra och släppa många små filer inne i Dolphin är nu snabbare, och användare kan ångra namnbytesjobb i bakgrunden.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

För att göra det ännu njutbarare att arbeta med kommandoraden, kan nu <a href='https://www.kde.org/applications/system/konsole/'>Terminal</a>, KDE:s terminalemulator, se ännu snyggare ut:

- Det går att ladda ner färgscheman via Heta nyheter.
- Rullningslisten smälter in bättre med det aktiva färgschemat.
- Normalt visas flikraden bara när det behövs.

Bidragsgivarna till Terminal slutade inte där, utan introducerade många nya funktioner:

- Ett nytt skrivskyddat läge och en profilegenskap för att sätta på eller stänga av kopiering av text som HTML har lagts till.
- Med Wayland stöder nu Terminal dra-och-släpp menyn.
- Flera förbättringar har skett avseende protokollet ZMODEM: Terminal kan nu hantera zmodem-uppladdningsindikeringen B01, och visar förloppet medan data överförs, knappen Avbryt i dialogrutan fungerar nu som den ska, och överföring av större filer har bättre stöd genom att läsa in dem i minnet 1 MiB i taget.

Ytterligare förbättringar omfattar:

- Mushjulsrullning med libinput har rättats, och nu förhindras att gå igenom skalhistoriken vid rullning med mushjulet .
- Sökningar uppdateras efter att ha ändrat alternativet för att sökningen ska matcha reguljära uttryck, och när 'Ctrl'+Baksteg används matchar Terminal beteendet hos xterm.
- Genvägen '--background-mode' har rättats.

### Multimedia

<a href='https://juk.kde.org/'>JuK</a>, KDE:s musikspelare, har nu stöd för Wayland. Nya funktioner i användargränssnittet inkluderar möjlighet att dölja menyraden och att få en visuell indikering av spåret som för närvarande spelas. När dockning i systembrickan är inaktiverad, kraschar inte JuK längre vid försök att avsluta via fönstrets stängningsikon, och användargränssnittet förblir synligt. Fel avseende att JuK automatiskt börjar spela vid återstart efter viloläge i Plasma 5, och hantering av spellistans kolumn har också rättats.

Bidragsgivare till <a href='https://kdenlive.org/'>Kdenlive</a>, KDE's ickelinjära videoeditor, fokuserade på underhåll för utgåva 18.04:

- Storleksändring av klipp skadar inte längre borttoningar och nyckelbilder.
- Användare av skärmskalning på bildskärmar med hög upplösning kan njuta av skarpare ikoner.
- En möjlig krasch vid start för vissa konfigurationer har rättats.
- MLT måste nu ha version 6.6.0 eller senare, och kompatibilitet har förbättrats.

#### Grafik

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Under de senaste månaderna har bidragsgivare till <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE:s bildvisare och organisatör, arbetat med en uppsjö olika förbättringar. Nämnvärda är:

- Stöd för MPRIS-kontroll har lagts till så att man nu kan styra fullständiga skärmbildspel via KDE-anslut, tangentbordets mediatangenter och Plasmoiden mediaspelare.
- Miniatyrbildsknapparna som visas när musen hålls över kan nu stängas av.
- Beskärningsverktyget har förbättrats på flera sätt, eftersom dess inställningar koms ihåg vid byte till en annan bild, formen på markeringsrutan kan nu låsas genom att hålla nere 'Skift'- och 'Ctrl'-tangenterna och den kan också låsas till proportionen för bilden som för närvarande visas.
- I fullskärmsläge går det nu att avsluta genom att använda tangenten 'Escape', och färgpaletten motsvarar det aktiva färgtemat. Om Gwenview avslutas i detta läge, koms inställningen ihåg och den nya sessionen startar också i fullskärmsläge.

Omsorg om detaljer är viktigt, alltså har Gwenview finputsats på följande områden:

- Gwenview visar mer läsbara filsökvägar i listan 'Senaste kataloger', visa riktiga sammanhangsberoende menyer för alternativ i listan 'Senaste filer', och glömma bort allt på rätt sätt när funktionen 'Inaktivera historik' används.
- Att klicka på en katalog i sidoraden gör det möjligt att växla mellan bläddrings- och visningsläge, och kommer ihåg senast använda läge vid byte mellan kataloger, och tillåter på så sätt snabbare navigering i mycket stora bildsamlingar.
- Tangentbordsnavigering har strömlinjeformats ytterligare genom att indikera tangentbordsfokus på riktigt sätt i bläddringsläge.
- Funktionen 'Anpassa till bredd' har ersatts med en generaliserad 'Fyllfunktion'.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Även mindre förbättringar kan ofta göra användarens arbetsflöde mer njutbart:

- För att vara följdriktigare, förstoras nu SVG-bilder som alla andra bilder när 'Bildvy → Förstora mindre bilder' är aktiverad.
- Efter att ha redigerat en bild eller ångrat ändringar, blir inte bildvyn och miniatyrbilden osynkroniserade längre.
- Vid namnbyte av bilder, avmarkeras filnamnsändelsen normalt, och dialogrutan 'Flytta/Kopiera/Länka' visar nu normalt nuvarande katalog.
- Många visuella irritationsmoment har rättats, t.ex. i webbadressraden, verktygsraden för fullskärmsläge, och animering av miniatyrbildernas verktygstips. Saknade ikoner har också lagts till.
- Sist men inte minst, visas bilden direkt med fullskärmsknappen istället för att visa katalogens innehåll, och de avancerade inställningarna tillåter bättre kontroll över ICC färgåtergivningsalternativ.

#### Kontor

I <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE:s universella dokumentvisare, kan nu PDF-återgivning och textextrahering avbrytas om poppler version 0.63 eller senare används, vilket betyder att om man har en komplex PDF-fil och zoomning ändras medan den håller på att återges, avbryts den omedelbart istället för att vänta på att återgivningen blir färdig.

Det finns förbättrat PDF Javascript-stöd för AFSimple Calculate, och om poppler version 0.64 eller senare används, stöder Okular PDF Javascript-ändringar i formulär med skrivskyddat tillstånd.

Hantering av bokningsbekräftelsebrev i <a href='https://www.kde.org/applications/internet/kmail/'>Kmail</a>, KDE:s kraftfulla e-postprogram, har avsevärt förbättrats för att stödja tågbokningar och använder en Wikidata-baserad flygplatsdatabas för att visa flyg med rätt tidszoninformation. För att förenkla saker och ting, har ett nytt extraheringsverktyg implementerats för e-post som inte innehåller strukturerad bokningsinformation.

Ytterligare förbättringar omfattar:

- Brevstukturen kan återigen visas med det nya expert-insticksprogrammet.
- Ett insticksprogram har lagts till i Sieve-editorn för att välja e-post från Akonadi-databasen.
- Textsökning i editorn har förbättrats med stöd för reguljära uttryck.

#### Verktyg

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Att förbättra användargränssnittet för <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE:s mångsidiga skärmbildsverktyg, har stått i fokus:

- Nedre radens knappar har setts över, och visar nu en knapp för att öppna inställningsfönstret och en ny verktygsknapp som visar sätt att öppna den senast använda skärmbildskatalogen och starta ett program för skärminspelning.
- Den senast använda metoden för att spara koms nu normalt ihåg.
- Fönsterstorleken anpassas nu till skärmbildens proportion, vilket ger en behagligare och utrymmeseffektivare miniatyrbild av skärmbilden.
- Inställningsfönstret har avsevärt förenklats.

Dessutom kommer användare att kunna förenkla sitt arbetsflöde med följande nya funktioner:

- När ett specifikt fönster har lagrats, kan dess rubrik automatisk läggas till i skärmbildsfilens namn.
- Användaren kan välja om Spectacle automatiskt avslutas eller inte efter en åtgärd som sparar eller kopierar.

Viktiga felrättningar omfattar:

- Dra och släpp i Chromium-fönster fungerar nu som förväntat.
- Upprepad användning av snabbtangenter för att spara en skärmbild resulterar inte längre i en tvetydig genvägsvarningsdialogruta.
- För skärmbilder av rektangulära områden kan markeringens nederkant justeras noggrannare.
- Förbättrad tillförlitlighet vid lagring av fönster som berör skärmkanter när sammansättning är avstängd.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

Med <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, KDE:s certifikathanterare och universella grafiska krypteringsgränssnitt, kan Curve 25519 EdDSA-nycklar genereras när det används med en nyare version av GnuPG. En 'anteckningsblocksvy' har lagts till för textbaserade krypteringsåtgärder, och det går nu att signera/kryptera och avkryptera/verifiera direkt i programmet. I ' certifikatinformationsvyn' finns nu ett exportåtgärd, som kan användas för att exportera som text för att kopiera och klistra in. Dessutom går det att importera resultatet via den nya 'anteckningsblockvyn'.

I <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE:s grafiska filkomprimerings- och uppackningsverktyg med stöd för många format, är det nu möjligt att stoppa komprimering eller uppackning när libzip-gränssnittet används för ZIP-arkiv.

### Program som går med i KDE-programs utgivningstidplan

KDE:s webbkamerainspelare <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> och säkerhetskopieringsprogrammet <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> kommer nu att följa utgivningarna av Program. Direktmeddelandeprogrammet <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> återintroduceras också efter att ha konverterats till KDE Ramverk 5.

### Program går till sina egna utgivningstidplaner

Hexeditorn <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> kommer att ha en egen utgivningstidplan på begäran av underhållsansvarig.

### Felutplåning

Mer än 170 fel har rättats i program, inklusive Kontact-sviten, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello med flera.

### Fullständig ändringslogg
