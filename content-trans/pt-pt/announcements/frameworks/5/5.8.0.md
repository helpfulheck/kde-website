---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novas plataformas:

- KPeople, que oferece o acesso a todos os contactos e as pessoas que os detêm
- KXmlRpcClient, para interagir com serviços XMLRPC

### Geral

- Um conjunto de correcções de compilação para compilar com a próxima versão do Qt 5.5

### KActivities

- O serviço de classificação dos recursos terminou entretanto

### KArchive

- Terminaram os erros nos ficheiros ZIP com descritores de dados redundantes

### KCMUtils

- Reposição do KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: adição do suporte para a chave Hidden

### KDeclarative

- Preferência da exposição das listas para QML com o QJsonArray
- Tratamento de 'devicePixelRatios' não-predefinidos nas imagens
- Exposição do 'hasUrls' no DeclarativeMimeData
- Permitir aos utilizadores configurar quantas linhas horizontais são desenhadas

### KDocTools

- Correcção da compilação no MacOS X quando é usado o Homebrew
- Melhores estilos dos objectos multimédia (imagens, ...) na documentação
- Codificação dos caracteres inválidos nas localizações usadas nas DTD's de XML, evitando alguns erros

### KGlobalAccel

- Tempo da activação definido como propriedade dinâmica na QAction activada.

### KIconThemes

- Correcção do QIcon::fromTheme(xxx, valorSubstituição), que não iria devolver o valor de substituição

### KImageFormats

- O leitor de imagens PSD agora é agnóstico em relação à ordem dos 'bytes'.

### KIO

- Descontinuação do UDSEntry::listFields e adição do método UDSEntry::fields, que devolve um QVector sem conversões pesadas.
- Sincronização do 'bookmarkmanager' apenas se a alteração foi feita por este processo (erro 343735)
- Correcção do arranque do serviço de D-Bus kssld5
- Implementação do 'quota-used-bytes' e do 'quota-available-bytes' do RFC 4331 para permitir a informação de espaço livre no IO-Slave de HTTP.

### KNotifications

- Atraso da inicialização do áudio até que realmente seja necessário
- Correcção da aplicação não-instantânea da configuração das notificações
- Correcção da paragem das notificações de áudio após a reprodução do primeiro ficheiro

### KNotifyConfig

- Adição da dependência opcional do QtSpeech para reactivar as notificações de fala.

### KService

- KPluginInfo: suporte de 'stringlists' (listas de textos) como propriedades

### KTextEditor

- Adição da estatística de número de palavras na barra de estado
- modo VI: correcção de estoiro ao remover a última linha no modo de Linha Visual

### KWidgetsAddons

- Fazer o KRatingWidget reagir bem com o 'devicePixelRatio'

### KWindowSystem

- O KSelectionWatcher e o KSelectionOwner podem ser usados sem dependerem do QX11Info.
- O KXMessages pode ser usado sem depender do QX11Info

### NetworkManagerQt

- Adição de novas propriedades e métodos do NetworkManager 1.0.0

#### Plataforma do Plasma

- Correcção do plasmapkg2 para os sistemas traduzidos
- Melhorias na disposição das dicas
- Tornar possível aos plasmóides carregarem programas fora do pacote do Plasma ...

### Mudanças no sistema de compilação (extra-cmake-modules)

- Extensão da macro ecm_generate_headers para também permitir ficheiros de inclusão em CamelCase.h

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
