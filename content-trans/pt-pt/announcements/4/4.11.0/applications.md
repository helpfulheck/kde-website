---
date: 2013-08-14
hidden: true
title: As Aplicações do KDE 4.11 Trazem um Grande Avanço na Gestão de Informações
  Pessoais e Outras Melhorias Globais
---
O gestor de ficheiros Dolphin traz muitas pequenas correcções de erros e optimizações nesta versão. O carregamento de pastas grandes foi acelerado e precisa de cerca de 30&#37; de memória a menos. A actividade intensa no disco e no CPU é evitada através do carregamento das antevisões apenas para os itens visíveis. Ocorreram muitas mais melhorias: por exemplo, muitos erros que afectavam as pastas expandidas na vista de Detalhes foram corrigidos, sem que apareçam mais ícones de substituição &quot;desconhecido&quot; quando entra numa pasta; por outro lado, se carregar com o botão do meio do rato sobre um pacote irá abrir agora uma página nova com o conteúdo do pacote, criando uma experiência global mais consistente.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`O novo fluxo de envio posterior no Kontact` width="600px">}}

## Melhorias no Pacote do Kontact

O Pacote Kontact viu uma vez mais um grande foco sobre a estabilidade, a performance e a utilização da memória. A importação de pastas, a mudança de mapas, a transferência do correio, a marcação ou a movimentação de grandes quantidades de mensagens e o tempo de arranque foram todos melhorados nos últimos 6 meses. Veja <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>este 'blog'</a> para saber mais detalhes. A <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/ '>funcionalidade de arquivo teve imensas correcções de erros</a> e ocorreram também algumas melhorias do Assistente de Importação, permitindo a importação da configuração do cliente de e-mail Trojitá e uma melhor importação a partir de outras aplicações diversas. Descubra mais informações <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>aqui</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`O agente de arquivo faz a gestão do correio no formato comprimido` width="600px">}}

Esta versão também vem com diversas funcionalidades novas. Existe um <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>novo editor de temas para os cabeçalhos das mensagens de e-mail</a> e as imagens das mensagens podem ser dimensionadas na hora. A funcionalidade de <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Envio Posterior</a> permite a calendarização do envio das mensagens numa data e hora específicas, com a possibilidade adicional de repetir o envio com base num intervalo específico. O suporte de filtros do Sieve para o KMail (uma funcionalidade de IMAP que permite a filtragem no servidor) foi melhorado, podendo os utilizadores gerar programas de filtragem do Sieve <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>com uma interface simples de utilizar</a>. Na área da segurança, o KMail introduz a 'detecção automática de burlas', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>mostrando um aviso</a> quando as mensagens tiverem alguns dos truques típicos de 'phishing' (burla). Irá receber agora uma <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notificação informativa</a> quando chegar correio novo e, por último, o escritor de 'blogs' Blogilo vem com um editor de HTML extremamente melhorado e baseado no QtWebKit.

## Suporte Alargado de Linguagens no Kate

O editor de texto avançado Kate introduz novos 'plugins': Python (2 e 3), JavaScript & JQuery, Django e XML. Eles introduzem funcionalidades como a completação automática estática e dinâmica, verificações de sintaxe, a inserção de excertos de código e a capacidade de indentar automaticamente o XML com uma combinação de teclas. Mas ainda existe mais para os amigos do Python: uma consola de Python que oferece informação aprofundada sobre um ficheiro aberto com código. Diversas pequenas melhorias da interface foram também feitas, incluindo as <a href="http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/">novas notificações passivas para a funcionalidade de pesquisa</a>, <a href="http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/">optimizações no modo do VIM</a> e <a href="http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/">novas funcionalidades de desdobramento de texto</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`O KStars mostra os próximos eventos interessantes da sua localização` width="600px">}}

## Outras Melhorias das Aplicações

Na área dos jogos e da educação, tiveram lugar diversas funcionalidades novas, grandes e pequenas, bem como algumas optimizações. Os dactilógrafos poderão gostar do suporte da direita-para-a-esquerda do KTouch, enquanto o amigo do observador de estrelas, o KStars, tem agora uma ferramenta que mostra os eventos interessantes que irão decorrer na sua área. As ferramentas matemáticas Rocs, Kig, Cantor e KAlgebra tiveram todas elas alguma atenção, suportando mais infra-estruturas e cálculos. Para além disso, o jogo KJumpingCube tem agora tamanhos de tabuleiros maiores, novos níveis de experiência, respostas mais rápidas e uma interface melhorada.

A aplicação simples de pintura Kolourpaint consegue lidar com o formato de imagens WebP e o visualizador de documentos universal Okular tem ferramentas de revisão configuráveis e introduz o suporte para desfazer/refazer alterações nos formulários e anotações. O marcador/leitor de áudio JuK suporta a reprodução e edição de meta-dados do novo formato de áudio Ogg Opus (contudo, isto precisa que o controlador de áudio e o TagLib também suportem o Ogg Opus).

#### Instalar Aplicações do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. <br />

##### Localizações dos Pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.11.0'>transferir à vontade</a> o código-fonte completo de 4.11.0. As instruções de compilação e instalação da aplicação do KDE 4.11.0 está disponível na <a href='/info/4/4.11.0#binary'>Página de Informações do 4.11.0</a>.

#### Requisitos do Sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.<br />Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.

## Também Anunciado Hoje:

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="A Área de Trabalho Plasma do KDE 4.11" width="64" height="64" /> A Área de Trabalho Plasma 4.11 Continua a Afinar a Experiência do Utilizador</a>

Destinando-se a uma manutenção a longo prazo, a Área de Trabalho do Plasma oferece mais melhorias para as funcionalidades básicas com uma barra de tarefas mais suave, um item de bateria mais inteligente e uma mesa de mistura de som melhorada. A introdução do KScreen traz um tratamento inteligente de vários monitores ao ambiente de trabalho, assim como algumas melhorias de performance em grande escala, combinadas com alguns ajustes de usabilidade, para que possa ter uma experiência de utilização global mais agradável.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="A Plataforma de Desenvolvimento do KDE 4.11"/> A Plataforma do KDE 4.11 Oferece uma Melhor Performance</a>

Esta versão da Plataforma do KDE 4.11 continua a focar-se na estabilidade. As funcionalidades novas estão a ser implementadas na nossa futura versão 5.0 da plataforma, mas para a versão estável continuámos a esforçar a optimização da nossa plataforma Nepomuk.
