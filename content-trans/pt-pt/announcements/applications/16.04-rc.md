---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: O KDE Lança a Versão 16.04 Pré-Lançamento das Aplicações
layout: application
release: applications-16.03.90
title: O KDE Lança a Versão 16.04 Pré-Lançamento das Aplicações
---
7 de Abril de 2016. Hoje o KDE lançou a versão de pré-lançamento das novas Aplicações do KDE. Com as dependências e as funcionalidades estabilizadas, o foco da equipa do KDE é agora a correcção de erros e mais algumas rectificações.

Com as diversas aplicações a basearem-se nas Plataformas do KDE 5, as versões 16.04 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalar os Pacotes Binários das Aplicações do KDE 16.04 RC

<em>Pacotes</em>. Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do 16.04 RC (internamente 16.03.90) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilar as Aplicações do KDE 16.04 RC

Poderá <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>transferir à vontade</a> o código-fonte completo do 16.04 RC (pré-lançamento). As instruções de compilação e instalação estão disponíveis na <a href='/info/applications/applications-16.03.90'>Página de Informações das Aplicações do KDE RC</a>.
