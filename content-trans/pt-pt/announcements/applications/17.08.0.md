---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: O KDE Lança as Aplicações do KDE 17.08.0
layout: application
title: O KDE Lança as Aplicações do KDE 17.08.0
version: 17.08.0
---
17 de Agosto de 2017. As Aplicações do KDE 17.08 estão aí. De forma global, temos trabalhado para tornar as aplicações e as bibliotecas subjacentes mais estáveis e simples de usar. Ao eliminar inconsistências e ao ouvir as suas reacções, a equipa tornou o pacote das Aplicações do KDE menos susceptíveis a problemas e muito mais amigáveis na utilização. Divirta-se com as suas novas aplicações!

### Mais Migrações para as Plataformas do KDE 5

Estamos satisfeitos pelo facto de as seguintes aplicações, antigamente baseadas nas 'kdelibs4', se basearem agora nas Plataformas do KDE 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat e umbrello. Muito obrigado pelo trabalho árduo dos programadores que voluntariaram o seu tempo e trabalho para fazer com que isto acontecesse.

### O que há de novo nas Aplicações do KDE 17.08

#### Dolphin

Os programadores do Dolphin avisam que o Dolphin agora mostra a 'Hora da Remoção' no Lixo, assim como a 'Data de Criação', caso o SO a suporte, como acontece nos BSD's.

#### KIO-Extras

O Kio-Extras agora oferece melhor suporte para as partilhas de Samba.

#### KAlgebra

Os programadores do KAlgebra estiveram a trabalhar na sua interface do Kirigami para a área de trabalho, assim como implementaram a completação de código.

#### Kontact

- No KMailtransport, os programadores reactivaram o suporte para o transporte por Akonadi, criaram o suporte para 'plugins' e voltaram a criar o suporte do transporte de correio por Sendmail.
- No SieveEditor, diversos erros nos programas de criação automática foram corrigidos e fechados. Em conjunto com as correcções de erros gerais, foi implementado um editor de linha para expressões regulares.
- No KMail, a capacidade de usar um editor externo foi implementada de novo como um 'plugin'.
- O assistente de importação do Akonadi agora tem o 'conversor global' como um 'plugin', para que os programadores possam criar novos conversores com facilidade.
- As aplicações agora dependem do Qt 5.7. Os programadores corrigiram uma grande quantidade de erros no Windows. Todo o 'kdepim' ainda não compila no Windows, mas os programadores fizeram grandes progressos. Para começaram, criaram uma 'receita' para o efeito. Muitas correcções de erros foram feitas para tornar o código mais moderno (C++11), o suporte do no Qt 5.9 e a adição de um recurso de Facebook no Kdepim-runtime.

#### Kdenlive

No Kdenlive, a equipa corrigir o 'efeito Congelado' com problemas. Nas versões recentes, era impossível mudar a imagem congelada para o efeito de congelamento. Agora é permitido um atalho de teclado para a Extracção da Imagem. Agora o utilizador pode gravar imagens da sua linha temporal com um atalho de teclado, e é sugerido um nome com base no número da imagem <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. correcção dos Lumas da transição transferida que não aparecem na interface: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Correcção dos 'clicks' de áudio (por agora necessita da compilação da dependência do MLT até uma nova versão do  MLT): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Os programadores terminaram a migração do 'plugin' de X11 para o Qt5, e o 'krfb' voltou a funcionar de novo com uma infra-estrutura de X11, que é muito mais rápida que o 'plugin' em Qt. Existe uma nova página de Configuração que permite ao utilizador mudar o 'plugin' de 'framebuffer' preferido.

#### Konsole

O Konsole agora permite ao posicionamento sem limites ultrapassar o limite dos 2GB (32 bits). Agora o Konsole permite aos utilizadores indicarem qualquer local para guardar os ficheiros de posicionamento. Da mesma forma, foi corrigida uma regressão, sendo que o Konsole consegue de novo fazer com que o KonsolePart invoque a janela para Gerir Perfis.

#### KAppTemplate

No KAppTemplate, agora existe uma opção para instalar modelos novos a partir do sistema de ficheiros. Foram removidos mais modelos do KAppTemplate e, em vez disso, foram integrados em produtos relacionados; o modelo do 'plugin' do ktexteditor e do kpartsapp (migrados agora para o Qt5/KF5) tornaram-se parte do KTextEditor e do KParts das Plataformas do KDE desde a versão 5.37.0. Estas alterações deverão simplificar a criação de modelos nas aplicações do KDE.

### Eliminação de Erros

Foram resolvidos mais de 80 erros nas aplicações, incluindo o Pacote Kontact, Ark, K3b, Dolphin, Kgpg, Kdenlive, Konsole entre outros!

### Registo de Alterações Completo
