---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: O KDE Lança as Aplicações do KDE 14.12.2.
layout: application
title: O KDE Lança as Aplicações do KDE 14.12.2
version: 14.12.2
---
3 de Fevereiro de 2015. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../14.12.0'>Aplicações do KDE 14.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 20 correcções de erros registadas incluem melhorias no jogo de anagramas Kanagram, no visualizador de documentos Okular, na ferramenta de UML Umbrello e no globo virtual Marble.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.16, a Plataforma de Desenvolvimento do KDE 4.14.5 e o pacote Kontact 4.14.5.
