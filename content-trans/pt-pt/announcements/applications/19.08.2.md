---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: O KDE Lança as Aplicações do KDE 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: O KDE Lança as Aplicações do KDE 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../19.08.0'>Aplicações do KDE 19.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Cerca de vinte correcções de erros registadas incluem melhorias no Kontact, no Dolphin, no Gwenview, no Kate, no Kdenlive, no Konsole, no Lokalize, no Spectacle, entre outros.

As melhorias incluem:

- O suporte para HiDPI foi melhorado no Konsole e noutras aplicações
- A mudança entre várias pesquisas no Dolphin agora actualiza correctamente os parâmetros de pesquisa
- O KMail consegue gravar de novo as mensagens directamente para pastas remotas
