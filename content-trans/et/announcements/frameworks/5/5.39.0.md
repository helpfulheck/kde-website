---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Ainult tegelike MIME tüüpide, mitte nt "CD toorpilt" vastavus (veateade 364884)
- pf.path() eemaldamine konteinerist, enne kui it.remove() referentsi segaseks ajab
- Siltide KIO-mooduli protokolli kirjelduse parandus
- Markdown-faile peetakse dokumentideks

### Breeze'i ikoonid

- overflow-menu ikooni lisamine (veateade 385171)

### CMake'i lisamoodulid

- Pythoni seoste kompileerimise parandus pärast 7af93dd23873d0b9cdbac192949e7e5114940aa6

### Raamistike lõimimine

- KStandardGuiItem::discard'i muutmine sobivaks QDialogButtonBox::Discard'iga

### KActivitiesStats

- Päringu vaikimisi piiranguks sai null
- Mudeli testimise lubamise valiku lisamine

### KCMUtils

- KCMultiDialog'i muutmine keritavaks (veateade 354227)

### KConfig

- KStandardShortcut::SaveOptions'i märkimine iganenuks

### KConfigWidgets

- KStandardAction::PasteText'i ja KPasteTextAction'i märkimine iganenuks

### KCoreAddons

- desktoptojson: pärandteenusetüübi tuvastamise heuristika täiustus (veateade 384037)

### KDeclarative

- Litsentsiks muudeti LGPL2.1+
- Meetodi openService() lisamine KRunProxy'le

### KFileMetaData

- Krahhi vältimine, kui hävitatakse üle ühe ExtractorCollection'i isendi

### KGlobalAccel

- "KGlobalAccel: portimine KKeyServer'i uue meetodi symXModXToKeyQt peale parandamaks numbriklahvistiku klahve" tagasivõtmine (veateade 384597)

### KIconThemes

- meetodi lisamine kohandatud paleti lähtestamiseks
- qApp-&gt;palette() kasutamine, kui kohandatud paletti ei ole määratud
- kohase puhvri suuruse eraldamine
- kohandatud paleti määramise lubamine värvivaliku asemel
- värvivaliku avalikustamine laaditabelile

### KInit

- Windows: "klauncher kasutab absoluutset kompileerimisaja paigaldusasukohta kioslave.exe leidmiseks" parandus

### KIO

- kioexec: fsili jälgimine, kui see on kopeerimise lõpetanud (veateade 384500)
- KFileItemDelegate: alati reserveeritakse ruumi ikoonidele (veateade 372207)

### Kirigami

- faili Theme ei algväärtustata BasicTheme'is
- uue edasinupu lisamine
- lehe kerimisriba taust on väiksema kontrastiga
- usaldusväärsem lisamine ja eemaldamine ületäitemenüüs
- ikooni renderdamise parem kontekst
- toimingunupu hoolikam tsentreerimine
- ikoonisuuruste kasutamine toimingunuppudel
- pikslitäpsusega ikoonisuurused töölaual
- valitud efekt ikooni võltspidemele
- pidemete värvi parandus
- parem värv peamisele toimingunupule
- töölauastiili kontekstimenüü parandus
- parem "rohkem" menüü tööriistaribal
- kohane menüü vahelehekülgede kontekstimenüüle
- tekstivälja lisamine, mis peaks esile tooma numbriklahvistiku
- krahhi vältimine käivitamisel olematute stiilidega
- Värvivsliku kontseptsioon teemas
- hiireratta haldamise lihtsustamine (veateade 384704)
- uus näiterakendus töölaua/mobiili põhi-qml-failidega
- tagamine, et currentIndex on kehtiv
- galeriirakenduse appstream'i metaandmete genereerimine
- QtGraphicalEffects'i otsimine, et pakendajad seda ei unustaks
- Kontrolli alumise dekoratsiooni üle ei kaasata  (veateade 384913)
- kergem toonimine, kui loendivaatel ei ole activeFocus
- mõningane RTL-paigutuse toetus
- Kiirklahvide keelamine, kui toiming on keelatud
- terve pluginastruktuuri loomine ehitamiskataloogis
- hõlbustuse parandus galerii pealeheküljel
- kui plasma pole saadaval, pole saadaval ka KF5Plasma. Peaks parandama CI tõrke

### KNewStuff

- Kirigami 2.1 nõudmine 1.0 asemel KNewStuffQuick'ile
- Kohane KPixmapSequence loomine
- Ei kurdeta nsregistry faili puudumise pärast, enne kui sellest kasu on

### KPackage raamistik

- kpackage: servicetypes/kpackage-generic.desktop'i koopia panemine kimpu
- kpackagetool: servicetypes/kpackage-generic.desktop'i koopia panemine kimpu

### KParts

- KPartsApp'i mall: kpart'i töölauafaili paigaldusasukoha parandus

### KTextEditor

- Vaikemärgise eiramine ikoonipiirdel ühe valitava märgise korral
- QActionGroup'i kasutamine sisendreziimi valikus
- Puuduva õigekirja kontrolli riba parandus (veateade 359682)
- Unicode'i &gt; 2555 märgi "mustjuse" tagavaraväärtuse parandus (veateade 385336)
- Lõpetava tühiruumi visualiseerimise parandus RTL-ridadel

### KWayland

- OutputConfig'i sendApplied'i / sendFailed'i saatmine ainult õigele ressursile
- Krahhi vältimine, kui klient (seaduslikult) kasutab kustutatud globaalset kontrastihaldurit
- XDG v6 toetus

### KWidgetsAddons

- KAcceleratorManager: ikooniteksti määramine CJK märgiste eemaldamise toimingutele (veateade 377859)
- KSqueezedTextLabel: teksti kokkusurumine treppimise või veerise muutmisel
- edit-delete ikooni kasutamine hävitusliku hülgamistoimingu puhul (veateade 385158)
- Veateate 306944 parandus - hiireratta kasutamine kuupäevade edasi- või tagasikerimiseks (veateade 306944)
- KMessageBox: küsimärgiikooni kasutamine küsimusega dialoogides
- KSqueezedTextLabel: taande, veerise ja raami laiuse austamine

### KXMLGUI

- KToolBar'i uuestijoonistamise silmuse parandus (veateade 377859)

### Plasma raamistik

- org.kde.plasma.calendar'i parandus Qt 5.10-ga
- [FrameSvgItem] järglassõlmede kohane itereerimine
- [Konteineriliides] konteineritoiminguid ei lisata töölaual apletitoimingutele
- Uue komponendi lisamine halliks muudetavatele pealdistele Item Delegate'ides
- FrameSVGItem'i parandus tarkvaralise renderdajaga
- IconItem'it ei animeerita tarkvaralises režiimis
- [FrameSvg] new-style ühenduse kasutamine
- possibility to set an attached colorscope to not inherit
- Täiendava visuaalse näidiku lisamine märkeruudu/raadionupu klaviatuurifookuse tähistamiseks
- null pikselrastrit ei looda uuesti 
- Elemendi edastamine rootObject()-le, sest see on nüüd singleton (veateade 384776)
- Kaartide nimesid ei loetleta kaks korda
- aktiivset fookust kaardil ei tunnistata
- QQuickItem'i versiooni 1 registreerimine
- [Plasma Components 3] RTL-i parandus mõnes vidinas
- Vigase id parandus viewitem'is
- e-kirja märguande ikooni uuendamine parema kontrasti tagamiseks (veateade 365297)

### qqc2-desktop-style

New module: QtQuickControls 2 style that uses QWidget's QStyle for painting This makes it possible to achieve an higher degree of consistency between QWidget-based and QML-based apps.

### Solid

- [solid/fstab] x-gvfs stiilis võtmete toetuse lisamine fstab'is
- [solid/fstab] tootja ja toote omaduse vahetamine, kirjeldus tõlkimise võimaldamine

### Süntaksi esiletõstmine

- Vigaste itemData viidete parandus 57 esiletõstmise failis
- Kohandatud otsinguteede toetuse lisamine rakendusepõhistesse süntaksi- ja teemadefinitsioonidesse
- AppArmor: DBus'i reeglite parandus
- Esiletõstmise indekseerija: väiksemate while-silmuste kontrollide ärajätmine
- ContextChecker: '!' konteksti lülitamise ja fallthroughContext'i toetus
- Esiletõstmise indekseerija: viidatud kontekstinimede olemasolu kontroll
- qmake'i esiletõstmise litsentsiks muudetakse MIT-i litsents
- qmake'i esiletõstmisel lastakse Prolog'ile peale jääda .pro-failide korral (veateade 383349)
- clojure'i sulgudega "@" makro toetus
- AppArmor'i profiilide süntaksi esiletõstmise lisamine
- Esiletõstmise indekseerija: vigaste a-Z/A-z vahemike tabamine regulaaravaldistes
- Vale tähesuurusega vahemike parandamine regulaaravaldistes
- puuduvate viitefailide lisamine testidesse, ma usun, et sellega peaks nüüd korras olema
- Inteli HEX-faili toetuse lisamine süntaksi esiletõstmise andmebaasi
- Õigekirja kontrollimise keelamine Sieve skriptide stringides

### ThreadWeaver

- Mälulekke parandus

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
