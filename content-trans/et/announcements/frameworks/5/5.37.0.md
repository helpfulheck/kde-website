---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Uus raamistik kirigami: QtQuick'i pluginakomplekt kasutajaliideste loomiseks KDE UX juhiste alusel

### Breeze'i ikoonid

- .h ja .h++ värvide uuendamine (veateade 376680)
- ktorrent'i väikese ühevärvilise ikooni eemaldamine (veateade 381370)
- bookmarks on toimingu-, mitte kataloogiikoon (veateade 381383)
- utilities-system-monitor'i uuendamine (veateade 381420)

### CMake'i lisamoodulid

- --gradle'i lisamine androiddeployqt'sse
- apk sihtmärgi paigaldamise parandus
- query_qmake'i kasutuse parandus: väljakutsete eristamine, mis ootavad qmake'i ja mis mitte
- API dox'i lisamine KDEInstallDirs'i KDE_INSTALL_USE_QT_SYS_PATHS'ile
- metainfo.yaml'i lisamine muutmaks ECM-i korralikuks raamistikuks
- Android: qml-failide uurimine lähte-, mitte paigalduskataloogis

### KActivities

- runningActivityListChanged'i väljastamine tegevuse loomisel

### KDE Doxygeni tööriistad

- HTML-i varjestamine otsingupäringu eest

### KArchive

- Conan'i failide lisamine esimese katsena pakkuda Conan'i toetust

### KConfig

- KConfig'i ehitamise lubamine ilma Qt5Gui'ta
- Standardkiirklahvid: Ctrl+PageUp/PageDown kasutamine eelmise/järgmise kaardi jaoks

### KCoreAddons

- Kasutamata init() deklatatsiooni eemaldamine K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL'ist
- KAboutLicense'i uus spdx API SPDX litsentsilausete saamiseks
- kdirwatch: võimaliku krahhi vältimine, kui d-ptr hävitatakse enne KDirWatch'i (veateade 381583)
- Ümardatud formatDuration'i näitamise parandus (veateade 382069)

### KDeclarative

- Parandus: plasmashell kippus tühistama QSG_RENDER_LOOP'i määramist

### KDELibs 4 toetus

- "KUrl::path()-i iganenud vihje on Windowsis vale" parandus (veateade 382242)
- kdelibs4support'i uuendamine kasutama kdewin'i pakutavat sihtmärgipõhist toetust
- Ka konstruktorite märkimine iganenuks
- KDE4Defaults.cmake'i sünkroonimine kdelibs'ist

### KDesignerPlugin

- Uue vidina kpasswordlineedit toetuse lisamine

### KHTML

- Ka SVG toetamine (veateade 355872)

### KI18n

- i18n kataloogide laadimise lubamine suvalisest kohast
- Tsgamine, et tsfiles'i sihtmärk genereeritakse

### KIdleTime

- Qt5X11Extras'i nõudmine ainult siis, kui seda tegelikult tarvis läheb

### KInit

- Kohase lipu kasutamine kill(2) kaasamiseks

### KIO

- Uue meetodi urlSelectionRequested lisamine KUrlNavigator'ile
- KUrlNavigator: KUrlNavigatorButton'i avalikustamine, kui see sai kukutamissündmuse
- Panipaika ladustamine ilma kasutajalt Kopeeri/Loobu dialoogiga järele pärimata
- Tagamine, et KDirLister uuendab elemente, mille siht-URL on muutunud (veateade 382341)
- "Avamine rakendusega" dialoogi muud valikud muudeti vaikimisi kokkukeritavaks ja peidetuks (veateade 359233)

### KNewStuff

- KMoreToolsMenuFactory menüüdele eellase andmine
- Puhvrist nõudmise korral kõigi kirjete korraga teatamine

### KPackage raamistik

- kpackagetool suudab nüüd väljastada appstream'i andmed failina
- uue KAboutLicense::spdx omaksvõtmine

### KParts

- url'i lähtestamine closeUrl()-is
- Lihtsa kpart'i-põhise rakenduse malli lisamine
- KDE_DEFAULT_WINDOWFLAGS'i kasutusest loobumine

### KTextEditor

- Hiirerattasündmuse ülitäpne käitlemine suurendamisel
- ktexteditor'i plugina malli lisamine
- algfaili õiguste kopeerimine salvestatud koopiasse (veateade 3777373)
- arvatavasti välditakse stringbuild'i krahhi (veateade 339627)
- Probleemi parandus * lisamisel reale väljaspool kommentaari (veateade 360456)
- Koopiana salvestamise parandus - puudus õigus sihtfaili üle kirjutada (veateade 368145)
- Käsk 'set-highlight': argumentide ühendamine tühimärgiga
- Krahhi vältimine vaate hävitamisel objektide mittedeterministliku puhastamise tõttu
- Signaalide väljastamine ikoonipiirdelt, kui ühtegi märki ei klõpsata
- Krahhi parandus vi sisendrežiimis (järgnevus: "o" "Esc" "O" "Esc" ".") (veateade 377852)
- Use mutually exclusive group in Default Mark Type

### KUnitConversion

- MPa ja PSI märkimine tavalisteks ühikuteks

### KWalleti raamistik

- CMAKE_INSTALL_BINDIR 'i kasutamine dbus'i teenuse genereerimiseks

### KWayland

- Kõigi registri loodud kwaylandi objektide hävitamine registri hävitamisel
- connectionDied'i väljastamine QPA hävitamisel
- [klient] kõigi loodud ConnectionThread'ide jälgimine ja API lisamine neile ligipääsuks
- [server] tekstisisendi lahkumise saatmine, kui fookuses pind pole enam seotud
- [server] osutusseadme lahkumise saatmine, kui fookuses pind pole enam seotud
- [klient] enteredSurface'i kohane jälgimine Keyboard'is
- [server] klaviatuuri lahkumise saatmine, kui klient hävitab fookuses pinna (veateade 382280)
- puhvri kehtivuse kontroll (veateade 381953)

### KWidgetsAddons

- lineedit'i paroolividina ekstraktimine =&gt; uus klass KPasswordLineEdit
- Krahhi vältimine, kui otsingu ajal on hõlbustus lubatud (veateade 374933)
- [KPageListViewDelegate] vidina edastamine drawPrimitive'ile drawFocus'is

### KWindowSystem

- Päise QWidget'ile toetumise eemaldamine

### KXMLGUI

- KDE_DEFAULT_WINDOWFLAGS'i kasutusest loobumine

### NetworkManagerQt

- ipv*.route-metric'i toetuse lisamine
- Defineerimata NM_SETTING_WIRELESS_POWERSAVE_FOO enumeraatorite parandus (veateade 382051)

### Plasma raamistik

- [Konteineriliides] konteineri korral väljastatakse alati contextualActionsAboutToShow
- Nupupealdiste kohtlemine lihttekstina
- Waylandi-põhised parandused jäetakse X'i peal olles tegemata (veateade 381130)
- KF5WindowSystem'i lisamine link'i liidesele
- AppManager.js'i deklareerimine pragma teegina
- [PlasmaComponents] Config.js eemaldamine
- pealdistel kasutatakse vaikimisi lihtteksti
- Tõlgete laadimine KPackage'i failidest, kui on on kimbus (veateade 374825)
- [PlasmaComponents Menu] Krahhi vältimine nulltoimingu korral
- [Plasma Dialog] Lipu tingimuste parandus
- akregator'i süsteemisalve ikooni uuendamine (veateade 379861)
- [Konteineriliides] Konteineri hoidmine RequiresAttentionStatus'is, kui kontekstimenüü on avatud (veateade 351823)
- Kaardiriba paigutuse käitlemise parandus RTL-keeltes (veateade 379894)

### Sonnet

- Lubamine ehitada Sonnet'it ilma Qt5Widgets'ita
- cmake: FindHUNSPELL.cmake ümberkirjutamine pkg-config'i kasutamiseks

### Süntaksi esiletõstmine

- Lubamine ehitada KSyntaxHighlighter'it ilma Qt5Gui'ta
- Esiletõstmise indekseerijale ristkompileerimise toetuse lisamine
- Teemad: kõigi kasutamata metaandmete (litsents, autor, kirjutuskaitstud) eemaldamine
- Teema: litsentsi- ja autorivälja eemaldamine
- Teema: kirjutuskaitstuse lipu ülevõtmine faililt kettal
- Andmemudelikeele YANG süntaksi esiletõstmise lisamine
- PHP: PHP 7 võtmesõnade lisamine (veateade 356383)
- PHP: PHP 5 teabe puhastamine
- gnuplot'i parandus, muutes eelnevad või järgnevad tühimärgid fataalseks
- 'else if' tuvastamise parandus, meil on tarvis lülitada konteksti, lisareegli lisamine
- indekseerija alustavate/lõpetavate tühimärkide kontrollid XML-i esiletõstmises
- Doxygen: Doxyfile'i esiletõstmise lisamine
- puuduvate standardtüüpide lisamine C esiletõstmisse ja uuendamine C11 peale (veateade 367798)
- Q_PI D =&gt; Q_PID
- PHP: topeltjutumärkide vahel looksulgudes muutujate esiletõstmise täiustus (veateade 382527)
- PowerShell'i esiletõstmise lisamine
- Haskell: faililaienduse .hs-boot (ellaadimise moodul) lisamine (veateade 354629)
- replaceCaptures() parandus, et see töötaks ka rohkem kui 9 hõivega
- Ruby: WordDetect'i kasutamine StringDetect'i asemel täielike sõnade vastendamisel
- BEGIN'i ja END'i väära esiletõstmise parandus sellistes sõnades nagu "EXTENDED" (veateade 350709)
- PHP: mime_content_type() eemaldamine iganenud funktsioonide loendist (veateade 371973)
- XML: XBEL'i laienduse/MIME tüübi lisamine xml-i esiletõstmisse (veateade 374573)
- Bash: käsu võtmete vale esiletõstmise parandus (veateade 375245)
- Perl: heredoc'i esiletõstmise parandus alustavate tühimärkide leidumisel piiritlejas (veateade 379298)
- SQL-i (Oracle'i) süntaksifaili uuendamine (veateade 368755)
- C++: '-' ei kuulu UDL-i stringi parandus (veateade 380408)
- C++: printf vorming määrab: lisada 'n' ja 'p', eemaldada 'P' (veateade 380409)
- C++: sümboli väärtusel on stringi värv parandus (veateade 380489)
- VHDL: esiletõstmise tõrke parandus sulgude ja atribuutide kasutamisel (veateade 368897)
- zsh esileõstmine: matemaatikaavaldise parandus alamstringi avaldises (veateade 380229)
- JavaScript'i esiletõstmine: E4X xml-i laienduse toetuse lisamine (veateade 373713)
- "*.conf" laienduse reegli eemaldamine
- Pug/Jade süntaks

### ThreadWeaver

- Puuduva ekspordi lisamine QueueSignals'ile

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
