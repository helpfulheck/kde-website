---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Qt5Network'i lisamine avaliku sõltuvusena

### BluezQt

- Kataloogi include parandus pri-failis

### Breeze'i ikoonid

- Puuduvate nimeruumi prefiksi definitsioonide lisamine
- SVG-ikoonide korraliku vorminduse kontroll
- Kõigil edit-clear-location-ltr ikoonide parandus (veateade 366519)
- kwin'i efekti ikooni toetuse lisamine
- caps-on sai nimeks input-caps-on
- suurtäheikoonide lisamine tekstisisendi tarbeks
- mõne gnome-spetsiifilise ikooni lisamine Sadi58 poolt
- Rakenduseikoonide lisamine gnastyle'ist
- Dolphini, Konsooli ja Umbrello ikoonid optimeeriti suurustele 16px, 22px, 32px
- VLC ikooni uuendati suurustele 22px, 32px ja 48px
- Subtiitrilooja rakenduseikooni lisamine
- Kleopatra uue ikooni parandus
- Kleopatra rakenduseikooni lisamine
- Wine ja Wine-qt ikoonide lisamine
- Esitluse vigase ikooni parandus tänu Sadi58-le (veateade 358495)
- system-log-out ikooni lisamine suuruses 32px
- 32px süsteemiikoonide lisamine, värviliste süsteemiikoonide eemaldamine
- pidgin'i ja banshee ikooni toetuse lisamine
- vlc rakenduseikooni eemaldamine litsentsiprobleemi tõttu, uue VLC ikooni lisamine (veateade 366490)
- gthumb'i ikooni toetuse lisamine
- kataloogiikoonidel HighlightedText'i kasutamine
- asukohtade kataloogi ikoonid kasutavad nüüd laaditabelit (esiletõstmise värve)

### CMake'i lisamoodulid

- ecm_process_po_files_as_qm: pooleli tõlgete vahelejätmine
- Logimiskategooriate vaikimisi tase peaks olema Info, mitte Warning
- Muutuja ARGS dokumenteerimine apk-* sihtmärkide loomisel
- Testi loomine projekti appstream-teabe valideerimiseks

### KDE Doxygeni tööriistad

- Tingimuse lisamine, kui grupi platvormid ei ole defineeritud
- Mall: platvormide sortimine tähestiku järgi

### KCodecs

- Kdelibs'ist faili ülevõtmine, millega genereeriti kentities.c

### KConfig

- Annetamise kirje lisamine KStandardShortcut'i

### KConfigWidgets

- Annetamise standardtoimingu lisamine

### KDeclarative

- [kpackagelauncherqml] eeldatakse, et töölauafaili nimi on sama plugina ID-ga
- QtQuick'i renderdamiseadistuste laadimine seadistusfailist ja nende määramine vaikeväärtuseks
- icondialog.cpp - kohane kompileerimisparandus, mis ei varjuta m_dialog'i
- Krahhi vältimine, kui QApplication puudub
- tõlkedomeeni avaldamine

### KDELibs 4 toetus

- Windowsi kompileerimistõrke parandus kstyle.h-s

### KDocTools

- seadistuse ja puhvri asukoha ning andmete lisamine general.entities'i
- Kuupäeva esitamine Briti versioonis
- Tühiku- ja Meta-klahvi lisamine src/customization/en/user.entities'i

### KFileMetaData

- Ainult Xattr'i nõudmine, kui operatsioonisüsteemiks on Linux
- Windowsi ehituse taastamine

### KIdleTime

- [xsync] XFlush simulateUserActivity's

### KIO

- KPropertiesDialog: hoiatuse eemaldamine dokumentatsioonist, see viga on kadunud
- [test program] suhteliste asukohtade lahendamine QUrl::fromUserInput'i abil
- KUrlRequester: veakasti parandus, kui faili valimisel avatakse uuesti failidialoog
- Varuvariandi pakkumine, kui mooduliloendis puudub kirje . (veateade 366795)
- Üle "desktop" protokolli nimeviida loomise parandus
- KNewFileMenu: nimeviida loomisel kasutatakse KIO::link'i asemelKIO::linkAs'i
- KFileWidget: kahekordse '/' parandus asukohas
- KUrlRequester: staatilise connect() süntaksi kasutamine, varem oli see ebaühtlane
- KUrlRequester: window() edastamine QFileDialog'ile eellasena
- välditakse connect(null, .....) väljakutsumist KUrlComboRequester'ist

### KNewStuff

- arhiivide lahtipakkimine alamkataloogidesse
- Võimaliku turvaaugu tõttu ei lubata enam paigaldada üldist andmekataloogi

### KNotification

- StatusNotifierWatcher'i omaduse ProtocolVersion saamine asünkroonselt

### Paketiraamistik

- contentHash'i iganenuks kuulutamise hoiatuste vaigistamine

### Kross

- "Kasutamata KF5 sõltuvuste eemaldamine" tagasivõtmine

### KTextEditor

- kiirklahvide kokkupõrke eemaldamine (veateade 363738)
- e-posti aadressi esiletõstmise parandus doxygen'is (veateade 363186)
- rohkemate json-failide - näiteks meie enda projektide ;) - tuvastamine
- MIME tüübi tuvastamise täiustamine (veateade 357902)
- Veateade 363280 - esiletõstmine: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (veateade 363280)
- Veateade 363280 - esiletõstmine: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Veateade 351496 - esialgse kirjutamise käigus Pythoni voltimine ei toiminud (veateade 351496)
- Veateade 365171 - Pythoni süntaksi esiletõstmine ei toiminud õigesti paojadade korral (veateade 365171)
- Veateade 344276 - php nowdoc'i ei volditud korralikult kokku (veateade 344276)
- Veateade 359613 - Mõned CSS3 omadused ei ole süntaksi esiletõstmisel toetatud (veateade 359613)
- Veateade 367821 - wineHQ süntaks: reg-faili lõik ei olnud õigesti esile tõstetud (veateade 367821)
- Saalefaili käitlemise täiustamine saalekataloogi määramise korral
- Krahhi vältimine automaatse reavahetusega dokumentide taaslaadimisel reapikkuse piirangu tõttu (veateade 366493)
- vi käsureaga seotud sagedaste krahhide vältimine (veateade 367786)
- Parandus: trükitud dokumentides on nüüd esimese rea number 1 (veateade 366579)
- Võrgufailide varundamine: ühendatud failide kohtlemine võrgufailidena
- otsimisriba loomise loogika puhastamine
- Magma esiletõstmise lisamine
- Rekursiivsuse lubamine ainul ühe taseme võrra
- Katkise saalefaili parandus windowsis
- Paik: bitbake'i toetuse lisamine süntaksi esiletõstmise mootorile
- autobrace: õigekirja kontrollimise atribuudi otsimine märgi sisestamisel (veateade 367539)
- QMAKE_CFLAGS'i esiletõstmine
- Põhikontekstist ei hüpata välja
- Mõne sagedamini kasutatava täitmisfaili nime lisamine

### KUnitConversion

- Briti massiühiku "stone" lisamine

### KWalleti raamistik

- kwallet-query docbook'i liigutamine õigesse alamkataloogi
- Sõnastuse parandamine: an -&gt; one

### KWayland

- linux/input.h kompileerimisaja muutmine lisavõimaluseks

### KWidgetsAddons

- Muude kui BMP märkide tausta parandus
- C octal escaped UTF-8 otsingu lisamine
- Vaikimisi KMessageBoxDontAskAgainMemoryStorage salvestatakse QSettings'isse

### KXMLGUI

- Portimine annetamise standardtoimingu peale
- Portimine iganenud authorizeKAction'i pealt ära

### Plasma raamistik

- Seadme 22px ikooni parandus, et see töötaks ka vana faili korral
- WindowThumbnail: GL'i väljakutsete tegemine õiges lõimes (veateade 368066)
- Make plasma_install_package work with KDE_INSTALL_DIRS_NO_DEPRECATED
- veerise ja polsterduse lisamine svgz-ikooni algusesse
- laaditabeli parandus arvutiikoonis
- kickerile arvuti ja sülearvuti ikooni lisamine (veateade 367816)
- defineerimatut ei saa double'ile omistada hoiatuse parandus DayDelegate'is
- parandus: svgz-failid ei ole üldse minuga sõbrad
- kickeri 22px ikoonide nimeks saab 22-22-x ja 32px ikoonide nimeks x
- [PlasmaComponents TextField] kasutamata nuppudele pole mõtet isegi hakata ikoone laadima
- Lisakaitse Containment::corona's süsteemisalve erijuhtumi korral
- Konteineri märkimisel kustutatuks samuti kõigi alamaplettide märkimine kustutatuks - parandab vea, mille korral süsteemisalve konteinerite seadistusi ei kustutatud
- Seadme mänguandja ikooni parandus
- system-search'i lisamine süsteemi 32px ja 22px suuruses
- kicker'i ühevärviliste ikoonide lisamine
- system-search'i ikooni värviskeemi määramine
- system-search'i liigutamine system.svgz-sse
- Vale või puuduva "X-KDE-ParentApp'i" parandus töölauafaili definitsioonis
- Plasma::PluginLoader'i API dox'i parandus: aplettide/andmemootorite/ teenuste ... segapuder
- system-search'i ikooni lisamine sddm teemale
- nepomuki 32px ikooni lisamine
- süsteemisalve puutepadja ikooni uuendamine
- Koodi eemaldamine, mida ei saagi kunagi kasutada
- [ContainmentView] paneelide näitamine, kui kasutajaliides on valmis
- Omadust implicitHeight ei deklareerita teist korda
- QQuickViewSharedEngine::setTranslationDomain kasutamine (veateade 361513)
- 22px ja 32px plasma breeze'i ikooni toetuse lisamine
- värviliste süsteemiikoonide eemaldamine ja 32px ühevärviliste lisamine
- Lisavõimalusena parooli näitamise nupu lisamine TextField'ile
- Standardsed kohtspikrid on nüüd paremalt vasakule kirja puhul peeglis
- Jõudlust kuude vahetamisel kalendris on tugevasti parandatud

### Sonnet

- Trigrammide parsimisel ei muudeta keelenimesid väiketäheliseks
- Käivitamisel kohese krahhi vältimine nullplugina viida tõttu
- Sõnastike käitlemine ka ilma korrektsete nimedeta
- Käsitsi hallatud loendi asendamine skriptikeele seostega, keelte sobivate nimede kasutamine
- Trigramme genereeriva tööriista lisamine
- Keele tuvastamise katkisuse kerge vähendamine
- Valitud keele kasutamine pakkumisena tuvastamisel
- Puhverdatud õigekirja kontrollimise tööriistade kasutamine keele tuvastamisel, jõudluse pisuke täiustamine
- Keele tuvastamise täiustamine
- Pakkumiste loendi võrdlemine ja filtreerimine saadaolevate sõnastike alusel, topeltkirjete eemaldamine
- Viimase trigrammi vaste lisamine jäetakse meelde
- Kontroll, kas mõni trigamm ka tegelikult sobis
- Sama tulemusega mitme keele käitlemine trigrammisobitajas
- Miinimumsuurust ei kontrollita kaks korda
- Keelteloendi kärpimine vastavalt saadaolevatele keeltele
- Sama miinimumpikkuse kasutamine kõikjal keeletuvastuses
- Kontroll, kas laaditud mudelis on igale keelele vajalik hulk trigramme

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
