---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE toob välja KDE rakendused 15.08.3
layout: application
title: KDE toob välja KDE rakendused 15.08.3
version: 15.08.3
---
November 10, 2015. Today KDE released the third stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Arki, Dolphini, Kdenlive'i, Kdepimi, Kigi, Lokalize ja Umbrello täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.14.
