---
date: 2013-08-14
hidden: true
title: KDE arendusplatvorm 4.11 pakub paremat jõudlust
---
KDE arendusplatvorm 4 on uuenduste mõttes külmutatud oleks alates versioonist 4.9. Seetõttu on selles versioonis pakkuda ainult mõningaid veaparandusi ja jõudluse täiustusi.

The Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages. The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage. Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of document formats including ODF and docx.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/dolphin.png" caption=`Semantilised omadused Dolphinis` width="600px">}}

Nepomuki optimeeritud salvestusvorming ja ümber kirjutatud e-kirjade indekseerija nõuavad oma kõvaketta sisu taasindekseerimist. Seetõttu nõuab taasindekseerimine mõnda aega (mis sõltub taasindekseeritava sisu mahust) ebatavaliselt palju arvutiressursse. Nepomuki andmebaasi teisendamine sooritatakse automaatselt esimesel sisselogimisel.

Lisaks on parandatud mõningaid väiksemaid vigu, mille loetelu leiab <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>giti logist</a>.

#### KDE arendusplatvormi paigaldamine

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Tarkvarapaketid

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettide asukohad

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Nõuded süsteemile

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Täna ilmusid veel:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Pikaajalise hoolduse huvides pakuvad Plasma töötsoonid edasisi põhifunktsioonide täiendusi, näiteks sujuvamalt tegutsevat tegumiriba, nutikamat akuvidinat ja täiustatud helimikserit. Uus KScreen juurutab töötsoonide nutika mitme monitori halduse. Kõigele sellele lisandub suurel hulgal jõudluse parandusi ja kui liita väiksemad kasutatavuse täiustused, siis ootab kasutajat ees kindlasti senisest parem kogemus.

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

See väljalase toob kaasa hulganisi täiustusi KDE PIM-i rakendustes, tagades tunduvalt parema jõudluse ja mitmeid uusi võimalusi. Kate täiustused lubavad neil, kellele meeldib kasutada Pythonit ja JavaScripti, uute pluginate abil produktiivsust suurendada. Dolphin on kiirem ning õpirakendused pakuvad mimesuguseid uusi võimalusi.
