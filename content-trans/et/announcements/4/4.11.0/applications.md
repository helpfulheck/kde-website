---
date: 2013-08-14
hidden: true
title: KDE rakendused 4.11 astuvad suure sammu edasi isikliku teabe haldamisel ja
  pakuvad rohkelt kõikvõimalikke parandusi ja täiustusi
---
The Dolphin file manager brings many small fixes and optimizations in this release. Loading large folders has been sped up and requires up to 30&#37; less memory. Heavy disk and CPU activity is prevented by only loading previews around the visible items. There have been many more improvements: for example, many bugs that affected expanded folders in Details View were fixed, no &quot;unknown&quot; placeholder icons will be shown any more when entering a folder, and middle clicking an archive now opens a new tab with the archive contents, creating a more consistent experience overall.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Uus hiljem saatmise võimalus Kontactis` width="600px">}}

## Kontacti komplekti täiustused

Kontacti komplektis on taas pühendatud suurt tähelepanu stabiilsusele, jõudlusele ja mälukasutusele. Kataloogide ja kaustade importimine, kaartide vahel lülitumine, kirjade tõmbamine ja märkimine, suure arvu kirjade liigutamine ja käivitamise aeg on viimasel poolaastal aina paremaks muutunud. Loe üksikasju <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>selles ajaveebis</a>. <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>Arhiveerimisvõimalus on saanud rohkelt veaparandusi</a> ning täiustusi on ka impordinõustajas, mis lubab nüüd importida e-posti kliendi Trojitá seadistusi ning paremini importida ka teistest rakendustest. Täpsemat teavet leiab <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>siit</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Arhiveerimisagent hoolitseb kirjade salvestamise eest tihendatud kujul` width="600px">}}

This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks. You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.

## Kate keelte toetus avardus

Täiustatud tekstiredaktor Kate toob välja uued pluginad: Python (2 ja 3), JavaScript ja Query, Django ja XML. Need pakuvad selliseid omadusi, nagu staatiline ja dünaamiline automaatme lõpetamine, süntaksikontroll, koodijuppide lisamine ning kiirklahvi abil XML-i automaatne treppimine. Pythoni sõpradele on uudiseid veelgi, nimelt Pythoni konsool, mis näitab põhjalikku teavet avatud lähtekoodifaili kohta. Samuti on mõningaid väiksemaid parandusi kasutajaliideses, näiteks <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>otsingu uus passiivsete märguannete võimalus</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>VIM-i režiimi optimeerimine</a> ja <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>uus koodi voltimise võimalus</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars näitab peatseid huvipakkuvaid sündmusi sinu asukohas` width="600px">}}

## Muude rakenduste täiustused

Mängude ja õpirakenduste vallas on mitmeid väiksemaid ja suuremaid uusi omadusi ja optimeerimisi. Tulevastele usinatele masinakirjutajatele kindlasti meeldib KTouchi paremalt vasakule kirjutamise toetus. Tähistaeva vaatlejate sõber KStars pakub nüüd tööriista, mis näitab sündmusi, mida kasutaja oma piirkonnas peatselt näha võib. Hoolitsust on pälvinud matemaatikarakendused Rocs, Kig, Cantor ja KAlgebra, mis toetavad nüüd rohkem taustaprogramme ja arvutusi. Mängul KJumpingCube aga on nüüd suuremaid mängulaudu, uusi mängutasemeid, kiirem reageerimine ja etem kasutajaliides.

Lihtne joonistamisrakendus Kolourpaint tuleb nüüd toime WebP pildivorminguga ning universaalne dokumendinäitaja Okular pakub seadistatavaid annteerimistööriistu ning võimaldab tagasivõtmist ja uuestitegemist vormides ja annotatsioonides. Helimängija/sildistaja JuK toetab nüüd uue Ogg Opuse helivormingu esitamist ja metaandmete muutmist (siiski peavad selleks helidraiver ja TagLib samuti Ogg Opust toetama).

#### KDE rakenduste paigaldamine

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Tarkvarapaketid

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettide asukohad

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Nõuded süsteemile

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Täna ilmusid veel:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

Pikaajalise hoolduse huvides pakuvad Plasma töötsoonid edasisi põhifunktsioonide täiendusi, näiteks sujuvamalt tegutsevat tegumiriba, nutikamat akuvidinat ja täiustatud helimikserit. Uus KScreen juurutab töötsoonide nutika mitme monitori halduse. Kõigele sellele lisandub suurel hulgal jõudluse parandusi ja kui liita väiksemad kasutatavuse täiustused, siis ootab kasutajat ees kindlasti senisest parem kogemus.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

KDE arendusplatvormi versioon 4.11 keskendub endiselt stabiilsusele. Tulevase KDE Frameworks 5.0 puhuks oleme näinu vaeva ka uute omadustega, aga stabiilses väljalaskes on meie tähelepanu keskmes olnud eelkõige Nepomuki raamistiku optimeerimine.
