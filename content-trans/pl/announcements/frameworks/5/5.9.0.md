---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New module: ModemManagerQt (Qt wrapper for ModemManager API) Alternatively, upgrade to Plasma-NM 5.3 Beta when upgrading to ModemManagerQt 5.9.0.

### KAktywności

- Zaimplementowano zapominanie zasobu
- Poprawki w budowaniu
- Dodano wtyczkę do rejestrowania zdarzeń dla powiadomień KRecentDocument

### KArchiwum

- Ustawienie KZip::extraField jest uwzględniane nawet przy zapisywaniu wpisów centralnego nagłówka
- Usunięto dwa błędne założenia, występujące przy pełnym dysku, błąd 343214

### KBookmarks

- Naprawiono budowanie z Qt 5.5

### KCMUtils

- Używany jest nowy system wtyczek opartych na json. KCM-y są wyszukiwane w kcms/. Na dzisiaj pliki pulpitu nadal muszą być wgrywane w kservices5/ dla zgodności
- Wczytuj i owijaj wersje tylko-QML kcms, jeśli jest to możliwe

### KConfig

- Naprawiono assert przy użyciu KSharedConfig w globalnym destruktorze obiektów.
- kconfig_compiler: dodano obsługę dla CategoryLoggingName w plikach *.kcfgc, aby generować wywołania qCDebug(category).

### KI18n

- wczytaj z wyprzedzeniem globalny katalog Qt przy używaniu i18n()

### KIconThemes

- KIconDialog można teraz wyświetlić przy użyciu zwykłych metod QDialog show() oraz exec()
- Naprawiono obsługę różnych devicePixelRatios przez KIconEngine::paint

### KIO

- Do KPropertiesDialog dodano wyświetlanie wolnego miejsca na zdalnych systemach plików (np. smb)
- Naprawiono KUrlNavigator z piksmapami o wysokim DPI
- KFileItemDelegate obsługuje nie-domyślne devicePixelRatio w animacjach

### KItemModels

- KRecursiveFilterProxyModel: przeprojektowano, aby emitowało poprane sygnały o właściwym czasie
- KDescendantsProxyModel: Obsługa ruchów zgłoszonych przez model źródłowy.
- KDescendantsProxyModel: Naprawiono zachowanie, gdy zaznaczono w trakcie resetowania.
- KDescendantsProxyModel: Zezwól na tworzenie i używane KSelectionProxyModel z QML.

### KJobWidgets

- Propaguj kod błędu w interfejsie JobView DBus

### KNotifications

- Dodano wersję event(), który nie przyjmuje ikony i będzie używał domyślnej ikony
- Dodano wersję event(), który przyjmuje StandardEvent eventId oraz QString iconName

### KLudzie

- Zezwolono na rozwijanie metadanych działań poprzez nastawione typy
- Po usunięciu kontaktu z apletu Ludzie model od teraz uaktualnia się poprawnie

### KPty

- Gdy KPty zostało zbudowane przy użyciu biblioteki utempter, to zostanie to wyświetlone

### KTextEditor

- Dodano plik podświetleń kdesrc-buildrc
- syntax: dodano obsługę dla l. całkowitych dwójkowych w pliku podświetleń PHP

### KWidgetsAddons

- Animacje KMessageWidget są gładkie przy użyciu wysokiego współczynnika pikseli dla urządzenia

### KWindowSystem

- Dodano fikcyjną implementację Waylanda dla KWindowSystemPrivate
- KWindowSystem::icon z NETWinInfo nie jest powiązana z platformą X11.

### KXmlGui

- Domena tłumaczenia zostaje zachowana przy scalaniu plików .rc
- Naprawiono ostrzeżenie biblioteki uruchomieniowej QWidget::setWindowModified: Tytuł okna nie zawiera znaku wieloznacznego '[*]'

### KXmlRpcClient

- Instalowanie tłumaczeń

### Szkielety Plazmy

- Naprawiono oderwane podpowiedzi, gdy tymczasowy właściciel podpowiedzi zniknął lub stał się pusty
- Naprawiono wstępne ustawienie elementów TabBar, co można zaobserwować w np. Kickoff
- Przejścia pomiędzy stosami stron od teraz używają Animatorów dla płynniejszych animacji
- Przejścia pomiędzy grupami kart od teraz używają Animatorów dla płynniejszych animacji
- Spraw, aby Svg,FrameSvg działały z QT_DEVICE_PIXELRATIO

### Solid

- Odśwież właściwości baterii po wznowieniu

### Zmiany w systemie budowania

- Dodatkowe Moduły CMake (ECM) są od teraz wersjonowane tak jak Szkielety KDE, dlatego teraz jest to 5.9, podczas gdy poprzednio było 1.8.
- Dostosowano wiele szkieletów tak, aby były używalne bez konieczności wyszukiwania ich prywatnych zależności.Oznacza to, że aplikacje szukające tylko w szkieletach potrzebują tylko ich publicznych zależności, a nie prywatnych.
- Lepsza obsługa SHARE_INSTALL_DIR dla układów wielo-architekturowych

### Integracja Szkieletów

- Usunięto możliwą usterkę przy niszczeniu QSystemTrayIcon (napotykana przez np. Trojitę), błąd 343976
- Naprawiono natywne okna dialogowe plików w QML, błąd 334963

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
