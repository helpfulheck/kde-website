---
aliases:
- ../announce-applications-16.12-rc
date: 2016-12-02
description: KDE wydało Aplikacje 16.12 (kandydat do wydania).
layout: application
release: applications-16.11.90
title: KDE wydało kandydata do wydania Aplikacji KDE 16.12
---
2 grudnia 2016. Dzisiaj KDE wydało kandydata do wydania nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym ulepszaniu.

Sprawdź <a href='https://community.kde.org/Applications/16.12_Release_Notes'>listę zmian społeczności</a>, aby uzyskać informację dotyczącą archiwów, obecnie opartych na KF5 oraz ich znanych błędów. Bardziej kompletny komunikat będzie dostępny po wydaniu wersji ostatecznej

Aplikacje KDE, wydanie 16.12 potrzebuje wypróbowania go w szerokim zakresie, aby utrzymać i ulepszyć jakość i odczucia użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie kandydata do wydania i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.
