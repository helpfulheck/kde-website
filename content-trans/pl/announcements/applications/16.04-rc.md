---
aliases:
- ../announce-applications-16.04-rc
custom_spread_install: true
date: 2016-04-05
description: KDE wydało kandydata do wydania Aplikacji KDE 16.04
layout: application
release: applications-16.03.90
title: KDE wydało kandydata do wydania Aplikacji KDE 16.04
---
26 marca 2015. Dzisiaj KDE wydało kandydata do wydania nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 16.04 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu poprzez zainstalowanie wydania i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 16.04  kandydata do wydania

<em>Pakiety</em>Niektórzy wydawcy systemów operacyjnych Linuks/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 16.04 kandydata do  wydania (wewnętrznie 16.03.90) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 16.04 kandydata do wydania

Pełny kod źródłowy dla Aplikacji KDE 16.04 kandydata do wydania można <a href='http://download.kde.org/unstable/applications/16.03.90/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-16.03.90.php'>Stronie informacyjnej Aplikacji KDE kandydata do wydania</a>.
