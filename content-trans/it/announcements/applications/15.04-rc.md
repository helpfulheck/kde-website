---
aliases:
- ../announce-applications-15.04-rc
date: '2015-03-26'
description: KDE rilascia Applications 15.04 Release Candidate.
layout: application
title: KDE rilascia la Release Candidate di KDE Applications 15.04
---
26 marzo 2015. Oggi KDE ha rilasciato la release candidate della nuova versione delle applicazioni KDE (Applications). Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 15.04 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando il rilascio <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
