---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE rilascia Applications 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE rilascia KDE Applications 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../19.04.0'>KDE Applications 19.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Circa cinquanta errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Dolphin, JuK, Kdenlive, KmPlot, Okular e Spectacle.

I miglioramenti includono:

- È stato corretto un blocco durante la visualizzazione di determinati documenti EPUB in Okular
- Le chiavi segrete possono essere di nuovo esportate dal gestore di crittografia Kleopatra
- Il gestore di avvisi KAlarm si avvia di nuovo con le librerie PIM più recenti
