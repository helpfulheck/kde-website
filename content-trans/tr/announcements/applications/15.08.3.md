---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE, KDE Uygulamaları 15.08.3'yi Gönderdi
layout: application
title: KDE, KDE Uygulamaları 15.08.3'yi Gönderdi
version: 15.08.3
---
November 10, 2015. Today KDE released the third stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kaydedilen 20'den fazla hata düzeltmesi arasında ark, dolphin, kdenlive, kdepim, kig, lokalize ve umbrello için iyileştirmeler bulunuyor.

This release also includes Long Term Support version of KDE Development Platform 4.14.14.
