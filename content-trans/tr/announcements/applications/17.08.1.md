---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE, KDE Uygulamaları 17.08.1'i Gönderdi
layout: application
title: KDE, KDE Uygulamaları 17.08.1'i Gönderdi
version: 17.08.1
---
7 Eylül 2017. Bugün KDE, <a href='../17.08.0'>KDE Uygulamaları 17.08</a> için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, KDE oyunları, ve birçok uygulama için 20'den fazla hata düzeltmesi.

This release also includes Long Term Support version of KDE Development Platform 4.14.36.
