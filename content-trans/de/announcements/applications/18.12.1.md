---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE veröffentlicht die Anwendungen 18.12.1.
layout: application
major_version: '18.12'
title: KDE veröffentlicht die KDE-Anwendungen 18.12.1
version: 18.12.1
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../18.12.0'>KDE-Anwendungen 18.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

About 20 recorded bugfixes include improvements to Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular, among others.

Verbesserungen umfassen:

- Akregator now works with WebEngine from Qt 5.11 or newer
- Sorting columns in the JuK music player has been fixed
- Konsole renders box-drawing characters correctly again
