---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE veröffentlicht die KDE-Anwendungen 15.08.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.08.1
version: 15.08.1
---
15. September 2015. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../15.08.0'>KDE-Anwendungen 15.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 40 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für kdelibs, kdepim, Kdenlive, Dolphin, Kompare, Konsole, Ark und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
