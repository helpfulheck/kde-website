---
aliases:
- ../announce-applications-16.04.2
changelog: true
date: 2016-06-14
description: KDE veröffentlicht die KDE-Anwendungen 16.04.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.04.2
version: 16.04.2
---
14. Juni 2016. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../16.04.0'>KDE-Anwendungen 16.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für Akonadi, Ark, Artikulate, Dolphin, Kdenlive, und KDEPim.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
