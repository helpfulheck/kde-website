---
aliases:
- ../../kde-frameworks-5.52.0
date: 2018-11-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Unbreak build with BUILD_QCH=TRUE
- Actually use fileNameTerms and xAttrTerms
- [Balooshow] Avoid out-of-bounds access when accessing corrupt db data
- [Extractor] Do not check QFile::exists for an empty url
- [Scheduler] Use flag to track when a runner is going idle
- [Extractor] Handle documents correctly where mimetype should not be indexed
- [Scheduler] Fix wrong usage of obsolete QFileInfo::created() timestamp (bug 397549)
- [Extractor] Make extractor crash resilient (bug 375131)
- Pass the FileIndexerConfig as const to the individual indexers
- [Config] Remove KDE4 config support, stop writing arbitrary config files
- [Extractor] Improve commandline debugging, forward stderr
- [Scheduler] Reuse fileinfo from FilteredDirIterator
- [Scheduler] Reuse mimetype from UnindexedFileIterator in indexer
- [Scheduler] Remove superfluous m_extractorIdle variable
- Perform checks for unindexed files and stale index entries on startup
- [balooctl] Print current state &amp; indexing file when monitor starts (bug 364858)
- [balooctl] Monitor also for state changes
- [balooctl] Fix "index" command with already indexed, but moved file (bug 397242)

### BluezQt

- Add Media and MediaEndpoint API header generation

### Breeze Icons

- Change package manager icons to emblems
- Re-add monochrome link icon as action
- Improve emblem contrast, legibility and consistency (bug 399968)
- Support "new" mimetype for .deb files (bug 399421)

### Extra CMake-Module

- ECMAddQch: help doxygen by predefining more Q*DECL** macros
- Bindings: Support using sys paths for python install directory
- Bindings: Remove INSTALL_DIR_SUFFIX from ecm_generate_python_binding
- Add support for the fuzzer sanitizer

### KCMUtils

- support for multi pages kcms

### KConfig

- Add mechanism to notify other clients of config changes over DBus
- Expose getter method for KConfig::addConfigSources

### KConfigWidgets

- Allow KHelpCenter to open the right pages of KDE help when KHelpClient is invoked with an anchor

### KCrash

- KCrash: fix crash (ironic heh) when used in an app without QCoreApplication

### KDeclarative

- make push/pop part of ConfigModule API

### KDED

- Remove useless "No X-KDE-DBus-ServiceName found" message

### KDesignerPlugin

- Reference product "KF5" in widget metadata, instead of "KDE"

### KDocTools

- API dox: add minimal docs to KDocTools namespace, so doxygen covers it
- Create a QCH file with the API dox, optionally, using ECMAddQCH
- Wait for docbookl10nhelper to be built before building our own manpages
- Use specified Perl interpreter instead of relying on PATH

### KFileMetaData

- [ExtractorCollection] Use only best matching extractor plugin
- [KFileMetaData] Add extractor for generic XML and SVG
- [KFileMetaData] Add helper for XML encoded Dublin Core metadata
- implement support for reading ID3 tags from aiff and wav files
- implement more tags for asf metadata
- extract ape tags from ape and wavpack files
- provide a list of supported mimetypes for embeddedimagedata
- compare with QLatin1String and harmonize handling of all types
- Don't crash on invalid exiv2 data (bug 375131)
- epubextractor: Add property ReleaseYear
- refactor taglibextractor to functions specific for metadata type
- add wma files/asf tags as supported mimetype
- use own extractor for testing the taglibwriter
- add a string suffix to test data and use for unicode testing of taglibwriter
- remove compile time check for taglib version
- extend test coverage to all supported mimetypes for taglibextractor
- Use variable with already fetched text instead of fetching again

### KGlobalAccel

- Fix keyboard layout change notifications (bug 269403)

### KHolidays

- Add Bahrain Holiday File
- Make KHolidays work as static library too

### KIconThemes

- Add a QIconEnginePlugin to allow QIcon deserialization (bug 399989)
- [KIconLoader] Replace heap-allocated QImageReader with stack-allocated one
- [KIconLoader] Adjust emblem border depending on icon size
- Center icons properly if size doesn't fit (bug 396990)

### KIO

- Do not try to fallback to "less secure" SSL protocols
- [KSambaShare] Trim trailing / from share path
- [kdirlistertest] Wait a little longer for the lister to finish
- Display sorry message if file is not local
- kio_help: Fix crash in QCoreApplication when accessing help:// (bug 399709)
- Avoid waiting for user actions when kwin Focus stealing prevention is high or extreme
- [KNewFileMenu] Don't open an empty QFile
- Added missing Icons to Places Panel code from KIO
- Get rid of the raw KFileItem pointers in KCoreDirListerCache
- Add 'Mount' option to context menu of unmounted device in Places
- Add a 'Properties' entry in the places panel context menu
- Fix warning "macro expansion producing 'defined' has undefined behavior"

### Kirigami

- Fix missing items in static builds
- basic support for hidden pages
- load icons from proper icon themes
- (many other fixes)

### KNewStuff

- More useful error messages

### KNotification

- Fixed a crash caused by bad lifetime management of canberra-based audio notification (bug 398695)

### KParts

- Fix Cancel being not handled in deprecated BrowserRun::askEmbedOrSave
- Port to undeprecated variant of KRun::runUrl
- Port KIO::Job::ui() -&gt; KJob::uiDelegate()

### KWayland

- Add KWayland virtual desktop protocol
- Guard data source being deleted before processing dataoffer receive event (bug 400311)
- [server] Respect input region of sub-surfaces on pointer surface focus
- [xdgshell] Add positioner constraint adjustment flag operators

### KWidgetsAddons

- API dox: fix "Since" note of KPageWidgetItem::isHeaderVisible
- Add a new property headerVisible

### KWindowSystem

- Do not compare iterators returned from two separate returned copies

### KXMLGUI

- Take 1..n KMainWindows in kRestoreMainWindows

### NetworkManagerQt

- Add missing ipv4 setting options
- Add vxlan setting

### Plasma Framework

- revert icons scaling on mobile
- Support mnemonic labels
- Remove PLASMA_NO_KIO option
- Properly look for fallback themes

### Purpose

- Set Dialog flag for JobDialog

### Solid

- [solid-hardware5] List icon in device details
- [UDisks2] Power down drive on remove if supported (bug 270808)

### Sonnet

- Fix breakage of language guessing

### Syndication

- Add missing README.md file (needed by various scripts)

### Syntax Highlighting

- z/OS CLIST file syntax highlighting
- Creating new syntax highlighting file for Job Control Language (JCL)
- Remove open mode from too new Qt version
- inc version + fixup required kate version to current framework version
- keyword rule: Support for keywords inclusion from another language/file
- No spell checking for Metamath except in comments
- CMake: Add XCode related variables and properties introduced in 3.13
- CMake: Introduce new features of upcoming 3.13 release

### Sicherheitsinformation

Der veröffentlichte Quelltext wurde mit GPG mit folgendem Schlüssel signiert: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärer Fingerprint des Schlüssels: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Sie können im Kommentarabschnitt des <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot-Artikels</a> über diese Veröffentlichung diskutieren und Ideen einbringen.
