---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE lanza las Aplicaciones 14.12.2.
layout: application
title: KDE lanza las Aplicaciones de KDE 14.12.2
version: 14.12.2
---
Hoy, 3 de febrero de 2015, KDE ha lanzado la segunda actualización de estabilización para las <a href='../14.12.0'>Aplicaciones</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 soluciones de errores registrados, se incluyen mejoras para el juego de anagramas Kanagram, para la herramienta de modelado UML Umbrello, para el visor de documentos Okular y para el globo terráqueo virtual Marble.

También se incluyen versiones de los Espacios de trabajo Plasma 4.11.16, de la Plataforma de desarrollo de KDE 4.14.5 y de la suite Kontact 4.14.5 que contarán con asistencia a largo plazo.
