---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE lanza las Aplicaciones de KDE 17.08.3
layout: application
title: KDE lanza las Aplicaciones de KDE 17.08.3
version: 17.08.3
---
Hoy, 9 de noviembre de 2017, KDE ha lanzado la tercera actualización de estabilización para las <a href='../17.08.0'>Aplicaciones 17.08</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las aproximadamente 12 correcciones de errores registradas, se incluyen mejoras en Kontact, Ark, Gwenview, KGpg, KWave, Okular y Spectacle, entre otras aplicaciones.

También se incluye la última versión de la Plataforma de desarrollo de KDE 4.14.38.

Las mejoras incluyen:

- Se ha subsanado una regresión en Samba 4.7 en los recursos compartidos SMB protegidos con contraseña.
- Okular ya no se cuelga tras ciertos trabajos de rotación.
- Ark preserva las fechas de modificación de los archivos al extraer archivos ZIP.
