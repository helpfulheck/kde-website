---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE lanza las Aplicaciones de KDE 16.04.3
layout: application
title: KDE lanza las Aplicaciones de KDE 16.04.3
version: 16.04.3
---
Hoy, 12 de julio de 2016, KDE ha lanzado la tercera actualización de estabilización para las <a href='../16.04.0'>Aplicaciones 16.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones y será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en ark, cantor, kate, kdepim y umbrello, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.22 que contará con asistencia a largo plazo.
