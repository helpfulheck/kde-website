---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos CMake adicionales

- Nuevos argumentos parar ecm_add_tests(). (error 345797)

### Integración con Frameworks

- Usar el «initialDirectory» correcto en KDirSelectDialog
- Asegurar que se indica el esquema cuando se redefine el valor de URL inicial
- Aceptar solo directorios existentes en el modo FileMode::Directory

### KActivities

(no se ha proporcionado ningún registro de cambios)

### KAuth

- Hacer que KAUTH_HELPER_INSTALL_ABSOLUTE_DIR esté disponible para todos los usuarios de KAuth

### KCodecs

- KEmailAddress: añadir sobrecarga para extractEmailAddress y firstEmailAddress que devuelve un mensaje de error.

### KCompletion

- Se ha corregido una selección no deseada cuando se editaba un nombre de archivo en el diálogo de archivos (error 344525)

### KConfig

- Prevenir un cuelgue si QWindow::screen() es nulo
- Añadir KConfigGui::setSessionConfig() (error 346768)

### KCoreAddons

- Nueva API de conveniencia para KPluginLoader::findPluginById()

### KDeclarative

- Permitir la creación de ConfigModule a partir de KPluginMetdata
- Corregir eventos pressAndhold

### Soporte de KDELibs 4

- Usar QTemporaryFile en lugar de programar a mano archivos temporales.

### KDocTools

- Actualización de traducciones
- Actualización de customization/ru
- Corregir entidades con enlaces erróneos

### KEmoticons

- Usar caché para el tema del complemento de integración

### KGlobalAccel

- [runtime] Mover código específico de la plataforma a complementos

### KIconThemes

- Optimizar KIconEngine::availableSizes()

### KIO

- No intentar completar usuarios y afirmación cuando lo que se antepone no es vacío (error 346920).
- Usar KPluginLoader::factory() al cargar KIO::DndPopupMenuPlugin
- Se ha corregido una situación que conducía a un punto muerto al usar proxys de red (error 346214)
- Corregir KIO::suggestName para conservar las extensiones de los archivos
- Lanzar kbuildsycoca4 al actualizar sycoca5.
- KFileWidget: No aceptar archivos en el modo de solo directorios
- KIO::AccessManager: Hacer que se pueda tratar de modo asíncrono las secuencias de QIODevice

### KNewStuff

- Añadir el nuevo método fillMenuFromGroupingNames
- KMoreTools: añadir numerosos nuevos agrupamientos
- KMoreToolsMenuFactory: gestión de «clientes-y-acciones-git»
- createMenuFromGroupingNames: hacer que el parámetro url de make sea opcional

### KNotification

- Se ha solucionado un error en «NotifyByExecute» cuando no se había establecido ningún elemento gráfico (error 348510).
- Mejora del manejo de notificaciones que se cierran (bug 342752)
- Sustituir el uso de QDesktopWidget por QScreen
- Asegurar que KNotification se pueda usar desde un hilo sin interfaz gráfica

### Framework para paquetes

- Proteger el acceso a la estructura qpointer (error 347231)

### KPeople

- Usar QTemporaryFile en lugar de programar a mano /tmp.

### KPty

- Usar tcgetattr y tcsetattr si están disponibles

### Kross

- Corregir la carga de los módulos de Kross «forms» y «kdetranslation»

### KService

- Cuando se ejecuta como usuario root, preservar el propietario de los archivos existentes en la caché (error 342438)
- Protegerse contra la imposibilidad de abrir un flujo de datos (error 342438)
- Se ha corregido la comprobación de permisos no válidos al escribir archivos (error 342438)
- Se han solucionado las consultas a ksycoca para los pseudotipos MIME x-scheme-handler/* (error 347353).

### KTextEditor

- Permitir que las aplicaciones y complementos de terceras partes puedan instalar sus propios archivos XML de resaltado de sintaxis en katepart5/syntax, como en los tiempos de KDE 4.x
- Añadir KTextEditor::Document::searchText()
- Recuperar el uso de KEncodingFileDialog (error 343255)

### KTextWidgets

- Añadir un método para borrar el decorador
- Permitir el uso de decoradores personalizados de Sonnet
- Implementar «buscar anterior» en KTextEdit.
- Volver a añadir el uso de voz a texto

### KWidgetsAddons

- KAssistantDialog: Volver a añadir el botón de Ayuda presente en la versión de KDELibs4

### KXMLGUI

- Añadir gestión de sesiones a KMainWindow (error 346768)

### NetworkManagerQt

- Desechar el uso de WiMAX en NM 1.2.0+

### Framework de Plasma

- Los componentes del calendario ya pueden mostrar los números de las semanas (error 338195)
- Usar QtRendering para los tipos de letras en los campos de contraseñas
- Solucionada la búsqueda en AssociatedApplicationManager cuando tiene un tipo MIME (error 340326).
- Corregir el color del fondo de los paneles (error 347143)
- Deshacerse del mensaje «No se ha podido cargar la miniaplicación»
- Capacidad para cargar kcms QML en las ventanas de configuración de plasmoides.
- No usar DataEngineStructure para Applets
- Independizar libplasma de sycoca todo lo posible
- [plasmacomponents] Ahora, SectionScroller sigue los ListView.section.criteria
- Ya no se ocultan automáticamente las barras de desplazamiento cuando se detecta una pantalla táctil (error 347254)

### Sonnet

- Utilizar una caché central para los SpellerPlugins.
- Reducir las asignaciones de memoria temporales.
- Optimización: no borrar la cache dict cuando se copian objetos speller.
- Optimización de llamadas save() llamándolas una vez al final si se necesita.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
