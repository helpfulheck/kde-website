---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---
### Iconos Brisa

- Se ha mejorado el icono de KFloppy (error 412404).
- Se han añadido iconos para las acciones «format-text-underline-squiggle» (error 408283).
- Se ha añadido el icono «preferences-desktop-filter» en color (error 406900).
- Se ha añadido el icono de la aplicación para control de drones Kirogi.
- Se han añadido guiones para crear un tipo de letra web de los iconos de acciones de Brisa.
- Se han añadido los iconos «enablefont» y «disablefont» para el módulo de preferencias de «kfontinst».
- Se han corregido los iconos grandes de reinicio del sistema que rotaban en una dirección no consistente (error 411671).

### Módulos CMake adicionales

- Nuevo módulo ECMSourceVersionControl.
- Se ha corregido FindEGL cuando se usa Emscripten.
- ECMAddQch: se ha añadido el argumento INCLUDE_DIRS.

### Integración con Frameworks

- Se ha asegurado que winId() no se llame en los widgets no nativos (error 412675).

### KCalendarCore

Nuevo módulo, anteriormente conocido como «kcalcore» en «kdepim».

### KCMUtils

- Se han suprimido los eventos del ratón en los módulos de preferencias que provocaban movimientos de la ventana.
- Se han ajustado los márgenes de KCMultiDialog (error 411161).

### KCompletion

- [KComboBox] Se ha desactivado correctamente la terminación automática integrada de Qt (corrige una regresión).

### KConfig

- Se ha corregido la generación de propiedades que empiezan con una letra mayúscula.

### KConfigWidgets

- Se ha hecho que KColorScheme sea compatible con QVariant.

### KContacts

Nuevo módulo, anteriormente parte de KDE PIM.

### KCoreAddons

- Se ha añadido KListOpenFilesJob.

### KDeclarative

- Se ha borrado el contexto QQmlObjectSharedEngine en sincronización con QQmlObject.
- [KDeclarative] Se ha adaptado para no usar el método obsoleto QWheelEvent::delta() en favor de angleDelta().

### Soporte de KDELibs 4

- Se usa NetworkManager 1.20 y se compila realmente el motor de NM.

### KIconThemes

- Se han marcado como obsoletos los métodos globales [Small|Desktop|Bar]Icon().

### KImageFormats

- Se han añadido archivos para probar el error 411327.
- xcf: se ha corregido una regresión al leer archivos con propiedades «no contempladas».
- xcf: se lee correctamente la resolución de la imagen.
- Se ha portado el cargador de imagen HDR (Radiance RGBE) a Qt5.

### KIO

- [Panel de lugares] Se ha renovado la sección de «Guardados recientemente».
- [DataProtocol] Compilar sin conversión implícita desde ASCII.
- Se considera suficiente el uso de los métodos de WebDAV para asumir WebDAV.
- REPORT también permite la cabecera Depth.
- Hacer que se puede reutilizar la conversión QSslError::SslError &lt;-&gt; KSslError::Error.
- Se ha marcado como obsoleto el constructor KSslError::Error de KSslError.
- [Windows] Se ha corregido el listado del directorio padre de «C:lo-que-sea», para que sea «C:» en lugar de «C:».
- Se ha corregido un fallo de salida en kio_file (error 408797).
- Se han añadido los operadores == y != a KIO::UDSEntry.
- Se ha cambiado KSslError::errorString por QSslError::errorString.
- Move/copy job: skip stat'ing sources if the destination dir isn't writable (bug 141564)
- Fixed interaction with DOS/Windows executables in KRun::runUrl
- [KUrlNavigatorPlacesSelector] Properly identify teardown action (bug 403454)
- KCoreDirLister: fix crash when creating new folders from kfilewidget (bug 401916)
- [kpropertiesdialog] se han añadido iconos para la sección de tamaño.
- Add icons for "Open With" and "Actions" menus
- Avoid initializing an unnecessary variable
- Move more functionality from KRun::runCommand/runApplication to KProcessRunner
- [Permisos avanzados] se han corregido nombres de iconos (error 411915).
- [KUrlNavigatorButton] Fix QString usage to not use [] out of bounds
- Make KSslError hold a QSslError internally
- Split KSslErrorUiData from KTcpSocket
- Adaptar kpac de QtScript.

### Kirigami

- always cache just the last item
- more z (bug 411832)
- fix import version in PagePoolAction
- PagePool is Kirigami 2.11
- take into account dragging speed when a flick ends
- Fix copying urls to the clipboard
- check more if we are reparenting an actual Item
- Implementación básica de las acciones de «ListItem».
- Introducir «cachePages».
- fix compatibility with Qt5.11
- Introducir «PagePoolAction».
- new class: PagePool to manage recycling of pages after they're popped
- make tabbars look better
- some margin on the right (bug 409630)
- Revert "Compensate smaller icon sizes on mobile in the ActionButton"
- don't make list items look inactive (bug 408191)
- Revert "Remove scaling of iconsize unit for isMobile"
- Layout.fillWidth should be done by the client (bug 411188)
- Se ha añadido una plantilla para el desarrollo de aplicaciones en Kirigami.
- Add a mode to center actions and omit the title when using a ToolBar style (bug 402948)
- Compensate smaller icon sizes on mobile in the ActionButton
- Fixed some undefined properties runtime errors
- Fix ListSectionHeader background color for some color schemes
- Remove custom content item from ActionMenu separator

### KItemViews

- [KItemViews] Port to non-deprecated QWheelEvent API

### KConfigWidgets

- cleanup dbus related objects early enough to avoid hang on program exit

### KJS

- Added startsWith(), endsWith() and includes() JS String functions
- Fixed Date.prototype.toJSON() called on non-Date objects

### KNewStuff

- Bring KNewStuffQuick to feature parity with KNewStuff(Widgets)

### KPeople

- Declarar Android como plataforma soportada.
- Deploy default avatar via qrc
- Empaquetar archivos de complementos en Android.
- Desactivar partes de DBus en Android.
- Fix crash when monitoring a contact that gets removed on PersonData (bug 410746)
- Use fully qualified types on signals

### KRunner

- Consider UNC paths as NetworkShare context

### KService

- Move Amusement to Games directory instead of Games &gt; Toys (bug 412553)
- [KService] Add copy constructor
- [KService] add workingDirectory(), deprecate path()

### KTextEditor

- try to avoid artifacts in text preview
- Variable expansion: Use std::function internally
- QRectF instead of QRect solves clipping issues, (bug 390451)
- next rendering artifact goes away if you adjust the clip rect a bit (bug 390451)
- avoid the font choosing magic and turn of anti aliasing (bug 390451)
- KadeModeMenuList: corregir fugas de memoria y otros.
- try to scan for usable fonts, works reasonable well if you use no dumb scaling factor like 1.1
- Status bar mode menu: Reuse empty QIcon that is implicitly shared
- Expose KTextEditor::MainWindow::showPluginConfigPage()
- Replace QSignalMapper with lambda
- KateModeMenuList: use QString() for empty strings
- KateModeMenuList: add "Best Search Matches" section and fixes for Windows
- Variable expansion: Support QTextEdits
- Add keyboard shortcut for switching Input modes to edit menu (bug 400486)
- Variable expansion dialog: properly handle selection changes and item activation
- Variable expansion dialog: add filter line edit
- Backup on save: Support time and date string replacements (bug 403583)
- Variable expansion: Prefer return value over return argument
- Initial start of variables dialog
- use new format API

### Framework KWallet

- Implementación de HiDPI.

### KWayland

- Sort files alphabetically in cmake list

### KWidgetsAddons

- Make OK button configurable in KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] Fix QTimeLine::start warning at runtime
- Improve naming of KTitleWidget icon methods
- Add QIcon setters for the password dialogs
- [KWidgetAddons] port to non-deprecated Qt API

### KWindowSystem

- Set XCB to required if building the X backend
- Make less use of deprecated enum alias NET::StaysOnTop

### KXMLGUI

- Move "Full Screen Mode" item from Settings menu to View menu (bug 106807)

### NetworkManagerQt

- ActiveConnection: connect stateChanged() signal to correct interface

### Framework de Plasma

- Export Plasma core lib log category, add a category to a qWarning
- [pluginloader] Use categorized logging
- make editMode a corona global property
- Respetar el factor de velocidad de animación global.
- properly install whole plasmacomponent3
- [Dialog] Apply window type after changing flags
- Se ha cambiado la lógica del botón de revelar contraseña.
- Se ha corregido un fallo en la destrucción con el «ConfigLoader» de «Applet» (error 411221).

### QQC2StyleBridge

- Se han corregido varios errores del sistema de compilación.
- Usar los márgenes de «qstyle».
- [Tab] Se ha corregido el tamaño (error 409390).

### Resaltado de sintaxis

- Se ha añadido el archivo de resaltado de sintaxis para RenPy (.rpy) (error 381547).
- Regla de detección de palabras: Detectar delimitadores en el borde interno de la cadena.
- Resaltado de archivos de «GeoJSON» como si fueran de JSON sin formato.
- Se ha añadido resaltado de sintaxis para subtítulos de texto SubRip (SRT).
- Se ha corregido «skipOffset» con «RegExpr» dinámicas (error 399388).
- bitbake: handle embedded shell and python
- Jam: Se ha corregido el identificador en una «SubRule».
- Se ha añadido la definición de sintaxis para Perl6 (error 392468).
- support .inl extension for C++, not used by other xml files at the moment (bug 411921)
- Permitir «*.rej» para resaltado de sintaxis de «diff» (error 411857).

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
