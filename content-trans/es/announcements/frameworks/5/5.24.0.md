---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Cambios generales

- La lista de las plataformas soportadas en cada «framework» es ahora más explícita. Se ha añadido Android a la lista de plataformas soportadas en todas las «frameworks» donde sea aplicable.

### Baloo

- DocumentUrlDB::del solo emite una aserción cuando los hijos del directorio existen realmente.
- Ignorar peticiones mal formadas que tienen un operador binario sin el primer argumento.

### Iconos Brisa

- Muchos iconos nuevos y otros mejorados
- Corregir el error 364931: El icono «user-idle» no es visible (error 364931).
- Añadir un programa para convertir los archivos enlazados simbólicamente en alias de qrc.

### Módulos CMake adicionales

- Integrar las rutas de biblioteca relativas a APK.
- Usar «${BIN_INSTALL_DIR}/data» para «DATAROOTDIR» en Windows.

### KArchive

- Asegurar que la extracción de un archivo comprimido no instale archivos fuera de la carpeta de extracción, por razones de seguridad. En su lugar, extraer dichos archivos en la raíz de la carpeta de extracción.

### KBookmarks

- Limpiar «KBookmarkManagerList» antes de que salga «qApp» para evitar interbloqueos con el hilo de DBus.

### KConfig

- Marcar como obsoleto «authorizeKAction()» en favor de «authorizeAction()».
- Corregir la reproductibilidad de las compilaciones asegurando la codificación utf-8.

### KConfigWidgets

- KStandardAction::showStatusbar: Devolver la acción prevista.

### KDeclarative

- Hacer que «epoxy» sea opcional.

### KDED

- [OS X] Hacer que «kded5» sea un agente, y compilarlo como una aplicación normal.

### Soporte de KDELibs 4

- Se ha eliminado la clase «KDETranslator» porque ya no existe «kdeqt.po».
- Documentar la sustitución de «use12Clock()».

### KDesignerPlugin

- Añadir soporte para «KNewPasswordWidget».

### KDocTools

- Permitir que KDocTools siempre pueda localizar al menos sus propias cosas instaladas.
- Usar «CMAKE_INSTALL_DATAROOTDIR» para buscar «docbook» en lugar de compartir.
- Se ha actualizado el docbook de la página de manual de «qt5options» a Qt 5.4.
- Se ha actualizado el docbook de la página de manual de «kf5options».

### KEmoticons

- Se ha movido el tema «Cristal» a «kde-look».

### KGlobalAccel

- Usar «QGuiApplication» en lugar de «QApplication».

### KHTML

- Se ha corregido la aplicación del valor heredado para la propiedad abreviada del esquema.
- Manejo inicial y herencia del radio del borde.
- Descartar la propiedad si se ha obtenido una longitud o un porcentaje no válidos como «background-size».
- «cssText» debe generar valores separados por comas para estas propiedades.
- Se ha corregido el análisis de «background-clip» de forma abreviada.
- Implementar el análisis abreviado de «background-size».
- Marcar las propiedades como definidas cuando se repiten patrones.
- Se ha corregido la herencia de las propiedades de fondo.
- Corregir la aplicación de «Initial» y de «Inherit» para la propiedad «background-size».
- Implementar el archivo «khtml kxmlgui» en un archivo de recurso de Qt.

### KI18n

- Buscar también en los catálogos variantes eliminadas de valores de la variable de entorno «LANGUAGE».
- Corregir el análisis de los valores con el modificador WRT y «codeset» de variables de entorno, que se realizaba en el orden incorrecto.

### KIconThemes

- Permitir la carga y el uso de un tema de iconos en un archivo RCC automáticamente
- Documentar el despliegue de temas de iconos en MacOS y Windows. Consulte https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Permitir un tiempo de espera en «reset_oom_protection» mientras se espera «SIGUSR1».

### KIO

- KIO: Añadir «SlaveBase::openPasswordDialogV2» para una mejor comprobación de errores. Por favor, porte sus «kioslaves» para que lo usen.
- Se ha corregido que «KUrlRequester» abra el diálogo de archivos en el directorio equivocado (error 364719).
- Se han corregido las conversiones inseguras de «KDirModelDirNode*».
- Se ha añadido la opción de cmake «KIO_FORK_SLAVES» para definir el valor predeterminado.
- Filtro de ShortUri: Se ha corregido el filtrado de «mailto:user@host».
- Se ha añadido «OpenFileManagerWindowJob» para resaltar el archivo dentro de una carpeta.
- KRun: añadir el método runApplication
- Añadir el proveedor de búsqueda soundcloud
- Se ha corregido un problema de alineación en el estilo «macintosh» nativo de OS X.

### KItemModels

- Añadir KExtraColumnsProxyModel::removeExtraColumn, que será necesario para StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - definir HAVE_SYS_PARAM_H correctamente

### KNewStuff

- Asegurarse de que disponemos de un tamaño para ofrecer (error 364896).
- Se ha corregido «El diálogo de descarga falla cuando faltan todas las categorías».

### KNotification

- Corregir notificaciones de la barra de tareas

### KNotifyConfig

- KNotifyConfigWidget: añadir el método disableAllSounds() (error 157272)

### KParts

- Añadir un interruptor para desactivar el manejo de títulos de ventanas de KParts.
- Añadir el elemento de menú «Donar» al menú de ayuda de nuestras aplicaciones.

### Kross

- Se ha corregido el nombre del enumerador «StandardButtons» de QDialogButtonBox.
- Eliminar el primer intento de cargar una biblioteca porque se va a acabar probando con «libraryPaths».
- Se ha corregido un fallo cuando un método expuesto a «Kross» devuelve un «QVariant» con datos no relocalizables.
- No usar reinterpretaciones de estilo C con «void*» (error 325055).

### KRunner

- [QueryMatch] Añadir iconName

### KTextEditor

- Mostrar la vista previa del texto de la barra de desplazamiento tras una demora de 250 ms.
- Ocultar la vista previa y demás al desplazar el contenido de la vista.
- Definir «parent» + «toolview». Creo que es necesario para evitar la entrada del selector de tareas en Win10.
- Eliminar «KDE-Standard» del cuadro de codificaciones.
- Vista previa plegable activada por defecto.
- Evitar el subrayado de rayas para la vista previa y evitar el envenenamiento de la caché del diseño de línea.
- Activar siempre la opción «Mostrar vista previa del texto plegado».
- TextPreview: Ajustar «grooveRect-height» cuando está activado «scrollPastEnd».
- Vista previa de barra de desplazamiento: usar un rectángulo de ranura si la barra de desplazamiento no usa la altura completa.
- Se ha añadido «KTE::MovingRange::numberOfLines()» de modo semejante al de «KTE::Range».
- Vista de código plegado: Definir la altura de la ventana emergente para que quepan todas las líneas ocultas.
- Se ha añadido una opción para desactivar la vista previa de texto plegado.
- Se ha añadido el modeline «folding-preview» de tipo bool.
- ConfigInterface: Permitir «folding-preview» de tipo «bool».
- Se han añadido «KateViewConfig::foldingPreview()» y «setFoldingPreview(bool)».
- Funcionalidad: Mostrar vista previa del texto al situar el cursor sobre un bloque de código plegado.
- KateTextPreview: Se han añadido «setShowFoldedLines()» y «showFoldedLines()».
- Se han añadido los «modelines» «scrollbar-minimap [bool]» y «scrollbar-preview [bool]».
- Activar la barra de desplazamiento de minimapa de forma predeterminada.
- Nueva funcionalidad: Mostrar vista previa del texto al situar el cursor sobre la barra de desplazamiento.
- KateUndoGroup::editEnd(): Pasar «KTE::Range» como referencia constante.
- Corregir el manejo del acceso rápido del «modo vim», tras haber cambiado su comportamiento en Qt 5.5 (error 353332).
- Paréntesis automáticos: No insertar el carácter ' en el texto.
- ConfigInterface: Añadir tecla de configuración del minimapa de la barra de desplazamiento para activar/desactivar el minimapa.
- Corregir «KTE::View::cursorToCoordinate()» cuando el widget de mensaje superior está visible.
- Refactorización de la barra de órdenes emulada.
- Se han corregido los defectos de dibujo al desplazarse mientras las notificaciones están visibles (fallo 363220).

### KWayland

- Añadir un evento «parent_window» a la interfaz de ventanas de Plasma.
- Manejar correctamente la destrucción de un recurso «Pointer/Keyboard/Touch».
- [servidor]: Se ha borrado código muerto en «KeyboardInterface::Private::sendKeymap».
- [servidor] Añadir soporte para definir la «DataDeviceInterface» de la selección del portapapeles de forma manual.
- [servidor] Asegurarse de que «Resource::Private::get» devuelve «nullptr» si se pasa un «nullptr».
- [servidor] Se ha añadido comprobación de recursos en «QtExtendedSurfaceInterface::close».
- [servidor] Borrar la definición del puntero «SurfaceInterface» en los objetos referenciados cuando se destruyen.
- [servidor] Se ha corregido el mensaje de error en la interfaz «QtSurfaceExtension».
- [servidor] Introducir una señal «Resource::unbound» emitida desde el controlador de desenlace.
- [servidor] No emitir una aserción al destruir un «BufferInterface» aún referenciado.
- Se ha añadido la petición de destructor a «org_kde_kwin_shadow» y  «org_kde_kwin_shadow_manager».

### KWidgetsAddons

- Se ha corregido la lectura de datos «Unihan».
- Se ha corregido el tamaño mínimo de «KNewPasswordDialog» (error 342523).
- Se ha corregido un constructor ambiguo en MSVC 2015.
- Se ha corregido un problema de alineación en el estilo «macintosh» nativo de OS X (error 296810).

### KXMLGUI

- KXMLGui: Se ha corregido la fusión de índices al eliminar clientes de «xmlgui» con acciones en grupos (error 64754).
- No emitir una advertencia sobre «archivo encontrado en ubicación compatible» cuando realmente no se ha encontrado.
- Añadir el elemento de menú «Donar» al menú de ayuda de nuestras aplicaciones.

### NetworkManagerQt

- No definir la etiqueta de «peap» según la versión de «peap».
- Realizar comprobaciones de la versión del gestor de redes en tiempo de ejecución (para evitar tiempo de compilación frente a tiempo de ejecución) (fallo 362736).

### Framework de Plasma

- [Calendario] Invertir los botones de flechas para los idiomas que se escriben de derecha a izquierda.
- Plasma::Service::operationDescription() debe devolver un QVariantMap
- No incluir contenedores empotrados en «containmentAt(pos)» (fallo 361777)
- Se ha corregido el tema de color para el icono de reinicio del sistema (pantalla de inicio de sesión) (fallo 364454).
- Desactivar las miniaturas de la barra de tareas con «llvmpipe» (fallo 363371)
- protegerse contra miniaplicaciones no válidas (error 364281)
- PluginLoader::loadApplet: restaurar la compatibilidad para miniaplicaciones mal instaladas
- Carpeta correcta para PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: mejorar el mensaje de error sobre la compatibilidad de la versión del complemento
- Corregir la comprobación para mantener un QMenu en pantalla en las configuraciones multipantalla
- Nuevo tipo de contenedor para la bandeja del sistema

### Solid

- Corregir la comprobación de que la CPU es válida
- Permitir la lectura de /proc/cpuinfo para procesadores ARM
- Encontrar las CPU por el subsistema en lugar de por el controlador

### Sonnet

- Marcar el ejecutable auxiliar como aplicación sin interfaz gráfica
- Permitir que «nsspellcheck» se pueda compilar en mac por omisión

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
