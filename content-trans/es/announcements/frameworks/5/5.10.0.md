---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (no se ha proporcionado ningún registro de cambios)

### KConfig

- Generar clases a prueba de QML usando el kconfigcompiler

### KCoreAddons

- Nueva macro kcoreaddons_add_plugin de cmake para crear complementos basados en KPluginLoader más fácilmente.

### KDeclarative

- Corregir cuelgue en la caché de texturas.
- y otras correcciones

### KGlobalAccel

- Añadir el nuevo método globalShortcut, que obtiene el acceso rápido de teclado que se define en las preferencias globales.

### KIdleTime

- Impedir que kidletime se cuelgue en la plataforma wayland

### KIO

- Se han añadido KPropertiesDialog::KPropertiesDialog(urls) y KPropertiesDialog::showDialog(urls).
- Extracción de datos asíncrona basada en QIODevice para KIO::storedPut y KIO::AccessManager::put.
- Corregir condiciones con el valor de retorno de QFile::rename (error 343329)
- Se ha corregido KIO::suggestName para sugerir mejores nombres (error 341773)
- kioexec: se ha corregido la ruta para ubicaciones con permiso de escritura en kurl (error 343329)
- Guardar marcadores solo en user-places.xbel (error 345174)
- Duplicar la entrada para documentos recientes si dos archivos diferentes tienen el mismo nombre
- Mensaje de error más claro si un archivo es demasiado grande para la papelera (error 332692)
- Corregir un cuelgue de KDirLister sobre redirección cuando se llama a openURL

### KNewStuff

- Nuevo conjunto de clases, llamadas KMoreTools y similares. KMoreTools ayuda a añadir consejos sobre herramientas externas que posiblemente no estén instaladas todavía. Además, acorta los menús grandes proporcionando una o más secciones principales que el usuario puede configurar.

### KNotifications

- Corregir KNotifications cuando se usan con NotifyOSD de Ubuntu (error 345973)
- No emitir actualizaciones de notificaciones cuando se ajustan las mismas propiedades (error 345973)
- Introducir el indicador LoopSound que permite que las notificaciones reproduzcan un sonido en bucle si es necesario (error 346148)
- Evitar un cuelgue si una notificación no posee un elemento gráfico

### KPackage

- Añadir una función KPackage::findPackages similar a KPluginLoader::findPlugins

### KPeople

- Usar KPluginFactory para crear instancias de complementos en lugar de KService (que se mantiene por compatibilidad).

### KService

- Se ha corregido la división incorrecta de la ruta de entrada (error 344614)

### KWallet

- El agente de migración también comprueba ahora si la cartera antigua está vacía antes de iniciar el proceso (error 346498)

### KWidgetsAddons

- KDateTimeEdit: se ha corregido para que la entrada del usuario siempre quede registrada. Se han corregido los márgenes dobles.
- KFontRequester: se ha corregido la selección de de tipos de letra de ancho fijo solo

### KWindowSystem

- No depender de QX11Info en KXUtils::createPixmapFromHandle (error 346496)
- Nuevo método NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Se han corregido los accesos rápidos de teclado cuando se define un acceso rápido secundario (error 345411)
- Actualizar la lista de productos y componentes de bugzilla para informar de errores (error 346559)
- Accesos rápidos globales: permitir configurar también el acceso rápido alternativo

### NetworkManagerQt

- Las cabeceras instaladas se organizan ahora como en el resto de infraestructuras.

### Plasma framework

- PlasmaComponents.Menu permite ahora el uso de secciones
- Usar KPluginLoader en lugar de ksycoca para cargar motores de datos de C++
- Considerar la rotación visualParent en popupPosition (error 345787)

### Sonnet

- No usar el resaltado si no se ha encontrado un corrector ortográfico. Esto conducía a un bucle infinito al repetir constantemente el temporizador rehighlighRequest.

### Integración con Frameworks

- Se han corregido los diálogos de archivos nativos de QFileDialog: ** Los diálogos de archivos creados con exec() y sin padre se abrían, pero cualquier interacción del usuario se bloqueaba y no había modo de seleccionar archivos ni de cerrar el diálogo. ** Los diálogos de archivos creados con open() o con show() ni siquiera se abrían.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
