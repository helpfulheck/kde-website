---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nueva infraestructura: kirigami, un conjunto de complementos de QtQuick para construir interfaces de usuario basados en las directrices de KDE UX.

### Iconos Brisa

- Se han actualizado los colores .h y .h++ (fallo 376680).
- Se ha eliminado el icono monocromo pequeño de ktorrent (fallo 381370).
- Para marcadores se usa un icono de acción, no uno de carpeta (fallo 381383).
- Se ha actualizado el monitor de utilidades del sistema (fallo 381420).

### Módulos CMake adicionales

- Se ha añadido «--gradle» a «androiddeployqt».
- Se ha corregido la instalación del objetivo «apk».
- Se ha corregido el uso de «query_qmake»: diferencias entre llamadas que esperan qmake o no.
- Se ha añadido la API dox para «KDEInstallDirs» KDE_INSTALL_USE_QT_SYS_PATHS.
- Se ha añadido un «metainfo.yaml» para hacer de ECM de make una infraestructura correcta.
- Android: buscar archivos qml en el directorio de origen, no en el directorio de instalación.

### KActivities

- Emitir «runningActivityListChanged» al crear actividades.

### Herramientas KDE Doxygen

- Escapar código HTML en las peticiones de búsqueda.

### KArchive

- Se ha añadido el uso de archivos Conan como primer experimento para implementar Conan.

### KConfig

- Permitir la compilación de KConfig sin Qt5Gui.
- Accesos rápidos estándar: usar Ctr+RePág/AvPág para anterior/siguiente pestaña.

### KCoreAddons

- Se ha eliminado la declaración «init()» no usada de K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL.
- Nueva API de spdx sobre KAboutLicense para obtener expresiones de licencias de SPDX.
- kdirwatch: evitar un cuelgue potencial si se destruye «d-ptr» antes que KDirWatch (fallo 381583).
- Se ha corregido la visualización de «formatDuration» sin redondear (fallo 382069).

### KDeclarative

- Se ha corregido la desasignación de QSG_RENDER_LOOP en plasmashell.

### Soporte de KDELibs 4

- Se ha corregido «La sugerencia desaconsejada para KUrl::path() es errónea en Windows» (fallo 382242).
- Se ha actualizado «kdelibs4support» para usar la implementación basada en objetivos que proporciona kdewin.
- Marcar también constructores como desaconsejados.
- Sincronizar el «KDE4Defaults.cmake» de kdelibs

### KDesignerPlugin

- Se ha añadido la implementación del nuevo widget «kpasswordlineedit».

### KHTML

- Permitir también el uso de SVG (fallo 355872).

### KI18n

- Permitir la carga de catálogos «i18n» desde ubicaciones arbitrarias.
- Asegurar que se genera el objetivo «tsfiles».

### KIdleTime

- Solicitar «Qt5X11Extras» solo cuando sea realmente necesario.

### KInit

- Usar el indicador de funcionalidad correcto para incluir «kill(2)».

### KIO

- Se ha añadido el nuevo método «urlSelectionRequested» a KUrlNavigator.
- KUrlNavigator: exponer el «KUrlNavigatorButton» que ha recibido un evento de soltar.
- Reservar sin preguntar al usuario con una ventana emergente de copiar o cancelar.
- Asegurar que KDirLister actualiza elementos cuya URL de destino ha cambiado (fallo 382341).
- Hacer que las opciones avanzadas del diálogo «abrir con» se puedan contraer y ocultar por omisión (fallo 359233).

### KNewStuff

- Proporcionar un padre a los menús de «KMoreToolsMenuFactory».
- Cuando se solicite por parte de la caché, notificar todas las entradas a la vez.

### Framework KPackage

- «kpackagetool» puede proporcionar salida de datos «appstream» a un archivo.
- Adoptar la nueva «KAboutLicense::spdx».

### KParts

- Reiniciar URL en «closeUrl()».
- Se ha añadido una plantilla para aplicaciones sencillas basadas en kpart.
- Descartar el uso de «KDE_DEFAULT_WINDOWFLAGS».

### KTextEditor

- Gestionar el uso de eventos de rueda minuciosos a hacer zum.
- Se ha añadido una plantilla para complementos de «ktexteditor».
- Copiar permisos del archivo original al guardar una copia (fallo 377373).
- Es posible que se haya solucionado un cuelgue al construir cadenas (fallo 339627).
- Corregir problema al añadir «*» en líneas fuera de los comentarios (fallo 360456).
- Se ha corregido guardar al copiar; carecía de la posibilidad de sobrescribir el archivo de destino (fallo 368145).
- Orden «set-highlight»: Unir argumentos con espacios.
- Se ha corregido un cuelgue al destruir vistas a causa de una limpieza de objetos no determinista.
- Emitir señales desde el borde de los iconos cuando se hace clic en ninguna marca.
- Se ha corregido un cuelgue en el modo de introducción de datos «vi» (secuencia: «o» «Esc» «O» «Esc» «.») (fallo 377852).
- Usar grupo mutuamente exclusivo en el tipo de marca por omisión.

### KUnitConversion

- Marcar MPa y PSI como unidades comunes.

### Framework KWallet

- Usar CMAKE_INSTALL_BINDIR para la generación del servicio DBus.

### KWayland

- Destruir todos los objetos «kwayland» creados por registro cuando este se destruye.
- Emitir «connectionDied» si se destruye el QPA.
- [cliente] Rastrear todos los hilos de conexiones creados y añadir API para acceder a ellos.
- [servidor] Enviar abandono de entrada de texto si la superficie que tiene el foco se libera.
- [servidor] Enviar abandono de puntero si la superficie que tiene el foco se libera.
- [cliente] Rastrear correctamente «enteredSurface» en el teclado.
- [servidor] Enviar abandono de teclado cuando el cliente destruye la superficie que tiene el foco (fallo 382280).
- Comprobar la validez del búfer (fallo 381953).

### KWidgetsAddons

- Extraer el widget de edición de línea de contraseña =&gt; nueva clase «KPasswordLineEdit».
- Se ha corregido un cuelgue que se producía al buscar con la implementación de accesibilidad activada (fallo 374933).
- [KPageListViewDelegate] Pasar widget a «drawPrimitive» en «drawFocus».

### KWindowSystem

- Eliminar la dependencia de QWidget en cabeceras.

### KXMLGUI

- Descartar el uso de «KDE_DEFAULT_WINDOWFLAGS».

### NetworkManagerQt

- Se ha añadido el uso de «ipv*.route-metric».
- Se ha corregido la enumeración NM_SETTING_WIRELESS_POWERSAVE_FOO sin definir (fallo 382051).

### Framework de Plasma

- [Interfaz de contenedores] Emitir siempre «contextualActionsAboutToShow» para los contenedores.
- Tratar las etiquetas de botones y herramientas de botón como texto sin formato.
- No realizar correcciones específicas de wayland en X (fallo 381130).
- Se ha añadido KF5WindowSystem a la interfaz de enlaces.
- Declarar «AppManager.js» como biblioteca «pragma».
- [PlasmaComponents] Se ha eliminado «Config.js».
- Las etiquetas contienen texto sin formato por omisión.
- Cargar las traducciones de los archivos de KPackage si se han empaquetado (fallo 374825).
- [Menú de PlasmaComponents] Se ha corregido un cuelgue al producirse una acción nula.
- [Diálogo de Plasma] Se han corregido condiciones de indicadores.
- Se ha actualizado el icono de akregator en la bandeja del sistema (fallo 379861).
- [Interfaz de contenedores] Mantener el contenedor en «RequiresAttentionStatus» mientras se abre el menú de contexto (fallo 351823).
- Se ha corregido el manejo de teclas en distribuciones de barras de pestañas para RTL (fallo 379894).

### Sonnet

- Permitir la compilación de Sonnet sin Qt5Widgets.
- cmake: Se ha reescrito «FindHUNSPELL.cmake» para que use «pkg-config».

### Resaltado de sintaxis

- Permitir la compilación de «KSyntaxHighlighter» sin Qt5Gui.
- Se ha añadido el uso de compilación cruzada en el indexador de resaltado de sintaxis.
- Temas: Se han eliminado los metadatos no usados (licencia, autor, solo lectura).
- Tema: Se han eliminado los campos de licencia y de autor.
- Tema: Derivar el indicador de solo lectura del archivo de disco.
- Se ha añadido el resaltado de sintaxis para el lenguaje de modelado de datos YANG.
- PHP: Se han añadido 7 palabras clave de PHP (fallo 356383).
- PHP: Se ha limpiado información sobre PHP 5.
- Se ha corregido un error en gnuplot generado por los espacios finales de línea.
- Se ha corregido la detección de «else if»; se necesita cambiar de contexto y añadir una regla adicional.
- Comprobaciones del indexador para los espacios en blanco al final de línea en el resaltado de sintaxis XML.
- Doxygen: Se ha añadido el resaltado de sintaxis para Doxyfile.
- Se han añadido los tipos estándar ausentes en el resaltado de sintaxis de C y se ha actualizado para C11 (fallo 367798).
- Q_PI D =&gt; Q_PID
- PHP: Se ha mejorado el resaltado de sintaxis de las variables entre llaves en las comillas dobles (fallo 382527).
- Se ha añadido el resaltado de sintaxis para PowerShell.
- Haskell: Se ha añadido la extensión de archivo «.hs-boot» (módulo de arranque) (fallo 354629).
- Se ha corregido «replaceCaptures()» para que funciones con más de 9 capturas.
- Ruby: Usar «WordDetect» en lugar de «StringDetect» para la coincidencia de palabras completas.
- Se ha corregido el resaltado de sintaxis incorrecto para «BEGIN» y «END» en palabras como «EXTENDED» (fallo 350709).
- PHP: Se ha eliminado «mime_content_type()» de la lista de funciones desaconsejadas (fallo 371973).
- XML: Se ha añadido la extensión/tipo MIME «XBEL» al resaltado de sintaxis XML (fallo 374573).
- Bash: Se ha corregido el resaltado de sintaxis incorrecto para las opciones de las órdenes (fallo 375245).
- Perl: Se ha corregido el resaltado de sintaxis de «heredoc» con espacios sobrantes en el delimitador (fallo 379298).
- Se ha actualizado el archivo de sintaxis de SQL (Oracle) (fallo 368755).
- C++: Se ha corregido que «-» no forma parte de cadenas UDL (fallo 380408).
- C++: el formato de «printf» indica: añadir «n» y «p», eliminar «P» (fallo 380409).
- C++: Se ha corregido que el valor de «char» tenga el color de las cadenas (fallo 380489).
- VHDL: Se ha corregido un error de resaltado de sintaxis al usar corchetes y atributos (fallo 368897).
- Resaltado de sintaxis de zsh: Se ha corregido el uso de expresiones matemáticas en expresiones de subcadena (fallo 380229).
- Resaltado de sintaxis de JavaScript: Se ha añadido el uso de la extensión «E4X» de XML (fallo 373713).
- Se ha eliminado la regla de la extensión «*.conf».
- Sintaxis de Pug/Jade.

### ThreadWeaver

- Se ha añadido un «export» ausente a «QueueSignals».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
