---
aliases:
- ../../kde-frameworks-5.66.0
date: 2020-01-11
layout: framework
libCount: 70
---
### Todas las infraestructuras

- Adaptar de «QRegExp» a «QRegularExpression».
- Port from qrand to QRandomGenerator
- Fix compilation with Qt 5.15 (e.g. endl is now Qt::endl, QHash insertMulti now requires using QMultiHash...)

### Attica

- Don't use a verified nullptr as a data source
- Support multiple children elements in comment elements
- Set a proper agent string for Attica requests

### Baloo

- Correctly report if baloo_file is unavailable
- Check cursor_open return value
- Initialise QML monitor values
- Move URL parsing methods from kioslave to query object

### BluezQt

- Se ha añadido la interfaz «Battery1».

### Iconos Brisa

- Change XHTML icon to be a purple HTML icon
- Merge headphones and zigzag in the center
- Se ha añadido el icono «application/x-audacity-project» (error 415722).
- Se ha añadido el icono de 32 píxeles para las preferencias del sistema.
- Se ha añadido el icono «application/vnd.apple.pkpass» (error 397987).
- icon for ktimetracker using the PNG in the app repo, to be replaced with real breeze SVG
- add kipi icon, needs redone as a breeze theme svg [or just kill off kipi]

### Módulos CMake adicionales

- [android] Se ha corregido el destino de la instalación de un «apk».
- Support PyQt5 compiled with SIP 5

### Integración con Frameworks

- Remove ColorSchemeFilter from KStyle

### Herramientas KDE Doxygen

- Display fully qualified class/namespace name as page header (bug 406588)

### KCalendarCore

- Improve README.md to have an Introduction section
- Make incidence geographic coordinate also accessible as a property
- Fix RRULE generation for timezones

### KCMUtils

- Desaconsejar el uso de «KCModuleContainer».

### KCodecs

- Fix invalid cast to enum by changing the type to int rather than enum

### KCompletion

- Desaconsejar el uso de «KPixmapProvider».
- [KHistoryComboBox] Add method to set an icon provider

### KConfig

- kconfig EBN transport protocol cleanup
- Expose getter to KConfigWatcher's config
- Fix writeFlags with KConfigCompilerSignallingItem
- Add a comment pointing to the history of Cut and Delete sharing a shortcut

### KConfigWidgets

- Rename "Configure Shortcuts" to "Configure Keyboard Shortcuts" (bug 39488)

### KContacts

- Align ECM and Qt setup with Frameworks conventions
- Specify ECM dependency version as in any other framework

### KCoreAddons

- Se ha añadido «KPluginMetaData::supportsMimeType».
- [KAutoSaveFile] Use QUrl::path() instead of toLocalFile()
- Unbreak build w/ PROCSTAT: add missing impl. of KProcessList::processInfo
- [KProcessList] Optimize KProcessList::processInfo (bug 410945)
- [KAutoSaveFile] Improve the comment in tempFileName()
- Fix KAutoSaveFile broken on long path (bug 412519)

### KDeclarative

- [KeySequenceHelper] Grab actual window when embedded
- Añadir un subtítulo opcional al delegado de la cuadrícula.
- [QImageItem/QPixmapItem] No perder precisión durante los cálculos.

### KFileMetaData

- Solución parcial para los caracteres acentuados en los nombres de archivos en Windows.
- Se han eliminado las declaraciones privadas no necesarias de «taglibextractor».
- Solución parcial para aceptar caracteres acentuados en Windows.
- xattr: fix crash on dangling symlinks (bug 414227)

### KIconThemes

- Set breeze as default theme when reading from configuration file
- Marcar como obsoleta la función de alto nivel «IconSize()».
- Fix centering scaled icons on high dpi pixmaps

### KImageFormats

- pic: Fix Invalid-enum-value undefined behaviour

### KIO

- [KFilePlacesModel] Fix supported scheme check for devices
- Embed protocol data also for Windows version of trash ioslave
- Adding support for mounting KIOFuse URLs for applications that don't use KIO (bug 398446)
- Se ha añadido el uso de truncado a «FileJob».
- Desaconsejar el uso de «KUrlPixmapProvider».
- Marcar como obsoleto «KFileWidget::toolBar».
- [KUrlNavigator] Se ha añadido el uso de RPM a «krarc» (error 408082).
- KFilePlaceEditDialog: Se ha corregido un fallo al editar el lugar «Papelera» (error 415676).
- Add button to open the folder in filelight to view more details
- Show more details in warning dialog shown before starting a privileged operation
- KDirOperator: Use a fixed line height for scroll speed
- Additional fields such as deletion time and original path are now shown in the file properties dialog
- KFilePlacesModel: properly parent tagsLister to avoid memleak. Introduced with D7700
- HTTP ioslave: call correct base class in virtual_hook(). The base of HTTP ioslave is TCPSlaveBase, not SlaveBase
- Ftp ioslave: fix 4 character time interpreted as year
- Volver a añadir «KDirOperator::keyPressEvent» para preservar el «BC».
- Usar «QStyle» para determinar el tamaño de los iconos.

### Kirigami

- ActionToolBar: Only show the overflow button if there are visible items in the menu (bug 415412)
- No compilar ni instalar plantillas de aplicaciones en Android.
- No usar un margen codificado a mano en «CardsListView».
- Permitir el uso de componentes de visualización personalizados en «Action».
- Permitir que el resto de componentes pueda crecer si hay más cosas en la cabecera.
- Se ha eliminado la creación dinámica de elementos en «DefaultListItemBackground».
- Volver a introducir el botón para colapsar (error 415074).
- Mostrar el icono de la ventana de la aplicación en la página de «acerca de».

### KItemModels

- Añadir «KColumnHeadersModel».

### KJS

- Se han añadido pruebas para «Math.exp()».
- Se han añadido pruebas para diversos operadores de asignación.
- Probar casos especiales de operadores de multiplicación (*, / y %).

### KNewStuff

- Asegurar que el título del diálogo es correcto con un motor no inicializado.
- No mostrar el icono de información en el delegado de vista previa grande (error 413436).
- Support archive installs with adoption commands (bug 407687)
- Send along the config name with requests

### KPeople

- Exponer «enum» al compilador de metaobjetos.

### KQuickCharts

- Also correct the shader header files
- Cabeceras de licencia correctas para los sombreadores.

### KService

- Desaconsejar el uso de «KServiceTypeProfile».

### KTextEditor

- Se ha añadido la propiedad «line-count» a «ConfigInterface».
- Avoid unwanted horizontal scrolling (bug 415096)

### KWayland

- [plasmashell] Update docs for panelTakesFocus to make it generic
- [plasmashell] Se ha añadido una señal para el cambio «panelTakesFocus».

### KXMLGUI

- KActionCollection: Proporcionar una señal «changed()» como sustituta de «removed()».
- Se ha ajustado el título de la ventana de configuración de accesos rápidos de teclado.

### NetworkManagerQt

- Gestor: Permitir el uso de «AddAndActivateConnection2».
- CMake: considerar las cabeceras de «NM» como cabeceras del sistema.
- Sincronizar «Utils::securityIsValid» con «NetworkManager» (fallo 415670).

### Framework de Plasma

- [ToolTip] Round position
- Permitir eventos de la rueda del ratón en los deslizadores.
- Sync QWindow flag WindowDoesNotAcceptFocus to wayland plasmashell interface (bug 401172)
- [calendar] Check out of bounds array access in QLocale lookup
- [Plasma Dialog] Use QXcbWindowFunctions for setting window types Qt WindowFlags doesn't know
- [PC3] Complete plasma progress bar animation
- [PC3] Only show progress bar indicator when the ends won't overlap
- [RFC] Fix Display Configuration icon margins (bug 400087)
- [ColorScope] Work with plain QObjects again
- [Breeze Desktop Theme] Add monochrome user-desktop icon
- Remove default width from PlasmaComponents3.Button
- [PC3 ToolButton] Have the label take into account complementary color schemes (bug 414929)
- Added background colors to active and inactive icon view (bug 370465)

### Purpose

- Use standard ECMQMLModules

### QQC2StyleBridge

- [ToolTip] Round position
- Actualizar la sugerencia de tamaño cuando cambia el tipo de letra.

### Solid

- Display first / in mounted storage access description
- Ensure mounted nfs filesystems matches their fstab declared counterpart (bug 390691)

### Sonnet

- The signal done is deprecated in favour of spellCheckDone, now correctly emitted

### Resaltado de sintaxis

- LaTeX: se han corregido las llaves de varias órdenes (fallo 415384).
- TypeScript: add "bigint" primitive type
- Python: improve numbers, add octals, binaries and "breakpoint" keyword (bug 414996)
- SELinux: add "glblub" keyword and update permissions list
- Se ha realizado varias mejoras en la definición de la sintaxis de «gitolite».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
