---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE dobavlja Plasma delovni prostore, aplikacije in platformo 4.11.
title: Zbirka programske opreme KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`KDE Plasma Workspaces 4.11` >}} <br />

14. avgust 2013. Skupnost KDE s ponosom napove najnovejše glavne posodobitve plazme delovnih prostorov, aplikacij in razvojne platforme, ki prinašajo nove funkcije in popravke, hkrati pa pripravlja platformo za nadaljnji razvoj. Delovni prostori plazme 4.11 bodo deležni dolgoročne podpore, saj se skupina osredotoča na tehnični prehod na okvire 5. To nato predstavlja zadnjo kombinirano izdajo delovnih prostorov, aplikacij in platforme pod isto številko različice.<br />

Ta izdaja je posvečena spominu na <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atula 'toolz’-a Chitnisa</a>, velikega prvaka prostega in odprto-kodnega programja iz Indije. Atul je vodil konference Linux Bangalore in FOSS.IN od leta 2001 in obe sta bili ključna dogodka na indijskem prizorišču FOSS. KDE Indija se je rodila na prvi konferenci FOSS.in decembra 2005. Na teh dogodkih so začeli številni indijski avtorji prispevkov KDE. Samo zaradi Atulovega spodbujanja je bil projektni dan KDE v FOSS.IN vedno velik uspeh. Atul nas je zapustil 3. junija po boju z rakom. Naj njegova duša počiva v miru. Hvaležni smo za njegov prispevek k boljšemu svetu.

Vse te izdaje so prevedene v 54 jezikov; pričakujemo, da bodo KDE bo v naslednjih mesečnih izdajah dodano še več jezikov in manjših popravkov napak. Skupina za dokumentacijo je za to izdajo posodobila 91 priročnikov za uporabo.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma Workspaces 4.11 še naprej izboljšuje uporabniško izkušnjo</a>

Kot priprava za dolgoročno vzdrževanje Plasma Workspaces prinaša nadaljnje izboljšave osnovne funkcionalnosti z bolj gladko opravilno vrstico, pametnejšim pripomočkom za baterijo in izboljšanim mešalnikom zvoka. Uvedba KScreen prinaša inteligentno upravljanje z več monitorji v delovne prostore, obsežne izboljšave zmogljivosti v kombinaciji z majhnimi popravki uporabnosti pa zagotavljajo na splošno lepšo izkušnjo.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/>Aplikacije KDE 4.11 prinašajo ogromen korak naprej pri upravljanju osebnih podatkov in splošne izboljšave</a>

Ta izdaja zaznamuje velike izboljšave sklada KDE PIM, kar daje veliko boljšo zmogljivost in številne nove zmožnosti. Kate izboljšuje produktivnost razvijalcev Python in Javascript z novimi vtičniki, Dolphin je postal hitrejši in izobraževalne aplikacije prinašajo različne nove zmožnosti.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/>Platforma KDE 4.11 zagotavlja boljšo zmogljivost</a>

Ta izdaja platforme KDE 4.11 se še naprej osredotoča na stabilnost. Nove zmožnosti se izdelujejo za našo prihodnjo izdajo KDE Frameworks 5.0, toda za stabilno izdajo smo uspeli stisniti optimizacije za naš okvir Nepomuk.

<br />
Pri nadgradnji upoštevajte <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>opombe ob izdaji</a>.<br />

## Širite besedo in poglejte, kaj se dogaja: označujte s &quot;KDE&quot;

KDE spodbuja vse, da širite besedo na družbenih omrežjih. Pošiljajte zgodbe na strani z novicami, uporabljajte kanale, kot so delicious, digg, reddit, twitter, identi.ca. Naložite posnetke zaslona v storitve, kot so Facebook, Flickr, ipernity in Picasa, in jih objavite v ustreznih skupinah. Ustvarite posnetke zaslona in jih naložite na YouTube, Blip.tv in Vimeo. Označite objave in naloženo gradivo s &quot;KDE&quot;. Tako jih je enostavno najti in s tem nudite promocijski skupini KDE način za analizo pokritosti o izdaji programske opreme KDE 4.11.

## Zabave ob izidu

Kot običajno člani skupnosti KDE organizirajo zabave ob izdaji po vsem svetu. Veliko jih je že načrtovanih, še več pa jih bo prišlo kasneje. Poiščite <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>seznam zabav tukaj</a>. Vabljeni k sodelovanju! Kombinacija zanimivega druženja in navdihujočih pogovorov ter hrane in pijače. To je odlična priložnost, da izveste več o tem, kaj se dogaja v KDE, da se vključite ali preprosto spoznate druge uporabnike in sodelavce.

Spodbujamo ljudi, da organizirajo svoje zabave. Zabavno jih je gostiti in so odprte za vse! Preverite <a href='http://community.kde.org/Promo/Events/Release_Parties'>nasvete, kako organizirati zabavo</a> .

## O teh objavah izdaje

Te objave izdaje so pripravili Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin in drugi člani ekipe za promocijo KDE in širše skupnosti KDE. Pokrivajo poudarke številnih sprememb programske opreme KDE v zadnjih šestih mesecih.

#### Podprite KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE eV novi <a href='http://jointhegame.kde.org/'>program podpornih članov</a> je zdaj odprt. Za 25 EUR na četrtletje lahko zagotovite, da se mednarodna skupnost KDE še naprej razvija in ustvarja prosto programsko opremo svetovnega razreda.
