---
aliases:
- ../announce-applications-18.08-beta
date: 2018-07-20
description: KDE publie la version 18.08 « bêta » des applications de KDE.
layout: application
release: applications-18.07.80
title: KDE publie la version « bêta » pour les applications de KDE 18.08.
---
20 Juillet 2018. Aujourd'hui, KDE a publié la version « bêta » des nouvelles versions des applications de KDE. Les dépendances et les fonctionnalités sont stabilisées et l'équipe KDE se concentre à présent sur la correction des bogues et sur les dernières finitions.

Veuillez vérifier les <a href='https://community.kde.org/Applications/18.08_Release_Notes'>notes de mises à jour de la communauté</a> pour plus d'informations sur les nouveaux fichiers compressés et les problèmes connus. Une annonce plus complète sera disponible avec la mise à jour finale.

Les versions des applications de KDE 18.08 ont besoin de tests intensifs pour maintenir et améliorer la qualité et l'interface utilisateur. Les utilisateurs actuels sont très importants pour maintenir la grande qualité de KDE. En effet, les développeurs ne peuvent tester toutes les configurations possibles. Votre aide est nécessaire pour aider à trouver les bogues suffisamment tôt pour qu'ils puissent être corrigés avant la version finale. Veuillez contribuer à l'équipe en installant la version « bêta » et <a href='https://bugs.kde.org/'>en signalant tout bogue</a>.
