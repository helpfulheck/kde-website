---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE publie la version 19.08 des applications.
layout: application
release: applications-19.08.0
title: KDE publie les applications de KDE en version 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

La communauté KDE est heureuse d'annoncer la publication d'applications de KDE en version 19.08.

Cette version reflète l'engagement de KDE à fournir continuellement des versions améliorées des programmes que nous livrons à nos utilisateurs. Les nouvelles versions des Applications apportent plus de fonctionnalités et des logiciels mieux conçus qui augmentent la facilité d'utilisation et la stabilité d'applications comme Dolphin, Konsole, Kate, Okular, et tous vos autres utilitaires KDE préférés. Notre objectif est de nous assurer que vous restez productif, et de rendre les logiciels KDE plus faciles et plus agréables à utiliser.

KDE espère que vous serez enchanté par toutes les nouvelles améliorations que vous trouverez dans 19.08 !

## Quoi de neuf dans les applications de KDE 19.08

Plus de 170 bogues ont été corrigés. Ces corrections implémentent à nouveau des fonctionnalités désactivées, homogénéisent les raccourcis et éliminent des plantages. Elles rendent les applications de KDE plus conviviales et vous permettent d'être plus productif.

### Dolphin

Dolphin est l'explorateur de fichiers et de dossiers de KDE. V pouvez maintenant le lancer de n'importe où en utilisant le nouveau raccourci global <keycap>Meta + E</keycap>. Il y a aussi une nouvelle fonctionnalité qui minimise l'encombrement sur votre bureau. Lorsque Dolphin est déjà en cours d'exécution, si vous ouvrez des dossiers avec d'autres applications, ces dossiers s'ouvriront dans un nouvel onglet de la fenêtre existante au lieu d'une nouvelle fenêtre Dolphin. Notez que ce comportement maintenant activé par défaut peut être désactivé.

Le panneau d'information (situé par défaut à droite du panneau principal de Dolphin) a été amélioré. Vous pouvez, par exemple, choisir de lire automatiquement les fichiers multimédia lorsque vous les mettez en évidence dans le panneau principal, et vous pouvez maintenant sélectionner et copier le texte affiché dans le panneau. Si vous voulez changer ce que le panneau d'information montre, vous pouvez le faire directement dans le panneau, car Dolphin n'ouvre pas une fenêtre séparée lorsque vous choisissez de configurer le panneau.

Plusieurs coupures d'affichage et de petits bogues ont été aussi corrigés, afin de vous offrir une ergonomie parfaitement homogène lors de l'utilisation de Dolphin.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Nouvelle fonctionnalité de signets pour Dolphin` caption=`Nouvelle fonctionnalité de signets pour Dolphin` width="600px" >}}

### Gwenview

Gwenview est l'afficheur d'images de KDE. Dans cette version, les développeurs ont amélioré sa fonction d'affichage des vignettes de manière générale. Gwenview peut désormais utiliser un mode « faible utilisation des ressources » affichant les vignettes en basse résolution (lorsqu'elles sont disponibles). Ce nouveau mode est beaucoup plus rapide et plus économe en ressources lors du chargement des vignettes des images « JPEG » et des fichiers « RAW ». Dans les cas où Gwenview ne peut pas générer de vignette pour une image, il affiche désormais une image de remplacement plutôt que de réutiliser la vignette de l'image précédente. Les problèmes que Gwenview rencontrait pour afficher les vignettes des appareils photo Sony et Canon ont également été résolus.

Outre les changements apportés au département des vignettes, Gwenview a également mis en place un nouveau menu « Partager » qui permet d'envoyer des images à divers endroits, et de charger et d'afficher correctement les fichiers dans les emplacements distants auxquels on accède à l'aide de KIO. La nouvelle version de Gwenview affiche également beaucoup plus de métadonnées EXIF pour les images RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Nouveau menu « Partage » de Gwenview` caption=`Nouveau menu « Partage » de Gwenview` width="600px" >}}

### Okular

Les développeurs ont apporté de nombreuses améliorations aux annotations dans Okular, l'afficheur de documents de KDE. Outre l'amélioration de l'interface utilisateur pour les boîtes de dialogue de configuration des annotations, les annotations de ligne peuvent désormais être agrémentées de divers éléments visuels, permettant de les transformer en flèches, par exemple. Les annotations peuvent également maintenant être développées et réduites en une seule fois.

La prise en charge des documents EPub par Okular a également été améliorée dans cette version. Okular ne plante plus lorsqu'il tente de charger des fichiers ePub corrompus, et ses performances avec les fichiers ePub volumineux ont été considérablement améliorées. La liste des modifications apportées dans cette version comprend l'amélioration des bordures de page et l'outil Marqueur du mode Présentation en mode High DPI.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Paramètres d'Okular pour l'outil d'annotation avec une nouvelle option pour la fin de ligne` caption=`Paramètres d'Okular pour l'outil d'annotation avec une nouvelle option pour la fin de ligne` width="600px" >}}

### Kate

Grâce à nos développeurs, trois bugs gênants ont été éliminés dans cette version de l'éditeur de texte avancé de KDE. Kate met à nouveau en avant sa fenêtre existante lorsqu'on lui demande d'ouvrir un nouveau document depuis une autre application. La fonction « Ouverture rapide » trie les éléments par ordre d'utilisation récente et présélectionne l'élément principal. Le troisième changement concerne la fonction « Documents récents », fonctionnant désormais lorsque la configuration actuelle est configurée pour ne pas enregistrer les paramètres des fenêtres individuelles.

### Konsole

Le changement le plus notable dans Konsole, l'application émulateur de terminal de KDE, est l'amélioration de la fonction de tuilage. Vous pouvez maintenant diviser le volet principal comme vous le souhaitez, à la fois verticalement et horizontalement. Les sous-volets peuvent ensuite être divisés à nouveau comme vous le souhaitez. Cette version ajoute également la possibilité de faire glisser et de déposer les volets, de sorte que vous pouvez facilement réorganiser la mise en page en fonction de votre flux de travail.

A côté de cela, la fenêtre de configuration a reçu un remaniement pour la rendre plus claire et plus facile à utiliser.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle est l'application de copie d'écran de KDE et elle bénéficie de plus en plus de fonctionnalités intéressantes à chaque nouvelle version. Cette version ne fait pas exception, puisque Spectacle est maintenant livrée avec plusieurs nouvelles fonctionnalités qui régulent sa fonctionnalité de retardement. Lorsque vous effectuez une copie d'écran différée, Spectacle affiche le temps restant dans le titre de sa fenêtre. Cette information est également visible dans l'élément du Gestionnaire des tâches.

Toujours en ce qui concerne la fonction de retardement, le bouton du gestionnaire des tâches de Spectacle affiche également une barre de progression, ce qui vous permet de savoir quand la photo sera prise. Enfin, si vous dé-minimisez Spectacle pendant que vous attendez, vous verrez que le bouton « Prendre une nouvelle copie d'écran » s'est transformé en bouton « Annuler ». Ce bouton contient également une barre de progression, vous donnant la possibilité d'arrêter le compte à rebours.

L'enregistrement des copies d'écran a également une nouvelle fonctionnalité. Lorsque vous avez enregistré une copie d'écran, Spectacle affiche un message dans l'application qui vous permet d'ouvrir la copie d'écran ou le dossier qui la contient.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, la suite de programmes de messagerie / calendrier / contacts et de logiciels de groupe de travail de KDE, apporte au compositeur de courriels, la prise en charge des émoticônes en couleurs « Unicode » et du langage « Markdown ». Non seulement la nouvelle version de KMail vous permettra d'améliorer l'apparence de vos messages, mais, grâce à l'intégration de correcteurs grammaticaux tels que LanguageTool et Grammalecte, elle vous aidera à vérifier et à corriger votre texte.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Sélecteur d'émoticônes` caption=`Sélecteur d'émoticônes` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Intégration du vérificateur grammatical dans KMail` caption=`Intégration du vérificateur grammatical dans KMail` width="600px" >}}

Lors de la planification d'évènements, les courriels d'invitation dans KMail ne sont plus supprimés après y avoir répondu. Il est désormais possible de déplacer un évènement d'un calendrier à un autre dans l'éditeur d'évènements de KOrganizer.

Enfin, KAddressBook peut désormais envoyer des messages SMS à ses contacts par le biais de KDE Connect, ce qui permet une intégration plus pratique de votre ordinateur de bureau et de vos appareils mobiles.

### Kdenlive

La nouvelle version de Kdenlive, le logiciel de montage vidéo de KDE, dispose d'un nouvel ensemble de combinaisons clavier-souris qui vous aideront à être plus productif. Vous pouvez, par exemple, modifier la vitesse d'un clip dans la ligne de temps en appuyant sur « CTRL » et en faisant glisser le curseur sur le clip, ou activer l'aperçu des vignettes des clips vidéo en maintenant la touche « MAJ » enfoncée et en déplaçant la souris sur une vignette de clip dans le panier du projet. Les développeurs ont également fait de gros efforts en matière de convivialité en rendant les opérations de montage en 3 points cohérentes avec celles des autres éditeurs vidéo, ce que vous apprécierez sûrement si vous passez à Kdenlive depuis un autre éditeur.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
