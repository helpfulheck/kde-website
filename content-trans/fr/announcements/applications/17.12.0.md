---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE publie les applications de KDE en version 17.12.0
layout: application
title: KDE publie les applications de KDE en version 17.12.0
version: 17.12.0
---
14 Décembre 2017. Les applications de KDE 17.12.0 ont été mises à jour.

Nous travaillons de façon continue sur l'amélioration des logiciels dont la série d'applications de KDE. Nous espérons que vous trouverez toutes les nouvelles améliorations et les corrections de bogues utiles ! 

### Quoi de neuf dans les applications de KDE 17.12

#### Système

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, notre gestionnaire de fichiers, peut désormais enregistrer les recherches et limiter la recherche uniquement aux dossiers. Renommer des fichiers est désormais plus facile ; il suffit de double-cliquer sur le nom du fichier. Vous disposez désormais de plus d'informations sur les fichiers, puisque la date de modification et l'URL d'origine des fichiers téléchargés sont désormais affichées dans le panneau d'information. De plus, de nouvelles colonnes Genre, Bitrate et Année de création ont été introduites.

#### Graphiques

A notre puissant visualiseur de documents <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> a été ajouté la prise en charge des écrans HiDPI et du langage Markdown/ Le rendu des documents lents à charger s'affiche désormais progressivement. Une option est désormais disponible pour partager un document par e-mail.

L'afficheur d'images <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> peut désormais ouvrir et mettre en valeur les images dans le gestionnaire de fichiers, le zoom est plus fluide, la navigation au clavier a été améliorée et il prend désormais en charge les formats « FITS » et « Truevision TGA ». Les images sont désormais protégées contre une suppression accidentelle par la touche « Suppr » lorsqu'elles ne sont pas sélectionnées.

#### Multimédia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> utilise désormais moins de mémoire lors de la gestion de projets vidéo comprenant de nombreuses images, les profils de proxy par défaut ont été affinés, et un bug gênant lié au saut d'une seconde en avant lors de la lecture en arrière a été corrigé.

#### Utilitaires

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

Le support zip de <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> dans le backend libzip a été amélioré. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> a un nouveau <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>Preview plugin</a> qui vous permet de voir un aperçu en direct du document texte dans le format final, en appliquant tous les plugins KParts disponibles (par exemple pour Markdown, SVG, Dot graph, Qt UI, ou des correctifs). Ce plugin fonctionne également dans <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Développement

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> propose désormais un menu contextuel dans la zone de "diff", permettant un accès plus rapide aux actions de navigation ou de modification. Si vous êtes un développeur, vous trouverez utile le nouvel aperçu dans le volet de KUIViewers de l'objet UI décrit par les fichiers Qt UI (widgets, dialogues, etc). Il supporte maintenant aussi l'API de streaming KParts.

#### Bureau

L'équipe <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> a travaillé intensément pour améliorer et affiner cette nouvelle version. Une grande partie du travail a consisté à moderniser le code, mais les utilisateurs remarqueront également que l'affichage des messages cryptés a été amélioré et que le support a été ajouté pour les text/pgp et <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Il y a maintenant une option pour sélectionner le dossier IMAP pendant la configuration des vacances, un nouvel avertissement dans KMail lorsqu'un courrier est rouvert et que l'identité/le transport de courrier n'est pas le même, un nouveau <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>support pour Microsoft® Exchange™</a>, le support pour Nylas Mail et l'importation Geary améliorée dans l'assistant d'importation akonadi-import, ainsi que diverses autres corrections de bogues et améliorations générales.

#### Jeux

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> peut désormais toucher un public plus large, puisqu'il a été <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>porté sur Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a>, et <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> complètent le portage des jeux KDE sur Frameworks 5.

### Plus de portage vers l'environnement de développement version 5

Encore plus d'applications qui étaient basées sur kdelibs4 ont maintenant été portées sur KDE Frameworks 5. Il s'agit notamment du lecteur de musique <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, du gestionnaire de téléchargement <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, des utilitaires tels que <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> et <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, et <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> et Zeroconf-ioslave. Un grand merci aux développeurs assidus qui ont donné de leur temps et de leur travail!

### Passage des applications vers leurs propres calendriers de publication

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> a maintenant son propre calendrier de sortie ; consultez ce <a href='https://knro.blogspot.de'>blog du développeur</a> pour les annonces. Il convient de noter que plusieurs applications telles que Kopete et Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>ne sont plus livrées</a> avec la série Application, car elles n'ont pas encore été portées vers KDE Frameworks 5, ou ne sont pas activement maintenues pour le moment.

### Éradication des bogues

Plus de 110 corrections de bogues ont été apportées dans les applications dont la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello et bien d'autres !

### Journal complet des changements

Si vous voulez en savoir plus à propos des modifications dans cette version, veuillez consulter la <a href='/announcements/changelogs/applications/17.12.0'>note complète de version </a>. Même si elle parait intimidante par sa longueur, elle représente une façon excellente d'en apprendre plus sur les travaux internes de KDE et de découvrir les applications et les fonctionnalités que jamais vous n'auriez cru avoir.
