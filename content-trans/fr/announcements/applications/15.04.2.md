---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE publie KDE Applications 15.04.2
layout: application
title: KDE publie KDE Applications 15.04.2
version: 15.04.2
---
02 juin 2015. Aujourd'hui, KDE a publié la seconde mise à jour de consolidation pour les <a href='../15.04.0'>applications 15.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde

Plus de 30 corrections de bogues apportent des améliorations à gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui et umbrello.

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.20, de la plate-forme de développement 4.14.9 et de la suite Kontact4.14.9.
