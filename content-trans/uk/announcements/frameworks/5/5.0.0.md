---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE випущено перший випуск набору бібліотек Frameworks 5.
layout: framework
qtversion: 5.2
title: Перший випуск набору бібліотек KDE Frameworks 5
---
7 липня 2014 року. Спільнота KDE з гордістю оголошує про випуск KDE Frameworks 5.0. Frameworks 5 — нове покоління бібліотек KDE, поділених на модулі і оптимізованих для спрощення інтеграції із програмами на основі Qt. У Frameworks реалізовано широкий спектр загальних функціональних можливостей, код бібліотек було ретельно перевірено, самі бібліотеки пройшли тестування. Умови ліцензування бібліотек є необтяжливими. У цьому випуску понад 50 різних бібліотек, за допомогою яких можна здійснювати інтеграцію з обладнанням, передбачено підтримку роботи з файлами у різних форматах, додаткові віджети, функції для побудови графіків, перевірки правопису тощо. Багато з бібліотек Frameworks можуть працювати на багатьох програмних платформах, не мають зовнішніх залежностей або мають мінімальні залежності, що полегшує їхнє збирання та додавання до будь-якої програми Qt.

Бібліотеки  KDE Frameworks створено у межах програми з перетворення потужних бібліотек платформи KDE 4 на набір незалежних багатоплатформових модулів, якими зможуть безпосередньо користуватися розробники Qt для спрощення, пришвидшення та зменшення об'єму зусиль, необхідних для розробки Qt. Окремі бібліотеки Frameworks є багатоплатформовими, добре документованими та тестованими, ними буде просто користуватися розробникам Qt, оскільки їх створено відповідно до стилю і стандартів проєкту Qt. Frameworks розроблено відповідно до надійної моделі керування KDE з передбачуваним розкладом випуском, прозорим та нейтральним щодо розробників процесом, відкритим керуванням та гнучким ліцензуванням (LGPL).

У Frameworks зрозуміла структура залежностей, поділена на категорії і рівні. Категорії стосуються динамічних залежностей:

- <strong>Функціональні</strong> елементи не мають динамічних залежностей.
- <strong>Інтеграція</strong> позначає код, який може потребувати динамічних залежностей для інтеграції, залежно від того, що пропонує операційна система або платформа.
- <strong>Рішення</strong> мають обов'язкові динамічні залежності.

<strong>Рівні</strong> стосуються залежностей під час збирання від інших Frameworks. Frameworks рівня 1 не мають залежностей від Frameworks і потребують для роботи Qt та інших пов'язаних із нею бібліотек. Frameworks рівня 2 можуть залежати лише від рівня 1. Frameworks рівня 3 можуть залежати від інших Frameworks рівня 3, а також від рівнів 2 та 1.

Перехід з платформи на Frameworks тривав понад 3 роки. Керівництво здійснювалося найкращими технічними спеціалістами KDE. Більше про Frameworks 5 можна дізнатися з <a href='http://dot.kde.org/2013/09/25/frameworks-5'>цієї статті, датованої попереднім роком</a>.

## Основні моменти

У поточній версії понад 50 бібліотек Frameworks. З повним списком можна ознайомитися за допомогою <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>документації з програмного інтерфейсу у інтернеті</a>. Нижче наведено відомості щодо частини функціональних можливостей Frameworks, призначених для розробників програм на основі Qt.

<strong>KArchive</strong> є самодостатньою, багатою на можливості і простою у користуванні бібліотекою для архівування та видобування файлів багатьох популярних форматів архівів. Достатньо просто передати цій бібліотеці відповідні файли, вам не доведеться повторно створювати набір функцій для роботи з архівами у вашій програмі на основі Qt!

<strong>ThreadWeaver</strong> — високорівневий програмний інтерфейс для керування потоками виконання з інтерфейсами для роботи з завданнями та чергами завдань. За його допомогою дуже просто виконується планування виконання потоку дій: достатньо вказати залежності між потоками, і потоки буде виконано з врахуванням цих залежностей, що значно спрощує використання у програмі багатопоточності.

<strong>KConfig</strong> є бібліотекою Framework для зберігання та отримання параметрів налаштування програми. Її програмний інтерфейс зорієнтовано на групи. Вона працює з файлами INI та сумісними з XDG вкладеними каталогами. Код налаштувань створюється на основі файлів XML.

<strong>Solid</strong> виконує виявлення обладнання і може інформувати програму щодо пристроїв зберігання даних, томів, процесора, стану акумулятора, керування живленням, стану мережі та інтерфейсів, а також Bluetooth. Для роботи із зашифрованими розділами, живленням та мережею у системі мають працювати відповідні фонові служби.

<strong>KI18n</strong> реалізує у програмах підтримку Gettext, що спрощує інтеграцію системи перекладу програм Qt до загальної інфраструктури перекладу багатьох проєктів.
