---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE에서 KDE 프로그램 18.08.1 출시
layout: application
title: KDE에서 KDE 프로그램 18.08.1 출시
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Cantor, Gwenview, Okular, Umbrello 등에 여러 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- 장치를 다른 프로그램에서 접근할 때 KIO-MTP 구성 요소가 더 이상 충돌하지 않음
- KMail에서 메일을 보낼 때 암호 프롬프트에 입력한 암호를 사용함
- Okular에서 PDF 문서를 저장할 때 사이드바 모드를 기억함
