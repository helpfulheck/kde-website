---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: KDE Aplikazioak 15.12 argitalpen hautagaia kaleratzen du.
layout: application
release: applications-15.11.90
title: KDEk, KDE Aplikazioak 15.12 argitalpen hautagaia kaleratu du
---
December 3, 2015. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

With the various applications being based on KDE Frameworks 5, the KDE Applications 15.12 the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
