---
version: "5.19.90"
title: "KDE Plasma 5.19.90, Beta Release"
type: info/plasma5
signer: Bhushan Shah
signing_fingerprint: FE0784117FBCE11D
---

This is a beta release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.19.90 announcement](/announcements/plasma/5/5.19.90).
