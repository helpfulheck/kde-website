---
version: "5.6.3"
title: "Plasma 5.6.3 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.6_Errata
    name: 5.6 Errata
type: info/plasma5
---

This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.6.3">Plasma 5.6.3 announcement</a>.
