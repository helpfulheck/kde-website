<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeaccessibility-4.4.80.tar.bz2">kdeaccessibility-4.4.80</a></td><td align="right">5.4MB</td><td><tt>6643ea59f92a28e2a0d03024d138047f1ac3e05b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeadmin-4.4.80.tar.bz2">kdeadmin-4.4.80</a></td><td align="right">1.4MB</td><td><tt>297574770d4decde400d1fbf4491ee84c2d36c0b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeartwork-4.4.80.tar.bz2">kdeartwork-4.4.80</a></td><td align="right">70MB</td><td><tt>68bfa2c11f537cbc023f6174330b8bdb35484385</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdebase-4.4.80.tar.bz2">kdebase-4.4.80</a></td><td align="right">2.5MB</td><td><tt>5973ad5aeca42090a48f8d0e6a1863fe5eb75149</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdebase-runtime-4.4.80.tar.bz2">kdebase-runtime-4.4.80</a></td><td align="right">5.5MB</td><td><tt>d32ba474d6adc4c1e5a8c0756f2771c87697d8b9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdebase-workspace-4.4.80.tar.bz2">kdebase-workspace-4.4.80</a></td><td align="right">70MB</td><td><tt>a31fcf38d6779139f01cab3116940789078d62fd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdebindings-4.4.80.tar.bz2">kdebindings-4.4.80</a></td><td align="right">4.8MB</td><td><tt>8ecb3a86363c85f854f3fb90cb1e91dc42528a76</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeedu-4.4.80.tar.bz2">kdeedu-4.4.80</a></td><td align="right">59MB</td><td><tt>4eb993f13455975b82468ef7cc2c46f8b7e8f2de</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdegames-4.4.80.tar.bz2">kdegames-4.4.80</a></td><td align="right">55MB</td><td><tt>7c4132f36ed0c2de794eb020b753dc00ea4254c1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdegraphics-4.4.80.tar.bz2">kdegraphics-4.4.80</a></td><td align="right">3.7MB</td><td><tt>d968dc9278b073e62247bd11fa4bc925abb31c4d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdelibs-4.4.80.tar.bz2">kdelibs-4.4.80</a></td><td align="right">14MB</td><td><tt>8e3da3b72cf32dd73428b9b9f684d2bb9207baa3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdemultimedia-4.4.80.tar.bz2">kdemultimedia-4.4.80</a></td><td align="right">1.5MB</td><td><tt>1ea8ffecb387d34966e23f6b459e5cf47574345d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdenetwork-4.4.80.tar.bz2">kdenetwork-4.4.80</a></td><td align="right">7.9MB</td><td><tt>0fec0b209976af8d58b6aedabd819fdfd32147cf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdepimlibs-4.4.80.tar.bz2">kdepimlibs-4.4.80</a></td><td align="right">2.5MB</td><td><tt>0184d7146a9ed4fd741d616e085657ffeaf06116</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeplasma-addons-4.4.80.tar.bz2">kdeplasma-addons-4.4.80</a></td><td align="right">1.7MB</td><td><tt>52e1701d8eaa03498b27b688525b3b512ea1adfc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdesdk-4.4.80.tar.bz2">kdesdk-4.4.80</a></td><td align="right">5.5MB</td><td><tt>93cc29ea74c269822b96172153471947dfd36ce7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdetoys-4.4.80.tar.bz2">kdetoys-4.4.80</a></td><td align="right">400KB</td><td><tt>7a3d28c0fbaef54d54a2b0b42f18f33a832b9c58</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdeutils-4.4.80.tar.bz2">kdeutils-4.4.80</a></td><td align="right">3.6MB</td><td><tt>00f597712bab2d8556dae46d6663ba5e54af99ff</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/kdewebdev-4.4.80.tar.bz2">kdewebdev-4.4.80</a></td><td align="right">2.1MB</td><td><tt>73507fb9d9fb2d6dd2bd2c4af4a45b3b0cdb11f2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.80/src/oxygen-icons-4.4.80.tar.bz2">oxygen-icons-4.4.80</a></td><td align="right">131MB</td><td><tt>219d8c0bb3a1109af71a4248cfc0e05895bdf63c</tt></td></tr>
</table>
