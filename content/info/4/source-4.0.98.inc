<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeaccessibility-4.0.98.tar.bz2">kdeaccessibility-4.0.98</a></td><td align="right">6,1MB</td><td><tt>853044d27a1645ae1f995c2ced7eb237</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeadmin-4.0.98.tar.bz2">kdeadmin-4.0.98</a></td><td align="right">1,8MB</td><td><tt>458230ce937bdf9723809f63bb66767c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeartwork-4.0.98.tar.bz2">kdeartwork-4.0.98</a></td><td align="right">41MB</td><td><tt>7a86d32c99eb0360fea32154b30a9254</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdebase-4.0.98.tar.bz2">kdebase-4.0.98</a></td><td align="right">4,3MB</td><td><tt>43a025d53f4d8a280db9c8b7da238bf6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdebase-runtime-4.0.98.tar.bz2">kdebase-runtime-4.0.98</a></td><td align="right">50MB</td><td><tt>564def1536a1cd646b0f8bd531db251b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdebase-workspace-4.0.98.tar.bz2">kdebase-workspace-4.0.98</a></td><td align="right">46MB</td><td><tt>a54ffc831d940955443d35b11f5879a7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdebindings-4.0.98.tar.bz2">kdebindings-4.0.98</a></td><td align="right">4,4MB</td><td><tt>254b290f8a19003422f5c1eb625a95d5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeedu-4.0.98.tar.bz2">kdeedu-4.0.98</a></td><td align="right">53MB</td><td><tt>43de6b77aed869ea54441d7395668926</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdegames-4.0.98.tar.bz2">kdegames-4.0.98</a></td><td align="right">31MB</td><td><tt>cdaa3925bef6ca32ff34866d772de1d5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdegraphics-4.0.98.tar.bz2">kdegraphics-4.0.98</a></td><td align="right">3,3MB</td><td><tt>2788ec617b07b2babd05ac601cf584d2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdelibs-4.0.98.tar.bz2">kdelibs-4.0.98</a></td><td align="right">8,8MB</td><td><tt>983c49b3e16add5809b39427f2c4ee4e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdemultimedia-4.0.98.tar.bz2">kdemultimedia-4.0.98</a></td><td align="right">1,4MB</td><td><tt>bf0be13f4c02c23c06af182325d5caf0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdenetwork-4.0.98.tar.bz2">kdenetwork-4.0.98</a></td><td align="right">7,1MB</td><td><tt>5e0bdd2209fb9f569e8f6c713481b277</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdepim-4.0.98.tar.bz2">kdepim-4.0.98</a></td><td align="right">13MB</td><td><tt>71d7396490f27ca4c5d9687515a3d8a9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdepimlibs-4.0.98.tar.bz2">kdepimlibs-4.0.98</a></td><td align="right">1,9MB</td><td><tt>acebf9f4f9883d8a1251f6a588304009</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeplasma-addons-4.0.98.tar.bz2">kdeplasma-addons-4.0.98</a></td><td align="right">3,9MB</td><td><tt>26f22c591af158c4f4d716f0be904089</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdesdk-4.0.98.tar.bz2">kdesdk-4.0.98</a></td><td align="right">4,7MB</td><td><tt>61d8b51f1102c40f5a4ad9008b7c898c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdetoys-4.0.98.tar.bz2">kdetoys-4.0.98</a></td><td align="right">1,3MB</td><td><tt>b01dfffe32605a33cb014b3ee9ede401</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdeutils-4.0.98.tar.bz2">kdeutils-4.0.98</a></td><td align="right">2,1MB</td><td><tt>e6536b7b8e52953ebe80ae5dd6e32abb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.0.98/src/kdewebdev-4.0.98.tar.bz2">kdewebdev-4.0.98</a></td><td align="right">2,5MB</td><td><tt>ddb2fc65adac4d8ee9defd35d1b76e49</tt></td></tr>
</table>
