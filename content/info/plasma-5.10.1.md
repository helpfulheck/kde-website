---
version: "5.10.1"
title: "KDE Plasma 5.10.1, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.10_Errata
    name: 5.10 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.10.1">Plasma 5.10.1 announcement</a>.
