-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1



KDE Security Advisory: PS/PDF file handling vulnerability
Original Release Date: 2003-04-09
URL: http://www.kde.org/info/security/advisory-20030409-1.txt

0. References
	http://bugs.kde.org/show_bug.cgi?id=53157
	http://bugs.kde.org/show_bug.cgi?id=53343
	http://bugs.kde.org/show_bug.cgi?id=56808

1. Systems affected:

	All KDE 2 and KDE 3 versions up to and including KDE 3.1.1.

2. Overview:

	KDE uses Ghostscript software for processing of PostScript (PS) 
	and PDF files in a way that allows for the execution of arbitrary 
	commands that can be contained in such files.

	An attacker can prepare a malicious PostScript or PDF file which will 
	provide the attacker with access to the victim's account and privileges 
	when the victim opens this malicious file for viewing or when the 
	victim browses a directory containing such malicious file and has
	file previews enabled.

	An attacker can provide malicious files remotely to a victim in an 
	e-mail, as part of a webpage, via an ftp server and possible other 
	means.

3. Impact:

	The vulnerabilities potentially enable local or remote attackers
	to compromise the privacy of a vicitim's data and to execute arbitrary
	shell commands with the victim's privileges, such as erasing files or 
	accessing or modifying data. 

4. Solution:

	The affected applications have been fixed in KDE 3.0.5b and
	KDE 3.1.1a, both released today. We strongly recommend upgrading
	to these releases. 

	KDE 3.0.5b can be downloaded from

	http://download.kde.org/stable/3.0.5b/

	KDE 3.1.1a can be downloaded from

	http://download.kde.org/stable/3.1.1a/

	Please note that the KDE 3.1.1a release only consists of updates
	to the affected KDE packages.

	For affected KDE 2 systems, two patches for the 2.2.2 source code 
	have been made available which fixes these vulnerabilities. Contact 
	your OS vendor / binary package provider for information about how 
	to obtain updated binary packages.

	People unable to upgrade to a version of KDE in which these problems
	have been fixed can, as a temporary workaround, remove the following
	files from their KDE installation: 
		thumbnail.protocol
		eps.kimgio
		kghostview.desktop
		kdvi.desktop
	After doing so KDE should be restarted. Note that this reduces KDE's
	functionality in areas related to file previews, print previews and 
	limits the ability to handle PostScript (PS), PDF and DVI files.

5. Patch:

	Patches for KDE 3.1.1 are available from

	ftp://ftp.kde.org/pub/kde/security_patches :

	a2ca01c77918b13464f2d1fc0858d6f0  post-3.1.1-kdebase-thumbnail.diff
	38e2483c835239185c9c895e82079dea  post-3.1.1-kdegraphics-kdvi.diff
	a64ab67e3d5ab5fdbfe444fd6e1764b7  post-3.1.1-kdegraphics-kghostview.diff
	9ef02df4d21b9040ef488b6f97e3e93a  post-3.1.1-kdelibs-kimgio.diff


	Patches for KDE 3.0.5a are available from

	ftp://ftp.kde.org/pub/kde/security_patches :

	b2e823b0a46ef2d949118f7fded7092e  post-3.0.5a-kdebase-thumbnail.diff
	8c783fbe36d25b30b4fc2c91777906a6  post-3.0.5a-kdegraphics-kdvi.diff
	952ec6e4ddafd7d833106b78df2d342f  post-3.0.5a-kdegraphics-kghostview.diff
	604d8de4116d0a4c4896521b03884a06  post-3.0.5a-kdelibs-kimgio.diff


        Patches for KDE 2.2.2 are available from 

	ftp://ftp.kde.org/pub/kde/security_patches :

	002e297cb41705f9b744f6cc64e9d79e  post-2.2.2-kdebase-thumbnail.diff
	a0a87a2f7617e83c831d1a9c2588830e  post-2.2.2-kdegraphics-kdvi.diff
	1e7520b590e6573446487efb3995d8a3  post-2.2.2-kdegraphics-kghostview-2.diff
	e2c50fb7c97e1fbc8939e30a30054e45  post-2.2.2-kdelibs-kimgio.diff


6. Timeline and credits:

	04/03/2003 Notification of security@kde.org by Maksim Orlovich
	           about bugreport 53157, filed by Philipp Hullmann and
		   bugreport 56808, filed by Keith Winstein.
	04/06/2003 OS vendors / binary package providers alerted.
	04/07/2003 OS vendors / binary package providers provided with 
	           patches.
	04/09/2003 Public Security Advisory by the KDE Security team.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2-rc1 (GNU/Linux)

iD8DBQE+lI7uvsXr+iuy1UoRAm3rAJwOig0qWdc1Tl4I7YuBZnCQPbddjQCgorps
M4wJ7KMX31RROgT/phUizUc=
=dqEb
-----END PGP SIGNATURE-----
