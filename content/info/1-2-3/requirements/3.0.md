---
title: KDE 3.0 Compilation Requirements
---

<p>
  <a name="source_code-library_requirements"></a><u>Library
  Requirements / Options</u>.
  KDE 3.0 requires or benefits from the following libraries, most of
  which should be already installed on your system or available from
  your OS CD or your vendor's website:
</p>
<table border="0" cellpadding="6" cellspacing="0">
  <tr valign="top">
    <td width="50%">
      <u>Basic</u>
    </td>
    <td width="50%">
      <u>Help/Manuals</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt 3.0.3</a> (<strong>required</strong>)
        </li>
        <li>
          an X Server (<strong>required</strong>) with various
          optional extensions (<a href="http://www.xfree86.org/">XFree86</a>
          4.2.x, which includes all the enumerated extensions, is
          recommended)
          <ul>
            <li>
	        the RENDER extension for beautiful anti-aliased fonts
	      </li>
            <li>
	        the DPMS extension Energy Star display power management
	      </li>
            <li>
              the Xinerama extension for modern multi-head displays
	      </li>
            <li>
              the XVideo extension for enhanced video playback
	      </li>
          </ul>
        </li>
        <li>
          <a href="http://www.cs.wisc.edu/~ghost/index.html">GhostScript</a>
          for PostScript/PDF support (preferably 6.50 or later)
        </li>
        <li>
          <a href="http://www.mysql.com/">MySQL</a>,
          <a href="http://postgresql.readysetnet.com/">PostgreSQL</a>,
          or <a href="http://genix.net/unixODBC/">unixODBC</a> for
          database support
        </li>
        <li>
          <a href="http://www.sleepycat.com/">Berkely DB II</a> is highly
          recommended for
          <a href="http://i18n.kde.org/tools/kbabel/">KBabel</a>, the
          KDE translation tool
        </li>
        <li>
          <a href="http://www.python.org/">Python</a> for scripting in some
          KOffice components
        </li>
        <li>
          <a href="http://www.perl.com/">Perl</a> for scripting in KSirc
          and automating updates of configuration files for new releases
        </li>
        <li>
          <a href="ftp://ftp.gnu.org/pub/gnu/tar/">gzip</a> and
          <a href="ftp://sourceware.cygnus.com/pub/bzip2/v100/">bzip2</a>
          for data compression
        </li>
        <li>
          <a href="http://www.dante.de/">TeX</a> and for LaTeX document
          processing
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://www.xmlsoft.org/downloads.html">libxml2</a>
          &gt;= 2.3.13 and
          <a href="http://xmlsoft.org/XSLT/downloads.html">libxslt</a>
          &gt;= 1.0.7 for reading documentation
        </li>
        <li>
          <a href="http://www.htdig.org/">ht://dig</a> for indexing and
          searching documentation with KDevelop
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Hardware</u>
    </td>
    <td>
      <u>Networking</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.cups.org/">CUPS</a> &gt;= 1.1.9 for
	      enhanced printing administration, options and usability
        </li>
        <li>
          <a href="http://www.gphoto.org/download.html">gphoto2</a> &gt;=
          2.0.1 devel for accessing images on digital cameras
        </li>
        <li>
          <a href="http://www.mostang.com/sane/">SANE</a> for scanning
          support
        </li>
        <li>
          <a href="http://www.netroedge.com/~lm78">lm-sensors</a>
          for monitoring motherboards
        </li>
        <li>
          <a href="http://mtools.linux.lu/">mtools</a> for accessing a
          floppy disk as <code>floppy:/</code> from Konqueror
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="ftp://cs.anu.edu.au/pub/software/ppp/">pppd</a> for
          dialup networking
        </li>
<!--
        <li>
          ?? USED ?? <a href="http://radscan.com/nas.html">NAS/libaudio</a>
          for network audio support
        </li>
-->
        <li>
          <a href="http://nicolas.brodu.free.fr/libsmb/">libsmb</a> for
          browsing Windows/NetBIOS shares
        </li>
        <li>
          <a href="http://www.openldap.org/">libldap</a> for
          LDAP address book support
        </li>
        <li>
          <a href="http://oss.sgi.com/projects/fam/">FAM</a> for efficient
          file/directory change notification
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Browsing/Internet</u>
    </td>
    <td>
      <u>Encryption/Security</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          Java Virtual Machine &gt;= 1.3 for
          <a href="http://java.sun.com/">Java</a> applet support
        </li>
        <li>
          a recent version of <a href="http://www.lesstif.org/">Lesstif</a>
          or Motif for Netscape Communicator plugin support
        </li>
        <li>
          <a href="http://www.winehq.org/">WINE</a> for executing certain
          MS Windows controls and applets
        </li>
        <li>
          <a href="http://www.codeweavers.com/products/crossover/">Crossover
          Plugin</a> for Quicktime, Shockwave Director, Windows Media
          Player 6.4 and MS Office viewer support
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x for
          HTTPS, SFTP, SSH, VPN and more (versions 0.9.5x are no longer
          supported)
        </li>
        <li>
          <a href="http://www.gnupg.org/">GnuPG</a> or PGP for email/document
          encryption/decryption
        </li>
        <li>
          <a href="http://java.sun.com/products/jsse/">JSSE</a> 1.0.2 for
          Java applets requiring SSL/HTTPS (common with online banking) -
          included with a JVM 1.4
        </li>
        <li>
          <a href="http://www.us.kernel.org/pub/linux/libs/pam/modules.html">PAM</a>
          for services (such as login) authentication
        </li>
      </ul>
    </td>
  </tr>
  <tr valign="top">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td>
      <u>Graphics</u>
    </td>
    <td>
      <u>Multimedia</u>
    </td>
  </tr>
  <tr valign="top">
    <td>
      <ul>
        <li>
          <a href="http://www.mesa3d.org/">OpenGL</a> for some 3D
          screensavers and some 3D graphics programs
        </li>
        <li>
          <a href="http://libsdl.org/">SDL</a> &gt;= 1.2.0 for some
          <a href="http://noatun.kde.org/">Noatun</a> plugins
        </li>
        <li>
          <a href="http://www.libtiff.org/">libtiff</a> for viewing
          facsimiles
        </li>
        <li>
          <a href="http://www.libmng.com/">libmng</a>,
	      <a href="http://www.libpng.org/pub/png/libpng.html">libpng</a> and
	      <a href="ftp://ftp.uu.net/graphics/jpeg/">libjpeg</a> for
          viewing images
        </li>
        <li>
          imlib 1 for using
          <a href="http://kuickshow.sourceforge.net/">Kuickshow</a> for
          viewing a wide variety of image formats
        </li>
        <li>
          <a href="http://www.freetype.org/">freetype 2</a> for anti-aliased
          font handling and manipulation
        </li>
        <li>
          <a href="http://www.englishlacrosse.co.uk/website/mag/pdfinfo.html">PDFinfo</a>
          for enhanced PDF file browsing
        </li>
      </ul>
    </td>
    <td>
      <ul>
        <li>
          <a href="http://mpeglib.sourceforge.net/">mpeglib</a> for video
          playback (included with kdemultimedia)
        </li>
        <li>
          <a href="http://www.alsaplayer.org/">ALSA</a> for more
          advanced audio support
        </li>
        <li>
          <a href="http://www.fokus.gmd.de/research/cc/glone/employees/joerg.schilling/private/cdrecord.html">cdtools/cdparanoia</a>
          for ripping audio CDs
        </li>
        <li>
          <a href="http://lame.sourceforge.net/">LAME</a> for encoding
          MP3 files
        </li>
        <li>
          <a href="http://www.xiph.org/ogg/vorbis/">libogg/libvorbis</a> for
          Ogg Vorbis encoding/playback
        </li>
        <li>
          <a href="http://webs1152.im1.net/~dsweet/XAnim/">XAnim</a>
          for aKtion!'s video engine
        </li>
        <li>
          <a href="http://oss.sgi.com/projects/audiofile/">libaudiofile</a>
          for playing .WAV audio files
        </li>
      </ul>
    </td>
  </tr>
</table>
<p>
  <u>Compiler Requirements</u>.
  KDE is designed to be cross-platform and therefore supposed to compile with a
  variety of GNU/Linux - UNIX compilers.  However, both the set of available compilers
  as well as KDE is advancing very rapidly, so the ability to compile KDE
  on various UNIX systems depends on users reporting compile problems with
  possible fixes to the KDE project.
</p>
<p>
  In addition, C++ support by <a href="http://gcc.gnu.org/">gcc</a>,
  the most popular compiler used on GNU/Linux and many traditional UNIX systems,
  has undergone major improvements. There are many known compile and
  code generation problems with older gcc/egcs releases, therefore the support
  for those compilers has been dropped.
</p>
<p>
  In particular, gcc versions earlier than gcc-2.95, such as egcs-1.1.2,
  gcc-2.8.x or gcc-2.7.x are no longer supported.
  In addition, some components of KDE 3.0, such as
  <a href="#aRts">aRts</a>, generally will be incorrectly compiled by unpatched
  versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0.x</a>
  (this problem should be fixed with the release of gcc-3.1). The
  code generation bug does not lead to a compile failure, but rather just
  unexpected problems at runtime.

  While there have been reports of successful KDE compilations with recent
  versions of the "Red Hat-gcc" 2.96 and gcc-3.1 CVS snapshots, the KDE
  project at this time
  continues to recommend the use of gcc-2.95.* for compiling KDE.
</p>
<!-- $Id: 3.0.php 215359 2003-03-20 23:09:56Z cullmann $ -->
