<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/extragear-plasma-3.95.0.tar.bz2">extragear-plasma-3.95.0</a></td><td align="right">3.5MB</td><td><tt>e7503e5b1bfd1ef9ad175c218e242469</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdeaccessibility-3.95.0.tar.bz2">kdeaccessibility-3.95.0</a></td><td align="right">7.4MB</td><td><tt>0ee45d7d53161780d066f457ebcbf5f4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdeadmin-3.95.0.tar.bz2">kdeadmin-3.95.0</a></td><td align="right">1.4MB</td><td><tt>31ba739955f63220e78734aabd5ae2fd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdeartwork-3.95.0.tar.bz2">kdeartwork-3.95.0</a></td><td align="right">44MB</td><td><tt>7874f806fade587f2a02c457cb754601</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdebase-3.95.0.tar.bz2">kdebase-3.95.0</a></td><td align="right">4.1MB</td><td><tt>d75d9211ffd25eb3b03065bcc3d9e70c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdebase-workspace-3.95.0.tar.bz2">kdebase-workspace-3.95.0</a></td><td align="right">11MB</td><td><tt>3e2bb5a5a882feb44030960d99f04afb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdebindings-3.95.0.tar.bz2">kdebindings-3.95.0</a></td><td align="right">2.2MB</td><td><tt>d72ed3e91541a5566fa38c7cf894b4f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdeedu-3.95.0.tar.bz2">kdeedu-3.95.0</a></td><td align="right">39MB</td><td><tt>a32dd6cb0589861bba96bd42774b10c4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdegames-3.95.0.tar.bz2">kdegames-3.95.0</a></td><td align="right">26MB</td><td><tt>33a6ec76b838411b529c7b62a8baf787</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdegraphics-3.95.0.tar.bz2">kdegraphics-3.95.0</a></td><td align="right">2.4MB</td><td><tt>01ef4421f9bf306f39fc0bb3c6f485e2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdemultimedia-3.95.0.tar.bz2">kdemultimedia-3.95.0</a></td><td align="right">1.1MB</td><td><tt>1971d98b5f85dd345c0108b1375bdfb5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdenetwork-3.95.0.tar.bz2">kdenetwork-3.95.0</a></td><td align="right">6.3MB</td><td><tt>8ee087dc8bebbad339e249c49fad1b46</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdepim-3.95.0.tar.bz2">kdepim-3.95.0</a></td><td align="right">13MB</td><td><tt>107be701601e1a9a9dc1065068a69cb4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdesdk-3.95.0.tar.bz2">kdesdk-3.95.0</a></td><td align="right">4.2MB</td><td><tt>e9167dbfea0752a539249f66db28ddf5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdetoys-3.95.0.tar.bz2">kdetoys-3.95.0</a></td><td align="right">2.2MB</td><td><tt>9861947cb01719de4e266435e9ef3a20</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.95/src/kdeutils-3.95.0.tar.bz2">kdeutils-3.95.0</a></td><td align="right">2.3MB</td><td><tt>9bfbb37c3bbdfdab84c362c44638a619</tt></td></tr>
</table>
