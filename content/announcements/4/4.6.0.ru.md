---
aliases:
- ../4.6
date: '2011-01-26'
custom_about: true
custom_contact: true
title: KDE придаёт вам власть над компьютером с новыми оболочками рабочего стола,
  приложениями и платформой разработки
---

<p>
Сообщество KDE радо объявить о новом выпуске оболочек рабочего стола Plasma,
приложений KDE и платформы KDE с основными обновлениями.
Эти выпуски с номером версии 4.6 добавляют много новых возможностей в каждый
из трёх продуктов KDE. Краткое описание нововведений:
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-w09.png">
	<img src="/announcements/4/4.6.0/thumbs/46-w09.png" class="img-fluid" alt="KDE Plasma Desktop, Gwenview and KRunner in 4.6">
	</a> <br/>
	<em>Рабочий стол KDE Plasma, Gwenview и диалог запуска KRunner в 4.6</em>
</div>
<br/>

<h3>
<a href="./plasma">
Оболочки Plasma придают вам власть над компьютером
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="Оболочки рабочего стола KDE Plasma 4.6.0" />
</a>

<b>Оболочки рабочего стола Plasma</b> обрели новую систему управления комнатами,
упрощающую связывание приложений с конкретными комнатами, например, с комнатами для деловых и личных задач.
Было переработано управление питанием: появились новые возможности,
но при этом и настраивать параметры управления питанием стало проще.
В KWin, диспетчере окон для оболочки рабочего стола Plasma,　 появилась 　 поддержка сценариев,
и в каждой оболочке Plasma был улучшен внешний вид.
У <b>Plasma Netbook</b>, оболочки рабочего стола KDE для мелкоформатных устройств,
таких как ноутбуки и нетбуки, была улучшена скорость работы и упрощено управление посредством сенсорного экрана.
Чтобы узнать подробности, прочитайте <a href="./plasma">полный анонс выпуска оболочек рабочего стола KDE Plasma 4.6</a>.

</p>

<h3>
<a href="./applications">
В Dolphin добавлена навигация по фасетам
</a>
</h3>

<p>

<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="Приложения KDE 4.6.0"/>
</a>
</a>
Команды разработки многих <b>приложений KDE</b> также выпустили их новые версии.
В частности, это расширение возможностей прокладки маршрутов в виртуальном глобусе Marble
и расширенный поиск и фильтрация по метаданным файлов в диспетчере файлов KDE,
Dolphin; это называется «поиск по фасетам».
В коллекции игр KDE было сделано множество улучшений, а программа просмотра изображений
Gwenview и программа для создания снимков экрана KSnapshot теперь позволяют
мгновенно публиковать изображения в одной из множества социальных сетей.
Чтобы узнать подробности, прочитайте <a href="./applications">полный анонс выпуска приложений KDE 4.6</a>.<br /><br />
</p>

<h3>
<a href="./platform">
Платформа KDE с облегчённой версией для мобильных устройств
</a>
</h3>

<p>

<a href="./platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="Платформа разработки KDE 4.6.0"/>
</a>

<b>Платформа KDE</b>, на которой основаны оболочки рабочего стола Plasma и приложения KDE,
также приобрела новую функциональность, доступную всем приложениям KDE.
С появлением возможности «мобильной сборки» стало проще развёртывать приложения на
мобильных устройствах. В инфраструктуре Plasma появилась поддержка
виджетов на QML, декларативном языке Qt, и поддержка новых интерфейсов JavaScript
для взаимодействия с данными.
Для Nepomuk, технологии, обеспечивающей поиск по меткам и метаданным в приложениях KDE,
стало возможно резервное копирование и восстановление. Вместо HAL для управления оборудованием
теперь можно также использовать UPower, UDev и UDisks. Улучшена поддержка Bluetooth.
Обновлёны элементы графического интерфейса пользователя для стиля Oxygen, а
новая тема Oxygen для приложений на GTK способна сделать их внешний вид практически идентичным
приложениям KDE.
Чтобы узнать подробности, прочитайте <a href="./platform">полный анонс выпуска платформы KDE 4.6</a>.

</p>

<h4>
    Несите благую весть!
</h4>
<p align="justify">
KDE призывает всех <strong>делиться информацией об этом выпуске</strong>
на социальных сервисах в сети Интернет.
Высылайте материалы на новостные сайты, распространяйте
информацию через delicious, digg, reddit, twitter и identi.ca.
Загружайте снимки экрана на Facebook, Flickr, ipernity и Picasa
и добавляйте их в соответствующие группы. Создавайте видеоролики
и загружайте их на YouTube, Blip.tv, Vimeo и другие веб-сайты.
И не забывайте помечать загруженные материалы
<em>тегом <strong>kde</strong></em>, чтобы людям было легче их
найти, а команда разработчиков KDE могла составить отчёт о том,
насколько полно был освещён анонс нового выпуска программного обеспечения KDE.
<strong>Расскажите о нас миру!</strong></p>

<p align="justify">
Следите за развитием событий вокруг выпуска
4.6 через
<a href="http://buzz.kde.org"><strong>ленту новостей сообщества KDE</strong></a>.
На этом сайте вы увидите всё, что происходит на
identi.ca, Twitter, YouTube, Flickr, PicasaWeb, в блогах и
других социальных сетях в реальном времени.
Лента новостей находится по адресу
<strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt">
    <a href="http://microbuttons.wordpress.com">микрокнопки</a>
</span>
</div>

<h4>Поддержите KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.6.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify">KDE e.V. открывает новую <a
href="http://jointhegame.kde.org/">акцию поддержки</a>. Платя &euro;25 в квартал, вы обеспечиваете
расширение сообщества KDE, которое создаёт свободное программное обеспечение мирового класса.</p>

<p>&nbsp;</p>
