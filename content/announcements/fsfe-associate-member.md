---
custom_about: true
custom_contact: true
title: KDE e.V. Becomes Associate Member of FSFE
date: "2006-05-09"
description: KDE e.V. and the Free Software Foundation Europe are proud to announce their associate status
---

<p>The <a href="http://ev.kde.org">KDE e.V.</a>, the organisation that represents the <a href="/">K Desktop Environment</a> (KDE®) in legal and financial matters, and the <a href="http://www.fsfeurope.org">Free Software Foundation Europe</a> (FSFE) are proud to announce their associate status, working together for the promotion and protection of Free Software on users' desktops in Europe and worldwide.</p>

<p>
<i>"The Free Software community started to build freedom into the most
basic building blocks of an operating system, finishing the operating
system GNU/Linux in the early 90s, taking over more than <a href="http://www.serverwatch.com/news/article.php/3596491">60 percent of
the web servers with Apache</a> and has become a seminal building
block of the networked society and economy"</i>, says Georg Greve,
president of FSFE. <i>"As a community, we then tackled the desktop as the
next big step. Today we have two fully functional desktops that bear
comparison to any other desktop in use around the world: GNOME and
KDE."</i>
</p>
<p>
<i>"Although GNU/Linux and KDE are used on desktops in many companies and
governments around the world, some people still have preconceptions
about lack of usability of GNU/Linux on the desktop,"</i> Eva
Brucherseifer, president of the KDE e.V., points out.
</p>
<p>
She continues: <i>"KDE has embraced the OpenUsability project and we
are now working closely together with their developers. This has already led
to numerous improvements on the usability side of KDE but will have
even bigger effect in the KDE 4 release cycle where we are able to make
more intrusive changes leading to enhanced usability in all kinds of
areas."</i>
</p>
<p>
<i>"The desktop is one of the most important battlefields for the long
term success of Free Software. In the antitrust cases it has become
apparent how Microsoft could take its quasi-monopoly on the desktop
and use it as leverage in other areas. FSFE is helping to put an end
to this in the EU antitrust case, but this is not enough: we need to
put an end to proprietary desktop monopolies that made such abuse
possible,"</i> Georg Greve explains the background for FSFE's decision to
take action in this area. <i>"Together with the KDE e.V. we seek to break
the stranglehold on the desktop, give people freedom, and explain to
them why this is important, and why they should not give it up again."</i>
</p>
<p>
One of the first projects discussed between the organisations will
involve the provision of more information to interested parties about
Free Software and inform them about alternatives. <i>"But this can be
only the first step"</i>, adds Eva Brucherseifer and points out:
<i>"Ultimately our goal must be to help build a strong environment of
Free Software service companies. We have been cooperating unofficially
in the past, and are now very happy to also work with the Free Software
Foundation Europe officially towards this goal."</i>
</p>

<h4>About KDE</h4>

<p>
   The KDE project consists of hundreds of developers, translators,
   artists and other contributors worldwide collaborating over the
   Internet. The community creates and freely distributes a stable,
   integrated and free desktop and office environment. KDE provides
   a flexible, component-based, network-transparent architecture and
   powerful development tools, offering an outstanding development
   platform. Reflecting its international team and focus, KDE 3.5 is
   currently available in over 80 different languages.
</p><p>
   KDE, which is based on Qt® technology from Trolltech®, is working
   proof that the Open Source "Bazaar-style" software development
   model can yield first-rate technologies on par with and superior
   to even the most complex commercial software.
</p>

<h4>About the Free Software Foundation Europe</h4>
<p>
   The Free Software Foundation Europe (FSF Europe) is a charitable
   non-governmental organisation dedicated to all aspects of Free
   Software in Europe. Access to software determines who may participate
   in a digital society. Therefore the freedoms to use, copy, modify and
   redistribute software - as described in the Free Software definition -
   allow equal participation in the information age. Creating awareness
   of these issues, securing Free Software politically and legally, and
   giving people freedom by supporting development of Free Software are
   central issues of the FSF Europe, which was founded in 2001 as the
   European sister organisation of the Free Software Foundation in the
   United States.
</p>
   

<h4>Contact</h4>
KDE e.V. <br/>
Eva Brucherseifer &lt;eva at kde.org&gt; <br/>
Phone: +49 6151 3 969 961 <br/>
<br/>
Sebastian Kügler &lt;sebas at kde.org&gt; <br/>
Phone: +31 6 483 709 28 <br/>
<br/>
FSF Europe: <br/>
Georg C. F. Greve   &lt;greve at fsfeurope.org&gt; <br/>
Phone: +41 76 561 18 66 <br/> <br/>
Joachim Jakobs      &lt;jakobs at fsfeurope.org&gt; <br/>
Phone: +49 179 6919565 <br/><br/>

<h4>Trademark Notices</h4>

<p>
   KDE® and the K Desktop Environment® logo are trademarks or
   registered trademarks of KDE e.V. in the European Union, the United
   States and elsewhere. Trolltech and Qt are trademarks or
   registered trademarks of Trolltech AS in Norway and other countries.
   Linux is a registered trademark of Linus Torvalds. All other
   trademarks are the property of their respective owners.
</p>


