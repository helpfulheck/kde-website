---
aliases:
- ../../plasma-5.11.3
changelog: 5.11.2-5.11.3
date: 2017-11-07
layout: plasma
youtube: nMFDrBIA0PM
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
  class: text-center mt-4
asBugfix: true
---

- Sync xwayland DPI font to wayland dpi. <a href="https://commits.kde.org/plasma-workspace/a09ddff824a076b395fc7bfa801f81eaf2b8ea42">Commit.</a>
- KDE GTK Config: Be flexible to systems without a gtkrc file in /etc. <a href="https://commits.kde.org/kde-gtk-config/952ab8f36e3c52a7ac6830ffd5b5c65f71fa0931">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382291">#382291</a>
- Make sure we store password for all users when kwallet is disabled. <a href="https://commits.kde.org/plasma-nm/ead62e9582092709472e1e76eb3940d742ea808b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/386343">#386343</a>
