---
aliases:
- ../../plasma-5.13.3
changelog: 5.13.2-5.13.3
date: 2018-07-10
layout: plasma
youtube: C2kR1_n_d-g
figure:
  src: /announcements/plasma/5/5.13.0/plasma-5.13.png
  class: text-center mt-4
asBugfix: true
---

- Fix for QtCurve settings crash when using global menu
- Fix places runner to open search, timeline and devices properly
- Fixes for stability and usability in Plasma Discover
- Correct Folder View sizing and representation switch behavior
