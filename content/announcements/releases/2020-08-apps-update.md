---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
title: KDE's August 2020 Apps Update
type: announcement
---

Dozens of [KDE apps](https://kde.org/applications/) are getting new releases from KDE's release service. New features, usability improvements, re-designs and bug fixes all contribute to helping boost your productivity and making this new batch of applications more efficient and pleasant to use. 

Here's what you can look forward to:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin shows previews of many types of files." style="width: 800px" >}}

Dolphin is KDE's file explorer that helps you search, copy, and open files and folders. As such, one of its main functions is to give you a clear idea of what each file contains. Showing thumbnails is crucial for this and Dolphin has been able to show you previews of images, video clips, even Blender 3D files for a long time now.

In this new version, Dolphin adds thumbnails for 3D Manufacturing Format (3MF) files to the list and you can also see previews of files and folders on encrypted file systems such as Plasma Vaults. This is done securely by storing the cached thumbnails on the file system itself, or falling back to generating them but not storing cached versions anywhere if necessary. Either way, you are still in absolute control of how and when Dolphin shows you a file's content, as you can independently configure the file size cut-off for displaying previews for local and remote files.

Another way of identifying files is through their names. In case the file name is too long to display, developers have refined the file name shortening behavior. Instead of cutting out the middle of a long file and folder name as Dolphin did in prior versions, the new version cuts off the end and always keeps the file extension (if present) visible after the ellipses. This makes identifying files with long names much easier.

Dolphin now remembers and restores the location you were viewing, as well as the open tabs, and split views you had open when you last closed it. This feature is on by default, but can be turned off in the "Startup" page of Dolphin’s "Settings" window.

Talking of usability improvements, Dolphin has always let you mount and explore remote shares, be it via FTP, SSH, or other protocols. But now Dolphin shows remote and FUSE mounts with user-friendly display names rather than the full path. Even better: You can also mount ISO images using a new menu item in the context menu. This means you can download and explore a file system you will later burn to a disk or USB stick.

{{< video src="dolphin_ISO.mp4" caption="Dolphin lets you mount ISO images." style="max-width: 900px" >}}

Dolphin has an astounding number of features, but you can add even more through plugins. Previously, if you wanted to add to the "Service" menu plugins (look under <I>Settings</I> > <I>Configure Dolphin</I> > <I>Services</I>) you often had to execute a script or install a distro package. Now you can install them right from the "Get new thing" window with no manual intermediate steps. Other changes to the "Services" settings page is that it has a brand new search field at the top to help you quickly find what you're looking for and the services list is now sorted alphabetically.

Moving stuff around is easier with the new Dolphin, as you now have new actions to quickly move or copy selected files in one pane of a split view into the folder in the other pane. To make clear what is going on, when dragging a file, the cursor now changes into a grabby hand by default rather than the "copying" cursor as it did in prior versions.

You can also compute and display on-disk folder sizes using the "Details" view and Dolphin’s "Information" panel now shows useful information for the Trash. It’s also possible to navigate from Dolphin's search field to the results view by pressing the down arrow key.

Finally, a new useful feature is that Dolphin 20.08 comes with a new "Copy Location" menu item...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole gets some exciting new features." style="width: 800px" >}}

… Just like in KDE's terminal application, Konsole! This means you can right click on an item shown in Konsole, pick "Copy Location" from the pop-up menu and then copy and paste the full path of the file or directory into a document, another terminal, or an app.

Konsole also comes with a new feature that displays a subtle highlight for new lines coming into view when the terminal output is rapidly scrolling by and shows you a thumbnail preview for image files that you hover your cursor over by default. What's more, when you right-click on an underlined file in Konsole, the context menu displays and "Open with" menu so you can open the file in any application you choose.

Being able to split Konsole's view and adding tabs to do several things at the same time has been a feature for a long time, but now split view headers can be disabled, and the thickness of the separator can be increased. You can also monitor a tab for the active process to complete and assign colors to Konsole tabs!

{{< img class="text-center" src="konsole-monitor.webp" caption="New monitor process in Konsole" style="width: 500px" >}}

Talking of tabs, Konsole's default Ctrl+Shift+L shortcut for "Detach current tab" has been removed. It turns out it was too easy to accidentally detach the current tab when what you really wanted to do was clear the display using Ctrl+Shift+K.

The final usability improvement in this version is that Konsole's I-beam cursor now adapts to the font size instead of always remaining the same size.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="New Yakuake icon" style="width: 200px" >}}

Talking of terminals, Yakuake is KDE's drop-down terminal emulator. It unfolds from the top of your screen every time you hit F12 and it has also got a bunch of improvements for this release. For example, it now lets you configure all the keyboard shortcuts that come from Konsole and there is a new system tray item that shows you when Yakuake is running. You can hide it, of course, but it makes it easier to tell whether Yakuake is active and ready or not. It also provides a graphical way to summon it.

Another thing is that we have improved Yakuake in Wayland. The main window used to appear under top panels you had showing, but that has been corrected now.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam is KDE's professional photo management application and it so happens that [the team have just released a major new version, 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Digikam Face Recognition" style="width: 800px" >}}

One of the main highlights of digiKam 7.0.0 is the improvement of face recognition that now uses deep learning AI algorithms. You can watch the Akademy talk from last year to see how this was done and there will be more upcoming work in future versions for face recognition and automatic detection.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Digikam Face Recognition" style="max-width: 800px" controls="true" >}}

The [main Flatpak package of digiKam is available through Flathub](https://flathub.org/apps/details/org.kde.digikam), as well as an unstable nightly build for testers. DigiKam is also available for Linux through AppImages and your distro's repositories. There are also versions of digiKam for Windows and Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

We've made a couple of tweaks to the look of our text editor Kate. The "Open Recent" menu now displays documents opened in Kate from the command line and other sources as well, not just the ones opened using the file dialog. Another change is that Kate's tab bar is now visually consistent with all the tab bars in other KDE apps and opens new tabs on the right, like most other tabs bars do.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa is a lightweight music player for desktop and mobile." style="width: 800px" >}}

Elisa is a lightweight music player that works on your desktop and your mobile phone. Elisa now lets you display all genres, artists, or albums in the sidebar, below other items. The playlist also displays the progress of the currently playing song inline and the top bar is now responsive to the window size and device form factor. That means it looks good with a portrait orientation window/device (for example, on your phone) and can scale down to be really tiny.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars is KDE's application for astronomy aficionados." style="width: 800px">}}

Finally, astronomy app KStars added some calibration and focus in the [new release](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [The Bahtinov mask is a device used to focus small astronomical telescopes accurately](https://en.wikipedia.org/wiki/Bahtinov_mask). It is useful for users who do not have a motorized focuser and prefer to focus manually with the aid of the mask. After capturing an image in the focus module with the Bahtinov mask algorithm selected, Ekos will analyze the images and stars within it. If Ekos recognizes the Bahtinov star pattern, it will draw lines over the star pattern in circles on the center and on an offset to indicate the focus.

{{< img class="text-center" src="kstars-focus.webp" caption="KStars new focus algorithm" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="KStars calibration" style="width: 600px" >}}

We added a "Calibration Plot" subtab to the right of the "Drift Plot". It shows the mount positions recorded during internal-guider calibration. If things are going well, it should display dots in two lines which are at right angles to each other, one when the calibration pushes the mount back and forth along the RA direction, and then when it does the same for the DEC direction. Not a ton of info, but can be useful to see. If the two lines are at a 30-degree angle, something's not going well with your calibration! Above is a picture of it in action using the simulator.

[KStars is available for download](https://edu.kde.org/kstars/) for Android, from the Snapcraft store, for Windows, macOS and, of course, from your Linux distro's repositories.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC with remote cursor" style="max-width: 900px" >}}

KRDC allows you to view and control the desktop session of another machine. The version released today now displays proper server-side cursors in VNC instead of a small dot with the remote cursor lagging behind it.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular is KDE's document viewer. It lets you read PDF documents, ePUB books and many more types of text-based files. A fix has put "Print" and "Print Preview" once again next to one another in the "File" menu.

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview is an image viewing application that comes with some basic editing features, like resizing and cropping. The new version saves the size of the last-used crop box, meaning you can quickly crop multiple images to the same size in rapid succession.

# Bugfixes

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

- Saving has been moved to a thread to not freeze the interface while saving
- Implemented a D-Bus interface for hotkeys and controlling scanning

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

- Spectacle's timer feature and Shift + Printscreen (take full screen screenshot) and Meta + Shift + Printscreen (take rectangular region screenshot) shortcuts now work on Wayland.
- Spectacle no longer includes the mouse cursor in screenshots by default.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

- struct2osd uses castxml now (gccxml has been deprecated).
- Less deprecated Qt code usage, avoiding logged runtime warnings.
- Improved translations.
