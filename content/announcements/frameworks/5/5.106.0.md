---
qtversion: 5.15.2
date: 2023-05-13
layout: framework
libCount: 83
---


### Baloo

* baloosearch: Inform the user when the specified dir is not canonical (bug 447119)
* Aggressively resource constrain baloo_file

### Breeze Icons

* Make monochrome MS-DOS executable icons respect the color scheme
* Increase moon size for redshift-status-on icon
* Add mimetype icons for alembic files
* Add new Partition Manager app icon
* Make media-flash icon actually look like a typical flash memory card (bug 468006)
* Add new icons for the pixelate and blur tools in Spectacle

### Extra CMake Modules

* Query gradle plugin version from Qt
* ecm_process_po_files_as_qm: use own subdir ECMPoQm/ for build artifacts
* Fix generate_export_header tests failures with parallel jobs (bug 464348)
* ECMGenerateExportHeader: add USE_VERSION_HEADER arg (& related tune args)
* ECMGenerateExportHeader: avoid helper C++ macro shared across gen. headers

### KArchive

* karchiveentry.h: add missing KArchive forward declaration

### KCalendarCore

* Deprecate Calendar relations API
* Deprecate the notebook support
* Deprecated the non-functional and unused save/reload API

### KCMUtils

* KCModuleQml: Set and inherit LayoutMirroring on an off-screen window

### KConfig

* Add workaround to support config name with minus sign

### KCoreAddons

* exportUrlsToPortal: let it work in a non-KDE session (bug 458643)
* exportUrlsToPortal: don't export when there are non-sendable non-local files
* KDirWatch: Don't append fileName to fileName in Delete event (bug 467095)

### KDeclarative

* Guard nullable property access, and bind instead of assigning once
* CalendarEventsPlugin: fix wrong deprecation for signal alternateDateReady

### KDE WebKit

* Remove CI support for kdewebkit

### KFileMetaData

* Test HEIF in Exiv2Extractor
* Test JXL in Exiv2Extractor, extend JPEG coverage
* Remove usage of deprecated MP4::ItemListMap in taglib extractor
* Modernize and extend CMake check for Python3
* Make fb2 extractor compilation dependent on KF5Archive

### KImageFormats

* Fix wrong alpha conversion (bug 468288)

### KIO

* ApplicationLauncherJob: find mimetype before showing open with dialog
* KPropertiesDialog: Don't quote the path unnecessarily (bug 467369)
* KEncodingFileDialog: Sort the list of encodings by name
* WidgetsAskUserActionHandler: make sure all dialogs are created in the main thread (bug 364039)
* WidgetsAskUserActionHandler: create message dialog in the main thread (bug 465454)
* file: make sure to cancel reading if the worker was aborted (bug 358231)
* KFileItemActions: don't add service submenus that don't have active actions
* KCoreDirLister: AutoUpdate wasn't incremented when a new lister opened an already listed dir (bug 387663)
* SlaveInterface: Set button icon for "Continue Loading"
* Set ExitType when running applications as transient systemd services (bug 423756)

### Kirigami

* Fix spacing issue in OverlaySheet
* GlobalToolBar: Fix enabled state of the BackButton in RTL mode
* CardsGridView: improve columns property codeblock
* SelectableLabel: Prevent it from participating in Tab chain
* Fix disabled text color in Material style with dark color scheme
* ActionButton: Fix reference to a tooltip attached property
* BreadcrumbControl: Fix wrong heading offset in RTL layout direction
* ContextDrawer: Remove top padding as well
* BreadcrumbControl: Fix errors from wrong indexing
* Fix accessibility of back and forward button
* fix dragging from toolbar (bug 467857)
* SwipeListItem: Fix for QT_QUICK_CONTROLS_MOBILE (bug 467884)
* OverlayDrawer: Fix z value being too high for OverlaySheets (bug 466219)
* Added keyboard search icon to SearchField

### KNewStuff

* Remove unused dependencies

### KPackage Framework

* Deprecate KPackage::Package::name, add default for parameters

### KService

* Fix querying actions for service of a service action
* Parse actions last when creating a service

### KTextEditor

* Fix Save Copy As.. not working anymore (bug 468672)
* Fix indenting removes characters if line has tabs at start (bug 468495)
* Printer: AlignVCenter line numbers
* Dont ignore folding when printing code (bug 417138)
* Use m_fontAscent instead of fm.ascent()
* Draw caret ourselves (bug 452853)
* Fix selection highlight for RTL text with custom line height (bug 464087)
* Include range.js when initializing the engine (bug 456701)
* Fix RTL text with format incorrectly shaped (bug 438755)
* xml-indent: Optimize getCode

### KUnitConversion

* Add "kph" as a unit synonym for Kilometers per Hour

### KXMLGUI

* kcheckaccelerators: Don't blindly cast to QWidget (bug 468576)

### NetworkManagerQt

* Add Q_ENUM to VPN Connection enums

### Plasma Framework

* Fix osd- icon IDs (bug 469303)
* Label: set horizontalAlignment explicitly
* Rename nepomuk icon to search (bug 416072)

### Prison

* Fix PDF417 with binary content not producing any output with ZXing 2.0

### QQC2StyleBridge

* Don't attempt to sync colors when application is shutting down

### Solid

* Implement solid-hardware monitor mode for property changes
* Connect UPowerDevice propertyChanged signal to frontend
* Avoid cache bypass of `UPowerDevice::allProperties`
* Use values from PropertiesChanged instead of invalidating whole cache

### Syntax Highlighting

* Backport theme changes to kf5
* Backport master hl file updates
* Add mimetype for markdown

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
