---
aliases:
- ../../kde-frameworks-5.83.0
qtversion: 5.15
date: 2021-06-12
layout: framework
libCount: 83
---


### Baloo

* Embed JSON metadata in KIOSlaves
* Convert protocol files to JSON
* [balooctl] Allow clearing a document which has been deleted

### BluezQt

* Add missing Qt5::DBus to qml plugin

### Breeze Icons

* Added new KMyMoney icon again with some extra tweaks
* Add Goodvibes icon
* Add an icon for skanpage
* Tweaked the Rust mimetypes to better match the official branding (bug 434346)
* Made links relative
* Updated icons and added symlinks
* Separated icons to -left and -right

### Extra CMake Modules

* Add LicenseRef-KDE-Accepted-GPL to license compatibility Matrix
* ecm_gperf_generate(): add option for target arg to add the gen. source to
* ecm_qt_declare_logging_category: catch alias targets before failing internally
* Add module to find libmount
* Drop FindFontConfig.cmake
* Make sure that dir path exists before attempting to copy file
* Attempt to find the Play store icon if it's not explicitly set
* Add address sanitizer for MSVC
* ecm_create_qm_loader: support target as alternative argument
* Don't pass through list rich text elements, Google Play can't handle those
* Fix doc build with Sphinx 4
* Use pkg_check_modules in FindTaglib.cmake
* Find PkgConfig quietly
* Handle text-less tags
* ECMAddAppIcon: support target as argument
* Fall back to English for missing translated elements
* ECMSetupVersion: phase out deprecated *_VERSION_STRING CMake variables
* Extend Android language mappings for translated metadata

### KActivitiesStats

* Turns out the tier field was wrong, but not the subgroup

### KArchive

* [KArchive] Use better wording for open error
* Use imported target for ZLib

### KCalendarCore

* Allow sorting of todos by category (tag)
* Use UTC times when calculating the transition dates of standard and dst phases
* Remove conditional compilation for libical older than 3
* Fix Compat-libical3-eGroupware.ics

### KCompletion

* Deprecate KLineEdit::passwordMode

### KConfig

* kconfig_add_kcfg_files: catch alias targets before failing internally
* Don't write position data for maximized windows (bug 434116)

### KConfigWidgets

* Deprecate KTipDatabase and KTipDialog
* [KHamburgerMenu] Ignore native menus
* Add KCommandBar - a hud-style dialog for quickly executing QActions

### KContacts

* Allow setting email, phonenumber, impp from QML
* [addressee] Add property for photo
* Add properties to Picture

### KCoreAddons

* kcoreaddons_add_plugin: make SOURCES argument optional
* KJob: if a delegate is already attached to a job, warn when setJob() is called
* Make KAboutData::productName accessible for KCrash
* Do not return duplicate plugins in KPluginLoader::findPlugins

### KCrash

* Use target_sources() to add sources to targets
* write metadata to a cache file when applicable
* Pass Bugzilla product name to DrKonqi when explicitly set in the app
* init rlp struct and don't cast rlim_t to int

### KDeclarative

* [KeySequenceItem] Display ampersands in shortcuts (bug 437718)

### KFileMetaData

* [ExtractionResult] Remove automatic mimetype determination
* [ExternalWriter] Test property serialization
* [ExternalExtractor/Writer] Avoid unnecessary map value lookup from key
* [ExternalWriter] Fix infinite loop when serializing properties
* Avoid leaking a QObject in ExternalWriter
* [TaglibWriter] Fix memory leak when updating Cover images in ASF (WMA)

### KGlobalAccel

* Revert "Prevent kglobalaccel5 getting activated on non-Plasma systems" (bug 437034)

### KDE GUI Addons

* Fix crash in keysequence recorder due to out of bound access

### KI18n

* ki18n_wrap_ui: catch alias targets before failing internally

### KIdleTime

* add std::chrono overload for addIdleTimeout

### KImageFormats

* avif: Adjust for libavif breaking change
* Enable HEIC plugin to save all ICC profiles
* Color profile loading/saving fixes
* xcf: Make sure offsets are not negative
* xcf: Fix Stack-buffer-overflow WRITE on broken files

### KIO

* New job: KTerminalLauncherJob
* Remove the cache KCM
* Remove the Browser Identification KCM
* Fix outdated URL for RAE Search Provider
* KOpenWithDialog: expand category on selection (bug 437885)
* Support wayland's xdg_activation_v1
* [kopenwithdialog] Show better error message when specifying a non-executable file (bug 437880)
* Updating caching logic for thumbnails, to detect encrypted devices (bug 411919)
* Port ioslave template to use JSON metadata
* webshortcuts KCM: Remove unneeded rebuildKSycoca method call
* Remove ServiceTypes property from webshortcuts
* protocoltojson: Do not create empty array for ExtraNames value
* kio_file: remove pmount-related code
* Honor the port number when fetching favicons
* [KCoreDirLister] Guard uiDelegate(), it might be null (bug 437153)
* [src/widgets/kfileitemactions] Dont try to connect non-existing plugin
* Improve strings for file/folder and URL link functionality
* TCPSlaveBase: deprecate socket() and introduce tcpSocket()
* Allow selecting directories in KFileWidget (bug 419874)
* MimeTypeFinderJob: the StatJob details should include the mimetype (bug 398908)
* kio_file: pass the absolute path to QMimeDatabase::mimeTypeForFile()
* When finding a job delegate extension, also try the uiDelegate() of its parents (bug 436049)
* Capture sender object in connect() call instead of using sender()
* MimeTypeFinderJob: Resolve symlinks for a local file
* Put compatibility code which uses KMimeTypeTrader in deprecation wrappers
* PreviewJob: Create a larger SHM when necessary (bug 430862)
* [KUrlNavigator] Improve presentation of search results
* Port KFileItemActions::runPreferredApplications away from trader constraint
* Port KFileItemActions::associatedApplications away from trader constraint

### Kirigami

* Update template app
* [ActionTextField] Switch actions to use small sizing rather than smallMedium
* fix(a11y): use semantically correct ellipses instead of semantically incorrect triple period
* use ecm_generate_qmltypes cmake macro
* SwipeNavigator hotfixes
* Fix back button in layers not asking Page::backRequested first before closing the page
* Split PageTab out of SwipeNavigator
* Split TabViewLayout out of SwipeNavigator
* [ActionTextField] Add more spacing between actions and text
* Improve look of card by making border a bit more subble
* [ActionTextField] Respect actions' enabled properties
* Adapt BasicListItemTest.qml to new API
* [BasicListItem] Offer vertical fill options for leading/trailing items
* Fix non-atomic updating of PlatformTheme colours
* Add BasicListItemTest case for leading+trailing items with skinny list items
* [BasicListItem] Increase icon size when showing or reserving space for subtitle
* [BasicListItem] Fix sizing for reserveSpaceForSubtitle: true case
* Fix BasicListItem.reserveSpaceForSubtitle to take into account textSpacing
* Fix rendering of small size borders of ShadowedTexture
* Add new fadeContent property to BasicListItem (bug 436816)
* Introduce Kirigami.Heading.type
* Disconnect before destroying items (bug 436707)
* [BasicListItem] Expose spacing between label and subtitle as property
* [BasicListItem] Increase spacing between label and subtitle
* Don't unnecessarily use the std::function overload of qmlRegisterSingletonType

### KNewStuff

* Ensure XmlLoader works in the expected, asynchronous fashion
* Add functionality to have a local providers file
* Don't show Go to... action unless it has children
* Fix opensearch parsing
* Make Syndication optional and only build OPDS plugin when present
* Change the role names in the search preset to custom ones
* Add in tagfilters and filter out entries that are not openaccess for now
* Add 'restart' search preset so people can go back to the search they started out with
* Add recommended and subscription to the searchpreset types
* Ensure to use the 'self' link relation for resolving relative urls
* Add download size if said information is available
* Add concept of search presets to hold navigation links for opds feed
* Make sure the documents are valid feed documents
* Add the start link as an entry so we can at the least always return to the root of the opds catalog
* Fix some misunderstandings with Download Link information
* Make caching work a bit better, and ensure it shows the installed entries
* Make the download links a bit prettier
* Try to implement the loading of new feeds from entries
* Initial patch for opds support
* Adjust help text for knewstuff-dialog
* Engine: Deprecate configSearchLocations and setConfigLocationFallback methods
* Make knewstuff-dialog work more reliable
* KNSCore::Engine: Add method to list all available knsrc files
* KNSCore::Question: Use nested event loop for waiting

### KParts

* Deprecate BrowserExtension::actionSlotMap()

### KPeople

* Expose PersonsSortFilterProxyModel header
* Fix DBus check

### KQuickCharts

* Implement monotonic cubic interpolation for line charts (bug 435268)
* Prevent an infinite loop in LegendLayout when minTotalWidth equals available
* Reduce severity of column not found to debug
* Expose preferredWidth as a property on LegendLayout and Legend

### KRunner

* RunnerManager::runnerMetaDataList: Do not return duplicate plugin IDs

### KService

* Do not crash if there's no Konsole
* Launch KCMs in kcmshell5 if systemsettings5 is not installed
* KToolInvocation: deprecate kdeinitExec()

### KTextEditor

* Avoid bad negative search offsets
* Fix vim cursor gets stuck with 'b'
* Limit shortcut context to active view for Reuse Word Above/Below (bug 352902)
* ensure remove trailing spaces value is in range
* Revert "Add option to keep spaces to the left of cursor when saving" (bug 433455)
* Revert "Update remove-trailing-spaces modeline"
* Revert "compute lastChar only if needed"
* Revert "Use KTextEditor::Cursor instead of ints in removeTrailingSpaces"
* Add "Transpose Words" feature (bug 436621)
* Don't apply the offset for the quoted regexp, as it is not zero-length
* Fix on-the-fly spell checking with recent Qt (bug 436004)
* fix :e for vi-mode to properly open non-local files, too
* fix :e filename for untitled documents
* Fixup registering text hint providers
* Add a separator line after 'Automatic Selection'

### KTextWidgets

* Ensure instances of derived Private classes are properly destructed
* Un-overload KReplace::replace() signal

### KWayland

* Use protocol files from plasma-wayland-protocols
* Update Plasma Wayland Protocols dependency to 1.3.0
* Do not emit Surface::leave twice when an output is removed

### KWidgetsAddons

* Ensure instances of derived Private classes are properly destructed
* Fix crash in KSelectAction (bug 436808)
* KFontChooser: always prepend the default font style in the styles list

### KWindowSystem

* Provide KWindowSystem::lastInputSerial
* Include API for wayland's xdg_activation_V1
* Drop Qt5Widgets dependency if KWINDOWSYSTEM_NO_WIDGETS=ON

### KXMLGUI

* Add KCommandBar to KXMLGui-using apps by default
* Remove the Attica requirement by using the store's new links
* Use new version-controlled enumerator deprecation warning macros
* Ensure instances of derived Private classes are properly destructed
* Don't translate urls (bug 436052)

### ModemManagerQt

* Make it compile with current flags

### NetworkManagerQt

* Fix missing definition for IPv6 property when building against older NM
* IPv6Setting: add option to disable IPv6
* fix(pppoe): add parent

### Oxygen Icons

* high def new version printer
* more versions of the printer icon size 16x16
* more size versions of preferences-kde-connect

### Plasma Framework

* Fix crash when there is already exactly 5 events displayed
* Remove call to setClearBeforeRendering
* Fix missing definition of HAVE_X11
* Change qqc2-desktop-style Units.fontMetrics property to a FontMetrics
* Support for blur behind plasmoids
* Filter configure action out of overflow menu correctly
* Allow building the project without deprecations
* PC3 TabButton: Center align text without icon, use icon property
* PC3 TabButton: Fix horizontal content alignment with max text width
* Fix events appearing in year or decade view (bug 436999)
* Check QAction enablement before triggering
* Introduce new SVG element: `menubaritem`
* Add sizeForLabels icon size to Units
* Fix Label having non-integer sizes
* [pluginloader] Deprecate listContainments and listContainmentsOfType
* Deprecate PluginLoader::listContainmentsForMimeType
* add missing signal is setConstraintHints
* Allow ExpandableListItem to not be expandable when it has no valid actions
* [dataengine] Emit sourceRemoved earlier (bug 435991)

### QQC2StyleBridge

* [scrollview] Consider scroll bars when calculating implicit size
* User HoverHandler instead of MouseArea

### Solid

* Ensure inherited PIMPL instances are properly destructed

### Sonnet

* Add Malayalam trigram

### Syntax Highlighting

* Rust syntax - Separate ControlFlow from Keywords
* Fix perl qw< > highlighting
* Update GAS syntax to highlight reg/instrs/jmp instrs

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
