---
qtversion: 5.15.2
date: 2023-02-12
layout: framework
libCount: 83
---


### Baloo

* Fix automoc issues on generated DBus source files
* Don't skip automoc for DBus interfaces

### Extra CMake Modules

* ECMAddQtDesignerPlugin: use correct plugin IID
* ECMAddQch: support doc linking also against Qt6 docs
* KDEPackageAppTemplates: add "_kapptemplate" postfix to target names
* Fix compatibility with newer Gradle provided with Qt 5.15.8

### KArchive

* Limit size of file to be added by what the tar header supports

### KCalendarCore

* Add Q_PROPERTY for IncidenceBase::url as well
* Avoid calling twice update() in setAttendees()
* Unbreak static build consumers

### KCMUtils

* KCModuleQml: Specify URL for root component's QML context
* Set systemsettings as alias when generating desktop files

### KCodecs

* Remove winsami2 from codecs list
* Replace jis7 with ISO-2022-JP in offered codecs
* Fix name for CP 949 in KCharsets::encodingsByScript (bug 463848)

### KConfig

* kconf_update: use standard exit status code
* kconf_update: use CMAKE_INSTALL_FULL_LIBDIR (bug 465125)
* KCoreConfigSkeleton::ItemEnum::Choice2: add deprecation warning markup
* KCoreConfigSkeleton::ItemEnum::choices2(): add deprecation markup
* Android: Fix a bug where new file wouldn't be created

### KConfigWidgets

* [kcodecaction] Deprecate QTextCodec and mib-based API (bug 463421)
* [kcodecaction] Add name-based trigger signal
* kcommandbar: Fix positioning when no mainwindow parent
* kcommandbar: Fix no central widget crashes app (bug 463251)

### KCoreAddons

* don't needlessly copy urls while iterating
* KSandbox: Warn when running without flatpak-spawn privileges

### KDeclarative

* Remove unused QtGraphicalEffects import
* Fix anchors error in AbstractKCM & SimpleKCM types
* QML/JS: Prefer const over let, and use strict === equality

### KDED

* Only recreate icons if an icon dir changed (bug 463353)

### KDELibs 4 Support

* Fix transparent borders on the right and bottom sides of Italy flag

### KFileMetaData

* Also add external extractors to vector of all plugins (bug 463598)
* Fix loading of external extractors and writers (bug 463598)

### KGlobalAccel

* Send X11 startup id when invoking service actions

### KDE GUI Addons

* waylandinhibition singleton pattern for ShortcutsInhibitManager
* waylandclipboard: roundtrip to get accurate focus state (bug 463199)
* KColorSchemeWatcherMac: do not use new NSAppearance API on older macOS versions (bug 463752)

### KImageFormats

* psd: conversion speed improvements (kf5)
* Fix writing TGA alpha depth flag
* HDR support removed from RAW plugin
* heif: reject invalid files with zero size

### KInit

* kdeinit: Use close_range for cleanup_fds if available

### KIO

* CommandLauncher: Emit error when command was not found
* Add KUrlNavigator::supportedSchemes, to replace customProtocols
* Add missing copyFromFile entries to http.json for webdav and webdavs (bug 464450)
* Mark WorkerBase::needSubUrlData as deprecated
* [commandlauncherjob] Deprecate setIcon
* job_error: add missing space between sentences for one string (bug 464631)
* Correct setDesktopName() docs regarding optionality of .desktop suffix
* Drop outdated docs for mailto kioslave
* Add deprecation warnings for KDirOperator::setView(KFile::FileView)
* Deprecate unused Scheduler::connect/disconnects methods
* Deprecate Slave::isConnected/setConnected
* Restore old behavior for KFileFilterCombo::setFilter (bug 463309)
* KCoreDirListerCache: Remove unnecessary assert
* KFilePlacesView: Set transientParent on context menu (bug 453532)
* KURISearchFilterEngine: Use DuckDuckGo as a default shortcut
* widgets/renamefiledialog: remove bulk rename number limit

### Kirigami

* PageHeader: Fix title's leading padding in RTL environment
* PageHeader: Factor out common sub-expressions
* PageHeader: Break down long and complex expression into multiple lines
* Take into account the extra spacing nav buttons have
* Take into account more button after is visible (bug 464988)
* private/PrivateActionToolButton: Remove unused icon component
* Fix escape key closing non-modal OverlayDrawers
* Take into account side margins (bug 465052)
* Disable HoverHandler (bug 464829)
* BasicListItem: Patch tooltip tests for Qt5
* BasicListItem: Add test for tooltip feature
* tests: Sort tests alphabetically in CMake
* Add hover-tooltip to the labels of BasicListItem
* Remove examples for components which are going to be removed in KF6
* ForwardButton: Remove unused initial properties
* SwipeNavigator: Hotfix failure to load the component
* OverlayDrawer: Remove unused object id
* Fix the only compile-time warning left in KF5
* OverlaySheet: Switch from int to real as appropriate
* Fix last instances of imports without aliases
* Explicitly specify arguments in signal handlers (x2)
* Explicitly specify arguments in signal handlers
* Fix some code style/formatting
* Avatar: Add tests for action triggering
* Avatar: Fix triggering main action
* ColumnView: Explicitly support RTL in leading separator
* ColumnView: Use Kirigami.Theme.Window colorSet for separators
* ColumnView: Remove obsolete code
* ColumnView: Use RTL-friendly name for internal method
* ColumnView: Make consistent calls to shared instance
* Optimize connections on parent change, and make sure to unbind if target set to null
* WheelHandler: Fix scrolling over ScrollBars on ScrollablePage (bug 438526)
* ScrollablePage: Enhance code style
* ScrollablePage: Drop extra rounding code
* a11y: Ignore action buttons for invisible actions
* Page: Split default page title delegate into separate component
* Page: Fix title delegate elision glitch
* ActionToolButton: Replace hack with Accessible role
* ShadowedTexture: Fix crash in cases where QSGTextureProvider::textureChanged.
* Move license file of template to correct location

### KNewStuff

* Fix crash in QQuickQuestionListener (bug 464624)
* Install desktop file for knewstuff-dialog (bug 464668)

### KPackage Framework

* Update README with new json metadata
* Show deprecation message about the desktop to json conversion being removed

### KParts

* PartLoader: move UI strings out of template code in header
* Deprecate class PartSelectEvent
* Deprecate PartBase::setPluginInterfaceVersion(int)

### KTextEditor

* Fix indent failing due to unknown method 'replace'
* Don't show selection count when there are none

### KWidgetsAddons

* Restore ABI compatibility broken in 29bb6d

### KXMLGUI

* Prevent user from expanding tooltips by key, if they weren't expandable
* Remove duplicate "Whats This?" tooltips

### NetworkManagerQt

* Fix remaining 5 compile warnings (unused parameters)

### Plasma Framework

* Dialog: Fix calculation of distance between dialog and parent applet (bug 464513)
* Refactor glowbar SVG
* QMenuProxy & Plasma::Types::PopupPlacement: Reverse direction for RTL
* QMenuProxy: Fix relative positioning with embedded/off-screen parent windows
* IconItemTest: Wait for rendering before capturing an image of IconItem
* iconitem: Use InOutCubic for the animation's easing curve
* iconitem: Adjust fade animation to not flicker as much (bug 463685)

### QQC2StyleBridge

* ScrollBar: Add feature that Alt+Click inverts scrollToClickPosition behavior
* ScrollBar: Fix glitch when animating back in transient mode
* ScrollBar: React to style changes property
* ScrollBar: Fix visuals in edge cases of rounding errors
* ScrollBar: Fix visuals in overshoot state
* ScrollBar: Never mirror horizontal scroll bars, even in RTL layout
* ScrollBar: Remove duplicate property assignment
* ScrollBar: Reduce string comparisons
* ScrollBar,ScrollView: Use Control::mirrored property for RTL
* ScrollView: Factor out subexpression in a way that actually works reliably
* ScrollBar: Fix active control state after mouse release
* ScrollBar: Fix jump target position for horizontal scrollbars
* ScrollBar: Use newer shorthand properties to make code shorter
* ScrollBar: Shuffle components around and flatten code
* ScrollBar: Optimize for non-interactive mode: use property interceptor syntax
* ScrollBar: Optimize for interactive mode: don't run invisible animations
* ScrollBar: Factor constants out of Math.round()
* Deduplicate identical pixelMetric call
* ScrollView: Remove custom background-colored rectangle
* Switch: Fix broken object id reference
* Workaround for QTBUG-106489
* Replace `icon.color.a > 0` check with proper Qt.colorEqual
* Refactor DefaultListItemBackground with a grain of type-safety
* DelayButton: Fix hasFocus binding
* Menu: Factor out property assignment
* RadioButton: Add support for icons, just like in CheckBox (bug 442986)
* TabBar: Fix glitchy overlap
* TabButton: Refactor expressions using modern QQC2 attached properties
* TabButton: Fix/add icons rendering
* Revisit usages of QtQuick.Controls/AbstractButton::icon grouped property
* Optimize loading icons from properties

### Sonnet

* Change underscores to spaces in Esperanto trigrams

### Syntax Highlighting

* Log: fix slow search regex ; add Critical section ; some improvement in Log File (advanced) (bug 464424)
* Cobol: add extensions ; add exec sql block ; picture clause more permissive
* Cobol: replace tab with 2 spaces
* Add Zig language
* Add Cabal syntax
* Add Log File syntax
* avoid copies of Theme and QString in functions that manipulate themes
* FormatPrivate now contains a definition name rather than a reference to a definition
* add noexcept
* Theme::m_data is now always initialized with a valid pointer
* optimize ThemeData::textStyleOverride
* Replace DefinitionRef in StateData with a definition id
* Add COBOL syntax
* Indexer: suggest more minimal=1 or other rule for RegExpr with lookhaed=1 and .*
* Indexer: check xml validity
* feat: Add new syntax for `Earthfile`

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
