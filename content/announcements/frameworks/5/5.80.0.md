---
aliases:
- ../../kde-frameworks-5.80.0
qtversion: 5.14
date: 2021-03-13
layout: framework
libCount: 83
---

### Baloo

* [balooshow] Always print parsed document/device/inode id
* [balooshow] Allow hexadecimal format for deviceid parameter
* [baloosearch] Allow to print document ID
* [ResultIterator] Provide document ID
* [ResultIterator] Rework for future extension
* Don't try to index ninja files
* Don't try to index .swp files (bug 433095)

### Breeze Icons

* Fix and improve "add-subtitle" icon, add 16px version
* add new document scan icons
* Swap the system-upgrade icon for something more identifiable
* KMyMoney: move colorful icons back to actions and rename
* kmymoney: add 32/48px icon for the accounts
* kmymoney: add 32/48px icon for the ledger, update 16/22px
* Add actions/48 section
* Delete tor-browser icon (bug 432674)
* Add icons for keyframe actions (kdenlive)

### Extra CMake Modules

* Fix ECMCheckOutboundLicenseTest to find the FindReuseTool.cmake file
* ecm_qt_declare_logging_category: support target arg to append source file to
* Add FindIsoCodes module
* Add FindLibcap.cmake
* Fix relative path edge case in ECMGeneratePriFile
* Define relative paths when KDE_INSTALL_USE_QT_SYS_PATHS is enabled

### KActivitiesStats

* Remove KActivities DBUS string macro magic

### KActivities

* Stringify DBUS paths

### KDE Doxygen Tools

* Fix enum doc in the dark theme
* Add support for CuteHMI (Qbs)
* Kill caching with web workers
* Fix padding issues in the two sidebars
* Fix bad detection of bad class name in inherited stuff
* Add licensing clarifications

### KCalendarCore

* Add a flag not to update the lastModified field automatically

### KCMUtils

* Fix wrong margins on QML-based KCMs (bug 431855)

### KConfig

* Add KEntryMap::constFindEntry() method
* Fix window positioning regression due to incorrect fallback in fallback (bug 432661)

### KConfigWidgets

* fix maxItems == 0, really ensure no items are remembered (bug 421391)

### KContacts

* Use FindIsoCodes from ECM

### KCoreAddons

* FreeBSD: use procstat again
* Un-overload KJob signals
* kpluginfactory: Use the user-provided base factory in our factory macro (bug 410851)
* [KProcessList] Check size of data before accessing it (bug 431334)

### KCrash

* Fix X11 dependency conditions
* Fix KCrash usage inside a systemd unit

### KDeclarative

* don't use mouse-over effects on mobile
* [GridDelegate] Fix vertical misalignment when showing a subtitle
* [GridDelegate] Remove centering hack

### KDE DNS-SD

* Revert "Switch from custom K_D to Q_DECLARE_PRIVATE_D & Q_D" (bug 432949)

### KFileMetaData

* [FFmpegExtractor] Use categorized logging

### KGlobalAccel

* Use DBus activation for applications that are dbus activatable

### KHolidays #

* Update mu_en_islamic, holidays/holidays.qrc
* Update mu_en, create mu_en_islamic

### KIconThemes

* Avoid race condition on loading the plugin
* Allow icon themes to also use the colour scheme's selected foreground colour for icon colouring
* Fix loading of icons from resources like :/icons/..
* Register our KIconEngine plugin for SVG icons
* Remove cfeck as maintainer

### KImageFormats

* Fix Non-square Radiance/RGBE/.hdr images failing to load
* Check the input buffer before passing it to libheif
* Check primaries returned from libavif
* Add plugin for High Efficiency Image File Format (HEIF)
* Quality option can be returned without parsing input file

### KInit

* Use findLibcap from ECM

### KIO

* PreviewJob: Add support for DeviceRatioPixel
* Fix icon for link to address (Url)
* Add missing mailto helper protocol
* Add KIO slave kapptemplate
* KUrlRequester: un-overload returnPressed() signal
* Extract a new job MimeTypeFinderJob from OpenUrlJob
* PreviewJob: Fix Thumb::Size check to be spec compliant
* PreviewJob: consider file size for cache invalidation (bug 433127)
* PreviewJob: Added handlesSequences() to determine whether a ThumbSequenceCreator was used. Also fixed broken binary compatibility from my last commit
* Add single character web shortcuts
* [KPropertiesDialog] Resolve symlink target
* ApplicationLauncherJob: improve handling of KServiceAction
* Fix KDirOperator crash with libc++, due to unique_ptr difference
* KFileWidget: fix crash on FreeBSD (from the destructor)
* KCoreDirLister: un-overload redirection() signal
* KPropertiesDialog: Show MIME type name as tooltip
* RenameDialog: fix a potential buffer overflow (bug 432610)

### Kirigami

* Use standard durations for global drawer animations (bug 433948)
* [controls/Avatar]: Add focus ring to indicate keyboard focus
* [controls/Avatar]: Set activeFocusOnTab: true when Avatar has main action
* Support for Toolbars in standalone pages (bug 432541)
* Change and improve PlaceholderMessage new API
* Fix issues with recent PlaceholderMessage changes
* Add correct accessible roles to the Separator and FormLayout components
* Mark Accessible.Heading role to Kirigami Heading
* [Units] Add iconSizes.sizeForLabels docs, remove iconSizes.desktop docs
* controls/PlaceholderMessage: introduce title element
* different approach to remove the buttons visibility flickering
* controls/PageRouter: fix crash on beginCreate failing
* event compress the nav buttons show/hide (bug 413841)
* Handle visibility changes for toolbarlayoutdelegate and morebutton instances
* disable animations during replace (bug 426770)
* support icon.name (bug 433472)
* Add link to Kirigami tutorial and update hig link
* Make Theme internal
* Fix wrong right margin of OverlaySheet header on Mobile
* [Units] Add sizeForLabels iconSize for setting icon size to the default text bounding rect size
* api to include the handles in the toolbar
* Improve global drawer with section look
* better overlaysheet beavior with flick by touch
* Better manage sheet contents resizing
* Use custom QEvents for communicating between PlatformTheme instances
* Do not load Theme.qml as Theme singleton, instead use BasicThemeDefinition
* Add flatpak manifest to Kirigami template
* Use a common function to wait for events and increase wait time slightly
* Store owner of PlatformThemeData as QPointer<PlatformTheme>
* Use std::underlying_type<> for std::unordered_map of enums
* Fix android-specific code path in StyleSelector
* Set theme inherit to false on several items
* Port Material style to use the new BasicThemeDefinition
* Deprecate protected PlatformTheme::setPalette
* Completely rework BasicTheme
* Move style selection and lookup code out of KirigamiPlugin
* Replace PlatformThemePrivate with an implementation that shares data
* Add an environment variable to force the current theme and disable fallback
* [controls/PageRouter]: Fix bug with extended properties when replacing a route with navigateToRoute
* Add ToolTips for drawer handles

### KItemModels

* Add checkState role name to KCheckableProxyModel
* Deprecate KConcatenateRowsProxyModel also by compiler & runtime warnings

### KNewStuff

* Fix a copy/paste error in Button
* Fix preview delegate's update badge being cut off (bug 433535)
* qtquickengine: Do not emit entriesChanged signal when we clear an empty list (bug 431568)
* Put NewStuff.DialogContent's handle into the global toolbar
* Improve visuals in case initialization of engine failed
* qtquickengine: Check if KNSCore::Engine is valid before searching (bug 402432)
* Deprecate QWidgets components

### KNotification

* Don't attempt to strip markup from a KNotification's title

### KParts

* Deprecate KParts::ReadOnlyPart::completed(bool)

### KPlotting

* Remove cfeck as maintainer

### KRunner

* Simplify SingleRunnerMode code
* Add header and CMake configuration to help writing tests

### KService

* Revert "CMake: Specify add_custom_command() dependencies"
* Un-overload databaseChagned() signal
* src/services/yaac.y: use '%define api.pure'

### KTextEditor

* Dont use F11 for Toggle-Line-Numbers
* Remove trailing spaces on modified lines per default
* Add Editor::font() to access use set editor font
* Use correct function to determine with of line number characters (bug 430712)
* Add regression test for matching adjacent brackets (e.g. [])
* [KateViewInternal] Ensure that findMatchingBracket always works
* Add Tests for AutomaticCompletionPreselectFirst
* Make tab key select the first completion entry when none is selected
* Add option to preselect nothing when automatic completion is invoked (bug 316413)
* [KateCompletionWidget::execute] Return if the completion was successful
* Temporarily save unfinished search/replace text (bug 411923)
* ensure rangesForLine caching is correcly updated
* avoid crash on clearing of bookmarks (bug 433006)
* [Vimode] Make reformatting a single action
* [Vimode] Do not remove empty lines when reformatting (bug 340550)
* [Vimode] Display message directly in findPatternWorker instead of using a mutable m_lastSearchWrapped
* [Vimode] Display message when search is wrapped
* Fix a corner case with camel cursor

### KWidgetsAddons

* KPageView: Make it possible to access titleWidget

### KWindowSystem

* Un-overload windowChanged() signal

### KXMLGUI

* Add a missing addAction new-slot-syntax call
* Correct name of menu item (bug 433157)

### ModemManagerQt

* Bearer: drop initializers for non-primitive types
* Bearer: add initializers for IpConfig members

### Plasma Framework

* Use more standard durations (bug 433948)
* Make durations match Kirigami durations
* [plasmacomponents3/TextField|TextArea] Fix placeholder text (bug 433864)
* [Icon Item] Load .ico files using QIcon (bug 429927)
* Use adaptive transparency and tweak contrast effect to make better use of it
* [PlaceholderMessage] Sync with Kirigami version
* add missing sizeForLabels
* Print which file is responsible for an error
* [PC3 ToolTip] Reduce margins to smallSpacing (bug 433243)
* Port Plasma Style Kirigami Theme plugin to new Kirigami API

### QQC2StyleBridge

* Use more standard duration values (bug 433948)
* Respect the animation speed setting for menu open/close animation (bug 433948)
* [RoundButton] Rework the contentItem, improve background accuracy
* Spinbox: Update value during editing (bug 433066)
* [Label] Remove unnecessary code
* Use new custom event handling to trigger color sync in Platform theme
* Use QQuickWindow::setTextRenderType() for the default text rendering type
* Update Kirigami platform theme to new upstream API
* [radiobutton] Fix focus indicator

### Solid

* Adding isEncrypted method to AccessStorage
* solid-hardware: demarshall DBus types for nonportableinfo
* [UDisks2] Correctly demarshall QDbusObjectPath in `isEncryptedCleartext`
* Predicate Parser: fix deprecated directive

### Sonnet

* Remove enchant plugin

### Syntax Highlighting

* brightscript: Add inline lambda call syntax
* Fix Dracula theme colors
* Enhance XML highlighter colors
* Indexer: request to add ^ with <RegExpr column="0" ...>
* Indexer: request to merge RegExpr
* Indexer: request to merge AnyChar/DetectChar
* Update monokai & dracula for diff colors
* Indexer: check for duplicate InclusionRules
* adapt schema to reality of usage of weakDeliminator & additionalDeliminator
* Zsh: replace some Command with Variable style and add ! as variable name
* Indexer: suggest RegExpr to RangeDetect and some unnecessary quantifier
* Indexer: check additionalDeliminator/wordDelimiters and RegExpr as DetectIdentifier
* Bash/Zsh: fix comment and = style in VarName ; highlight parameter for getopts and let
* C++ Highlighting: add some QVariant-related variants
* PHP: fix string, number and folding region ; refactor styles (bug 429651)
* Breeze Light: restore original black color for Control Flow

### ThreadWeaver

* Un-overload signals

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
