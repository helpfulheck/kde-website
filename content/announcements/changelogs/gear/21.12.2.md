---
aliases:
- ../../fulllog_releases-21.12.2
title: KDE Gear 21.12.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Use all interface include dirs needed for LibXml2 (unbreak Windows build). [Commit.](http://commits.kde.org/akonadi/b5cb21626bdf63fba5adf10c8c47c78f6fb3d710) 
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/akonadi/189fe645974a293e3e976c2f22da27de82d7b420) 
{{< /details >}}
{{< details id="akonadi-contacts" title="akonadi-contacts" link="https://commits.kde.org/akonadi-contacts" >}}
+ Explicitely link to KCoreAddons. [Commit.](http://commits.kde.org/akonadi-contacts/62ee5874ecc0e9a478815809e7338d236e8457ec) 
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/akonadi-contacts/f4a6ddfcbe3837dbf62f4a512b06a3a8170dff44) 
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ BUG 449158 Akregator crashes while removing feeds folder. [Commit.](http://commits.kde.org/akregator/39a844af59f2ca63f0750272dc72b4eaf95f664f) Fixes bug [#449158](https://bugs.kde.org/449158)
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Overwrite dialog: Don't pretend source and dest url are the same. [Commit.](http://commits.kde.org/ark/7fc74b673e153addeba5bc541d6318f21ad5d4d4) Fixes bug [#436556](https://bugs.kde.org/436556)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Revert "[DetailsView] Improve zooming". [Commit.](http://commits.kde.org/dolphin/43e915213e64f03d785c87cc5cf8db1c6b86a1a2) Fixes bug [#447215](https://bugs.kde.org/447215)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ ListBrowserDelegate: set spacings and paddings in a better way. [Commit.](http://commits.kde.org/elisa/133d5e2a2c8af7b816cb9e8040757cd14622965d) 
+ Don't let rating stars take up space for unrated tracks. [Commit.](http://commits.kde.org/elisa/667fd8ef2e1dea0cd926ccbbfdcdf672f959c1af) Fixes bug [#448016](https://bugs.kde.org/448016)
+ Enable unicode normalization on track data before matching. [Commit.](http://commits.kde.org/elisa/1747155bf2e32f0d182fcb1dca0db6431dbd136b) Fixes bug [#391462](https://bugs.kde.org/391462)
+ Improve Sort menu button's behaviors to be more conventional. [Commit.](http://commits.kde.org/elisa/448a9e578ce73a7e4712cf68c1a88da8d393daf0) 
+ Fixup identical IDs introduced in the previous commit. [Commit.](http://commits.kde.org/elisa/6dc33833a21e452a7d540939c19ef398f2fce045) 
+ Fix sort menu positioning. [Commit.](http://commits.kde.org/elisa/45635e362aebe526b00cbde0196d452f16600965) Fixes bug [#448083](https://bugs.kde.org/448083)
+ Fix crash when QUrl in Enqueued files is empty. [Commit.](http://commits.kde.org/elisa/e2ccbc8c98e14dc5e709e9ea5e35937b9fb6e7b5) Fixes bug [#428218](https://bugs.kde.org/428218)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/eventviews/89a789f6c087fe1cdbe8107eb90605c1663b5f70) 
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Announce that we accept urls, not just files in the desktop file. [Commit.](http://commits.kde.org/filelight/f94a6f7fb1de8612d6d9f5134b586997385b8120) Fixes bug [#448088](https://bugs.kde.org/448088)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Revert "Prefer mime type from content over file name when loading". [Commit.](http://commits.kde.org/gwenview/1281004fff1fbafcee221400aee8dae644260092) Fixes bug [#441698](https://bugs.kde.org/441698)
+ Guard against null QScreen. [Commit.](http://commits.kde.org/gwenview/d812d1df88bb2ffd1b7e4a4d049f798cadc47fd7) Fixes bug [#442424](https://bugs.kde.org/442424)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 21.12.2 release notes. [Commit.](http://commits.kde.org/itinerary/3aa7795a1909555470495e32f92d6f54dc16b02e) 
+ Add 21.12.1 release notes. [Commit.](http://commits.kde.org/itinerary/3afc713169d0c4c8f84be503232149dacd977e81) 
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix path traversal in Album Artist field for CD ripping. [Commit.](http://commits.kde.org/k3b/8078d7f04df90303696a31e8c9f2db200891e2ca) 
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ Add form factors to embedded json metadata. [Commit.](http://commits.kde.org/kaccounts-integration/22537240c7b21583111c23d4299ed5b0c5c9bba7) 
{{< /details >}}
{{< details id="kajongg" title="kajongg" link="https://commits.kde.org/kajongg" >}}
+ Fix regressions from cf8391e01f05f3d913a37a6c7865e5d82344d893. [Commit.](http://commits.kde.org/kajongg/4e9c9373bf39c51190d532aa750b0ec7423472d6) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ For command alarms, always use path to find executables. [Commit.](http://commits.kde.org/kalarm/add311033f1a5fd9d20a0e4e2ed200f38edaad0b) 
+ Fix deleted calendar resources reappearing when KAlarm restarts. [Commit.](http://commits.kde.org/kalarm/342e85382b44f929fba020245ab6805cf3813fc5) 
+ Bug 448212: Fix crash after Defer is selected in alarm notification message. [Commit.](http://commits.kde.org/kalarm/22b19b0176deb7e512acd44fec08260396212731) 
+ Make auto-close work for message windows. [Commit.](http://commits.kde.org/kalarm/aa94131c825763d81ba1bcec55e518554631cd11) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Improve QProcess handling. [Commit.](http://commits.kde.org/kate/7e08a58fb50d28ba96aedd5f5cd79a9479b4a0ad) 
+ Avoid that we execute LSP binaries from cwd. [Commit.](http://commits.kde.org/kate/c5d66f3b70ae4778d6162564309aee95f643e7c9) 
+ Step down warning level when LSP not found. [Commit.](http://commits.kde.org/kate/92a9c65e30b4b63b8b116eb5c8dcb1e1a2d867bc) Fixes bug [#448549](https://bugs.kde.org/448549)
+ Lspclient: send didSave notification if so requested. [Commit.](http://commits.kde.org/kate/6fc3bf6e5bd540e842e32c4a959c2158c8573be5) 
+ Lspclient: consider some additional server capabilities. [Commit.](http://commits.kde.org/kate/361dd43e42994829dbdb35e78fb7698d27cbb0e2) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Fix display of comments in invitation. [Commit.](http://commits.kde.org/kcalutils/22aa218435a052d112fd3d37865acd7cb8735c72) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix sometimes cannot move grouped clip right when only 1 empty frame. [Commit.](http://commits.kde.org/kdenlive/f1ca6d017b65f58ec17e12afc97cc27f0e0c4093) 
+ When saving effect, show it under its name, not id in effect list. [Commit.](http://commits.kde.org/kdenlive/ce05e2e448a0e4e3cb4a3a3e4b862df3d5681ac5) 
+ Fix fade effects not correctly saved or pasted. [Commit.](http://commits.kde.org/kdenlive/0b7ab8dd6a7b3dafa8e635d837ee499d411b7150) 
+ Fix clip monitor allowing seek past clip length with transparency background enabled. [Commit.](http://commits.kde.org/kdenlive/2883c62a2e3c98eddebe76ef5811519b0d0418b9) 
+ Fix green tint on first image extract. [Commit.](http://commits.kde.org/kdenlive/c2558c7afc4ae73c4934e1c08044c1d3622991ce) 
+ Minor cleanup of add marker ui. [Commit.](http://commits.kde.org/kdenlive/371e21deecbf1a28e21d00cb0cb3248125fbe3e4) 
+ Ensure thumbnail preview profile is not changed by clip resolution. [Commit.](http://commits.kde.org/kdenlive/991989ab761023bbab8052deefd84b047f8bd654) 
+ Fix alpha render and add utvideo. [Commit.](http://commits.kde.org/kdenlive/136a07aa2563b4e59d881bb74b431d992e947e38) Fixes bug [#448010](https://bugs.kde.org/448010). See bug [#436879](https://bugs.kde.org/436879)
+ Fix freeze trying to drag a clip that was just added to Bin. [Commit.](http://commits.kde.org/kdenlive/44a217e78ebf9ebf61eae8321f32935527a3379e) 
+ Uptade frei0r.scale0tilt.xml with Scale X and Y parameters now animated. [Commit.](http://commits.kde.org/kdenlive/e9e08f0e0442446cfa39e609066999efbeb4e938) 
+ Fix cherry-pick typo. [Commit.](http://commits.kde.org/kdenlive/dd3aca47e7f72c1452f55359ba9eeec6280c83e4) 
+ Fix timeline ruler not working after effect drop in some circumstances. [Commit.](http://commits.kde.org/kdenlive/17eea317ed3d8fb2ea0bae415246e725797f99df) 
+ Fix various bugs in timeremap (keyframes random move, crashes). [Commit.](http://commits.kde.org/kdenlive/d8b2a2569dbaf3d212c70c9f5f93bb2b395d869e) 
+ Time Remap: don't allow keyframe after last frame of source clip. [Commit.](http://commits.kde.org/kdenlive/c7d53f8a169772fdb191061be3ba9eb46755d728) 
+ Protect timeline preview list with mutex. [Commit.](http://commits.kde.org/kdenlive/7937551fb91c9fa7664af5d1e5a7020fa8573a2d) 
+ Fix slideshow duration not updated on profile change. [Commit.](http://commits.kde.org/kdenlive/3739fe99f6f52146acddc62329ec1220426c7a79) 
+ Fix detection of missing timeline preview chunks on opening. [Commit.](http://commits.kde.org/kdenlive/433572a69c25d640e9a70970ce5465429a7cb3fa) 
+ Don't attempt to create audio thumbs if thumbs are disabled. [Commit.](http://commits.kde.org/kdenlive/c1e7f6a0917fbdd04cac51c09930c49fd523f913) Fixes bug [#448304](https://bugs.kde.org/448304)
+ Speedup loading of projects with timeline preview. [Commit.](http://commits.kde.org/kdenlive/d0a5fb9c983b5b77260e24482bef552aeef23c98) 
+ Add some default LUT files. [Commit.](http://commits.kde.org/kdenlive/e3b712379359923e8efce0bb2156da542231e6da) 
+ [Wizzard] Update link to troubleshooting docs. [Commit.](http://commits.kde.org/kdenlive/934bcffcaf0e6b6782b60fa1c7efc82a93b7ff29) 
+ Try to find mediainfo on windows automatically too. [Commit.](http://commits.kde.org/kdenlive/77adf710fd5dd15908df724b549b37673eeb9784) 
+ [Setup Wizard] Show codes if there are only info messages, fix doc link. [Commit.](http://commits.kde.org/kdenlive/419e1b2fb86411efd2f90a8dfad46212f0131e2e) 
+ Disable "Change Speed" and "Time Remap" actions if the other one is. [Commit.](http://commits.kde.org/kdenlive/9d3a7c6ecc726eae2242abeb477d3d8c3f908e68) Fixes bug [#443613](https://bugs.kde.org/443613)
+ Check for mediainfo in setup wizard. [Commit.](http://commits.kde.org/kdenlive/ade1c515e697ad367e45b6298ee369d5f7087bef) 
+ Fix extract frame on Windows (also used for Titler and scopes). [Commit.](http://commits.kde.org/kdenlive/c04ff48b4bc416954d2733a8ed842a39f1feb40e) 
+ Improve monitor zoom. [Commit.](http://commits.kde.org/kdenlive/e6694e6ba457aeecd5657f1ec3627bec7b6f6ac2) Fixes bug [#434404](https://bugs.kde.org/434404)
+ Use a SPDX standard license identifier in Appstream data. [Commit.](http://commits.kde.org/kdenlive/307bedfe593eb344e2db2604208e2c7147263d79) Fixes bug [#448134](https://bugs.kde.org/448134)
+ Rename forgotten rgb24 and rgb24a after MLT 7 port. [Commit.](http://commits.kde.org/kdenlive/88b3d381864573a9db256e476157a866e60dfb90) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix build with GCC 12 (more standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kdepim-addons/b3bcc3d8db63f9b771e5222dbdafc1ab05c667a2) 
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kdepim-addons/e99d09fe9e7ace7c40e6da42b08d6673fc70b95e) 
+ Fix header width. [Commit.](http://commits.kde.org/kdepim-addons/a287d490e64f45853ac999c5642082176589ed74) Fixes bug [#378689](https://bugs.kde.org/378689)
{{< /details >}}
{{< details id="kdiamond" title="kdiamond" link="https://commits.kde.org/kdiamond" >}}
+ Fix rendering the background when in hidpi. [Commit.](http://commits.kde.org/kdiamond/e7c98d9af6d0833f85ea19d51083bc1f822cc585) 
{{< /details >}}
{{< details id="kgeography" title="kgeography" link="https://commits.kde.org/kgeography" >}}
+ Fix color of some United States Minor Outlying Islands. [Commit.](http://commits.kde.org/kgeography/3b13476ef825be7bf803020120b33f7a725f24d5) Fixes bug [#449048](https://bugs.kde.org/449048)
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ Add missing space between gnupg arguments when generating key in expert mode. [Commit.](http://commits.kde.org/kgpg/09b95f4d27b0475a0c8b937969125d31533317c2) Fixes bug [#447611](https://bugs.kde.org/447611)
+ Always close the input channel of a text decryption. [Commit.](http://commits.kde.org/kgpg/b328227cdd4dc227c1df8ba6bb5e4a133e551656) Fixes bug [#444848](https://bugs.kde.org/444848)
{{< /details >}}
{{< details id="kimap" title="kimap" link="https://commits.kde.org/kimap" >}}
+ Handle pure SSL/TLS negotation in Session instead of LoginJob. [Commit.](http://commits.kde.org/kimap/dbcedd5aaab1a4e691966d1e37de1a0b413ca605) Fixes bug [#449184](https://bugs.kde.org/449184)
+ LoginJob: Verify that encryption is established before authentication. [Commit.](http://commits.kde.org/kimap/02d87190612b5de81490908222d91e70435dad74) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Sftp: Don't compare size_t against -1. [Commit.](http://commits.kde.org/kio-extras/1b3ef4a6e34a12aa5b1a782bb018654ac4eb7f6e) 
+ Sftp: Allow compression if necessary. [Commit.](http://commits.kde.org/kio-extras/f3ad370f237c69c018c968bab6f79b16c27edbba) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Round up when converting vector bounding box to integers. [Commit.](http://commits.kde.org/kitinerary/e977e7909176303b07b1a606660ac78f89913990) 
+ Include the pen width in computing vector bounding boxes. [Commit.](http://commits.kde.org/kitinerary/2988fd1c592b23078e28c9ab9f243f0730a7a13d) 
+ Only consider rectangular strokes for the PDF vector barcode detection. [Commit.](http://commits.kde.org/kitinerary/380d868ee7ad6f5c15b367a3ae64a3a573ec1651) 
+ Fix build against KF5.91+. [Commit.](http://commits.kde.org/kitinerary/a5cf8674495f165abba585d817f8ed96a88ce8f1) 
+ Add extractor script for Skymark booking emails. [Commit.](http://commits.kde.org/kitinerary/0b789f0e4b88cd7778532768aeb8e4e4f718defd) 
+ Normalize flight numbers during post-processing. [Commit.](http://commits.kde.org/kitinerary/ea2af0d91edef793b3c343a1168bf52a5effd8e0) 
+ Make the same city detection consider more information. [Commit.](http://commits.kde.org/kitinerary/a21e9a8a48c2ffc94464945148fcce71554bd5ee) 
{{< /details >}}
{{< details id="kldap" title="kldap" link="https://commits.kde.org/kldap" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kldap/9c3060655c93f1cf5f564b18432cf6cf8824b538) 
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ Make hidpi aware. [Commit.](http://commits.kde.org/kmahjongg/28b0b97cfffcfb3017b11bd131d4147992caafe9) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kmail/766f72c2798ebf651fa66e41a21efb691e62cb35) 
{{< /details >}}
{{< details id="kmailtransport" title="kmailtransport" link="https://commits.kde.org/kmailtransport" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kmailtransport/8f18ea2f5f63d318b23ca33e8e2bc0b15f5131f3) 
{{< /details >}}
{{< details id="knotes" title="knotes" link="https://commits.kde.org/knotes" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/knotes/b19bf674aa1b9222a54c460e6776de0fc3cb68ab) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Only update wayland activation token if one was provided. [Commit.](http://commits.kde.org/konversation/ec4ec05663c0d2b8b8d25a92212d3265afb01c27) 
+ Fix clicking the close button in systray mode if not foreground window. [Commit.](http://commits.kde.org/konversation/b3d8612968ad4f0102e5640c429999d820f60717) 
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/korganizer/278ed7955d3843cdad5550e3d66b66cc61e13e04) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/kpimtextedit/e570898fa2f7abd217865506477610b50b23e295) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix infinite recursion in dummy backend. [Commit.](http://commits.kde.org/kpmcore/7f41b7c2aac54c68788ed31d4ebcb7cd509d6c83) Fixes bug [#432704](https://bugs.kde.org/432704)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Disambiguate station name used in the Navitia name-based query tests. [Commit.](http://commits.kde.org/kpublictransport/5b53ae4d140c86bec3014c3f546233170e51df24) 
+ Fix Navitia location queries for rental bike stations and any type. [Commit.](http://commits.kde.org/kpublictransport/fa577b1827750014c403f69e32e9249e2714bb8f) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Be HiDPI aware. [Commit.](http://commits.kde.org/libkdegames/646577813c7e92635c53c4a5cd245ad2ccdb7a5a) 
{{< /details >}}
{{< details id="libkmahjongg" title="libkmahjongg" link="https://commits.kde.org/libkmahjongg" >}}
+ Make the code hidpi aware. [Commit.](http://commits.kde.org/libkmahjongg/ffaa4b43370aab56fa2b27249c14ad60aa1eaad2) 
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/libksieve/28b8fe676308f47f35823c55b02284003281bc0d) 
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Remove unneeded includes. [Commit.](http://commits.kde.org/lokalize/9c146eb94c066ed8c3b70bf866fe605a6f0ed6ac) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/mailcommon/dbe9645ef81bdd68de62533865b3608814003049) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix Passing a PGP message via KToolInvocation::invokeMailer() it will not show up in the composer. [Commit.](http://commits.kde.org/messagelib/43754e2876962e9db329eb72aa64d8b1babd7fb5) Fixes bug [#332368](https://bugs.kde.org/332368)
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/messagelib/98d360877e289c4235176c28ecb4bc0243c23079) 
+ Fix  Spam false-positive, because KMail mis-parses some links. [Commit.](http://commits.kde.org/messagelib/229f211cee02196e7e878ecb2e9dfdd74b178fb4) Fixes bug [#448029](https://bugs.kde.org/448029)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix opening epub files with non ascii names on Windows. [Commit.](http://commits.kde.org/okular/5d1d412092ada95da12190ef6738140c6667cbd1) Fixes bug [#448274](https://bugs.kde.org/448274)
+ Make sure helper apps we start are in path. [Commit.](http://commits.kde.org/okular/feef900250958e59a7a09eee60d732ef47a84b79) 
+ XPS: fix multipiece image loading. [Commit.](http://commits.kde.org/okular/da5fdc151b75346a0f829d5f3027418f4d3a14e8) 
+ Fix saving to okular archive on Windows. [Commit.](http://commits.kde.org/okular/3e919b42873081574e3a095349b736f47675eae1) Fixes bug [#431717](https://bugs.kde.org/431717)
+ Fix previewing embedded files on Windows. [Commit.](http://commits.kde.org/okular/6795ae7f2c02c388cbd6db619891c20477496139) Fixes bug [#448735](https://bugs.kde.org/448735)
+ Fix two Signature issues on multipage/multisignature documents. [Commit.](http://commits.kde.org/okular/44c86de7e415e30572d774a3ec3e90681bc8abfa) 
{{< /details >}}
{{< details id="parley" title="parley" link="https://commits.kde.org/parley" >}}
+ Fix application crash. [Commit.](http://commits.kde.org/parley/c415c7ce5f6a463a582451d02293d004d1852c34) 
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Fix build with GCC 12 (more standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/pim-data-exporter/88ee9ad36860da0022bbc136e3db6a674ae30ee5) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Fix build with GCC 12 (more standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/pim-sieve-editor/3002ef39bf647eee405858605fa9c38b1ab6dc0e) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Fix build with GCC 12 (standard attributes in middle of decl-specifiers). [Commit.](http://commits.kde.org/pimcommon/2edf84eb03e9275fcdd94212e2da854cdbbcdcb3) 
{{< /details >}}
{{< details id="signon-kwallet-extension" title="signon-kwallet-extension" link="https://commits.kde.org/signon-kwallet-extension" >}}
+ Build: Pass the version to project(). [Commit.](http://commits.kde.org/signon-kwallet-extension/49fd904c574e4ef264b0d0b89eec79681db5562e) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Show magnifier immediately after toggle. [Commit.](http://commits.kde.org/spectacle/4ffe4a537a6fe63e9f42cad92719cb38651fec76) 
+ Make tools button visible even if no screenshot has been taken. [Commit.](http://commits.kde.org/spectacle/3d588f3886daee6aea4141060e8ae8f64f902f26) 
{{< /details >}}
