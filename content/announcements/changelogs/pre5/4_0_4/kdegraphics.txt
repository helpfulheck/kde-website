2008-03-30 00:03 +0000 [r791602]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/lib/mimetypeutils.cpp: Use
	  KMimeType::findByUrl() for remote urls too. It's necessary for
	  sftp. BUG:159618 Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@791601

2008-03-31 20:51 +0000 [r792339]  pino

	* branches/KDE/4.0/kdegraphics/okular/part.cpp: Backport: enable
	  and disable the print action from the right place, so it is
	  handled properly with remote documents. CCBUG: 160173

2008-03-31 23:03 +0000 [r792389]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/bookmarklist.cpp:
	  Backport: dynamically get the tooltip of bookmark items.
	  CCCCMAIL: 160189

2008-03-31 23:14 +0000 [r792392]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/bookmarkmanager.cpp:
	  Backport: keep the page cache updated. CCBUG: 160190

2008-04-03 09:52 +0000 [r793231]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/guiutils.cpp: Backport:
	  properly escape rich text in html-ish tooltips. CCBUG: 160306

2008-04-08 21:21 +0000 [r794918]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageviewannotator.cpp:
	  Backport: constructing a text highlight annotation with no text
	  inside is pointless. CCBUG: 160502

2008-04-08 23:38 +0000 [r794965]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageviewannotator.cpp:
	  backport: do not leak the empty selection

2008-04-11 18:51 +0000 [r795884]  aacid

	* branches/KDE/4.0/kdegraphics/okular/ui/pageview.cpp: Backport
	  r795883

2008-04-16 11:05 +0000 [r797550]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageview.h,
	  branches/KDE/4.0/kdegraphics/okular/ui/pageview.cpp: backport:
	  properly detect when the visible pixmap request is called as
	  result of scollbar event

2008-04-16 11:12 +0000 [r797554]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/presentationwidget.cpp:
	  backport: when we are requested to paint but the pixmap has not
	  been rendered yet, fill the paint area with the background color

2008-04-18 22:02 +0000 [r798697]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/annotationwidgets.cpp,
	  branches/KDE/4.0/kdegraphics/okular/ui/annotationpropertiesdialog.cpp:
	  backport: properly align the text labels to the right, as per HIG

2008-04-19 18:46 +0000 [r798869]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/comicbook/generator_comicbook.cpp,
	  branches/KDE/4.0/kdegraphics/okular/generators/comicbook/document.cpp,
	  branches/KDE/4.0/kdegraphics/okular/generators/comicbook/document.h:
	  Backport: Add a close() method for the comicbook document so: -
	  resources can be freed when closing the Okular document for real
	  (and not just when opening a new comicbook document or quitting)
	  - we do not crash when doing an opening sequence like: cbz -> cbr
	  -> cbz

2008-04-19 20:17 +0000 [r798892-798891]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/textdocumentgenerator.cpp:
	  backport: clear TOC and document info when closing a
	  textdocument-based document

	* branches/KDE/4.0/kdegraphics/okular/generators/ooo/converter.cpp:
	  backport: create the style information object only during the
	  conversion; this way, there are no stale information across
	  multiple openings

2008-04-19 20:31 +0000 [r798898]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/plucker/generator_plucker.cpp:
	  backport: clear the document information when a plucker document
	  is closed

2008-04-20 09:49 +0000 [r799078]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/textdocumentgenerator.cpp:
	  backport: - when the loading of a textdocument-based document
	  fails, cleanup all the stuff eventually gathered from the
	  converter - cleanup also the annotation position when closing a
	  textdocument

2008-04-21 08:13 +0000 [r799322]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/annotationpropertiesdialog.cpp:
	  backport: make the text of the creation and modification date
	  labels copyable

2008-04-27 18:06 +0000 [r801781]  aacid

	* branches/KDE/4.0/kdegraphics/okular/core/document.cpp: Correctly
	  calculate the amount of memory to free, should fix bug 153675
	  BUGS: 153675

