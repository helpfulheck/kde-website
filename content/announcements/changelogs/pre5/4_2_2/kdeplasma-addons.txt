------------------------------------------------------------------------
r933157 | scripty | 2009-02-28 07:41:09 +0000 (Sat, 28 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r933569 | mfuchs | 2009-03-01 11:23:15 +0000 (Sun, 01 Mar 2009) | 2 lines

Backport r933566

------------------------------------------------------------------------
r933574 | mfuchs | 2009-03-01 11:37:12 +0000 (Sun, 01 Mar 2009) | 2 lines

Backport r933573

------------------------------------------------------------------------
r933713 | mfuchs | 2009-03-01 14:33:22 +0000 (Sun, 01 Mar 2009) | 2 lines

Backport r933711

------------------------------------------------------------------------
r933904 | mfuchs | 2009-03-01 21:35:05 +0000 (Sun, 01 Mar 2009) | 2 lines

Adds parentheses.

------------------------------------------------------------------------
r933912 | mfuchs | 2009-03-01 22:05:45 +0000 (Sun, 01 Mar 2009) | 2 lines

Check if the app needs to be configured.

------------------------------------------------------------------------
r934463 | scripty | 2009-03-03 07:43:26 +0000 (Tue, 03 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r936603 | mfuchs | 2009-03-08 03:51:07 +0000 (Sun, 08 Mar 2009) | 4 lines

Always enable "Jump to current Strip", that way it also acts as refresh
action.
CCBUG: 185068

------------------------------------------------------------------------
r936627 | scripty | 2009-03-08 07:36:43 +0000 (Sun, 08 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r940556 | dhaumann | 2009-03-17 16:48:31 +0000 (Tue, 17 Mar 2009) | 2 lines

svnbackport: use antialiasing when drawing the filled circles

------------------------------------------------------------------------
r941242 | scripty | 2009-03-19 07:49:54 +0000 (Thu, 19 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941286 | mlaurent | 2009-03-19 11:15:34 +0000 (Thu, 19 Mar 2009) | 6 lines

Backport:
Patch from glad.deschrijver@gmail.com
fix when we use custom decimal separator
Thanks for the patch


------------------------------------------------------------------------
r942600 | scripty | 2009-03-22 07:44:29 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943010 | scripty | 2009-03-23 07:46:43 +0000 (Mon, 23 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943409 | pino | 2009-03-23 19:52:04 +0000 (Mon, 23 Mar 2009) | 4 lines

make sure to extract strings also from subdirectories
(this adds ~35 strings)
CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r943683 | scripty | 2009-03-24 08:21:22 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944177 | scripty | 2009-03-25 08:47:38 +0000 (Wed, 25 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944812 | scripty | 2009-03-26 08:14:30 +0000 (Thu, 26 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
