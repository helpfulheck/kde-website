------------------------------------------------------------------------
r961989 | nielsslot | 2009-05-01 11:01:51 +0000 (Fri, 01 May 2009) | 5 lines

Backport revision 961988.
Prevent KTurtle from crashing after a program was stopped and started while executing a for loop.
The crash occured because it was possible that KTurtle would delete the globalVariableTable more than once.
Solves bug 191229

------------------------------------------------------------------------
r962654 | uwolfer | 2009-05-02 20:12:10 +0000 (Sat, 02 May 2009) | 5 lines

Backport:
SVN commit 962653 by uwolfer:

Remove mail address of former maintainer for bugreporting.
CCBUG:191383
------------------------------------------------------------------------
r963048 | sstein | 2009-05-03 20:15:20 +0000 (Sun, 03 May 2009) | 19 lines

backport from Trunk (https://bugs.kde.org/token.cgi?t=8cDojiBlcS&a=cfmem):

Implements a better way to check if a task was solved correctly by using
the == operator of the ratio class itself (fix bug 178007).

In contrast to Trunk, I have not added a help message to explain users why
tasks are counted as wrong if they were not entered in mixed notation. This
is not possible due to string freeze!

additional changes:
- removed a lot of debug statements from the code
- added clean() function to task; useful for debugging
- some whitespace changes introduced by kdevelop

Changes are partly based on patch provided by Tadeu Araujo. Thanks for your
contribution!

BUG:178007

------------------------------------------------------------------------
r963060 | sstein | 2009-05-03 20:34:49 +0000 (Sun, 03 May 2009) | 7 lines

backport from Trunk

CCBUG:191451

Ouch! The results for the percentage task are hard-coded and so is the
wrong result.

------------------------------------------------------------------------
r963072 | sstein | 2009-05-03 21:02:20 +0000 (Sun, 03 May 2009) | 7 lines

backport from Trunk

The answer will now be shown in all cases. KBruch tried to optimise the
output too much.

CCBUG:183063

------------------------------------------------------------------------
r964664 | scripty | 2009-05-07 07:40:30 +0000 (Thu, 07 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r966501 | scripty | 2009-05-11 09:09:08 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r966703 | sstein | 2009-05-11 19:13:01 +0000 (Mon, 11 May 2009) | 8 lines

backport from Trunk:

Exercises of type "convert" can now be controlled again by just using the
keyboard. The "next task" button has now the default focus again.

CCBUG:192363


------------------------------------------------------------------------
r966707 | sstein | 2009-05-11 19:32:14 +0000 (Mon, 11 May 2009) | 6 lines

backport from Trunk

Show dash correctly above irrational number.

CCBUG:192364

------------------------------------------------------------------------
r966756 | sstein | 2009-05-11 20:35:43 +0000 (Mon, 11 May 2009) | 6 lines

backport from Trunk/

Decimal symbol in "conversion" task now depends on the KDE GUI language.

CCBUG:192365

------------------------------------------------------------------------
r966862 | jriddell | 2009-05-12 01:10:20 +0000 (Tue, 12 May 2009) | 1 line

make it compile with Phonon built from Qt
------------------------------------------------------------------------
r966864 | jriddell | 2009-05-12 01:13:49 +0000 (Tue, 12 May 2009) | 1 line

make it compile with Phonon built from Qt
------------------------------------------------------------------------
r966938 | jriddell | 2009-05-12 10:13:02 +0000 (Tue, 12 May 2009) | 1 line

revert phonon header change, Qt creates Global now, http://qt.gitorious.org/qt/qt/commit/5299240db14579960358edeebfc72fcef905af13
------------------------------------------------------------------------
r967313 | scripty | 2009-05-13 08:07:03 +0000 (Wed, 13 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r967685 | ksvladimir | 2009-05-13 18:40:27 +0000 (Wed, 13 May 2009) | 2 lines

Backported a bugfix from r967684.

------------------------------------------------------------------------
r971947 | schwarzer | 2009-05-23 19:21:06 +0000 (Sat, 23 May 2009) | 15 lines

backport of r970794

remove hack that removes the weekday from longDate

This hack causes trouble if the longDate string is modified by the user
or just different in another locale.

E.g. it makes
    "Wednesday 20 May 2009" -> "20 May 2009"
but breaks with strings like
    "Wednesday, 20. May 2009" -> " , 20. May 2009"

CCBUG: 193115


------------------------------------------------------------------------
r972981 | scripty | 2009-05-26 08:09:29 +0000 (Tue, 26 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r974130 | kbal | 2009-05-28 14:52:04 +0000 (Thu, 28 May 2009) | 2 lines

Backport of 956904 by lauranger
correct a crash/hang bug when # of capital to play with is less than 4 (ie:Israel)
------------------------------------------------------------------------
