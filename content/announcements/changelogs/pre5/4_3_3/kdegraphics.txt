------------------------------------------------------------------------
r1031807 | gateau | 2009-10-06 07:14:46 +0000 (Tue, 06 Oct 2009) | 1 line

Bumped versions
------------------------------------------------------------------------
r1032483 | pino | 2009-10-07 22:29:06 +0000 (Wed, 07 Oct 2009) | 3 lines

hide the top bar when moving out of the presentation widget
for example when moving the cursor to another screen

------------------------------------------------------------------------
r1032945 | pino | 2009-10-08 22:20:11 +0000 (Thu, 08 Oct 2009) | 5 lines

when we remove the file:/ from the argument, do the precent-encoding removal to avoid being percent-encoded more than needed
based on a patch by Albert, slightly changed by me
will be in kde 4.3.3
CCBUG: 207461

------------------------------------------------------------------------
r1034050 | pino | 2009-10-11 20:24:42 +0000 (Sun, 11 Oct 2009) | 5 lines

Better check for libchmfile loading errors.
Will be in KDE 4.3.3.

CCBUG: 210234

------------------------------------------------------------------------
r1037352 | sars | 2009-10-18 20:32:34 +0000 (Sun, 18 Oct 2009) | 1 line

Back-port the sane_cancel() not being thread safe fix.
------------------------------------------------------------------------
r1037415 | pino | 2009-10-19 00:21:04 +0000 (Mon, 19 Oct 2009) | 4 lines

check the actual mime type names and not their pointers
will be in kde 4.3.3
CCBUG: 210882

------------------------------------------------------------------------
r1039156 | aacid | 2009-10-22 20:52:17 +0000 (Thu, 22 Oct 2009) | 5 lines

Backport r1039155 | aacid | 2009-10-22 22:51:18 +0200 (Thu, 22 Oct 2009) | 3 lines

crop area needs to be just against user rotation, not taking account page orientation since the cropbox is done on page orientation coords
BUGS: 196761

------------------------------------------------------------------------
r1039192 | aacid | 2009-10-22 22:37:17 +0000 (Thu, 22 Oct 2009) | 4 lines

backport r1039167 and r1039159
use a queue for the rendering requests of the rendering thread
this should avoid multiple generator instance to step on each other toes, making spectre crashing

------------------------------------------------------------------------
r1039571 | aacid | 2009-10-23 20:53:51 +0000 (Fri, 23 Oct 2009) | 6 lines

Backport r1039570 | aacid | 2009-10-23 22:52:12 +0200 (Fri, 23 Oct 2009) | 4 lines

use the correct variable to calculate dpis
Fixes scrolling on a rotated page being SLOOOOOOOOOOOOW and also fixes bug 211467
BUGS: 211467

------------------------------------------------------------------------
r1040358 | cgilles | 2009-10-26 09:25:22 +0000 (Mon, 26 Oct 2009) | 2 lines

install svg

------------------------------------------------------------------------
r1040361 | cgilles | 2009-10-26 09:27:20 +0000 (Mon, 26 Oct 2009) | 2 lines

install svg

------------------------------------------------------------------------
r1040367 | cgilles | 2009-10-26 09:30:50 +0000 (Mon, 26 Oct 2009) | 2 lines

polish

------------------------------------------------------------------------
r1040791 | aacid | 2009-10-26 21:04:09 +0000 (Mon, 26 Oct 2009) | 4 lines

SVN commit 1040789 by aacid:

using Part as class name makes it very easy to collide with other Parts out there and very ugly things will happen, add a namespace

------------------------------------------------------------------------
r1042043 | pino | 2009-10-29 00:40:16 +0000 (Thu, 29 Oct 2009) | 3 lines

bump version to 0.9.3
bump versions of poppler, spectre and chm generators

------------------------------------------------------------------------
