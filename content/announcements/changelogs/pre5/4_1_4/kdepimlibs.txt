------------------------------------------------------------------------
r879229 | winterz | 2008-11-02 19:37:25 +0000 (Sun, 02 Nov 2008) | 5 lines

backport SVN commit 879190 by osterfeld:

don't crash when timezone info is NULL. fixes a crash on korgac startup triggered when parsing the timezoneId 
from a null string

------------------------------------------------------------------------
r879825 | scripty | 2008-11-04 07:35:05 +0000 (Tue, 04 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r879951 | mueller | 2008-11-04 12:33:34 +0000 (Tue, 04 Nov 2008) | 1 line

backport 879950
------------------------------------------------------------------------
r880893 | woebbe | 2008-11-06 19:40:39 +0000 (Thu, 06 Nov 2008) | 1 line

memleak in Generic
------------------------------------------------------------------------
r880897 | woebbe | 2008-11-06 19:51:29 +0000 (Thu, 06 Nov 2008) | 1 line

test before delete[] is not needed
------------------------------------------------------------------------
r880908 | woebbe | 2008-11-06 20:49:24 +0000 (Thu, 06 Nov 2008) | 5 lines

and now fix the real leak:

the private base classes had no virtual dtor.

this made knode nearly unusable (if you wanted to run it for some time).
------------------------------------------------------------------------
r882242 | scripty | 2008-11-10 07:35:41 +0000 (Mon, 10 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r883856 | tmcguire | 2008-11-13 18:12:56 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883808 by tmcguire from trunk to the 4.1 branch:

Merged revisions 877777 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r877777 | tilladam | 2008-10-30 11:47:12 +0100 (Thu, 30 Oct 2008) | 1 line
  
  First half of fixing the kio_sieve related klauncher crashes. Make sure not to emit error twice.
........


------------------------------------------------------------------------
r883857 | tmcguire | 2008-11-13 18:15:02 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883809 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883237 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883237 | mutz | 2008-11-12 14:30:34 +0100 (Wed, 12 Nov 2008) | 1 line
  
  More readable debug output (use symbolic status names instead of numeric value), return better errors for a) write() failures (gets the error from the system now) and b) for missing passphrases.
........


------------------------------------------------------------------------
r883860 | tmcguire | 2008-11-13 18:18:03 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883810 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883308 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883308 | mutz | 2008-11-12 17:38:31 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Fix signing of multiple uids (!= all, though) at the same time. We need to create artificial states after each UID selection; otherwise, action() isn't called and the whole thing blocks. Also, we're doing multihtreading here, so don't use a static char buf, but a member std::string.
........


------------------------------------------------------------------------
r883861 | tmcguire | 2008-11-13 18:20:58 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883811 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883317 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883317 | mutz | 2008-11-12 18:20:02 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Print the nextState() call also in the error case
........


------------------------------------------------------------------------
r883862 | tmcguire | 2008-11-13 18:22:31 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883812 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883319 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883319 | mutz | 2008-11-12 18:21:34 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Interpret GPGME_STATUS_ALREADY_SIGNED as an error, independant of state. Fixes "General Error" returned when trying to sign an already signed UID again with the same key.
........


------------------------------------------------------------------------
r883863 | tmcguire | 2008-11-13 18:25:59 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883813 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883322 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883322 | mutz | 2008-11-12 18:40:38 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Use better error codes (new in gpg-error >= 1.6)
........


------------------------------------------------------------------------
r883864 | tmcguire | 2008-11-13 18:27:59 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883814 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883324 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883324 | mutz | 2008-11-12 18:41:23 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Treat GPGME_STATUS_{KEY,SIG}EXPIRED as errors, too.
........


------------------------------------------------------------------------
r883866 | tmcguire | 2008-11-13 18:30:06 +0000 (Thu, 13 Nov 2008) | 12 lines

Backport r883815 by tmcguire from trunk to the 4.1 branch:

Merged revisions 883327 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

........
  r883327 | mutz | 2008-11-12 18:50:36 +0100 (Wed, 12 Nov 2008) | 1 line
  
  Add missing gpg_error() call (to set the correct source of the error)
........


------------------------------------------------------------------------
r886020 | binner | 2008-11-18 13:49:35 +0000 (Tue, 18 Nov 2008) | 7 lines

backport r884779:

Call to QUrl::fromPercentEncoding() is not needed anymore. (url.path() is already doing the decoding).

Fix fetching of article by message-id which contains a %.
BUG: 169140

------------------------------------------------------------------------
r886341 | weilbach | 2008-11-19 00:54:29 +0000 (Wed, 19 Nov 2008) | 2 lines

Backport of 886325.

------------------------------------------------------------------------
r887083 | weilbach | 2008-11-21 02:29:12 +0000 (Fri, 21 Nov 2008) | 2 lines

Backport GData tagging fix from Mehrdat.

------------------------------------------------------------------------
r890863 | tmcguire | 2008-11-30 14:54:52 +0000 (Sun, 30 Nov 2008) | 10 lines

Backport r886171 by tmcguire from trunk to the 4.1 branch:

Fix possible crash.
I couldn't reproduce triggering this crash, but apparently it can happen in KMail.

Benoît, can you reproduce this and if yes, how?

CCBUG: 174992


------------------------------------------------------------------------
r892407 | wstephens | 2008-12-04 11:18:06 +0000 (Thu, 04 Dec 2008) | 1 line

Backport r892405 - fixes for crashes when loading corrupt uid maps
------------------------------------------------------------------------
r892930 | wstephens | 2008-12-05 13:57:14 +0000 (Fri, 05 Dec 2008) | 1 line

Backport r892928 (workaround libical assert on bad input)
------------------------------------------------------------------------
r893121 | winterz | 2008-12-06 00:30:58 +0000 (Sat, 06 Dec 2008) | 5 lines

backport SVN commit 888995 by toma:

Make sure the hostname does not end with a \n. Strip it on input and strip it on retrieval.
Thanks for your bug report.

------------------------------------------------------------------------
r893737 | krake | 2008-12-07 12:09:59 +0000 (Sun, 07 Dec 2008) | 5 lines

Initialize the addressbook instance outside the constructor, so it does not try to load any resources.
Resources might use nested event loops to simulate synchronous load and result in re-entry of ::self() causing multiple instance of a class supposed to be a singleton.

Backport of Revision 889879

------------------------------------------------------------------------
r893938 | winterz | 2008-12-07 15:31:46 +0000 (Sun, 07 Dec 2008) | 4 lines

backport SVN commit 876184 by winterz:

use text.isEmpty() rather than text.isNull()

------------------------------------------------------------------------
r893947 | winterz | 2008-12-07 15:39:45 +0000 (Sun, 07 Dec 2008) | 4 lines

backport SVN commit 876740 by winterz:

properly initialize the ctor.  thanks valgrind.

------------------------------------------------------------------------
r893950 | winterz | 2008-12-07 15:41:58 +0000 (Sun, 07 Dec 2008) | 10 lines

backport SVN commit 878370 by winterz:

the VALARM TRIGGER property can be zero and that doesn't mean there's a problem.
if icaldurationtype_is_bad_duration() is true, then just use a 0 duration.
remove the kDebug() statement that there is no trigger time or duration.

btw, the rfc doesn't mention anything about a trigger time; it only
mentions a trigger duration.


------------------------------------------------------------------------
r893953 | winterz | 2008-12-07 15:44:51 +0000 (Sun, 07 Dec 2008) | 5 lines

backport SVN commit 881271 by winterz:

minor changes I found while testing for inline attachment crashes.
with an extra unit test.

------------------------------------------------------------------------
r893956 | winterz | 2008-12-07 15:48:11 +0000 (Sun, 07 Dec 2008) | 9 lines

backport SVN commit 887087 by winterz:

fix crashes in this old, 3rd party code that we should really try to
find updates for.

SVN commit 888036 by chehrlic:

msvc compile++

------------------------------------------------------------------------
r894620 | winterz | 2008-12-08 23:20:04 +0000 (Mon, 08 Dec 2008) | 4 lines

backport SVN commit 882122 by osterfeld:

do not crash when mPort wasn't created

------------------------------------------------------------------------
r894622 | winterz | 2008-12-08 23:24:11 +0000 (Mon, 08 Dec 2008) | 5 lines

backport SVN commit 888590 by winterz:

don't assert on invalid dates in a calendar -- deal with them appropriately.
should fix export as vcs from korganizer.

------------------------------------------------------------------------
r894875 | tnyblom | 2008-12-09 13:57:06 +0000 (Tue, 09 Dec 2008) | 4 lines

Fix LDAP using simple authentication.
Port of r894869


------------------------------------------------------------------------
r894879 | winterz | 2008-12-09 14:04:34 +0000 (Tue, 09 Dec 2008) | 9 lines

backport r894876 by winterz
port from e3 SVN commit 891698 by vkrause:

Quote CN parameters correctly.

Kolab issue 3281

I updated a few reference files for the unit tests.

------------------------------------------------------------------------
r894881 | winterz | 2008-12-09 14:13:02 +0000 (Tue, 09 Dec 2008) | 2 lines

update reference test files

------------------------------------------------------------------------
r894903 | winterz | 2008-12-09 15:48:48 +0000 (Tue, 09 Dec 2008) | 2 lines

merge over the updated version from trunk.

------------------------------------------------------------------------
r894905 | winterz | 2008-12-09 15:51:05 +0000 (Tue, 09 Dec 2008) | 2 lines

use .shell versions of tests so we don't have to use the installed versions.

------------------------------------------------------------------------
r894985 | otrichet | 2008-12-09 18:40:42 +0000 (Tue, 09 Dec 2008) | 6 lines

backport 893724:

Try to login just after the connection to the server. Some servers do not give access to some groups otherwise.

CCBUG: 167718

------------------------------------------------------------------------
r895056 | winterz | 2008-12-09 22:04:38 +0000 (Tue, 09 Dec 2008) | 2 lines

remove. put here by mistake

------------------------------------------------------------------------
r895515 | wstephens | 2008-12-10 21:44:38 +0000 (Wed, 10 Dec 2008) | 3 lines

Backport r895513 - Use the configured "store password" setting for the
"store password" checkbox.

------------------------------------------------------------------------
r895906 | winterz | 2008-12-11 23:09:29 +0000 (Thu, 11 Dec 2008) | 2 lines

revert to fix testing

------------------------------------------------------------------------
r895909 | winterz | 2008-12-11 23:10:52 +0000 (Thu, 11 Dec 2008) | 2 lines

revert to fix testing

------------------------------------------------------------------------
r895910 | winterz | 2008-12-11 23:11:15 +0000 (Thu, 11 Dec 2008) | 2 lines

revert to fix testing

------------------------------------------------------------------------
r895918 | winterz | 2008-12-11 23:27:03 +0000 (Thu, 11 Dec 2008) | 2 lines

fix a backporting boo-boo

------------------------------------------------------------------------
r895924 | winterz | 2008-12-12 00:04:49 +0000 (Fri, 12 Dec 2008) | 5 lines

backport SVN commit 895781 by ervin:

Make it simpler... In fact make it work for me. :-)
Otherwise for date only KDateTime I get an invalid due date.

------------------------------------------------------------------------
r896487 | winterz | 2008-12-13 16:11:33 +0000 (Sat, 13 Dec 2008) | 6 lines

backport SVN commit 896144 by smartins:

Fix a crash when using egroupware.

iterator.key() was being used after removal.

------------------------------------------------------------------------
r896770 | krake | 2008-12-14 13:04:03 +0000 (Sun, 14 Dec 2008) | 4 lines

Initialize addressbook right after instance creation, before setting the global static deleter.
Fixed a crash in KOrganizer when the addressbook contains a LDAP resource.


------------------------------------------------------------------------
r898353 | smartins | 2008-12-18 01:11:56 +0000 (Thu, 18 Dec 2008) | 3 lines

Fixes an assert crash when unloading and then loading a vCal resource.
The assert was Q_ASSERT( mTodos.value( uid ) == todo ); in calendarlocal.cpp

------------------------------------------------------------------------
r899979 | winterz | 2008-12-21 23:03:23 +0000 (Sun, 21 Dec 2008) | 5 lines

backport SVN commit 897432 by ahartmetz:

Use the updated date as a fallback for the published date. This is like the logic in dateUpdated() but the other way around. Fixes this feed: http://www.heise.de/newsticker/heise-atom.xml. It is a high traffic geek website... :)


------------------------------------------------------------------------
r899982 | winterz | 2008-12-21 23:05:07 +0000 (Sun, 21 Dec 2008) | 4 lines

backport SVN commit 898361 by winterz:

Fix for improperly formatted mailto: links in the KOrganizer eventviewer.

------------------------------------------------------------------------
r904282 | djarvie | 2009-01-01 19:55:45 +0000 (Thu, 01 Jan 2009) | 2 lines

Fix for CID:3969 - crash if data() returns null

------------------------------------------------------------------------
r905120 | abizjak | 2009-01-03 18:23:24 +0000 (Sat, 03 Jan 2009) | 2 lines

Backport revision 905118

------------------------------------------------------------------------
r905542 | tmcguire | 2009-01-04 15:26:22 +0000 (Sun, 04 Jan 2009) | 9 lines

Backport r905534 by tmcguire from trunk to the 4.1 branch:

Update the password of the cloned transport of the config dialog if the 
transport manager has loaded the password, otherwise we go into an infinite
password-loading loop in certain situations.

CCBUG: 170728


------------------------------------------------------------------------
r905607 | winterz | 2009-01-04 18:01:16 +0000 (Sun, 04 Jan 2009) | 5 lines

backport SVN commit 903304 by mkoller:

Fix parsing of time string to be more conforming to ISO 8601
which allows a basic and an extended format

------------------------------------------------------------------------
