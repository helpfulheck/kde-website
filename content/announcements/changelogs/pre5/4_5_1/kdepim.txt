------------------------------------------------------------------------
r1168567 | scripty | 2010-08-27 03:33:45 +0100 (dv, 27 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1168533 | djarvie | 2010-08-26 22:55:31 +0100 (dj, 26 ago 2010) | 2 lines

Always update mEarliestAlarm when an alarm is deleted, to prevent possible crashes

------------------------------------------------------------------------
r1167772 | lueck | 2010-08-25 10:33:13 +0100 (dc, 25 ago 2010) | 1 line

backport from trun: remove from khelpcenter navigation tree
------------------------------------------------------------------------
r1167604 | scripty | 2010-08-25 03:15:07 +0100 (dc, 25 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1167043 | mlaurent | 2010-08-23 12:43:07 +0100 (dl, 23 ago 2010) | 2 lines

Backport: add missing i18n

------------------------------------------------------------------------
r1165849 | vkrause | 2010-08-20 09:56:56 +0100 (dv, 20 ago 2010) | 18 lines

Backport the remaining parts of the MDN fetch fix as well, should unbreak flag changes.

Merged revisions 1164584,1164593 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepim

........
  r1164584 | vkrause | 2010-08-17 10:22:21 +0200 (Tue, 17 Aug 2010) | 3 lines
  
  - use const references for Akonadi::Item
  - don't include a random header right in the middle of some code
........
  r1164593 | vkrause | 2010-08-17 10:55:04 +0200 (Tue, 17 Aug 2010) | 4 lines
  
  Get rid of one extra round through the event loop, all expensive
  operations happen inside jobs anyway nowadays. Makes this slightly
  more predictable and debuggable.
........

------------------------------------------------------------------------
r1165829 | tnyblom | 2010-08-20 09:06:05 +0100 (dv, 20 ago 2010) | 4 lines

SVN_SILENT

Fix UTF-8 encoding of copywrite

------------------------------------------------------------------------
r1165721 | tnyblom | 2010-08-20 06:43:58 +0100 (dv, 20 ago 2010) | 4 lines

SVN_SILENT

Fix UTF-8 encoding of copywrite

------------------------------------------------------------------------
r1164832 | tmcguire | 2010-08-17 19:26:27 +0100 (dt, 17 ago 2010) | 14 lines

SVN_MERGE:
Merged revisions 1164594 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/trunk/KDE/kdepim

........
  r1164594 | vkrause | 2010-08-17 10:57:40 +0200 (Tue, 17 Aug 2010) | 6 lines
  
  Get rid of an uneeded extra fetch per read mail, and deal with conflicts
  correctly. Well, just ignore them for the MDN attribute, as this is the
  only place where these are actually written, no user knows about what
  this is about anyway and the only important thing about this attribute
  is that it is set to avoid double MDN processing.
........

------------------------------------------------------------------------
r1164716 | skelly | 2010-08-17 15:20:25 +0100 (dt, 17 ago 2010) | 1 line

Make sure to finish one beginInsert/endInsert pair before processing a new one.
------------------------------------------------------------------------
r1164363 | skelly | 2010-08-16 17:09:44 +0100 (dl, 16 ago 2010) | 1 line

Fix off-by-not error.
------------------------------------------------------------------------
r1164342 | skelly | 2010-08-16 16:15:02 +0100 (dl, 16 ago 2010) | 1 line

Add some asserts.
------------------------------------------------------------------------
r1164307 | vkrause | 2010-08-16 14:49:20 +0100 (dl, 16 ago 2010) | 15 lines

Merged revisions 1164226,1164244 via svnmerge from 
svn+ssh://vkrause@svn.kde.org/home/kde/trunk/KDE/kdepim

........
  r1164226 | vkrause | 2010-08-16 11:27:17 +0200 (Mon, 16 Aug 2010) | 2 lines
  
  bring back in sync with the copy in the email feeder
........
  r1164244 | vkrause | 2010-08-16 12:10:03 +0200 (Mon, 16 Aug 2010) | 4 lines
  
  Move the index compatibility check into the feeder base class so it's
  used not only by the email one. This should stop the unecessary
  re-indexing by e.g. the contact feeder on every Akonadi startup.
........

------------------------------------------------------------------------
r1164086 | djarvie | 2010-08-15 21:42:24 +0100 (dg, 15 ago 2010) | 1 line

Remove Akonadi build option (not implemented yet)
------------------------------------------------------------------------
r1164085 | djarvie | 2010-08-15 21:36:06 +0100 (dg, 15 ago 2010) | 2 lines

Uses akonadi-contact library

------------------------------------------------------------------------
r1163987 | winterz | 2010-08-15 14:43:23 +0100 (dg, 15 ago 2010) | 2 lines

cleanup

------------------------------------------------------------------------
r1163984 | winterz | 2010-08-15 14:39:33 +0100 (dg, 15 ago 2010) | 3 lines

build on Solaris too
CCBUG: 231054

------------------------------------------------------------------------
r1163926 | djarvie | 2010-08-15 12:21:12 +0100 (dg, 15 ago 2010) | 1 line

Reinstate i18nc string with misspelt hint, to avoid translation change
------------------------------------------------------------------------
r1163915 | djarvie | 2010-08-15 11:49:38 +0100 (dg, 15 ago 2010) | 2 lines

Trivial

------------------------------------------------------------------------
r1163913 | djarvie | 2010-08-15 11:43:24 +0100 (dg, 15 ago 2010) | 3 lines

Fix holiday selection initialisation.
Style.

------------------------------------------------------------------------
r1163676 | djarvie | 2010-08-14 20:22:41 +0100 (ds, 14 ago 2010) | 1 line

Fix library linkage
------------------------------------------------------------------------
r1163671 | djarvie | 2010-08-14 19:44:59 +0100 (ds, 14 ago 2010) | 2 lines

Remove unused Akonadi code

------------------------------------------------------------------------
r1163655 | djarvie | 2010-08-14 19:29:10 +0100 (ds, 14 ago 2010) | 2 lines

Remove unused Akonadi code

------------------------------------------------------------------------
r1163642 | djarvie | 2010-08-14 18:45:03 +0100 (ds, 14 ago 2010) | 2 lines

Krazy fixes

------------------------------------------------------------------------
r1162453 | scripty | 2010-08-12 03:07:08 +0100 (dj, 12 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1162111 | krake | 2010-08-11 13:11:30 +0100 (dc, 11 ago 2010) | 1 line

Fixing build on Windows: using local export macro
------------------------------------------------------------------------
r1161882 | scripty | 2010-08-11 03:15:49 +0100 (dc, 11 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1161771 | tmcguire | 2010-08-10 20:24:31 +0100 (dt, 10 ago 2010) | 13 lines

SVN_SILENT:
Recorded merge of revisions 1161742 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/trunk/KDE/kdepim

........
  r1161742 | tmcguire | 2010-08-10 20:36:48 +0200 (Tue, 10 Aug 2010) | 4 lines
  
  Subscribe renamed folders, otherwise they'll disappear after renaming.
  
  SVN_MERGE of r1161648
  Part of kolab/issue4469
........

------------------------------------------------------------------------
r1161768 | tmcguire | 2010-08-10 20:22:22 +0100 (dt, 10 ago 2010) | 3 lines

Backport: Subscribe renamed folders, otherwise they'll disappear after renaming.

Backport of r1161742.
------------------------------------------------------------------------
r1161113 | tokoe | 2010-08-09 20:16:15 +0100 (dl, 09 ago 2010) | 7 lines

Fix threading by using the right string for the messageId.

toUnicodeString() returns the messageId enclosed in angle brackets,
however the In-Reply-To and Reference fields use the identifiers() strings
which have no angle brackets -> no match is found and the 'subject matching'
fallback is used.

------------------------------------------------------------------------
r1160842 | scripty | 2010-08-09 03:21:56 +0100 (dl, 09 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1160403 | scripty | 2010-08-08 03:09:15 +0100 (dg, 08 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159413 | smartins | 2010-08-05 11:18:25 +0100 (dj, 05 ago 2010) | 4 lines

Also call KCalPrefs::instance()->writeConfig();

Now freebusy settings are written to file.

------------------------------------------------------------------------
r1159294 | djarvie | 2010-08-05 00:49:24 +0100 (dj, 05 ago 2010) | 2 lines

Convert line endings to Unix format

------------------------------------------------------------------------
r1159223 | lueck | 2010-08-04 20:39:03 +0100 (dc, 04 ago 2010) | 1 line

backport fixes from trunk
------------------------------------------------------------------------
