2007-05-14 17:05 +0000 [r664696]  zwabel

	* branches/KDE/3.5/kdevelop/lib/cppparser/driver.cpp,
	  branches/KDE/3.5/kdevelop/lib/cppparser/driver.h: Remove the
	  special mutex from the driver, that was needed for debugging
	  multithreading-crashes. Those crashes are fixed, so it's not
	  needed any more.

2007-05-15 11:04 +0000 [r664961]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/cppsupportpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletypenamespace.cpp:
	  2 little bug-fixes: 1. Allow code-completion in non-project files
	  again. This is not perfect, because include-paths can not be
	  resolved correctly for independent headers. However this is
	  better than nothing. 2. Prefer items in the code-model(from
	  project) before items in the catalog while completion,
	  navigation, etc.

2007-05-15 18:21 +0000 [r665064]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/cppsupportpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppsupportpart.h: Stop
	  the UIBlockTester-thread softly instead of calling terminate().
	  This hopefully fixes the new incarnation of bug 83352

2007-05-15 19:01 +0000 [r665073]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makeactionfilter.cpp:
	  Fix for Sun Compiler, patch from Paul Fee and finally closing the
	  bugreport BUG:132601

2007-05-17 21:57 +0000 [r665786]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/cppsupportpart.cpp: try
	  to fix bug 145563. CCBUG: 145563

2007-05-18 12:24 +0000 [r665952]  zwabel

	* branches/KDE/3.5/kdevelop/lib/interfaces/codemodel.cpp: When
	  updating the code-model, always respect changed permissions.
	  CCBUG: 145619

2007-05-18 23:28 +0000 [r666160]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp:
	  Fix saving of libraries, includes and targetdeps to not remove
	  all values first. This should help not messing up users .pro
	  files by not always creating a new variable at the end of the
	  .pro file. CCBUG:145639

2007-05-23 00:51 +0000 [r667515]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/custommakeconfigwidget.cpp:
	  The spinbox doesn't allow 0-value, thus the check after setting
	  its value always evaluates to true, fix that. BUG:129568

2007-05-25 01:15 +0000 [r668071]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp:
	  Fix several issues with the url requesters, they should now
	  behave as expected. Also includes fixes from Bernd Buschinski
	  BUG:145708 CCMAIL:b.buschinski@web.de

2007-05-25 01:26 +0000 [r668073]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makeactionfilter.cpp:
	  Use file extension for finding source files that are compiled,
	  patch from Heiko Gerdau, thanks. BUG:145687

2007-05-29 15:44 +0000 [r669527]  neundorf

	* branches/KDE/3.5/kdevelop/parts/outputviews/compileerrorfilter.cpp:
	  -allow spaces in the filename, makes the filename detection work
	  for me with spaces in the path Alex

2007-05-29 16:01 +0000 [r669534]  apaku

	* branches/KDE/3.5/kdevelop/vcs/perforce/perforcepart.cpp,
	  branches/KDE/3.5/kdevelop/vcs/perforce/perforcepart.h: Apply
	  Patch from Paul Fee to let p4 create a unified diff. @Paul:
	  Sorry, didn't see that you added a patch before sending my first
	  reply.. BUG:146114

2007-05-29 18:06 +0000 [r669570]  mueller

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdevelop/vcs/subversion/configure.in.in: trying
	  to get subversion autodetection work again

2007-05-29 22:44 +0000 [r669650]  mueller

	* branches/KDE/3.5/kdevelop/languages/cpp/includepathresolver.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/gdbcontroller.cpp,
	  branches/KDE/3.5/kdevelop/lib/astyle/astyle.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppcodecompletion.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletypenamespace.cpp,
	  branches/KDE/3.5/kdevelop/lib/antlr/antlr/CharScanner.hpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppcodecompletion.h,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/scope.cpp: the usual
	  "daily unbreak compilation"

2007-05-30 19:20 +0000 [r669913]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp:
	  Make Release&Debug an non-exclusive option, the QM now allows to
	  specify which build is the default target when using
	  Release&Debug build. BUG:145711
	  CCMAIL:kdevelop-devel@kdevelop.org

2007-05-30 23:07 +0000 [r669984]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_lex.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.yy,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.ll:
	  Fix parsing #foo \ lines

2007-06-02 18:56 +0000 [r670868]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makeactionfilter.cpp:
	  Revert my last commit, it breaks simple gcc lines (somehow the
	  DEBUG define that my configure gets doesn't work). CCBUG:145687

2007-06-02 21:14 +0000 [r670887]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp:
	  Always return the full list of sources, this avoids having to
	  stat the whole project tree but the list of sources will contain
	  directories and not just files. So far I haven't seen any
	  problems in other plugins, thus I'm committing. CCBUG: 146144

2007-06-02 22:25 +0000 [r670902]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.yy,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.cpp:
	  Fix parsing multiline values that don't start with whitespace

2007-06-03 15:31 +0000 [r671036]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/simpletypefunction.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletypenamespace.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletype.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletypecatalog.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppnewclassdlg.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/simpletype.h: Fix
	  compilation with gcc 4.2 BUG:146198

2007-06-03 15:40 +0000 [r671038]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makeactionfilter.cpp:
	  Fix the patch, a whitespace got lost because the patch was
	  provided inline. BUG:145687

2007-06-04 12:24 +0000 [r671292]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/kdevdriver.cpp: Use the
	  build-directory from the build-manager in the include-path
	  resolver. This should make the include-path resolution work with
	  out-of-source builds

2007-06-10 17:18 +0000 [r673615]  dukjuahn

	* branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewwidget.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/subversion_core.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/subversion_core.h,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.h: Fix the bug
	  that diff to previous context menu at logview widget couldn't
	  show difference for copied/moved/renamed entries.

2007-06-12 19:20 +0000 [r674650]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.cpp:
	  Use a standard folder instead of our home-grown icon. This looks
	  better and is consistent with other managers

2007-06-26 09:11 +0000 [r680459]  aclu

	* branches/KDE/3.5/kdevelop/languages/ruby/doc/ruby_bugs.toc: fix
	  url

2007-06-26 13:01 +0000 [r680520]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.yy,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.hpp:
	  Allow COMMENT_CONT as statement BUG:147194

2007-06-27 16:27 +0000 [r680978]  apaku

	* branches/KDE/3.5/kdevelop/vcs/cvsservice/cvspartimpl.cpp: add
	  some more verbose output when adding files

2007-06-27 19:23 +0000 [r681031]  pley

	* branches/KDE/3.5/kdevelop/vcs/cvsservice/cvspartimpl.cpp: Use
	  project dir instead of kdevelop's working dir to look for "CVS"
	  directory.

2007-06-28 12:18 +0000 [r681279]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/gdbcontroller.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/dbgpsdlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/variablewidget.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerpart.cpp:
	  use warningYesNo instead with a do-no-show-again checkbox instead
	  of error-messages, so the workflow isn't destroyed when the
	  debugger starts bugging around BUG:147320

2007-06-28 12:44 +0000 [r681287]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/gdboutputwidget.cpp:
	  Set focus on gdb edit field after executing a user-supplied gdb
	  command. Thanks to Christopher Layne for the patch. BUG:141320

2007-07-02 11:21 +0000 [r682308]  webb

	* branches/KDE/3.5/kdevelop/lib/astyle/ASBeautifier.cpp,
	  branches/KDE/3.5/kdevelop/lib/astyle/astyle.h,
	  branches/KDE/3.5/kdevelop/lib/astyle/ASEnhancer.cpp,
	  branches/KDE/3.5/kdevelop/lib/astyle/ASFormatter.cpp,
	  branches/KDE/3.5/kdevelop/lib/astyle/ASResource.cpp: Update to
	  Astyle 1.21 Fixes: BUG: 138699 BUG: 69106

2007-07-04 11:51 +0000 [r683216]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp:
	  Empty QStrings in filelists obviously cause problems, make sure
	  we never emit a signal with an empty QString in a list and we
	  ignore empty lines in the .filelist file. Thanks to Rob L. for
	  investigating and finding the problem. BUG:147029

2007-07-13 18:08 +0000 [r687477]  vprus

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerpart.cpp:
	  When debugger first stops, show framestack, not gdb console.
	  Patch from Dave Baker * debuggerpart.cpp (slotRun_part2): The
	  change proper.

2007-07-13 18:34 +0000 [r687489]  vprus

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/gdboutputwidget.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/gdboutputwidget.h:
	  When using "Copy All" in gdb console, do not copy markup tags. *
	  gdboutputwidget.h (GDBOutputWidget): New members
	  userCommandsRaw_, allCommandsRaw_. * gdboutputwidget.cpp
	  (newStdoutLine): Append to the new members. (slotReceivedStderr):
	  Store colored line in non-raw lists. Append to raw lists.
	  (copyAll): Construct the text to copy-paste from raw lists.

2007-07-15 08:23 +0000 [r688157]  dukjuahn

	* branches/KDE/3.5/kdevelop/vcs/subversion/subversion_part.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewwidget.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/subversion_core.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewoptiondlgbase.ui,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewwidget.h,
	  branches/KDE/3.5/kdevelop/vcs/subversion/subversion_core.h,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.h: Fix bug where
	  logview fails on moved/deleted revisions. There was no need to
	  fetch repository URL from wc path. Just give wc path to svn-api
	  and that's all.

2007-07-15 10:10 +0000 [r688187]  dukjuahn

	* branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewoptiondlgbase.ui:
	  Fix dialog cation. It's definitely wrong text and such text
	  shouldn't appear.

2007-07-19 20:18 +0000 [r690005]  gianni

	* branches/KDE/3.5/kdevelop/vcs/subversion/svn_logviewoptiondlgbase.ui:
	  fixuifiles *.ui

2007-07-23 08:39 +0000 [r691261]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Do not include .ui in the generated header file name, Qt3 uses
	  foo.h. BUG:148131

2007-07-25 19:32 +0000 [r692528]  dukjuahn

	* branches/KDE/3.5/kdevelop/vcs/subversion/subversion_part.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.cpp: Just pass
	  local url into svn C-api. Don't need to artificially fetch
	  repository URL from working copy. Doing this is errornous. Also
	  change default revision to BASE for blame operation. Specifying
	  HEAD get errors when requested file is not present in HEAD.

2007-07-26 13:26 +0000 [r692871-692870]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.yy,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_yacc.hpp:
	  Seems I forgot to commit this.

	* branches/KDE/3.5/kdevelop/buildtools/autotools/autosubprojectview.cpp:
	  Add standard file actions to autotools project view

2007-07-26 14:18 +0000 [r692899]  apaku

	* branches/KDE/3.5/kdevelop/parts/astyle/kdevpart_astyle.rc: Add
	  toolbar for the two astyle actions.

2007-07-26 16:16 +0000 [r692940]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.h,
	  branches/KDE/3.5/kdevelop/parts/fileview/stdfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/vcsfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp: Use
	  QMap instead of QList, I didn't think of that to get a set-like
	  storage structure. Patch from opal@scssoft.com BUG: 148229

2007-07-27 14:03 +0000 [r693217]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Don't crash if we have a null qstring.

2007-07-27 15:11 +0000 [r693226]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Ooops, this kdDebug shouldn't have been comitted.

2007-07-27 15:36 +0000 [r693236]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.h,
	  branches/KDE/3.5/kdevelop/parts/fileview/stdfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/vcsfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Revert the change from list to map as it breaks the automatic
	  addition of newly created files to the filetree. I don't know why
	  that breaks (as it should be handled by KFileTreeView from
	  kdelibs), but I don't have the time to investigate. Instead I
	  changed all contains() calls to findIndex, contains always
	  iterates the whole list, because it does actually a count(),
	  findIndex should speed things up considerably. If it doesn't
	  speed up enough, please re-open the bugreport and provide a patch
	  that doesn't break the automatic addition of new files to the
	  tree CCBUG: 148229

2007-07-28 09:42 +0000 [r693534]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  isEmpty is better than checking against QString::null

2007-07-28 12:30 +0000 [r693574]  zwabel

	* branches/KDE/3.5/kdevelop/languages/cpp/includepathresolver.cpp:
	  initialize tv_usec when touching files. Should fix some problems
	  with the include-path resolver.

2007-07-28 12:31 +0000 [r693576-693575]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Ask user wether already running application should be stopped
	  before starting another. Thanks to Thomas Hasart for the Patch
	  CCMAIL:thasart@gmx.de

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Sorry, I missed that this adds new strings, reverting. Thomas,
	  new strings are not allowed anymore in KDE3, so this can't be
	  comitted. CCMAIL: thasart@gmx.de

2007-07-28 15:45 +0000 [r693655-693652]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Damn, svnrevertlast needs an svn up before, reverted the wrong
	  change, so revert the revert.

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Really revert the change from Thomas, a fixed one will come in a
	  second

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp:
	  Apply updated patch from Thomas Hasart, this uses existing
	  strings from the autotools part and thus doesn't break the
	  freeze. Its also not really a feature but rather a bugfix,
	  because before one could start an app multiple times and always
	  lost the output from the earlier runs. CCMAIL:thasart@gmx.de

2007-07-28 20:26 +0000 [r693726]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.h,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/scope.cpp: Fix adding
	  of images to qt3 projects, along the way allow qt4 projects to
	  have IMAGES as well as qmake from qt4 handles that as well.
	  BUG:148287

2007-07-28 20:30 +0000 [r693729]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.cpp:
	  Don't sort child items as the order in the .pro file needs to be
	  reflected in the ui. Patch from Thomas Hasart
	  CCMAIL:thasart@gmx.de

2007-07-30 01:01 +0000 [r694087]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.h:
	  Remove the dirwatcher, it caused more trouble than its worth.
	  BUG:146144

2007-07-30 08:16 +0000 [r694176]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.h,
	  branches/KDE/3.5/kdevelop/parts/fileview/stdfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/vcsfiletreewidgetimpl.cpp,
	  branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Re-Apply the patch from 148229, it turns out that the problem I
	  saw here was only related to me not waiting long enough. It seems
	  there's a timer that starts the dirwatching in the filetree.
	  After waiting for a bit all touche'd files show up and new ones
	  show up instantly. CCBUG:148229

2007-07-30 15:25 +0000 [r694386]  dukjuahn

	* branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.cpp: Fix bug
	  where SSL certification failure message shows up continually,
	  even though the user choose "save to disk" SSL server certificate
	  and the file is actually present on disk. The reason was wrong
	  failure bit specified by callback function.

2007-08-01 13:12 +0000 [r695144]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.h,
	  branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Speedup project loading by exiting the loop earlier. Patch from
	  opal@scssoft.com BUG:148426

2007-08-03 19:46 +0000 [r696091]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectpart.h,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectpart.cpp:
	  - Use qmake -recursive for Qt4 - Use projectname.pro as first
	  option when searching the right .pro file Patch from Thomas
	  Hasart CCMAIL:thasart@gmx.de

2007-08-04 22:28 +0000 [r696466]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectpart.cpp:
	  Fix the build, I could swear I built before I comitted this
	  change. Sorry everybody.

2007-08-05 22:55 +0000 [r696811]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/autotools/autoprojectpart.cpp:
	  make some strings translateable. Patch from Stefan Johach.
	  BUG:148518

2007-08-05 23:18 +0000 [r696816]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/autotools/autoprojectpart.cpp:
	  Missed some typo's in the i18n fixes, so fix them before
	  translators notice.

2007-08-06 10:30 +0000 [r696937]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/custommanagerwidget.h,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/custommanagerwidget.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.h:
	  Don't allow to add absolute paths and paths outside the project
	  directory. This is not supported.

2007-08-19 20:24 +0000 [r701963]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectpart.cpp:
	  add needed whitespace after the -recursive option Patch by Thomas
	  Hasart CCMAIL:thasart@gmx.de

2007-08-29 18:20 +0000 [r706224]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.h:
	  s/QStringList/QSet/ for storing project file list (QSet doesn't
	  exist so we're utilizing QMap). Patch from Ondrej Karny
	  <opal@scssoft.com>, Thanks and keep these comming :) BUG:148678

2007-09-01 21:06 +0000 [r707429]  sansome

	* branches/KDE/3.5/kdevelop/parts/appwizard/importdlg.cpp: BUG:
	  149448 When importing a project, ensure that the suggested
	  project name doesn't contain invalid characters (like '-')

2007-09-20 21:32 +0000 [r714911]  mueller

	* branches/KDE/3.5/kdevelop/lib/astyle/astyle.h: fix compile

2007-10-03 14:24 +0000 [r720701]  okellogg

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui:
	  Fix typo. CCMAIL:mattr@kde.org

2007-10-03 18:09 +0000 [r720785]  okellogg

	* branches/KDE/3.5/kdevelop/languages/cpp/includepathresolver.cpp:
	  add space before "Output:"

2007-10-07 14:39 +0000 [r722530]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakesimple/qmakesimple.kdevtemplate,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4view_base.ui,
	  branches/KDE/3.5/kdevelop/vcs/subversion/commitdlg.h (added),
	  branches/KDE/3.5/kdevelop/parts/outputviews/compileerrorfilter.cpp,
	  branches/KDE/3.5/kdevelop/parts/classview/classviewpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/configure.in,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/automakeempty-Makefile.cvs,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.cpp,
	  branches/KDE/3.5/kdevelop/parts/quickopen/kdevquickopen.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake.ll,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kde4appui.rc,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeapp/qmakeapp.kdevtemplate,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/scope.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty/app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/example.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/qtopia4app.kdevtemplate,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4view.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kde4app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/cpp/ccconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/cppsupportpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.h,
	  branches/KDE/3.5/kdevelop/parts/classview/digraphview.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/main.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/automakeempty.kdevtemplate,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevquickopen.h
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty/qmakeempty.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty/makefileempty-Makefile,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/example.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kde4app.kdevtemplate,
	  branches/KDE/3.5/kdevelop/buildtools/script/kdevscriptproject.rc,
	  branches/KDE/3.5/kdevelop/buildtools/script/scriptprojectpart.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty/qmakeempty.kdevtemplate,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4.kcfg,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/example.html,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/choosesubprojectdlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4view.cpp,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/pdb/docpdbplugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/examplebase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakescopeitem.h,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/scope.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty
	  (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/automakeempty.png,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/Makefile,
	  branches/KDE/3.5/kdevelop/vcs/subversion/commitdlg.ui (removed),
	  branches/KDE/3.5/kdevelop/buildtools/script/scriptprojectpart.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kde4app.kdevelop.filelist,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopquickopen.desktop
	  (added),
	  branches/KDE/3.5/kdevelop/buildtools/lib/parsers/qmake/qmake_lex.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/settings.kcfgc,
	  branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdevelop/parts/quickopen/quickopen_part.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/kdevsvnd.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/app.kdevelop,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerconfigwidgetbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty/makefileempty.png,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty/qmakeempty.png,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/prefs_base.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/automakeempty-Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cpp.appwizard,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty/Makefile.am,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakeoptionswidget.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeempty/qmakeempty.pro,
	  branches/KDE/3.5/kdevelop/vcs/subversion/commitdlgbase.ui
	  (added),
	  branches/KDE/3.5/kdevelop/languages/ruby/kdevrubysupport.rc,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/automakeempty/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/classview/hierarchydlg.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/Example.png,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/trollprojectwidget.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/Makefile.am,
	  branches/KDE/3.5/kdevelop/vcs/subversion/commitdlg.cpp (added),
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/CMakeLists.txt,
	  branches/KDE/3.5/kdevelop/src/simplemainwindow.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/quickopen/quickopen_part.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/makefileempty/makefileempty.kdevtemplate,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/qmakeoptionswidgetbase.ui,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/doxygen/docdoxygenplugin.cpp,
	  branches/KDE/3.5/kdevelop/vcs/subversion/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4.desktop,
	  branches/KDE/3.5/kdevelop/parts/classview/classviewpart.h,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/README,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerconfigwidget.cpp,
	  branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/qtopia4app.png,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/main.cpp,
	  branches/KDE/3.5/kdevelop/parts/classview/hierarchydlg.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/example.cpp,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/app.pro,
	  branches/KDE/3.5/kdevelop/parts/classview/digraphview.h,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kde4app.png:
	  Merge the changes from the kdevelop/3.5 branch for the KDE 3.5.8
	  release. Leaving the version number at 3.5.0 to make it easier
	  for people that used the 3.4.90 packages to upgrade to 3.5.0 (it
	  has at least 2 bugfixes over 3.4.90)
	  CCMAIL:kdevelop-devel@kdevelop.org

2007-10-07 18:22 +0000 [r722624]  apaku

	* branches/KDE/3.5/kdevelop/src/simplemainwindow.cpp: Merge the
	  latest change from the branch. This again disables the shortcut
	  for first/last window because I implemented that in the wrong
	  way.

2007-10-08 11:06 +0000 [r722974]  coolo

	* branches/KDE/3.5/kdevelop/kdevelop.lsm: updating lsm

