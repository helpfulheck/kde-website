------------------------------------------------------------------------
r1121126 | annma | 2010-05-01 00:56:57 +1200 (Sat, 01 May 2010) | 2 lines

fix epod for good by extracting the pic in a smarter way

------------------------------------------------------------------------
r1124345 | aseigo | 2010-05-09 10:30:08 +1200 (Sun, 09 May 2010) | 3 lines

don't use if it isn't created
CCBUG:236864

------------------------------------------------------------------------
r1126861 | scripty | 2010-05-15 14:02:18 +1200 (Sat, 15 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127169 | astromme | 2010-05-16 04:57:24 +1200 (Sun, 16 May 2010) | 4 lines

Backport 1127165 to 4.4.x

CCBUG: 232945

------------------------------------------------------------------------
r1128186 | annma | 2010-05-19 03:44:37 +1200 (Wed, 19 May 2010) | 4 lines

Tested, thanks for the patch!
Will also commit to trunk.
CCBUG=238048

------------------------------------------------------------------------
r1129060 | gokcen | 2010-05-21 19:49:53 +1200 (Fri, 21 May 2010) | 5 lines

Some distros use different prefixes for icon install directories. Correct way is to use kde4_install_icons macro mentioned here[1]. You should also change icon names to use this macro. This commit is a workaround for this.

[1] http://techbase.kde.org/Development/CMake/Addons_for_KDE#Macros


------------------------------------------------------------------------
r1129353 | scripty | 2010-05-22 13:55:24 +1200 (Sat, 22 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130303 | scripty | 2010-05-25 14:34:58 +1200 (Tue, 25 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
