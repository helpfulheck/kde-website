------------------------------------------------------------------------
r1042925 | scripty | 2009-10-31 04:11:47 +0000 (Sat, 31 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1043183 | darioandres | 2009-10-31 23:48:31 +0000 (Sat, 31 Oct 2009) | 15 lines

Backport to 4.3 of:
SVN commit 1043127 by darioandres
SVN commit 1043176 by darioandres

- Fix the Speed and ETA of the file transfers not being shown in the Plasma
notifications
  - The applicationjobs dataengine now should notify about speed changes
  * The ETA and Speed are only shown if the job is running
  * It should not affect anything else, but it may need more testing...

If this is tested properly, should it be included on the already tagged 4.3.3 builds ?

BUG: 210081
CCMAIL: aseigo@kde.org

------------------------------------------------------------------------
r1043210 | scripty | 2009-11-01 04:08:24 +0000 (Sun, 01 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1043634 | scripty | 2009-11-02 04:24:57 +0000 (Mon, 02 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1043869 | dfaure | 2009-11-02 14:56:27 +0000 (Mon, 02 Nov 2009) | 5 lines

Backport r1019964:
Respect kioslave contract: send mimetype before data, as TransferJob::slotData now warns about.
+ fix mimetype determination when opening a thumbnail url directly in konqueror (at least the
code was obviously missing an assignment there).

------------------------------------------------------------------------
r1044025 | darioandres | 2009-11-02 21:14:38 +0000 (Mon, 02 Nov 2009) | 3 lines

- Add the plasma-netbook mapping as Kubuntu distributes the netbook UI with KDE4.3


------------------------------------------------------------------------
r1044097 | djarvie | 2009-11-03 01:26:26 +0000 (Tue, 03 Nov 2009) | 2 lines

Bug 212237: Allow use of zoneinfo time zones which are not in zone.tab.

------------------------------------------------------------------------
r1044116 | scripty | 2009-11-03 04:07:28 +0000 (Tue, 03 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1044216 | lmurray | 2009-11-03 09:50:16 +0000 (Tue, 03 Nov 2009) | 3 lines

Allow the logout blur effect to fully animate when the logout dialog is
closed.

------------------------------------------------------------------------
r1044420 | woebbe | 2009-11-03 18:17:37 +0000 (Tue, 03 Nov 2009) | 4 lines

compile

looks like broken backport.

------------------------------------------------------------------------
r1044473 | aseigo | 2009-11-03 21:09:53 +0000 (Tue, 03 Nov 2009) | 3 lines

slightly improved and backported fix for:
CCBUG:212913

------------------------------------------------------------------------
r1044550 | djarvie | 2009-11-04 01:01:46 +0000 (Wed, 04 Nov 2009) | 2 lines

Fix changes in system time zone not being detected.

------------------------------------------------------------------------
r1044964 | scripty | 2009-11-05 04:19:26 +0000 (Thu, 05 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1045431 | scripty | 2009-11-06 03:52:42 +0000 (Fri, 06 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1046357 | graesslin | 2009-11-08 13:35:18 +0000 (Sun, 08 Nov 2009) | 3 lines

Backport rev 1046354: Repaint the complete boxswitch frame on damage events.
CCBUG: 213627

------------------------------------------------------------------------
r1046542 | scripty | 2009-11-09 03:58:25 +0000 (Mon, 09 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1046970 | scripty | 2009-11-10 04:03:25 +0000 (Tue, 10 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1047334 | dfaure | 2009-11-11 01:02:26 +0000 (Wed, 11 Nov 2009) | 2 lines

Backport: Hide statusbar for toggleviews (like the sidebar) when loading from profile, just like konqguiclients.cpp:201 does at runtime. (BUG 100373)

------------------------------------------------------------------------
r1047616 | ogoffart | 2009-11-11 15:56:34 +0000 (Wed, 11 Nov 2009) | 8 lines

Fix rubber band on a graphicsview.

As we paint outside the mask, it would leads to ugly artefacts.
Better not to give a mask.

This has already been fixed in trunk


------------------------------------------------------------------------
r1047687 | mart | 2009-11-11 20:13:48 +0000 (Wed, 11 Nov 2009) | 2 lines

backport fix to bug 214169

------------------------------------------------------------------------
r1048131 | dfaure | 2009-11-12 19:39:01 +0000 (Thu, 12 Nov 2009) | 3 lines

backport r1047011: Make the initial preference higher than gwenview...
Thanks Dominik for the reminder, I didn't know the bug was in branch too.

------------------------------------------------------------------------
r1048881 | scripty | 2009-11-14 04:09:27 +0000 (Sat, 14 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1049316 | darioandres | 2009-11-15 01:37:17 +0000 (Sun, 15 Nov 2009) | 3 lines

- Reduce the ammount of not really useful reports (stricter rules to enable bugzilla reporting)


------------------------------------------------------------------------
r1049768 | lueck | 2009-11-15 19:51:43 +0000 (Sun, 15 Nov 2009) | 1 line

backport scrennshots to replace the kde3 ones
------------------------------------------------------------------------
r1049824 | darioandres | 2009-11-15 23:18:51 +0000 (Sun, 15 Nov 2009) | 3 lines

- Improve the bugzilla query for duplicates to get more accurate results


------------------------------------------------------------------------
r1049861 | darioandres | 2009-11-16 00:21:28 +0000 (Mon, 16 Nov 2009) | 3 lines

- Do not search in the "kde" product, as with the mappings file that is not useful anymore


------------------------------------------------------------------------
r1049898 | scripty | 2009-11-16 04:12:53 +0000 (Mon, 16 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1050288 | scripty | 2009-11-17 03:56:46 +0000 (Tue, 17 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1051178 | darioandres | 2009-11-18 23:20:25 +0000 (Wed, 18 Nov 2009) | 3 lines

- Replace "crap" with "garbage" in the debug output


------------------------------------------------------------------------
r1051238 | scripty | 2009-11-19 04:15:11 +0000 (Thu, 19 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1051768 | scripty | 2009-11-20 04:09:07 +0000 (Fri, 20 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1052824 | mmrozowski | 2009-11-22 15:22:59 +0000 (Sun, 22 Nov 2009) | 1 line

Backport from r1033402 to fix klipper empty actions bug #209737. Thanks for Esben for the patch.
------------------------------------------------------------------------
r1054155 | jacopods | 2009-11-25 17:36:35 +0000 (Wed, 25 Nov 2009) | 3 lines

backport fixes for bug 199735
CCBUG: 199735

------------------------------------------------------------------------
r1054480 | scripty | 2009-11-26 04:10:42 +0000 (Thu, 26 Nov 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1055146 | mueller | 2009-11-27 12:39:11 +0000 (Fri, 27 Nov 2009) | 2 lines

bump version

------------------------------------------------------------------------
