------------------------------------------------------------------------
r1226956 | cguthrie | 2011-04-03 23:59:19 +1200 (Sun, 03 Apr 2011) | 5 lines

Disable View management (close-tab and new-tab) when using PulseAudio.

Fix 'Can't add new tab when I remove all tabs'
CCBUG: 262879
(merges r1224440 from trunk)
------------------------------------------------------------------------
r1226957 | cguthrie | 2011-04-04 00:00:26 +1200 (Mon, 04 Apr 2011) | 3 lines

cleanup

(merges r1224441 from trunk)
------------------------------------------------------------------------
r1226958 | cguthrie | 2011-04-04 00:02:12 +1200 (Mon, 04 Apr 2011) | 7 lines

kmix: Various (mostly PulseAudio related) fixes from trunk.

Merges r1225537 through r1226957.

CCBUG: 237239
CCBUG: 265317
CCBUG: 264835
------------------------------------------------------------------------
r1227026 | lvsouza | 2011-04-04 14:32:20 +1200 (Mon, 04 Apr 2011) | 7 lines

Backporting r1227025 to 4.6. branch:
Fix memory leak when DialogSelectMaster dialogs are closed.
Thanks Felix Berger for this patch.

BUG: 260926
FIXED-IN: 4.6.2

------------------------------------------------------------------------
r1227407 | rdieter | 2011-04-09 04:41:38 +1200 (Sat, 09 Apr 2011) | 3 lines

backport r1227406
use proper PC_AV... vars as provided by FindFFMPEG.cmake

------------------------------------------------------------------------
r1227805 | scripty | 2011-04-13 02:51:50 +1200 (Wed, 13 Apr 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1228552 | mpyne | 2011-04-20 14:24:06 +1200 (Wed, 20 Apr 2011) | 4 lines

Backport use of correct KFile mode in file renamer to 4.6.3.

Patch contributed by Γιώργος Κυλάφας, thanks!.

------------------------------------------------------------------------
r1228556 | mpyne | 2011-04-20 15:12:39 +1200 (Wed, 20 Apr 2011) | 5 lines

Backport fix for non-en_US file renamer categories to 4.6.3.

This backports a fix for an issue discovered and diagnosed by Giorgos Kylafas
regarding adding categories in the File Renamer options dialog to KDE SC 4.6.3.

------------------------------------------------------------------------
r1229047 | mpyne | 2011-04-25 14:49:18 +1200 (Mon, 25 Apr 2011) | 25 lines

Backport proper tag editor updating to 4.6.3.

Full details are in the commit message for the relevant trunk commit
(r1229044), but this commit essentially properly updates the tag editor on
collection changes so that the displayed title, artist, and genre still
reflect the selected tracks. A bug prior to this fix reset those fields to the
first value in their respective list of possibilities.

This effect does not play well with file renaming, so I believe this also
fixes 271054 (or at least I cannot reproduce it) and the possibly-duplicate
bug 264092 (which would be a dup of 271054 if Michał's genre list had an
empty genre as its first entry). I also cannot reproduce 264092 with this fix,
so please reopen if I've closed the bugs too hastily.

And before I forget: The issue was identified and fixed by Giorgos Kylafas, so
send your thanks (if any) his way. ;)

In case it isn't clear, the 4.6.3 refers to the KDE compilation release, not
the internal JuK version.

CCMAIL:gekylafas@gmail.com
BUG:264092
BUG:271054
FIXED-IN:4.6.3

------------------------------------------------------------------------
r1229136 | mpyne | 2011-04-26 13:00:37 +1200 (Tue, 26 Apr 2011) | 9 lines

Backport proper i18n call to 4.6.3.

(No strings are affected, this simply makes the appropriate call to either
i18nc or i18n depending on if context is available).

Issue noted and fixed by Giorgos Kylafas.

CCMAIL:gekylafas@gmail.com

------------------------------------------------------------------------
