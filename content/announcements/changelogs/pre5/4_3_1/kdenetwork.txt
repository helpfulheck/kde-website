------------------------------------------------------------------------
r1004391 | scripty | 2009-07-30 03:12:35 +0000 (Thu, 30 Jul 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1005437 | scripty | 2009-08-01 03:06:28 +0000 (Sat, 01 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1006980 | uwolfer | 2009-08-04 20:04:12 +0000 (Tue, 04 Aug 2009) | 9 lines

Backport:
SVN commit 1006958 by uwolfer:

KStandardAction::fullScreen is the wrong thing for KRDC; we do a custom handling of fullscreen windows by our self. This reverts r977345.
Fixes wrong behavior and crashes.

But: adopt style more to KToggleFullScreenAction: shortcut and spelling: 'Fullscreen' -> 'Full Screen'.

CCBUG:200915
------------------------------------------------------------------------
r1008521 | dario | 2009-08-07 17:53:49 +0000 (Fri, 07 Aug 2009) | 3 lines

Fix for bug 202778: KGet Crashes when right clicking on an item.


------------------------------------------------------------------------
r1008522 | dario | 2009-08-07 17:57:26 +0000 (Fri, 07 Aug 2009) | 2 lines

Backport fix for bug: 202175

------------------------------------------------------------------------
r1008759 | dario | 2009-08-08 10:18:41 +0000 (Sat, 08 Aug 2009) | 3 lines

Re-enabling KonqMenuActions code


------------------------------------------------------------------------
r1008766 | dario | 2009-08-08 10:49:35 +0000 (Sat, 08 Aug 2009) | 2 lines

Backport fix for bug 201485 (tooltip not in place)

------------------------------------------------------------------------
r1008896 | lappelhans | 2009-08-08 15:45:54 +0000 (Sat, 08 Aug 2009) | 5 lines

Backport of rev 1008844:
First trigger the postDeleteEvent() and then delete the transfer from 
the list.
Fixes a crash inside the Model.

------------------------------------------------------------------------
r1009358 | lappelhans | 2009-08-09 19:51:29 +0000 (Sun, 09 Aug 2009) | 6 lines

Backport of rev 1009351:
Make dragndrop inside the list work correctly!
Also save the transfer to the history inside its destructor and not when 
removing it from a group.
CCBUG:202469

------------------------------------------------------------------------
r1009419 | lappelhans | 2009-08-09 22:51:38 +0000 (Sun, 09 Aug 2009) | 5 lines

backport of rev 1009411 by mfuchs:
Drag and dropping multiple transfers keeps their order.
CCBUG:202469
CCMAIL:mat69@gmx.net

------------------------------------------------------------------------
r1009453 | scripty | 2009-08-10 03:10:52 +0000 (Mon, 10 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009587 | pali | 2009-08-10 11:57:35 +0000 (Mon, 10 Aug 2009) | 4 lines

Make all SkypeOut contacts still offline

BUG: 200977

------------------------------------------------------------------------
r1009703 | dario | 2009-08-10 17:00:11 +0000 (Mon, 10 Aug 2009) | 3 lines

Backport potential fix for bugs: 194427, 194544, 198862, 202669, 202819


------------------------------------------------------------------------
r1011061 | uwolfer | 2009-08-13 20:54:07 +0000 (Thu, 13 Aug 2009) | 1 line

Backport:
------------------------------------------------------------------------
r1011560 | scripty | 2009-08-15 03:05:30 +0000 (Sat, 15 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1012177 | scripty | 2009-08-17 03:02:45 +0000 (Mon, 17 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1013033 | pali | 2009-08-18 16:43:04 +0000 (Tue, 18 Aug 2009) | 4 lines

Dont move all metacontacts to root group after init skype protocol

BUG: 202863

------------------------------------------------------------------------
r1013041 | pali | 2009-08-18 17:28:32 +0000 (Tue, 18 Aug 2009) | 8 lines

Move definition SKYPE_DEBUG_GLOBAL from cpp all source to CMakeLists.txt
Fixed quit skype, if user use wrapper (skype.real)
Fixed register/unregister skype client on dbus
Fixed sending file
Fixed show skype nick and display name (after init skype protocol too)
Fixed getting full name of skype contact
Fixed skypebuttons

------------------------------------------------------------------------
r1013295 | dario | 2009-08-19 13:31:12 +0000 (Wed, 19 Aug 2009) | 3 lines

Backport fix for wrong filename suggestion bugs


------------------------------------------------------------------------
r1014225 | pino | 2009-08-21 23:24:18 +0000 (Fri, 21 Aug 2009) | 4 lines

backport SVN commit 1014219 by anagl:

This also applies to FreeBSD and systems with FreeBSD kernel.

------------------------------------------------------------------------
r1014372 | casanova | 2009-08-22 16:24:10 +0000 (Sat, 22 Aug 2009) | 7 lines

Backport commit 1014207.

Check if we have a proper manager and a proper view before closing it.

BUG:204526


------------------------------------------------------------------------
r1015641 | scripty | 2009-08-26 02:51:24 +0000 (Wed, 26 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015852 | murrant | 2009-08-26 13:01:18 +0000 (Wed, 26 Aug 2009) | 5 lines

Backport:
SVN commit 1015620 by murrant:

Quick fix for disconnect crash when fullscreen.

------------------------------------------------------------------------
r1015967 | lukas | 2009-08-26 17:22:03 +0000 (Wed, 26 Aug 2009) | 2 lines

missing i18n()

------------------------------------------------------------------------
