2008-01-31 16:04 +0000 [r769126]  tsutton

	* branches/KDE/4.0/kdeedu/marble/src/lib/CMakeLists.txt,
	  branches/KDE/4.0/kdeedu/marble/CMakeLists.txt: Some tweaks for
	  building static under OSX and enable static cmake option for OSX
	  as well as windows.

2008-01-31 16:38 +0000 [r769136]  tsutton

	* branches/KDE/4.0/kdeedu/marble/CMakeLists.txt: Added more libs
	  needed for OSX when doing a static compile

2008-01-31 17:11 +0000 [r769148]  tsutton

	* branches/KDE/4.0/kdeedu/marble/CMakeLists.txt: Cocoa libs are not
	  needed for mac static build

2008-02-01 20:35 +0000 [r769678]  lueck

	* branches/KDE/4.0/kdeedu/doc/kturtle/wrapping.png (removed),
	  branches/KDE/4.0/kdeedu/doc/kgeography/index.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/using-kturtle.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/programming-reference.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/glossary.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/translator-guide.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/getting-started.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/index.docbook: documentation
	  backport from trunk

2008-02-04 19:25 +0000 [r770935]  rahn

	* branches/KDE/4.0/kdeedu/marble/src/plugins/navigator/CMakeLists.txt:
	  - a minor patch (on fedora /usr/lib64 isn't a symbolic link on
	  /usr/lib for x86_64) by Chitlesh Goorah

2008-02-04 21:01 +0000 [r770969]  lueck

	* branches/KDE/4.0/kdeedu/doc/kturtle/print.png (removed): removed
	  obsolete screenshot

2008-02-05 15:03 +0000 [r771246]  lueck

	* branches/KDE/4.0/kdeedu/kturtle/src/interpreter/translator.cpp:
	  adapt example to documentation and trunk

2008-02-06 14:06 +0000 [r771604]  lueck

	* branches/KDE/4.0/kdeedu/doc/kturtle/using-kturtle.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/programming-reference.docbook,
	  branches/KDE/4.0/kdeedu/doc/kturtle/index.docbook: backport from
	  trunk

2008-02-06 21:07 +0000 [r771745]  aacid

	* branches/KDE/4.0/kdeedu/kgeography/data/flags/canada/manitoba.png:
	  backport r771732 **************** fix manitoba flag, all
	  triangles of the union jack must be blue

2008-02-06 21:16 +0000 [r771753]  aacid

	* branches/KDE/4.0/kdeedu/kgeography/data/canada.png: Backport
	  r771752 **************** Fix The Belcher Islands in Hudson Bay
	  are shown belonging to Quebec and Akimiski Island in James Bay is
	  shown belonging to Ontario when they belong to Nunavut

2008-02-07 10:21 +0000 [r771959]  rahn

	* branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/Messages.sh: 2008-02-07 Torsten
	  Rahn <rahn@kde.org> * Messages.sh: - Fix for untranslatable
	  string by Burkhard Lück

2008-02-09 14:27 +0000 [r772727]  cniehaus

	* branches/KDE/4.0/kdeedu/kalzium/src/molcalcwidgetbase.ui,
	  branches/KDE/4.0/kdeedu/kalzium/src/detailinfodlg.cpp,
	  branches/KDE/4.0/kdeedu/kalzium/src/kalzium.cpp,
	  branches/KDE/4.0/kdeedu/kalzium/src/detailedgraphicaloverview.cpp,
	  branches/KDE/4.0/kdeedu/kalzium/src/kalziumdataobject.cpp,
	  branches/KDE/4.0/kdeedu/kalzium/src/molcalcwidget.cpp: Backport
	  the four bugfixes 157355,157359,157466,157473

2008-02-11 07:52 +0000 [r773489]  mlaurent

	* branches/KDE/4.0/kdeedu/kalzium/src/tools/obconverter.cpp:
	  Backport: Open kalzium help page until a specific page was
	  created

2008-02-11 09:21 +0000 [r773524]  nielsslot

	* branches/KDE/4.0/kdeedu/kturtle/src/interpreter/executer.cpp:
	  Backported commit 773521 which fixed bug 157639.

2008-02-14 12:18 +0000 [r774921]  annma

	* branches/KDE/4.0/kdeedu/doc/klettres/index.docbook: backport of
	  Burkhard doc fix 774860

2008-02-14 12:22 +0000 [r774923]  annma

	* branches/KDE/4.0/kdeedu/klettres/src/klettres.cpp: backport of
	  shortcut fixes

2008-02-14 14:00 +0000 [r774963]  lueck

	* branches/KDE/4.0/kdeedu/doc/klettres/index.docbook: revert
	  backport, this is only allowed with approval of kde-i18n-doc
	  CCMAIL:annemarie.mahfouf@free.fr

2008-02-15 09:45 +0000 [r775249]  annma

	* branches/KDE/4.0/kdeedu/khangman/src/khangmanui.rc: be consistent
	  with Menu name, thanks to Burkhard Lück

2008-02-17 21:36 +0000 [r776347]  ingwa

	* branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.cpp,
	  branches/KDE/4.0/kdeedu/marble/ChangeLog,
	  branches/KDE/4.0/kdeedu/marble/src/lib/TextureColorizer.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/GlobeScanlineTextureMapper.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/MarbleWidget.h,
	  branches/KDE/4.0/kdeedu/marble/src/lib/PlaceMarkLayout.cpp,
	  branches/KDE/4.0/kdeedu/marble/src/lib/VectorMap.cpp: BUGFIX:
	  Remove crashes by removing potential overflows in calculation. We
	  think that overflow in integer calculations (squaring the radius)
	  has caused both errors and crashes, especially on windows for
	  some reason. So this patch removes all the cases I could find by
	  either extending the integer range by using qint64 or converting
	  to double, whichever makes most sense in each case.

2008-02-20 11:21 +0000 [r777290]  rahn

	* branches/KDE/4.0/kdeedu/marble/INSTALL: - Addendum from Dennis
	  Veach concerning Bug 157780: marble in kde 4.0.1 fails build with
	  gpsd

2008-02-27 17:07 +0000 [r779973]  kavol

	* branches/KDE/4.0/kdeedu/marble/data/placemarks/cities.txt,
	  branches/KDE/4.0/kdeedu/marble/data/credits_data.html,
	  branches/KDE/4.0/kdeedu/marble/data/placemarks/COPYRIGHT:
	  backport of the trunk fixes for the Czech cities: - fixed
	  spelling - updated data according to the latest published by CzSO
	  (2006-12-31) - added copyright notice

