------------------------------------------------------------------------
r1230820 | schwarzer | 2011-05-08 10:17:18 +1200 (Sun, 08 May 2011) | 12 lines

Backport of r1230818: Fix crash when index is -1.

The combobox OwnerCB is emptied at some point sending the
currentIndexChanged signal with the value -1. This -1 is
handed over here as index which is then used in a QList
-> bang.

Thanks Sebastian Kühn for digging into it.

CCBUG: 270353


------------------------------------------------------------------------
r1234346 | scripty | 2011-06-01 04:13:21 +1200 (Wed, 01 Jun 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
