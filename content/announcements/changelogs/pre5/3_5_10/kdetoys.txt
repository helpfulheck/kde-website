2008-06-29 15:59 +0000 [r825901-825900]  anagl

	* branches/KDE/3.5/kdetoys/amor/amor.desktop,
	  branches/KDE/3.5/kdetoys/kteatime/kteatime.desktop,
	  branches/KDE/3.5/kdetoys/kworldwatch/kworldclock.desktop,
	  branches/KDE/3.5/kdetoys/kodo/kodo.desktop: Desktop validation
	  fixes: remove deprecated entries for Encoding and fix Categories.

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: Add
	  Belarus stations to kweather

2008-06-29 22:53 +0000 [r826184]  anagl

	* branches/KDE/3.5/kdetoys/kmoon/kmoonapplet.desktop,
	  branches/KDE/3.5/kdetoys/kweather/weatherbar.desktop,
	  branches/KDE/3.5/kdetoys/kweather/kweatherreport.desktop,
	  branches/KDE/3.5/kdetoys/kweather/kcmweather.desktop,
	  branches/KDE/3.5/kdetoys/kweather/kweather.desktop,
	  branches/KDE/3.5/kdetoys/kweather/kcmweatherservice.desktop,
	  branches/KDE/3.5/kdetoys/kweather/kweatherservice.desktop,
	  branches/KDE/3.5/kdetoys/eyesapplet/eyesapplet.desktop,
	  branches/KDE/3.5/kdetoys/ktux/ktux.desktop,
	  branches/KDE/3.5/kdetoys/kweather/weatherbar_add.desktop,
	  branches/KDE/3.5/kdetoys/fifteenapplet/kfifteenapplet.desktop,
	  branches/KDE/3.5/kdetoys/kworldwatch/kwwapplet.desktop: Desktop
	  validation fixes: remove deprecated entries for Encoding.

2008-07-28 10:48 +0000 [r838616]  bminisini

	* branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/weatherbutton.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kweather.h,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: BUG: 146007 Fix
	  kweather transparency. Make the kweather button behave like other
	  kicker buttons.

2008-07-28 12:00 +0000 [r838649]  bminisini

	* branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kweather.h: kweather: Fix
	  source code indentation

2008-08-06 09:46 +0000 [r842941]  adridg

	* branches/KDE/3.5/kdetoys/kmoon/pics/moon14.png: Resize kmoon
	  image #14 from 100x100 to 82x82 like all the rest of them. BUG:
	  102446

2008-08-17 12:39 +0000 [r848334]  bminisini

	* branches/KDE/3.5/kdetoys/eyesapplet/eyes.cpp,
	  branches/KDE/3.5/kdetoys/eyesapplet/eyes.h: Eyes applet is now
	  antialiased.

2008-08-19 19:48 +0000 [r849606]  coolo

	* branches/KDE/3.5/kdetoys/kdetoys.lsm: update for 3.5.10

