---
title: Plasma 5.24.4 complete changelog
version: 5.24.4
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Fix build without QtQuick and QtX11Extras. [Commit.](http://commits.kde.org/breeze/a19e74f0c5ab69a997393ae03ebb7e013ba5f156) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Fix searchfield focus on touch launch. [Commit.](http://commits.kde.org/discover/3cb49fc1db7ee83ba7a70c8d012e2e683c8eb5b2) 
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Add RtL support. [Commit.](http://commits.kde.org/kdecoration/d6094ab116f1f8271f4d3e73b15cf4c1a6361df6) Fixes bug [#432390](https://bugs.kde.org/432390)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ KCM: Center Orientation label when there's no automatic options. [Commit.](http://commits.kde.org/kscreen/e8679b66ae726b07a96b048efde660f502fe1e7d) 
+ X11: align touchscreen to internal display. [Commit.](http://commits.kde.org/kscreen/579358f501ae978aa527a25eb3ef9dd42557db46) Fixes bug [#415683](https://bugs.kde.org/415683)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Outputchangeset: set default values for vrr policy and rgb range. [Commit.](http://commits.kde.org/kwayland-server/a8e66dd1360624f9fef016598435182d7faa635a) Fixes bug [#442520](https://bugs.kde.org/442520)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: attempt a modeset on output disabling. [Commit.](http://commits.kde.org/kwin/3e7223a9af4dec6ebb73a46eae8ba0aeb5a2d657) Fixes bug [#449878](https://bugs.kde.org/449878)
+ Plugins/screencast: Fix a glitch in cursor bitmap. [Commit.](http://commits.kde.org/kwin/8fb509f5f4384eb3bedc97d5b3e244d1b5666bb9) 
+ Backends/drm: set max bpc in DrmPipeline. [Commit.](http://commits.kde.org/kwin/6e41d7288a7032a2479d72287dfd8794d761eb87) See bug [#448220](https://bugs.kde.org/448220)
+ Backends/drm: fall back to legacy mode in virtual machines. [Commit.](http://commits.kde.org/kwin/9f2ed15be073d3c4cc3e133acecf8d586322b2bf) Fixes bug [#427060](https://bugs.kde.org/427060)
+ Backends/drm: only allow ARGB8888 as the cursor format. [Commit.](http://commits.kde.org/kwin/0c2682ce8d137cddfff5f56b8cbbb0ad595361f2) 
+ Effects/blur: Fix window flickering when the clip intersected with the current blur region. [Commit.](http://commits.kde.org/kwin/5186f411885b8d8e5bcc177ef888e63277a515ba) Fixes bug [#421135](https://bugs.kde.org/421135)
+ Screencast: better solution for missing context on cursor move. [Commit.](http://commits.kde.org/kwin/4852474ab1c3439b1fb05ba14954fb4ee966c741) 
+ Revert "screencast: make context current in tryEnqueue". [Commit.](http://commits.kde.org/kwin/0d0a4574aefe4b59a2dbded88b3cc5b1dc54bc73) 
+ Src/kcmkwin: fix botched indentation code for final checkbox. [Commit.](http://commits.kde.org/kwin/1c92fba7120b4f243224347510f99a90f8e97c32) 
+ Backends/drm: don't do direct scanout with software rotation. [Commit.](http://commits.kde.org/kwin/4774cbc072fafa547703110fa7ab940c0e8b1a7f) 
+ Screencast: make context current in tryEnqueue. [Commit.](http://commits.kde.org/kwin/2c75fd289f514f1c5d13d4701537714963f07f93) 
+ Effects/desktopgrid: don't forget to schedule repaints when timeline is running. [Commit.](http://commits.kde.org/kwin/770a701ee53dfeadd7b3054844d474628a723c4f) Fixes bug [#444678](https://bugs.kde.org/444678)
+ Effects/desktopgrid: register down gesture. [Commit.](http://commits.kde.org/kwin/de5d7a849bf02c3f45c995d3e6b0ff088b56ba83) Fixes bug [#444694](https://bugs.kde.org/444694)
+ Inputmethod: Do not reset when hiding. [Commit.](http://commits.kde.org/kwin/da28a57a7dd5ea0af3698bed3c1e7cb7def6819a) 
+ Inputmethod: Listen to text-input enablement changes when starting disabled. [Commit.](http://commits.kde.org/kwin/2333be1893cbb60d01018473b36e3a055c51c76a) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Kcms/componentchooser: unify combobox lengths. [Commit.](http://commits.kde.org/plasma-desktop/c3fa9734d59f22adc59a7eca9c46cfa5c48ebab1) Fixes bug [#451365](https://bugs.kde.org/451365)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Mmplugin: set all connections to not autoconnect on setMobileDataEnabled(false) (#182). [Commit.](http://commits.kde.org/plasma-mobile/e0fe9019fa4e227e584ca2933f5d4d34086cb268) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Kcms/users: Adjust for padding in overlay sheet. [Commit.](http://commits.kde.org/plasma-workspace/80b726435c0704770248f6638e382a7ca13a7029) Fixes bug [#451031](https://bugs.kde.org/451031)
+ [Media Controller] Explicitly set slider "to" before setting "value". [Commit.](http://commits.kde.org/plasma-workspace/c3610a8a2a9cccef7af5f3cc05d6fbf263139ea9) 
+ [Notifications] Fix implicit size propagation in SelectableLabel. [Commit.](http://commits.kde.org/plasma-workspace/654ce8451d3e130c3babddbcbebce0e2eac99c6e) 
+ Applets/kicker: Skip creating KService for non-desktop files or folders. [Commit.](http://commits.kde.org/plasma-workspace/f1e9fd1f832b129ebefc09802cf09af8f0714429) Fixes bug [#442970](https://bugs.kde.org/442970)
+ Fix sleep/suspend sometimes not working. [Commit.](http://commits.kde.org/plasma-workspace/913c80ebb47aa6756d4e1f5b5a82c8250387a41d) 
+ Kicker/actionlist: Ensure we parse the args for the jumplist actions. [Commit.](http://commits.kde.org/plasma-workspace/86a1aefab627ba0bb46fcc40259f0de2f3c11f74) Fixes bug [#451418](https://bugs.kde.org/451418)
+ Libtaskmanager: recompute active task when a task is removed. [Commit.](http://commits.kde.org/plasma-workspace/288830060db41044cbe2cf3bc88e8d3ce0128234) 
+ Sddm/lockscreen: Fix weird behaviour. [Commit.](http://commits.kde.org/plasma-workspace/f4bf8a278df58f14643613539d4f64bf7418f694) 
+ [Notifications] Limit notification heading line count. [Commit.](http://commits.kde.org/plasma-workspace/dc09ed349b599fb62ef24dbc3720679881d245ff) 
{{< /details >}}

{{< details title="Plymouth KControl Module" href="https://commits.kde.org/plymouth-kcm" >}}
+ Substantially bump the helper timeout. [Commit.](http://commits.kde.org/plymouth-kcm/df2cc926359f986bf30ec334ee2e2aba4406fd47) Fixes bug [#400641](https://bugs.kde.org/400641)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Don't let back arrow be re-colored to monochrome. [Commit.](http://commits.kde.org/systemsettings/b579a5d94a3afbd6f85a95cf368a29e8c96c2b92) See bug [#451538](https://bugs.kde.org/451538)
+ Systemsettings runner: Ensure that we match keywords case insensitively. [Commit.](http://commits.kde.org/systemsettings/99908ccb3a8c1d0ffb6ccf6eca008feed6a42749) Fixes bug [#451634](https://bugs.kde.org/451634)
{{< /details >}}

