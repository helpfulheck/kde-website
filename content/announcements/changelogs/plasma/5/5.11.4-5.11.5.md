---
aliases:
- /announcements/plasma-5.11.4-5.11.5-changelog
hidden: true
plasma: true
title: Plasma 5.11.5 Complete Changelog
type: fulllog
version: 5.11.5
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Fix indentation of icon for left aligned toolbuttons. <a href='https://commits.kde.org/breeze/f5fd8fa82c6bcaf7066a7031002622458cedd2dd'>Commit.</a> See bug <a href='https://bugs.kde.org/381535'>#381535</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9281'>D9281</a>
- Fixed comments. <a href='https://commits.kde.org/breeze/2f1af50bfcfec45fae77b87e81923e2d2567467d'>Commit.</a>
- Fixed icon state. <a href='https://commits.kde.org/breeze/a26d3e97a4a8299f25d4df8e443d363195b0f661'>Commit.</a>
- When an icon is set to a QMenuBar Item, render the icon only, and ignore the text, when set, as this is what qmenubar expects. <a href='https://commits.kde.org/breeze/3fcebd4e627a0255631ec81d39ed9eac158ac374'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387539'>#387539</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Keep track of the kns entries status internally. <a href='https://commits.kde.org/discover/20645704ea2cb12dee3ecde5b954372ba2508306'>Commit.</a>
- Revert "Make sure m_isFetching is initialized when we use it". <a href='https://commits.kde.org/discover/fbc4467d3d925c2e708e6ba29f9663c6fa2a36d6'>Commit.</a>
- Make sure m_isFetching is initialized when we use it. <a href='https://commits.kde.org/discover/f50dd62a836d425e16e959e972c229fa556fad6a'>Commit.</a>
- Make it possible to navigate screenshots from the overlay. <a href='https://commits.kde.org/discover/571265a864d1451d7dc03a8c4dd614ae17122656'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387402'>#387402</a>
- Hide reviews page when opening the review dialog. <a href='https://commits.kde.org/discover/e152796b00eb333dbedc7bfee87b9f96027304e8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386902'>#386902</a>
- Revert "Improve error message when review dialog contents are wrong". <a href='https://commits.kde.org/discover/bce4f5c66e222791e196c858ec7dfc1b7b9937f3'>Commit.</a>
- Improve error message when review dialog contents are wrong. <a href='https://commits.kde.org/discover/f663bd84cf49190d373708de53a98c11f6ba1584'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387261'>#387261</a>
- Usually applications won't be compulsory for a desktop. <a href='https://commits.kde.org/discover/1b70104f31b0ed8e4834cf052ceae0339eb7e58f'>Commit.</a>
- Don't show packages tightly coupled to other desktops by default. <a href='https://commits.kde.org/discover/6cc89e068b9ca9aa0b9a511d6312a3e37fe79972'>Commit.</a> See bug <a href='https://bugs.kde.org/387379'>#387379</a>
- Readability. <a href='https://commits.kde.org/discover/da846ebf85f3cfcdd30df5a977e6a2b7e1320d28'>Commit.</a>
- PK: Use the stream to store the information rather than the transaction. <a href='https://commits.kde.org/discover/74b1746182a271112441b0493d3b63b7456c6d87'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386992'>#386992</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Also list GTK 3 themes without "gtk-3.0" subfolder. <a href='https://commits.kde.org/kde-gtk-config/1ba9f200f0fb53b1dab7e810f8a1920f18e55322'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9459'>D9459</a>
- Fix minimum height of cursor/icon theme comboboxes. <a href='https://commits.kde.org/kde-gtk-config/9615fc3994f962846791f790f9d18e2f42d774d2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9457'>D9457</a>
- Fixed GTK 3 preview dialog not previewing the currently selected theme. <a href='https://commits.kde.org/kde-gtk-config/90f1eeaf3910d0adcba577194b4291718224baba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9434'>D9434</a>
- Fixed broken background color in GTK 3 preview dialog. <a href='https://commits.kde.org/kde-gtk-config/f624000f8289eb9cee1e88233f7f17eef56d8a7d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9433'>D9433</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [comic] Fix update interval. <a href='https://commits.kde.org/kdeplasma-addons/095196ca643853aa2a6c49cba27e037f66870aa7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9338'>D9338</a>
- [comic] Make sure KNS download dialog is destroyed. <a href='https://commits.kde.org/kdeplasma-addons/19784bfc4b9cd7adefb127d4214a532c43211ab3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9131'>D9131</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Adjust kwinglplatform.cpp for Mesa renderer string changes. <a href='https://commits.kde.org/kscreenlocker/9959ead329d1e7aa69633d29868d2fe4ac61b678'>Commit.</a>
- Fix greeter crashing with software rendering on wayland. <a href='https://commits.kde.org/kscreenlocker/f6307641e51e74648cfb389bac6fbf2c1f219100'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9496'>D9496</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Add missing include. <a href='https://commits.kde.org/kwin/2d20aa0b1a0f4a3fe22f7426f774773f5bea4215'>Commit.</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Fix Config supportedFeatures not being passed between backend and clients. <a href='https://commits.kde.org/libkscreen/0f2ca3755d71eb732bd007e5ab953177ac5aa402'>Commit.</a>

### <a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a>

- Fix build without X11/XCB. <a href='https://commits.kde.org/oxygen/c10574bfcc4caa08c7d22682ddb7b978293ae34a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9385'>D9385</a>
- Fix indentation of icon for left aligned toolbuttons. <a href='https://commits.kde.org/oxygen/8e51240839744679939a5390dc280f312b20d340'>Commit.</a> See bug <a href='https://bugs.kde.org/381535'>#381535</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9282'>D9282</a>
- When an icon is set to a QMenuBar Item, render the icon only, and ignore the text, when set, as this is what qmenubar expects. <a href='https://commits.kde.org/oxygen/9f4fd9f43638527b719578f546d700d2f17b68bc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387539'>#387539</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Use automoc keyword for kimpanel-scim-panel. <a href='https://commits.kde.org/plasma-desktop/fa900378ac6c6146f7c1cb029e71d320025c30ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9245'>D9245</a>
- [Folder View] Show "Widgets unlocked" help only for containment. <a href='https://commits.kde.org/plasma-desktop/7e2801974ab58e1a3a72c3ebd7e2d98b0c97b5eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387589'>#387589</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9222'>D9222</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [widgetexplorer] Fix trying to show deleted dialog. <a href='https://commits.kde.org/plasma-workspace/acd7034952bc2f72454253056c5a8581b33e22ae'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9479'>D9479</a>
- Remove i18n() calls from iconSource names. <a href='https://commits.kde.org/plasma-workspace/b8c6a6969b00e2dd158a05fae659d205d18ea156'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D9232'>D9232</a>
- [widgetexplorer] Make sure KNS download dialog is destroyed. <a href='https://commits.kde.org/plasma-workspace/84a29ef86380fab40236bdf934b49bc3cf090853'>Commit.</a> See bug <a href='https://bugs.kde.org/355464'>#355464</a>. Phabricator Code review <a href='https://phabricator.kde.org/D9130'>D9130</a>
- [weather] NOAA ion: Show some icons for "Hot" and "Cold" forecasts. <a href='https://commits.kde.org/plasma-workspace/968096cd9c7439b9853bb331d886135c472d958a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365715'>#365715</a>